% main_postProcessTrialWiseData;close all;
% load('IMUProcessedData3134.mat')
% clearvars -except IMUProcessedData
% comboUsed = 'rval_4';
trialType = 'tr2'; % 'tr1'; 'tr2'; 'tr3'; 'tr4'; 'tr5'; 'tr6';

%%
processStraightSteps = 0;

% close all;
% error('havent fixed steps correlation issue')

%%
stepLengthRefSet = [];
stepWidthRefSet = [];
stepLengthIMUSet = [];
stepWidthIMUSet = [];
stepLengthwoZMPSet = [];
stepWidthwoZMPSet = [];

CoMLenRefSet  = [];
CoMWidRefSet  = [];
CoMLenIMUSet  = [];
CoMWidIMUSet  = [];
CoMLenwoZMPSet  = [];
CoMWidwoZMPSet  = [];


stepLengthRefSetturn = [];
stepWidthRefSetturn = [];
stepLengthIMUSetturn = [] ;
stepWidthIMUSetturn = [];
stepLengthwoZMPSetturn = [];
stepWidthwoZMPSetturn = [];

CoMLenRefSetturn  = [];
CoMWidRefSetturn  = [];
CoMLenIMUSetturn  = [];
CoMWidIMUSetturn  = [];
CoMLenwoZMPSetturn  = [];
CoMWidwoZMPSetturn  = [];

%%

usList = fieldnames(IMUProcessedData)   ;
% usList = {'us4'};
for usCt = 1:length(usList)
    usName = usList{usCt};
    
    trialList = fieldnames(IMUProcessedData.(usName));
    selectedTrialList =     trialList(startsWith(trialList,trialType));
    
    for trCt = 1: length(selectedTrialList) %
        trName = selectedTrialList{trCt};
        
        
        
        estDS = IMUProcessedData.(usName).(trName).EstData;
        refDS = IMUProcessedData.(usName).(trName).RefData;
        
        imuContact = estDS.IMUContacts.contactZV;
        refContact = refDS.Body.contact;
        
        refRFpos = refDS.RightFoot.IMUpos;
        refLFpos = refDS.LeftFoot.IMUpos;
        refPFpos = refDS.CoM.pos;
        
        refData.RightFoot.pos = refRFpos;
        refData.LeftFoot.pos  = refLFpos;
        refData.CoM.pos       = refPFpos;
        
        refDatacg.RightFoot.pos = refDS.cgFrame.RightFoot.IMUpos;
        refDatacg.LeftFoot.pos  = refDS.cgFrame.LeftFoot.IMUpos;
        refDatacg.CoM.pos       = refDS.cgFrame.CoM.pos;
        
        refDatacg.RightFoot.vel = refDS.cgFrame.RightFoot.IMUvel;
        refDatacg.LeftFoot.vel  = refDS.cgFrame.LeftFoot.IMUvel;
        refDatacg.CoM.vel       = refDS.cgFrame.CoM.vel;
        
        
        
        estimStatesR = estDS.IMUdata{1}.stateIndex.allIMU;
        estimStatesL = estDS.IMUdata{2}.stateIndex.allIMU;
        estimStatesP = estDS.IMUdata{3}.stateIndex.allIMU;
        
        estimData.RightFoot.pos = estDS.Rcombo.(comboUsed).xMat(estimStatesR(1:3),:)';
        estimData.LeftFoot.pos  = estDS.Rcombo.(comboUsed).xMat(estimStatesL(1:3),:)';
        estimData.CoM.pos    = estDS.Rcombo.(comboUsed).xMat(estimStatesP(1:3),:)';
        
        estimDwoZMP.RightFoot.pos = estDS.wozmp.x(estimStatesR(1:3),:)';
        estimDwoZMP.LeftFoot.pos  = estDS.wozmp.x(estimStatesL(1:3),:)';
        estimDwoZMP.CoM.pos    = estDS.wozmp.x(estimStatesP(1:3),:)';
        
        
        [imustepsR, imustepsL] = fun_findSteps(imuContact);
        [refstepsR, refstepsL] = fun_findSteps(refContact);
        
        [imustepsRL] = fun_findChronologicalSteps(imuContact);
        [refstepsRL] = fun_findChronologicalSteps(refContact);
        
        startStep = 4; removeEndSteps = 2;
        imustepsRLsnip = imustepsRL(startStep:end-removeEndSteps,:);
        refstepsRLsnip = refstepsRL(startStep:end-removeEndSteps,:);
        
        
        stepsArray.imu.SR{usCt,trCt} = imustepsR;
        stepsArray.imu.SL{usCt,trCt} = imustepsL;
        stepsArray.ref.SR{usCt,trCt} = refstepsR;
        stepsArray.ref.SL{usCt,trCt} = refstepsL;
        
        stepsWithFeetInfoRefus = [refstepsR(3:end-2,:) ones(length(refstepsR(3:end-2,:)),1);
            refstepsL(2:end-1,:) 2*ones(length(refstepsL(2:end-1,:)),1)];
        
        stepsWithFeetInfoIMUus = [imustepsR(3:end-2,:) ones(length(imustepsR(3:end-2,:)),1);
            imustepsL(2:end-1,:) 2*ones(length(imustepsL(2:end-1,:)),1)];
        
        
        %         [~,idx] = sort(stepsWithFeetInfoRefus(:,1)); % sort just the first column
        %         stepsWithFeetInfoRef = stepsWithFeetInfoRefus(idx,:);   % sort the whole matrix using the sort indices
        
        %         [~,idx] = sort(stepsWithFeetInfoIMUus(:,1)); % sort just the first column
        %         stepsWithFeetInfoIMU = stepsWithFeetInfoIMUus(idx,:);   % sort the whole matrix using the sort indices
        
        
        stepsWithFeetInfoRef = refstepsRLsnip;   % sort the whole matrix using the sort indices
        stepsWithFeetInfoIMU = imustepsRLsnip;   % sort the whole matrix using the sort indices
        
        
        
        
        
        [stepLengthsRef, stepWidthsRef, comLenRef, comWidRef]  = fun_findStepWiseSpatioTempParam(refData,stepsWithFeetInfoRef);
        [stepLengthsIMU, stepWidthsIMU, comLenIMU, comWidIMU]    = fun_findStepWiseSpatioTempParam(estimData,stepsWithFeetInfoIMU);
        [stepLengthswoZMP, stepWidthswoZMP, comLenwoZMP, comWidwoZMP]  = fun_findStepWiseSpatioTempParam(estimDwoZMP,stepsWithFeetInfoIMU);
        
        
        headingDir = estDS.cgVar.headInd;
        
        turnStepsInd = [];
        if length(headingDir) == length(stepsWithFeetInfoIMU)
            turnStepsInd = find(headingDir(:,4)>39);
        end
        
        
        startStep = 2; removeEndSteps = 1;
        
        stepLengthRefSet = [stepLengthRefSet; stepLengthsRef(startStep:end-1,1)];
        stepWidthRefSet = [stepWidthRefSet; stepWidthsRef(startStep:end-1,1)];
        stepLengthIMUSet = [stepLengthIMUSet; stepLengthsIMU(startStep:end-1,1)];
        stepWidthIMUSet = [stepWidthIMUSet; stepWidthsIMU(startStep:end-1,1)];
        stepLengthwoZMPSet = [stepLengthwoZMPSet; stepLengthswoZMP(startStep:end-1,1)];
        stepWidthwoZMPSet = [stepWidthwoZMPSet; stepWidthswoZMP(startStep:end-1,1)];
        
        
        CoMLenRefSet  = [CoMLenRefSet; comLenRef(startStep:end-1,1)];
        CoMWidRefSet  = [CoMWidRefSet; comWidRef(startStep:end-1,1)];
        CoMLenIMUSet  = [CoMLenIMUSet; comLenIMU(startStep:end-1,1)];
        CoMWidIMUSet  = [CoMWidIMUSet; comWidIMU(startStep:end-1,1)];
        CoMLenwoZMPSet  = [CoMLenwoZMPSet; comLenwoZMP(startStep:end-1,1)];
        CoMWidwoZMPSet  = [CoMWidwoZMPSet; comWidwoZMP(startStep:end-1,1)];
        
        
        if length(turnStepsInd) > 1
            stepLengthRefSetturn = [stepLengthRefSetturn; stepLengthsRef(turnStepsInd,1)];
            stepWidthRefSetturn = [stepWidthRefSetturn;  stepWidthsRef(turnStepsInd,1)];
            stepLengthIMUSetturn = [stepLengthIMUSetturn; stepLengthsIMU(turnStepsInd,1)] ;
            stepWidthIMUSetturn = [stepWidthIMUSetturn; stepWidthsIMU(turnStepsInd,1)];
            stepLengthwoZMPSetturn = [stepLengthwoZMPSetturn;  stepLengthswoZMP(turnStepsInd,1)];
            stepWidthwoZMPSetturn = [stepWidthwoZMPSetturn;  stepWidthswoZMP(turnStepsInd,1)];
            
            CoMLenRefSetturn  = [CoMLenRefSetturn;comLenRef(turnStepsInd,1)];
            CoMWidRefSetturn  = [CoMWidRefSetturn; comWidRef(turnStepsInd,1)];
            CoMLenIMUSetturn  = [CoMLenIMUSetturn; comLenIMU(turnStepsInd,1)];
            CoMWidIMUSetturn  = [CoMWidIMUSetturn;comWidIMU(turnStepsInd,1)];
            CoMLenwoZMPSetturn  = [CoMLenwoZMPSetturn;comLenwoZMP(turnStepsInd,1)];
            CoMWidwoZMPSetturn  = [CoMWidwoZMPSetturn; comWidwoZMP(turnStepsInd,1)];
        end
        
    end
    
    
end

%%
meanStepLengthRef   = [mean(stepLengthRefSet)   std(stepLengthRefSet)];
meanStepWidthsRef   = [mean(stepWidthRefSet)    std(stepWidthRefSet)];
meanStepLengthIMU   = [mean(stepLengthIMUSet)   std(stepLengthIMUSet)];
meanStepWidthsIMU   = [mean(stepWidthIMUSet)    std(stepWidthIMUSet)];
meanStepLengthwoZMP = [mean(stepLengthwoZMPSet) std(stepLengthwoZMPSet)];
meanStepWidthswoZMP = [mean(stepWidthwoZMPSet)  std(stepWidthwoZMPSet)];


%%
figure('name','L & R - Compare');

curpl =subplot(1,3,1);
AllstepLengths = [stepLengthRefSet(:,1); stepLengthIMUSet(:,1);stepLengthwoZMPSet(:,1)];
g = [zeros(length(stepLengthRefSet(:,1)), 1); ones(length(stepLengthIMUSet(:,1)), 1); 2*ones(length(stepLengthwoZMPSet(:,1)), 1)];
boxplot(AllstepLengths,g);
curpl.XTickLabel = {'VICON', 'IMU', 'No ZMP'};
ylabel('Step length (m)')
title('Step lengths')


diffTest = ttest2(stepLengthRefSet(:,1),stepLengthIMUSet(:,1));

if ~diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
    
end

diffTest = ttest2(stepLengthwoZMPSet(:,1),stepLengthIMUSet(:,1));

if ~diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([2 3]), [1 1]*max(yt)*1.1, '-k',  mean(xt([2 3])), max(yt)*1.15, '*k')
    
end

diffTest = ttest2(stepLengthRefSet(:,1),stepLengthwoZMPSet(:,1));

if ~diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 3]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 3])), max(yt)*1.15, '*k')
    hold off
    box off
end


curpl =subplot(1,3,2);
AllstepWidths = [stepWidthRefSet(:,1); stepWidthIMUSet(:,1);stepWidthwoZMPSet(:,1)];
g = [zeros(length(stepWidthRefSet(:,1)), 1); ones(length(stepWidthIMUSet(:,1)), 1); 2*ones(length(stepWidthwoZMPSet(:,1)), 1)];
boxplot(AllstepWidths,g);
curpl.XTickLabel = {'VICON', 'IMU', 'No ZMP'};
ylabel('Step Width (m)')
title(['Step Widths cmb: ' comboUsed])



diffTest = ttest2(stepWidthRefSet(:,1),stepWidthIMUSet(:,1));

if ~diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
end

diffTest = ttest2(stepWidthwoZMPSet(:,1),stepWidthIMUSet(:,1));

if ~diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([2 3]), [1 1]*max(yt)*1.1, '-k',  mean(xt([2 3])), max(yt)*1.15, '*k')
end

diffTest = ttest2(stepWidthwoZMPSet(:,1),stepWidthRefSet(:,1));

if ~diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 3]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 3])), max(yt)*1.15, '*k')
    hold off
    box off
end



curpl =subplot(1,3,3);
AllstepWidths = [CoMWidRefSet(:,1); CoMWidIMUSet(:,1); CoMWidwoZMPSet(:,1)];
g = [zeros(length(CoMWidRefSet(:,1)), 1); ones(length(CoMWidIMUSet(:,1)), 1); 2*ones(length(CoMWidwoZMPSet(:,1)), 1)];
boxplot(AllstepWidths,g);
curpl.XTickLabel = {'VICON', 'IMU', 'No ZMP'};
ylabel('CoM Widths (m)')
title('CoM Widths - US ')



diffTest = ttest2(CoMWidRefSet(:,1),CoMWidIMUSet(:,1));

if ~diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
end

diffTest = ttest2(CoMWidwoZMPSet(:,1),CoMWidIMUSet(:,1));

if ~diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([2 3]), [1 1]*max(yt)*1.1, '-k',  mean(xt([2 3])), max(yt)*1.15, '*k')
end

diffTest = ttest2(CoMWidwoZMPSet(:,1),CoMWidRefSet(:,1));

if ~diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 3]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 3])), max(yt)*1.15, '*k')
    hold off
    box off
end
%%
varNames = {'VICON','IMU', 'm'};
BAdispOpt = {'eq';'r';'RMSE';'n'};
slFig = figure;
[~, ~, stepLStat] = BlandAltman(slFig,stepLengthRefSet(1:end),stepLengthIMUSet(1:end),varNames,'Step Lengths',{},'corrinfo',BAdispOpt);
swFig = figure;
[~, ~, stepWStat] = BlandAltman(swFig,stepWidthRefSet(1:end),stepWidthIMUSet(1:end),varNames,'Step Widths',{},'corrinfo',BAdispOpt);
cwFig = figure;
[~, ~, stepCStat] = BlandAltman(cwFig,CoMWidRefSet(1:end),CoMWidIMUSet,varNames,'Mean CoM Widths',{},'corrinfo',BAdispOpt);
%%



axHandles =slFig.Children;
ax1 = axHandles(3);
scatter(ax1,stepLengthRefSetturn,stepLengthIMUSetturn,'ro','filled')

ax2 = axHandles(2);
delV = stepLengthIMUSetturn - stepLengthRefSetturn;
avgV = mean([stepLengthRefSetturn stepLengthIMUSetturn],2);
scatter(ax2,avgV,delV,'ro','filled')



axHandles =swFig.Children;
ax1 = axHandles(3);
scatter(ax1,stepWidthRefSetturn,stepWidthIMUSetturn,'ro','filled')

ax2 = axHandles(2);
delV = stepWidthIMUSetturn - stepWidthRefSetturn;
avgV = mean([stepWidthIMUSetturn stepWidthRefSetturn],2);
scatter(ax2,avgV,delV,'ro','filled')



axHandles =cwFig.Children;
ax1 = axHandles(3);
scatter(ax1,CoMWidRefSetturn,CoMWidIMUSetturn,'ro','filled')

ax2 = axHandles(2);
delV = CoMWidIMUSetturn - CoMWidRefSetturn;
avgV = mean([CoMWidIMUSetturn CoMWidRefSetturn],2);
scatter(ax2,avgV,delV,'ro','filled')

%%
% figure;
% subplot(231)
% histogram(stepLengthRefSet(1:end))
% subplot(234)
% histogram(stepLengthIMUSet(1:end))
%
% subplot(232)
% histogram(stepWidthRefSet(1:end))
% subplot(235)
% histogram(stepWidthIMUSet(1:end))
%
% subplot(233)
% histogram(CoMWidRefSet(1:end))
% subplot(236)
% histogram(CoMWidIMUSet(1:end))

