%% estXCoMParamsPostProc_AfNaf_AvgMoS
%% Estimate w0 as in Hof et al. 2005
% w0 = sqrt( g / leg length)

% Calculate leg length
legLengthBodyLengthRatioMale   = 0.260 + 0.2484; % Bron: drillis et al - body segment parameters
legLengthBodyLengthRatioFemale = 0.252 + 0.227;  % Bron: drillis et al - body segment parameters

switch subSpecs.Gender
    case 'F'
        legLengthBodyLengthRatio = legLengthBodyLengthRatioFemale;
    case 'M'
        legLengthBodyLengthRatio = legLengthBodyLengthRatioMale;
    otherwise
        error('Subject gender unknown')
end

subjectLegLength = subSpecs.Height * legLengthBodyLengthRatio;
legLengthFactor  = 1; % see Hof et al.
l0=subjectLegLength;
% Calculate w0
w0 = sqrt(9.81/(legLengthFactor * subjectLegLength));

%%
strSet   = 1;
noofSets = length(segmentedIMU.stepsUsed);
imuContact_vela =imustepsRL_vel(2:end,:);
for iStep = strSet:noofSets
    
    tVarR = estrfSeg{iStep};
    tVarL = estlfSeg{iStep};
    tVarP = estpsSeg{iStep};
    tVarV = segmentedIMU.CoM.vel.sets{iStep};
    tVarX = tVarP + (tVarV/w0);
    tVarRM_R = segmentedIMU.RightFoot.RotM.sets{iStep};
    tVarRM_L = segmentedIMU.LeftFoot.RotM.sets{iStep};
    
    tVarRefR = refrfSeg{iStep};
    tVarRefL = reflfSeg{iStep};
    tVarRefP = refpsSeg{iStep};
    tVarRefV = segmentedRef.CoM.vel.sets{iStep};
    tVarRefX = tVarRefP + (tVarRefV/w0);
    tVarRefRM_R = segmentedRef.RightFoot.RotM.sets{iStep};
    tVarRefRM_L = segmentedRef.LeftFoot.RotM.sets{iStep};
    
    estXcomSeg{iStep} = tVarX;
    refXcomSeg{iStep} = tVarRefX;
    clear p_toe_r p_heel_r p_amtoe_r p_gtoe_r p_altoe_r p_pltoe_r p_pmtoe_r p_amheel_r p_alheel_r p_plheel_r p_pmheel_r   ...
        p_toe_l p_heel_l p_amtoe_l p_gtoe_l p_altoe_l p_pltoe_l p_pmtoe_l p_amheel_l p_alheel_l p_plheel_l p_pmheel_l   ...
        p_frf  p_frh    p_flf p_flh apMOS mlMOS_rt mlMOS_rh mlMOS_lt mlMOS_lh intersectPoint mosval frontBoS_Line CoMXCoM_Line ...
        testmlMOSrSeg testmlMOSlSeg testapMosrSeg testapMoslSeg
    
    % Get all shoe positions
    [p_toe_r, p_heel_r, p_amtoe_r, p_gtoe_r, p_altoe_r, p_pltoe_r, p_pmtoe_r, p_amheel_r, p_alheel_r, p_plheel_r, p_pmheel_r,   ...
        p_toe_l, p_heel_l, p_amtoe_l, p_gtoe_l, p_altoe_l, p_pltoe_l, p_pmtoe_l, p_amheel_l, p_alheel_l, p_plheel_l, p_pmheel_l,   ...
        p_frf  , p_frh,    p_flf,     p_flh] = ...
        p_feet( tVarR', tVarL', tVarRM_R, tVarRM_L, subSpecs.shoe_size);
    
    
    stepIndArray = segmentedIMU.stepsUsed(iStep,1):segmentedIMU.stepsUsed(iStep,2);
    contactArray = imuContact;
%     contactArray = refContactFS;
%     contactArray = contact_DSadj;
%     contactArray = imuContactZV;
    
    lenIter = length(tVarR);
    
%     stepIndArray = imuContact_vela(iStep,1):imuContact_vela(iStep,2);
%     contactArray = estVcont;
%     lenIter = length(imuContact_vela(iStep,1):imuContact_vela(iStep,2));
    
    for selSamp = 1:lenIter
        if sum(contactArray(stepIndArray(selSamp),:)) < 2
            BoSposX = [p_heel_l(selSamp,1)   p_amheel_l(selSamp,1);   % #13 #20
                p_pmtoe_l(selSamp,1)  p_gtoe_l(selSamp,1);     % #17 #15
                p_gtoe_r(selSamp,1)   p_pmtoe_r(selSamp,1);    % #4  #6
                p_amheel_r(selSamp,1) p_heel_r(selSamp,1)  ];  % #9  #2
            
            BoSposY = [p_heel_l(selSamp,2)   p_amheel_l(selSamp,2);   % #13 #20
                p_pmtoe_l(selSamp,2)  p_gtoe_l(selSamp,2);     % #17 #15
                p_gtoe_r(selSamp,2)   p_pmtoe_r(selSamp,2);    % #4  #6
                p_amheel_r(selSamp,2) p_heel_r(selSamp,2)  ];  % #9  #2
            
            backLineX  = [BoSposX(1,1) BoSposX(4,2)]; %LH %RH
            backLineY  = [BoSposY(1,1) BoSposY(4,2)];
            frontLineX = [BoSposX(2,2) BoSposX(3,1)]; %LF %RF
            frontLineY = [BoSposY(2,2) BoSposY(3,1)];
            
            %     frontLineX = [p_toe_l(selSamp,1) p_toe_r(selSamp,1)]; %LF %RF
            %     frontLineY = [p_toe_l(selSamp,2) p_toe_r(selSamp,2)];
            
            estmosval = distancePointToLineSegment(tVarX(selSamp,1:2),[frontLineX(1) frontLineY(1)], [frontLineX(2) frontLineY(2)]);
            
            frontBoS_Line = [frontLineX; frontLineY];
            CoMXCoM_Line  = [tVarP(selSamp,1) tVarX(selSamp,1);
                tVarP(selSamp,2) tVarX(selSamp,2) ];
            
            intersectPoint = findIntersectionPoint(frontBoS_Line, CoMXCoM_Line);
            
            Rest(selSamp,:) = isPointOnLine(tVarP(selSamp,1:2), tVarX(selSamp,1:2), intersectPoint);
            
            apMOS = normh(intersectPoint - tVarX(selSamp,1:2));
            mlMOS_rt = distancePointToLineSegment(p_altoe_r(selSamp,1:2),tVarP(selSamp,1:2), tVarX(selSamp,1:2));
            mlMOS_rh = distancePointToLineSegment(p_plheel_r(selSamp,1:2),tVarP(selSamp,1:2), tVarX(selSamp,1:2));
            
            mlMOS_lt = distancePointToLineSegment(p_altoe_l(selSamp,1:2),tVarP(selSamp,1:2), tVarX(selSamp,1:2));
            mlMOS_lh = distancePointToLineSegment(p_plheel_l(selSamp,1:2),tVarP(selSamp,1:2), tVarX(selSamp,1:2));
            
            curStance = segmentedIMU.stepsUsed(iStep,3);
            if curStance == 1
                  testapMosrSeg(selSamp,:) = (p_toe_r(selSamp,1) - tVarX(selSamp,1));                
%                 testapMosrSeg(selSamp,:) =  apMOS;
                testmlMOSrSeg(selSamp,:) =  max([mlMOS_rt mlMOS_rh]);
            elseif curStance == 2
                 testapMoslSeg(selSamp,:) = (p_toe_l(selSamp,1) - tVarX(selSamp,1));
                testapMoslSeg(selSamp,:) =  apMOS;
                testmlMOSlSeg(selSamp,:) =  max([mlMOS_lt mlMOS_lh]);
            end
        end
    end
    if curStance == 1
        estapMosrSeg(iStep,:) = mean(testapMosrSeg);
        estmlMOSrSeg(iStep,:) =  mean(testmlMOSrSeg);
    elseif curStance == 2
        estapMoslSeg(iStep,:)  = mean(testapMoslSeg);
        estmlMOSlSeg(iStep,:) =  mean(testmlMOSlSeg);
    end
    
    clear p_toe_r p_heel_r p_amtoe_r p_gtoe_r p_altoe_r p_pltoe_r p_pmtoe_r p_amheel_r p_alheel_r p_plheel_r p_pmheel_r   ...
        p_toe_l p_heel_l p_amtoe_l p_gtoe_l p_altoe_l p_pltoe_l p_pmtoe_l p_amheel_l p_alheel_l p_plheel_l p_pmheel_l   ...
        p_frf  p_frh    p_flf p_flh apMOS mlMOS_rt mlMOS_rh mlMOS_lt mlMOS_lh intersectPoint mosval frontBoS_Line CoMXCoM_Line ...
        trefmlMOSrSeg trefmlMOSlSeg trefapMosrSeg trefapMoslSeg
    
    [p_toe_r, p_heel_r, p_amtoe_r, p_gtoe_r, p_altoe_r, p_pltoe_r, p_pmtoe_r, p_amheel_r, p_alheel_r, p_plheel_r, p_pmheel_r,   ...
        p_toe_l, p_heel_l, p_amtoe_l, p_gtoe_l, p_altoe_l, p_pltoe_l, p_pmtoe_l, p_amheel_l, p_alheel_l, p_plheel_l, p_pmheel_l,   ...
        p_frf  , p_frh,    p_flf,     p_flh] = ...
        p_feet( tVarRefR', tVarRefL', tVarRefRM_R, tVarRefRM_L, subSpecs.shoe_size);
    
    
    for selSamp = 1:lenIter
        if sum(contactArray(stepIndArray(selSamp),:)) < 2
            BoSposX = [p_heel_l(selSamp,1)   p_amheel_l(selSamp,1);   % #13 #20
                p_pmtoe_l(selSamp,1)  p_gtoe_l(selSamp,1);     % #17 #15
                p_gtoe_r(selSamp,1)   p_pmtoe_r(selSamp,1);    % #4  #6
                p_amheel_r(selSamp,1) p_heel_r(selSamp,1)  ];  % #9  #2
            
            BoSposY = [p_heel_l(selSamp,2)   p_amheel_l(selSamp,2);   % #13 #20
                p_pmtoe_l(selSamp,2)  p_gtoe_l(selSamp,2);     % #17 #15
                p_gtoe_r(selSamp,2)   p_pmtoe_r(selSamp,2);    % #4  #6
                p_amheel_r(selSamp,2) p_heel_r(selSamp,2)  ];  % #9  #2
            
            backLineX  = [BoSposX(1,1) BoSposX(4,2)]; %LH %RH
            backLineY  = [BoSposY(1,1) BoSposY(4,2)];
            frontLineX = [BoSposX(2,2) BoSposX(3,1)]; %LF %RF
            frontLineY = [BoSposY(2,2) BoSposY(3,1)];
            
            %     frontLineX = [p_toe_l(selSamp,1) p_toe_r(selSamp,1)]; %LF %RF
            %     frontLineY = [p_toe_l(selSamp,2) p_toe_r(selSamp,2)];
            
            mosval = distancePointToLineSegment(tVarRefX(selSamp,1:2),[frontLineX(1) frontLineY(1)], [frontLineX(2) frontLineY(2)]);
            
            frontBoS_Line = [frontLineX; frontLineY];
            CoMXCoM_Line  = [tVarRefP(selSamp,1) tVarRefX(selSamp,1);
                tVarRefP(selSamp,2) tVarRefX(selSamp,2) ];
            
            intersectPoint = findIntersectionPoint(frontBoS_Line, CoMXCoM_Line );
            
            Rref(selSamp,:) = isPointOnLine(tVarP(selSamp,1:2), tVarRefX(selSamp,1:2), intersectPoint);
            
            apMOS = normh(intersectPoint - tVarRefX(selSamp,1:2));
            mlMOS_rt = distancePointToLineSegment(p_altoe_r(selSamp,1:2),tVarRefP(selSamp,1:2), tVarRefX(selSamp,1:2));
            mlMOS_rh = distancePointToLineSegment(p_plheel_r(selSamp,1:2),tVarRefP(selSamp,1:2), tVarRefX(selSamp,1:2));
            
            mlMOS_lt = distancePointToLineSegment(p_altoe_l(selSamp,1:2),tVarRefP(selSamp,1:2), tVarRefX(selSamp,1:2));
            mlMOS_lh = distancePointToLineSegment(p_plheel_l(selSamp,1:2),tVarRefP(selSamp,1:2), tVarRefX(selSamp,1:2));
            
            if curStance == 1 %rightleg stance
                trefapMosrSeg(selSamp,:) = (p_toe_r(selSamp,1) - tVarRefX(selSamp,1));
%                 trefapMosrSeg(selSamp,:) =  apMOS;
                trefmlMOSrSeg(selSamp,:) =  max([mlMOS_rt mlMOS_rh]);
                
            elseif curStance == 2 %leftleg stance
                trefapMoslSeg(selSamp,:) = (p_toe_l(selSamp,1) - tVarRefX(selSamp,1));
%                 trefapMoslSeg(selSamp,:) =  apMOS;
                trefmlMOSlSeg(selSamp,:) =  max([mlMOS_lt mlMOS_lh]);
                
            end
        end
    end
    
    if curStance == 1
        refapMosrSeg(iStep,:) = mean(trefapMosrSeg);
        refmlMOSrSeg(iStep,:) =  mean(trefmlMOSrSeg);
        figure;plot(testapMosrSeg); hold on; plot(trefapMosrSeg); ylim([0 2])
        figure; scatter(tVarL(:,1),tVarL(:,2)); title('left foot swing')
        hold on; scatter(tVarRefL(:,1),tVarRefL(:,2)); 
    elseif curStance == 2
        refapMoslSeg(iStep,:)  = mean(trefapMoslSeg);
        refmlMOSlSeg(iStep,:) =  mean(trefmlMOSlSeg);
        figure;plot(testapMoslSeg); hold on; plot(trefapMoslSeg); ylim([0 2])
        figure; scatter(tVarR(:,1),tVarR(:,2)); title('right foot swing')
        hold on; scatter(tVarRefR(:,1),tVarRefR(:,2)); 
    end
    
end

if plotTestGraphs
    %     figure; plot(refmosEucDSeg(2:end,:)); hold on; plot(estmosEucDSeg)
    %   figure; plot(Rest); hold on; plot(Rref)
    
end

refapMosrSeg(refapMosrSeg == 0) = [];
refapMoslSeg(refapMoslSeg == 0) = [];
estapMosrSeg(estapMosrSeg == 0) = [];
estapMoslSeg(estapMoslSeg == 0) = [];
%%
CumTypeWiseXCSeg.Ref.(usName).(typeStr) = [CumTypeWiseXCSeg.Ref.(usName).(typeStr); ...
    refXcomSeg];
CumTypeWiseXCSeg.Est.(usName).(typeStr) = [CumTypeWiseXCSeg.Est.(usName).(typeStr); ...
    estXcomSeg];



switch affFoot
    case 'Right'
        
        CumTypeWiseMoS.APA.Ref.(usName).(typeStr) = [CumTypeWiseMoS.APA.Ref.(usName).(typeStr); ...
            refapMosrSeg];
        CumTypeWiseMoS.APA.Est.(usName).(typeStr) = [CumTypeWiseMoS.APA.Est.(usName).(typeStr); ...
            estapMosrSeg];
        
        CumTypeWiseMoS.APN.Ref.(usName).(typeStr) = [CumTypeWiseMoS.APN.Ref.(usName).(typeStr); ...
            refapMoslSeg];
        CumTypeWiseMoS.APN.Est.(usName).(typeStr) = [CumTypeWiseMoS.APN.Est.(usName).(typeStr); ...
            estapMoslSeg];
        
        CumTypeWiseMoS.MLA.Ref.(usName).(typeStr) = [CumTypeWiseMoS.MLA.Ref.(usName).(typeStr); ...
            refmlMOSrSeg];
        CumTypeWiseMoS.MLA.Est.(usName).(typeStr) = [CumTypeWiseMoS.MLA.Est.(usName).(typeStr); ...
            estmlMOSrSeg];
        
        CumTypeWiseMoS.MLN.Ref.(usName).(typeStr) = [CumTypeWiseMoS.MLN.Ref.(usName).(typeStr); ...
            refmlMOSlSeg];
        CumTypeWiseMoS.MLN.Est.(usName).(typeStr) = [CumTypeWiseMoS.MLN.Est.(usName).(typeStr); ...
            estmlMOSlSeg];
        
    case 'Left'
        
        CumTypeWiseMoS.APA.Ref.(usName).(typeStr) = [CumTypeWiseMoS.APA.Ref.(usName).(typeStr); ...
            refapMoslSeg];
        CumTypeWiseMoS.APA.Est.(usName).(typeStr) = [CumTypeWiseMoS.APA.Est.(usName).(typeStr); ...
            estapMoslSeg];
        
        CumTypeWiseMoS.APN.Ref.(usName).(typeStr) = [CumTypeWiseMoS.APN.Ref.(usName).(typeStr); ...
            refapMosrSeg];
        CumTypeWiseMoS.APN.Est.(usName).(typeStr) = [CumTypeWiseMoS.APN.Est.(usName).(typeStr); ...
            estapMosrSeg];
        
        
        CumTypeWiseMoS.MLA.Ref.(usName).(typeStr) = [CumTypeWiseMoS.MLA.Ref.(usName).(typeStr); ...
            refmlMOSlSeg];
        CumTypeWiseMoS.MLA.Est.(usName).(typeStr) = [CumTypeWiseMoS.MLA.Est.(usName).(typeStr); ...
            estmlMOSlSeg];
        
        CumTypeWiseMoS.MLN.Ref.(usName).(typeStr) = [CumTypeWiseMoS.MLN.Ref.(usName).(typeStr); ...
            refmlMOSrSeg];
        CumTypeWiseMoS.MLN.Est.(usName).(typeStr) = [CumTypeWiseMoS.MLN.Est.(usName).(typeStr); ...
            estmlMOSrSeg];
        
        
end

