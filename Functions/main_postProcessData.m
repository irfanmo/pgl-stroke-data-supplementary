% close all;
% load('IMUProcessedData3134.mat')
clearvars -except IMUProcessedData
usList = fieldnames(IMUProcessedData);
processStraightSteps = 0;


usList = {'us3','us4'};
usList = {'us4'};
for usCt = 1:length(usList)
    usName = usList{usCt};
    
    trialList = fieldnames(IMUProcessedData.(usName));
    trialList = {'tr44'};
    
    for trCt = 1:length(trialList)
        trName = trialList{trCt};
        
        estDS = IMUProcessedData.(usName).(trName).EstData;
        refDS = IMUProcessedData.(usName).(trName).RefData;
        
        imuContact = estDS.IMUContacts.contactZV;
        refContact = refDS.Body.contact;
        
        refRFpos = refDS.RightFoot.IMUpos;
        refLFpos = refDS.LeftFoot.IMUpos;
        refPFpos = refDS.CoM.pos;
        
        refData.RightFoot.pos = refRFpos;
        refData.LeftFoot.pos  = refLFpos;
        refData.CoM.pos       = refPFpos;        
        
        refData.RightFoot.vel = refDS.RightFoot.IMUvel;
        refData.LeftFoot.vel  = refDS.LeftFoot.IMUvel;
        refData.CoM.vel       = refDS.CoM.vel;
        
        refDatacg.RightFoot.pos = refDS.cgFrame.RightFoot.IMUpos;
        refDatacg.LeftFoot.pos  = refDS.cgFrame.LeftFoot.IMUpos;
        refDatacg.CoM.pos       = refDS.cgFrame.CoM.pos;        
        
        refDatacg.RightFoot.vel = refDS.cgFrame.RightFoot.IMUvel;
        refDatacg.LeftFoot.vel  = refDS.cgFrame.LeftFoot.IMUvel;
        refDatacg.CoM.vel       = refDS.cgFrame.CoM.vel;
        
        
        combos = fieldnames(estDS.Rcombo);
%           combos = {'rval_4','rval_5','rval_6'};             
        combos = {'rval_4'};
        for iCt = 1:length(combos)
            comboUsed = combos{iCt};
            
            
            estimStatesR = estDS.IMUdata{1}.stateIndex.allIMU;
            estimStatesL = estDS.IMUdata{2}.stateIndex.allIMU;
            estimStatesP = estDS.IMUdata{3}.stateIndex.allIMU;
            
            estimData.RightFoot.pos = estDS.Rcombo.(comboUsed).xMat(estimStatesR(1:3),:)';
            estimData.LeftFoot.pos  = estDS.Rcombo.(comboUsed).xMat(estimStatesL(1:3),:)';
            estimData.CoM.pos    = estDS.Rcombo.(comboUsed).xMat(estimStatesP(1:3),:)';
            
            estimDwoZMP.RightFoot.pos = estDS.wozmp.x(estimStatesR(1:3),:)';
            estimDwoZMP.LeftFoot.pos  = estDS.wozmp.x(estimStatesL(1:3),:)';
            estimDwoZMP.CoM.pos    = estDS.wozmp.x(estimStatesP(1:3),:)';
            
            estimData.RightFoot.vel = estDS.Rcombo.(comboUsed).xMat(estimStatesR(4:6),:)';
            estimData.LeftFoot.vel  = estDS.Rcombo.(comboUsed).xMat(estimStatesL(4:6),:)';
            estimData.CoM.vel    = estDS.Rcombo.(comboUsed).xMat(estimStatesP(4:6),:)';
            
            estimDwoZMP.RightFoot.vel = estDS.wozmp.x(estimStatesR(4:6),:)';
            estimDwoZMP.LeftFoot.vel  = estDS.wozmp.x(estimStatesL(4:6),:)';
            estimDwoZMP.CoM.vel    = estDS.wozmp.x(estimStatesP(4:6),:)';
            
            processEntry = 1;
            splitIntoSteps;
            
            %             [~, ~, stepLStat] = BlandAltman(stepLengthsRef,stepLengthsIMU);
            %             [~, ~, stepWStat] = BlandAltman(stepWidthsRef,stepWidthsIMU);
        end
        
        
        
    end
    
    
end

%%
