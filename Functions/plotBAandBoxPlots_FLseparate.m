%% plotBAandBoxPlots_FLseparate;

showStanceTimes = 0;
showAPMoS = 1;
showMLMoS = 1;
showSL_SW = 1;

BAdispOpt = {'eq';'r';'RMSE';'n'};
%% Single Stance Times
if showStanceTimes
    
    varNames = {'ForceShoe','IMU', 's'};
    
    
    stRFig = figure('name','ST R');
    BlandAltman(stRFig,CumSTR.Ref,CumSTR.Est,varNames,'Stance Time Right',{},'corrinfo',BAdispOpt);
    
    stLFig = figure('name','ST L');
    BlandAltman(stLFig,CumSTL.Ref,CumSTL.Est,varNames,'Stance Time Left',{},'corrinfo',BAdispOpt);
    
    figure('name','Boxplots Stance Time L v R');
    clear AllstepLengths
    curpl =subplot(1,2,1);
    val1 = CumSTR.Ref; val2 = CumSTL.Ref;
    
    VarArray = [val1; val2];
    g = [zeros(length(val1), 1); ones(length(val2), 1); ];
    boxplot(VarArray,g);
    curpl.XTickLabel = {'Right', 'Left'};
    ylabel('Single Stance Time (s)')
    title('ST ForceShoe')
    
    
    diffTest = ttest2(val1,val2);
    
    if diffTest
        yt = curpl.YTick;
        axis([xlim    0  ceil(max(yt)*1.2)])
        xt = curpl.XTick;
        hold on
        plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
        
    end
    
    clear VarArray
    curpl =subplot(1,2,2);
    val1 = CumSTR.Est; val2 = CumSTL.Est;
    VarArray = [val1; val2];
    g = [zeros(length(val1), 1); ones(length(val2), 1); ];
    boxplot(VarArray,g);
    curpl.XTickLabel = {'Right', 'Left'};
    ylabel('Single Stance Time (s)')
    title('ST IMU')
    
    
    diffTest = ttest2(val1,val2);
    
    if diffTest
        yt = curpl.YTick;
        axis([xlim    0  ceil(max(yt)*1.2)])
        xt = curpl.XTick;
        hold on
        plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
        
    end
    
    
end
%% MoS
dat1Mode = 'Compare'; % Compare or Truth
baStatsMode = 'Non-parametric'; % Gaussian or Non-parametric
varNames = {'ForceShoe','IMU', 'm'};
% BAdispOpt = {'eq';'r';'RMSE';'n'};
BAdispOptCor = {}; BAdispOptBA = {};

if showMLMoS
    
    % ML R
    mlrFig = figure('name','ML MoS R');
    % BlandAltman(mlrFig,CumMLRMoS.Ref,CumMLRMoS.Est,varNames,'ML MoS Right',{},'corrinfo',BAdispOpt);
    [~, ~,MLRstrt] = BlandAltmanDual(mlrFig,CumMLRMoS_SW.Ref,CumMLRMoS_SW.Est,varNames,...
        {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','b',...
        'baStatsMode',baStatsMode,'data1Mode',dat1Mode);
    hold on;
    [~, ~,MLRturn] = BlandAltmanDual(mlrFig,CumMLRMoS_FL.Ref,CumMLRMoS_FL.Est,varNames,...
        {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','r','symbols','o',...
        'baStatsMode','Gaussian','data1Mode',dat1Mode);
    
    MLRs_MAD = mean(abs(CumMLRMoS_SW.Ref - CumMLRMoS_SW.Est));
    MLRt_MAD = mean(abs(CumMLRMoS_FL.Ref - CumMLRMoS_FL.Est));
    
    MLRstrLoA = [MLRstrt.differenceMean-MLRstrt.rpc MLRstrt.differenceMean+MLRstrt.rpc]*100; % cm
    MLRturLoA = [MLRturn.differenceMean-MLRturn.rpc MLRturn.differenceMean+MLRturn.rpc]*100; % cm
    
    strCorP = MLRstrt.corrP <= 0.05;
    strCor = [num2str(round(MLRstrt.r,2)) char(strCorP* '*')];
    
    turCorP = MLRturn.corrP <= 0.05;
    turCor = [num2str(round(MLRturn.r,2)) char(turCorP* '*')];
    
    strRms = num2str(round(MLRstrt.RMSE*100,2));
    turRms = num2str(round(MLRturn.RMSE*100,2));
    
    curAx = mlrFig.CurrentAxes;
    LineList = curAx.Children;
    for i = [3 4 5 9 10 11]
        set(get(get(LineList(i),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
    end
    legend(['Straight,  r:' strCor ', RMSE: ' strRms ' cm'],['First and Last,        r:' turCor ', RMSE: ' turRms ' cm']);
    
    
    % ML L MoS
    mllFig = figure('name','ML MoS L');
    % BlandAltman(mllFig,CumMLLMoS.Ref,CumMLLMoS.Est,varNames,'ML MoS Left',{},'corrinfo',BAdispOpt);
    [~, ~,MLLstrt] = BlandAltmanDual(mllFig,CumMLLMoS_SW.Ref,CumMLLMoS_SW.Est,varNames,...
        {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','b',...
        'baStatsMode',baStatsMode,'data1Mode',dat1Mode);
    hold on;
    [~, ~,MLLturn] = BlandAltmanDual(mllFig,CumMLLMoS_FL.Ref,CumMLLMoS_FL.Est,varNames,...
        {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','r','symbols','o',...
        'baStatsMode','Gaussian','data1Mode',dat1Mode);
    
    MLLs_MAD = mean(abs(CumMLLMoS_SW.Ref - CumMLLMoS_SW.Est));
    MLLt_MAD = mean(abs(CumMLLMoS_FL.Ref - CumMLLMoS_FL.Est));
    
    MLLstrLoA = [MLLstrt.differenceMean-MLLstrt.rpc MLLstrt.differenceMean+MLLstrt.rpc]*100; % cm
    MLLturLoA = [MLLturn.differenceMean-MLLturn.rpc MLLturn.differenceMean+MLLturn.rpc]*100; % cm
    
    strCorP = MLLstrt.corrP <= 0.05;
    strCor = [num2str(round(MLLstrt.r,2)) char(strCorP* '*')];
    
    turCorP = MLLturn.corrP <= 0.05;
    turCor = [num2str(round(MLLturn.r,2)) char(turCorP* '*')];
    
    strRms = num2str(round(MLLstrt.RMSE*100,2));
    turRms = num2str(round(MLLturn.RMSE*100,2));
    
    curAx = mllFig.CurrentAxes;
    LineList = curAx.Children;
    for i = [3 4 5 9 10 11]
        set(get(get(LineList(i),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
    end
    legend(['Straight,  r:' strCor ', RMSE: ' strRms ' cm'],['First and Last,        r:' turCor ', RMSE: ' turRms ' cm']);
    
    
    figure('name','BP ML MoS');
    clear VarArray
    curpl =subplot(1,2,1);
    val1 = [CumMLRMoS_SW.Ref; CumMLRMoS_FL.Ref]; val2 = [CumMLLMoS_SW.Ref; CumMLLMoS_FL.Ref]; ylabel('ML MoS(m)')
    val1 = CumMLRMoS_SW.Ref; val2 = CumMLLMoS_SW.Ref; ylabel('ML MoS Straight (m)')
    VarArray = [val1; val2];
    g = [zeros(length(val1), 1); ones(length(val2), 1); ];
    boxplot(VarArray,g);
    curpl.XTickLabel = {'Right', 'Left'};    
    title('ML MoS ForceShoe')
    
    
    diffTest = ttest2(val1,val2);
    
    if diffTest
        yt = curpl.YTick;
        axis([xlim    0  ceil(max(yt)*1.2)])
        xt = curpl.XTick;
        hold on
        plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
        
    end
    
    clear VarArray
    curpl =subplot(1,2,2);
    val1 = [CumMLRMoS_SW.Est; CumMLRMoS_FL.Est]; val2 = [CumMLLMoS_SW.Est; CumMLLMoS_FL.Est]; ylabel('ML MoS(m)')
    val1 = CumMLRMoS_SW.Est; val2 = CumMLLMoS_SW.Est; ylabel('ML MoS Straight(m)')
    VarArray = [val1; val2];
    g = [zeros(length(val1), 1); ones(length(val2), 1); ];
    boxplot(VarArray,g);
    curpl.XTickLabel = {'Right', 'Left'};    
    title('ML MoS IMU')
    
    
    diffTest = ttest2(val1,val2);
    
    if diffTest
        yt = curpl.YTick;
        axis([xlim    0  ceil(max(yt)*1.2)])
        xt = curpl.XTick;
        hold on
        plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
        
    end
    
end



%% AP MoS

if showAPMoS
    imuVal = CumAPMoS_SW.Est; refVal = CumAPMoS_SW.Ref;
    figure; plot(imuVal); hold on; plot(refVal);
    
    outliers = [4 27 31 32 37 41 43 45 46 60 64 66 68 86];
    outliers = [58 86 90];
    
    % imuVal(outliers,:) = []; refVal(outliers,:) = []; warning('outliers removed for AP mos alone')
    apFig = figure('name','AP MoS');
    % BlandAltman(apFig,refVal,imuVal,varNames,'AP MoS',{},'corrinfo',BAdispOpt);
    
    [~, ~,APstrt] = BlandAltmanDual(apFig,refVal,imuVal,varNames,...
        {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','b',...
        'baStatsMode',baStatsMode,'data1Mode',dat1Mode);
    hold on;
    [~, ~,APturn] = BlandAltmanDual(apFig,CumAPMoS_FL.Ref,CumAPMoS_FL.Est,varNames,...
        {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','r','symbols','o',...
        'baStatsMode','Gaussian','data1Mode',dat1Mode);
    
    APs_MAD = mean(abs(CumAPMoS_SW.Ref - CumAPMoS_SW.Est));
    APt_MAD = mean(abs(CumAPMoS_FL.Ref - CumAPMoS_FL.Est));
    
    APstrLoA = [APstrt.differenceMean-APstrt.rpc APstrt.differenceMean+APstrt.rpc]*100; % cm
    APturLoA = [APturn.differenceMean-APturn.rpc APturn.differenceMean+APturn.rpc]*100; % cm
    
    strCorP = APstrt.corrP <= 0.05;
    strCor = [num2str(round(APstrt.r,2)) char(strCorP* '*')];
    
    turCorP = APturn.corrP <= 0.05;
    turCor = [num2str(round(APturn.r,2)) char(turCorP* '*')];
    
    strRms = num2str(round(APstrt.RMSE*100,2));
    turRms = num2str(round(APturn.RMSE*100,2));
    
    curAx = apFig.CurrentAxes;
    LineList = curAx.Children;
    for i = [3 4 5 9 10 11]
        set(get(get(LineList(i),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
    end
    legend(['Straight,  r:' strCor ', RMSE: ' strRms ' cm'],['First and Last,        r:' turCor ', RMSE: ' turRms ' cm']);
    
end
%% Step lengths
dat1Mode = 'Compare'; % Compare or Truth
baStatsMode = 'Gaussian'; % Gaussian or Non-parametric
varNames = {'ForceShoe','IMU', 'm'};
% BAdispOpt = {'eq';'r';'RMSE';'n'};
BAdispOptCor = {}; BAdispOptBA = {};

slRFig = figure('name','SL R');
% BlandAltman(slRFig,CumSLR.Ref,CumSLR.Est,varNames,'Step lengths Right',{},'corrinfo',BAdispOpt);
[~, ~,SLRstrt] = BlandAltmanDual(slRFig,CumSLR_SW.Ref,CumSLR_SW.Est,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','b',...
    'baStatsMode',baStatsMode,'data1Mode',dat1Mode);
hold on;
[~, ~,SLRturn] = BlandAltmanDual(slRFig,CumSLR_FL.Ref,CumSLR_FL.Est,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','r','symbols','o',...
    'baStatsMode','Gaussian','data1Mode',dat1Mode);

SLRs_MAD = mean(abs(CumSLR_SW.Ref - CumSLR_SW.Est));
SLRt_MAD = mean(abs(CumSLR_FL.Ref - CumSLR_FL.Est));

SLRstrLoA = [SLRstrt.differenceMean-SLRstrt.rpc SLRstrt.differenceMean+SLRstrt.rpc]*100; % cm
SLRturLoA = [SLRturn.differenceMean-SLRturn.rpc SLRturn.differenceMean+SLRturn.rpc]*100; % cm

strCorP = SLRstrt.corrP <= 0.05;
strCor = [num2str(round(SLRstrt.r,2)) char(strCorP* '*')];

turCorP = SLRturn.corrP <= 0.05;
turCor = [num2str(round(SLRturn.r,2)) char(turCorP* '*')];

strRms = num2str(round(SLRstrt.RMSE*100,2));
turRms = num2str(round(SLRturn.RMSE*100,2));

curAx = slRFig.CurrentAxes;
LineList = curAx.Children;
for i = [3 4 5 9 10 11]
    set(get(get(LineList(i),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
end
legend(['Straight,  r:' strCor ', RMSE: ' strRms ' cm'],['First and Last,        r:' turCor ', RMSE: ' turRms ' cm']);




slLFig = figure('name','SL L');
% BlandAltman(slLFig,CumSLL.Ref,CumSLL.Est,varNames,'Step lengths Left',{},'corrinfo',BAdispOpt);
[~, ~,SLLstrt] = BlandAltmanDual(slLFig,CumSLL_SW.Ref,CumSLL_SW.Est,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','b',...
    'baStatsMode',baStatsMode,'data1Mode',dat1Mode);
hold on;
[~, ~,SLLturn] = BlandAltmanDual(slLFig,CumSLL_FL.Ref,CumSLL_FL.Est,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','r','symbols','o',...
    'baStatsMode','Gaussian','data1Mode',dat1Mode);

SLLs_MAD = mean(abs(CumSLL_SW.Ref - CumSLL_SW.Est));
SLLt_MAD = mean(abs(CumSLL_FL.Ref - CumSLL_FL.Est));

SLLstrLoA = [SLLstrt.differenceMean-SLLstrt.rpc SLLstrt.differenceMean+SLLstrt.rpc]*100; % cm
SLLturLoA = [SLLturn.differenceMean-SLLturn.rpc SLLturn.differenceMean+SLLturn.rpc]*100; % cm

strCorP = SLLstrt.corrP <= 0.05;
strCor = [num2str(round(SLLstrt.r,2)) char(strCorP* '*')];

turCorP = SLLturn.corrP <= 0.05;
turCor = [num2str(round(SLLturn.r,2)) char(turCorP* '*')];

strRms = num2str(round(SLLstrt.RMSE*100,2));
turRms = num2str(round(SLLturn.RMSE*100,2));

curAx = slLFig.CurrentAxes;
LineList = curAx.Children;
for i = [3 4 5 9 10 11]
    set(get(get(LineList(i),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
end
legend(['Straight,  r:' strCor ', RMSE: ' strRms ' cm'],['First and Last,        r:' turCor ', RMSE: ' turRms ' cm']);



figure('name','BP SL LvR');
clear VarArray
curpl =subplot(1,2,1);
% val1 = [CumSLR_SW.Ref; CumSLR_FL.Ref]; val2 = [CumSLL_SW.Ref; CumSLL_FL.Ref]; ylabel('Step Lengths(m)'); title('SL ForceShoe')
val1 = CumSLR_SW.Ref; val2 = CumSLL_SW.Ref; ylabel('Step Lengths - Straight(m)'); title('SL Straight ForceShoe')
VarArray = [val1; val2];
g = [zeros(length(val1), 1); ones(length(val2), 1); ];
boxplot(VarArray,g);
curpl.XTickLabel = {'Right', 'Left'};

diffTest = ttest2(val1,val2);

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
    
end

clear VarArray
curpl =subplot(1,2,2);
val1 = [CumSLR_SW.Est; CumSLR_FL.Est]; val2 = [CumSLL_SW.Est; CumSLL_FL.Est]; ylabel('Step Lengths(m)'); 
val1 = CumSLR_SW.Est; val2 = CumSLL_SW.Est; ylabel('Step Lengths Straight(m)'); title('SL Straight IMU') 
VarArray = [val1; val2];
g = [zeros(length(val1), 1); ones(length(val2), 1); ];
boxplot(VarArray,g);
curpl.XTickLabel = {'Right', 'Left'};



diffTest = ttest2(val1,val2);

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
    
end


%% Step widths
swRFig = figure('name','SW R');
[~, ~, swRoverall] = BlandAltman(swRFig,[CumSWR_SW.Ref; CumSWR_FL.Ref],[CumSWR_SW.Est; CumSWR_FL.Est],varNames,'Step widths Right',{},'corrinfo',BAdispOpt);

swLFig = figure('name','SW L');
[~, ~, swLoverall] = BlandAltman(swLFig,[CumSWL_SW.Ref; CumSWL_FL.Ref],[CumSWL_SW.Est; CumSWL_FL.Est],varNames,'Step widths Left',{},'corrinfo',BAdispOpt);

SWRs_MAD = mean(abs(CumSWR_SW.Ref - CumSWR_SW.Est));
SWRt_MAD = mean(abs(CumSWR_FL.Ref - CumSWR_FL.Est));
SWLs_MAD = mean(abs(CumSWL_SW.Ref - CumSWL_SW.Est));
SWLt_MAD = mean(abs(CumSWL_FL.Ref - CumSWL_FL.Est));


figure('name','BP SW LvR');
clear VarArray
curpl =subplot(1,2,1);
val1 = [CumSWR_SW.Ref; CumSWR_FL.Ref]; val2 = [CumSWL_SW.Ref; CumSWL_FL.Ref]; ylabel('Step Widths (m)')
val1 = CumSWR_SW.Ref; val2 = CumSWL_SW.Ref; ylabel('Step Widths Straight(m)')
VarArray = [val1; val2];
g = [zeros(length(val1), 1); ones(length(val2), 1); ];
boxplot(VarArray,g);
curpl.XTickLabel = {'Right', 'Left'};
title('SW ForceShoe')


diffTest = ttest2(val1,val2);

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
    
end

clear VarArray
curpl =subplot(1,2,2);
val1 = [CumSWR_SW.Est; CumSWR_FL.Est]; val2 = [CumSWL_SW.Est; CumSWL_FL.Est]; ylabel('Step Widths (m)')
val1 = CumSWR_SW.Est; val2 = CumSWL_SW.Est; ylabel('Step Widths straight (m)')
VarArray = [val1; val2];
g = [zeros(length(val1), 1); ones(length(val2), 1); ];
boxplot(VarArray,g);
curpl.XTickLabel = {'Right', 'Left'};
title('SW IMU')


diffTest = ttest2(val1,val2);

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
    
end

%% Graph check variable
clear str1 compTableGaitQuality
headerNames = {'Trial','MLRstrtRMSE','MLRstrtMAD','MLRturnRMSE','MLRturnMAD',...
    'MLLstrtRMSE','MLLstrtMAD','MLLturnRMSE','MLLturnMAD',...
    'SLRstrtRMSE','SLRstrtMAD','SLRturnRMSE','SLRturnMAD',...
    'SLLstrtRMSE','SLLstrtMAD','SLLturnRMSE','SLLturnMAD',...
    'APstrtRMSE','APstrtMAD','APturnRMSE','APturnMAD',...
    'swRoverall','swLoverall'};
compTableGaitQuality = array2table(zeros(0,length(headerNames)));
compTableGaitQuality.Properties.VariableNames = headerNames;
trialTypeList = fieldnames(CumStatsCpos.Overall.rRMSE);
eCol = 1;
bookEnders = '';% '' or '$'
connString = ' � '; % ' \pm ' or �

cTrialType = trialTypeList{1};
% all values in cm
str1{1,eCol} = cTrialType; eCol = eCol + 1;
% ML R MoS
str1{1,eCol} = ([bookEnders num2str(round(MLRstrt.RMSE*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(MLRs_MAD*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(MLRturn.RMSE*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(MLRt_MAD*100,sigDig)) bookEnders]);eCol = eCol + 1;

% ML L MoS
str1{1,eCol} = ([bookEnders num2str(round(MLLstrt.RMSE*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(MLLs_MAD*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(MLLturn.RMSE*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(MLLt_MAD*100,sigDig)) bookEnders]);eCol = eCol + 1;

% SL R
str1{1,eCol} = ([bookEnders num2str(round(SLRstrt.RMSE*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(SLRs_MAD*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(SLRturn.RMSE*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(SLRt_MAD*100,sigDig)) bookEnders]);eCol = eCol + 1;

% SL L
str1{1,eCol} = ([bookEnders num2str(round(SLLstrt.RMSE*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(SLLs_MAD*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(SLLturn.RMSE*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(SLLt_MAD*100,sigDig)) bookEnders]);eCol = eCol + 1;

% AP
str1{1,eCol} = ([bookEnders num2str(round(APstrt.RMSE*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(APs_MAD*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(APturn.RMSE*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(APt_MAD*100,sigDig)) bookEnders]);eCol = eCol + 1;

% SW R
str1{1,eCol} = ([bookEnders num2str(round(swRoverall.RMSE*100,sigDig)) bookEnders]);eCol = eCol + 1;
% str1{1,eCol} = ([bookEnders num2str(round(APs_MAD*100,sigDig)) bookEnders]);eCol = eCol + 1;
% SW L
str1{1,eCol} = ([bookEnders num2str(round(swLoverall.RMSE*100,sigDig)) bookEnders]);eCol = eCol + 1;
% str1{1,eCol} = ([bookEnders num2str(round(APt_MAD*100,sigDig)) bookEnders]);eCol = eCol + 1;


compTableGaitQuality = [compTableGaitQuality; str1];
