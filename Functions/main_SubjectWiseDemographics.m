%% main_SubjectWiseDemographics

% sub 1 6 7 12 only
age = [70 67 65 71];
monthsPS = [89 40 16 17];
weight = [97 80 92 67];
height = [1.74 1.62 1.86 1.53];
bbs = [52 43 52 56];
fm = [49 56 53 63];
speed10M = [0.76 0.54 0.94 0.83];

%%
clear str1 patDemographics  headerNames
headerNames = {'Age','PSMonths','Weight', ...
    'Height','BBS','FM','Speed10M', ...
    };
patDemographics = array2table(zeros(0,length(headerNames)));
patDemographics.Properties.VariableNames = headerNames;


sigDig = 1;
sigDig2 = 1;

bookEnders = '';% '' or '$'
connString1 = ' ( '; % ' \pm ' or �
connString2 = ' ) '; % ' \pm ' or �


eCol = 1;
% age
str1{1,eCol} = ([num2str(round(mean(age),sigDig)) connString1 num2str(round(std(age),sigDig2)) connString2]);eCol = eCol + 1;
% months post stroke
str1{1,eCol} = ([num2str(round(mean(monthsPS),sigDig)) connString1 num2str(round(std(monthsPS),sigDig2)) connString2]);eCol = eCol + 1;
% weight
str1{1,eCol} = ([num2str(round(mean(weight),sigDig)) connString1 num2str(round(std(weight),sigDig2)) connString2]);eCol = eCol + 1;
% height
str1{1,eCol} = ([num2str(round(mean(height),sigDig)) connString1 num2str(round(std(height),sigDig2)) connString2]);eCol = eCol + 1;
% bbs
str1{1,eCol} = ([num2str(round(mean(bbs),sigDig)) connString1 num2str(round(std(bbs),sigDig2)) connString2]);eCol = eCol + 1;
% fm
str1{1,eCol} = ([num2str(round(mean(fm),sigDig)) connString1 num2str(round(std(fm),sigDig2)) connString2]);eCol = eCol + 1;
% speed10M
str1{1,eCol} = ([num2str(round(mean(speed10M),sigDig)) connString1 num2str(round(std(speed10M),sigDig2)) connString2]);eCol = eCol + 1;

patDemographics = [patDemographics; str1];
