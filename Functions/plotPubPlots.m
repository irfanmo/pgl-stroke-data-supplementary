%% plotPubPlots;

%% Single Stance Times
% BAdispOpt = {'eq';'r';'RMSE';'n'};
CorrdispOpt = {};
BAdispOpt = {};
baStatsMode = 'Gaussian'; % Gaussian or Non-parametric
varNames = {'Reference','PGL', 's'};

[~, ~,STABA] =  BlandAltman(CumSTA.Ref,CumSTA.Est,varNames,{},{},'corrinfo',CorrdispOpt,...
    'baStatsMode',baStatsMode,'baInfo',BAdispOpt);
SigDiff.StanceTimeAff = ttest2(CumSTA.Ref,CumSTA.Est);
OverAllMean.STAff_Ref = [mean(CumSTA.Ref) std(CumSTA.Ref)];
OverAllMean.STAff_Est = [mean(CumSTA.Est) std(CumSTA.Est)];

strCorP = STABA.corrP <= 0.05;
strCor = [num2str(round(STABA.r,2)) char(strCorP* '*')];
strRms = num2str(round(STABA.RMSE*1000,2));

curfig = gcf;
curAx = curfig.CurrentAxes;
LineList = curAx.Children;
for i = [5]
    set(get(get(LineList(i),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
end
legend(['r:' strCor ', RMSE: ' strRms ' ms']);
curfig.Name = 'ST Aff';


[~, ~,STNBA] =  BlandAltman(CumSTN.Ref,CumSTN.Est,varNames,{},{},'corrinfo',CorrdispOpt,...
    'baStatsMode',baStatsMode,'baInfo',BAdispOpt);
SigDiff.StanceTimeNAff = ttest2(CumSTN.Ref,CumSTN.Est);

OverAllMean.STNAff_Ref = [mean(CumSTN.Ref) std(CumSTN.Ref)];
OverAllMean.STNAff_Est = [mean(CumSTN.Est) std(CumSTN.Est)];

strCorP = STNBA.corrP <= 0.05;
strCor = [num2str(round(STNBA.r,2)) char(strCorP* '*')];
strRms = num2str(round(STNBA.RMSE*1000,2));

curfig = gcf;
curAx = curfig.CurrentAxes;
LineList = curAx.Children;
for i = [5]
    set(get(get(LineList(i),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
end
legend(['r:' strCor ', RMSE: ' strRms ' ms']);
curfig.Name = 'ST Less Aff';

%% Boxplots Stance Time

figure('name','Boxplots Stance Time AFF v LessAff');
clear AllstepLengths
curpl =subplot(1,2,1);
val1 = CumSTA.Ref; val2 = CumSTN.Ref;

VarArray = [val1; val2];
g = [zeros(length(val1), 1); ones(length(val2), 1); ];
bpAx = boxplot(VarArray,g); set(bpAx,{'linew'},{2})
curpl.XTickLabel = {'Affected', 'Less Affected'};
ylabel('Single Stance Time (s)')
title('Reference')


diffTest = ttest2(val1,val2);

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
    
end

curpl.FontWeight = 'bold'; curpl.FontSize = 16;


clear VarArray
curpl =subplot(1,2,2);
val1 = CumSTA.Est; val2 = CumSTN.Est;
VarArray = [val1; val2];
g = [zeros(length(val1), 1); ones(length(val2), 1); ];
bpAx = boxplot(VarArray,g);
set(bpAx,{'linew'},{2})
curpl.XTickLabel = {'Affected', 'Less Affected'};
ylabel('Single Stance Time (s)')
title('PGL')


diffTest = ttest2(val1,val2);

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
    
end
curpl.FontWeight = 'bold'; curpl.FontSize = 16;

%% Step lengths
dat1Mode = 'Compare'; % Compare or Truth
baStatsMode = 'Gaussian'; % Gaussian or Non-parametric
varNames = {'Reference','PGL', 'm'};
% BAdispOpt = {'eq';'r';'RMSE';'n'};
BAdispOptCor = {}; BAdispOptBA = {};

slAFig = figure('name','SL Aff');
% BlandAltman(slRFig,CumSLR.Ref,CumSLR.Est,varNames,'Step lengths Aff',{},'corrinfo',BAdispOpt);
[~, ~,SLAstrt] = BlandAltmanDual(slAFig,CumSLA_SW.Ref,CumSLA_SW.Est,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','b',...
    'baStatsMode',baStatsMode,'data1Mode',dat1Mode);
hold on;
[~, ~,SLAturn] = BlandAltmanDual(slAFig,CumSLA_FL.Ref,CumSLA_FL.Est,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','r','symbols','o',...
    'baStatsMode','Gaussian','data1Mode',dat1Mode);

SigDiff.SLAff_SW = ttest2(CumSLA_SW.Ref,CumSLA_SW.Est);
SigDiff.SLAff_FL = ttest2(CumSLA_FL.Ref,CumSLA_FL.Est);

OverAllMean.SLAff_Ref = [mean([CumSLA_SW.Ref; CumSLA_FL.Ref]) std([CumSLA_SW.Ref; CumSLA_FL.Ref])];
OverAllMean.SLAff_Est = [mean([CumSLA_SW.Est; CumSLA_FL.Est]) std([CumSLA_SW.Est; CumSLA_FL.Est])];



SLAs_MAD = mean(abs(CumSLA_SW.Ref - CumSLA_SW.Est));
SLAt_MAD = mean(abs(CumSLA_FL.Ref - CumSLA_FL.Est));

SLAstrLoA = [SLAstrt.differenceMean-SLAstrt.rpc SLAstrt.differenceMean+SLAstrt.rpc]*100; % cm
SLAturLoA = [SLAturn.differenceMean-SLAturn.rpc SLAturn.differenceMean+SLAturn.rpc]*100; % cm

strCorP = SLAstrt.corrP <= 0.05;
strCor = [num2str(round(SLAstrt.r,2)) char(strCorP* '*')];

turCorP = SLAturn.corrP <= 0.05;
turCor = [num2str(round(SLAturn.r,2)) char(turCorP* '*')];

strRms = num2str(round(SLAstrt.RMSE*100,2));
turRms = num2str(round(SLAturn.RMSE*100,2));

curAx = slAFig.CurrentAxes;
LineList = curAx.Children;
for i = [3 4 5 9 10 11]
    set(get(get(LineList(i),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
end
legend(['Steady state, r:' strCor ', RMSE: ' strRms ' cm'],['Other,             r:' turCor ', RMSE: ' turRms ' cm']);

slNFig = figure('name','SL Less Aff');
% BlandAltman(slLFig,CumSLL.Ref,CumSLL.Est,varNames,'Step lengths Less Aff',{},'corrinfo',BAdispOpt);
[~, ~,SLNstrt] = BlandAltmanDual(slNFig,CumSLN_SW.Ref,CumSLN_SW.Est,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','b',...
    'baStatsMode',baStatsMode,'data1Mode',dat1Mode);
hold on;
[~, ~,SLNturn] = BlandAltmanDual(slNFig,CumSLN_FL.Ref,CumSLN_FL.Est,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','r','symbols','o',...
    'baStatsMode','Gaussian','data1Mode',dat1Mode);

SigDiff.SLNAff_SW = ttest2(CumSLN_SW.Ref,CumSLN_SW.Est);
SigDiff.SLNAff_FL = ttest2(CumSLN_FL.Ref,CumSLN_FL.Est);



OverAllMean.SLNAff_Ref = [mean([CumSLN_SW.Ref; CumSLN_FL.Ref]) std([CumSLN_SW.Ref; CumSLN_FL.Ref])];
OverAllMean.SLNAff_Est = [mean([CumSLN_SW.Est; CumSLN_FL.Est]) std([CumSLN_SW.Est; CumSLN_FL.Est])];

SLNs_MAD = mean(abs(CumSLN_SW.Ref - CumSLN_SW.Est));
SLNt_MAD = mean(abs(CumSLN_FL.Ref - CumSLN_FL.Est));

SLNstrLoA = [SLNstrt.differenceMean-SLNstrt.rpc SLNstrt.differenceMean+SLNstrt.rpc]*100; % cm
SLNturLoA = [SLNturn.differenceMean-SLNturn.rpc SLNturn.differenceMean+SLNturn.rpc]*100; % cm

strCorP = SLNstrt.corrP <= 0.05;
strCor = [num2str(round(SLNstrt.r,2)) char(strCorP* '*')];

turCorP = SLNturn.corrP <= 0.05;
turCor = [num2str(round(SLNturn.r,2)) char(turCorP* '*')];

strRms = num2str(round(SLNstrt.RMSE*100,2));
turRms = num2str(round(SLNturn.RMSE*100,2));

curAx = slNFig.CurrentAxes;
LineList = curAx.Children;
for i = [3 4 5 9 10 11]
    set(get(get(LineList(i),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
end
legend(['Steady state, r:' strCor ', RMSE: ' strRms ' cm'],['Other,             r:' turCor ', RMSE: ' turRms ' cm']);


%% boxplot step length
% figure('name','BP SL AvLA');
figure('name','BP SL AvLA all steps');
clear VarArray
curpl =subplot(1,2,1);
val1 = [CumSLA_SW.Ref; CumSLA_FL.Ref]; val2 = [CumSLN_SW.Ref; CumSLN_FL.Ref]; 
ylabel('Step Lengths(m)'); title('SL ForceShoe')
% val1 = CumSLA_SW.Ref; val2 = CumSLN_SW.Ref;
VarArray = [val1; val2];
g = [zeros(length(val1), 1); ones(length(val2), 1); ];
bpAx = boxplot(VarArray,g); ylabel('Step Lengths (m)');
title('Reference')
set(bpAx,{'linew'},{2})
curpl.XTickLabel = {'Affected', 'Less Affected'};
curpl.FontWeight = 'bold'; curpl.FontSize = 16;
diffTest = ttest2(val1,val2);

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
    
end
curylim = [-0.2 0.8];
ylim(curylim);

clear VarArray
curpl =subplot(1,2,2);
val1 = [CumSLA_SW.Est; CumSLA_FL.Est]; val2 = [CumSLN_SW.Est; CumSLN_FL.Est]; 
ylabel('Step Lengths(m)'); title('SL IMU')
% val1 = CumSLA_SW.Est; val2 = CumSLN_SW.Est;
VarArray = [val1; val2];
g = [zeros(length(val1), 1); ones(length(val2), 1); ];
bpAx = boxplot(VarArray,g); ylabel('Step Lengths (m)');
title('PGL')
set(bpAx,{'linew'},{2})
curpl.XTickLabel = {'Affected', 'Less Affected'};



diffTest = ttest2(val1,val2);

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
    
end
curpl.FontWeight = 'bold'; curpl.FontSize = 16;
ylim(curylim);


%% MoS BA plots
dat1Mode = 'Compare'; % Compare or Truth
baStatsMode = 'Non-parametric'; % Gaussian or Non-parametric
varNames = {'ForceShoe','IMU', 'm'};
% BAdispOpt = {'eq';'r';'RMSE';'n'};
BAdispOptCor = {}; BAdispOptBA = {};


% ML R
mlrFig = figure('name','ML MoS Affected');
% BlandAltman(CumMLAMoS_SW.Ref,CumMLAMoS_SW.Est,varNames,'ML MoS Aff',{},'corrinfo',BAdispOpt);
[~, ~,MLAstrt] = BlandAltmanDual(mlrFig,CumMLAMoS_SW.Ref,CumMLAMoS_SW.Est,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','b',...
    'baStatsMode',baStatsMode,'data1Mode',dat1Mode);
hold on;
[~, ~,MLAturn] = BlandAltmanDual(mlrFig,CumMLAMoS_FL.Ref,CumMLAMoS_FL.Est,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','r','symbols','o',...
    'baStatsMode','Gaussian','data1Mode',dat1Mode);

SigDiff.MLMoSAff_SW = ttest2(CumMLAMoS_SW.Ref,CumMLAMoS_SW.Est);
SigDiff.MLMoSAff_FL = ttest2(CumMLAMoS_FL.Ref,CumMLAMoS_FL.Est);



OverAllMean.MLAff_Ref = [mean([CumMLAMoS_SW.Ref; CumMLAMoS_FL.Ref]) std([CumMLAMoS_SW.Ref; CumMLAMoS_FL.Ref])];
OverAllMean.MLAff_Est = [mean([CumMLAMoS_SW.Est; CumMLAMoS_FL.Est]) std([CumMLAMoS_SW.Est; CumMLAMoS_FL.Est])];

MLAs_MAD = mean(abs(CumMLAMoS_SW.Ref - CumMLAMoS_SW.Est));
MLAt_MAD = mean(abs(CumMLAMoS_FL.Ref - CumMLAMoS_FL.Est));

MLAstrLoA = [MLAstrt.differenceMean-MLAstrt.rpc MLAstrt.differenceMean+MLAstrt.rpc]*100; % cm
MLAturLoA = [MLAturn.differenceMean-MLAturn.rpc MLAturn.differenceMean+MLAturn.rpc]*100; % cm

strCorP = MLAstrt.corrP <= 0.05;
strCor = [num2str(round(MLAstrt.r,2)) char(strCorP* '*')];

turCorP = MLAturn.corrP <= 0.05;
turCor = [num2str(round(MLAturn.r,2)) char(turCorP* '*')];

strRms = num2str(round(MLAstrt.RMSE*100,2));
turRms = num2str(round(MLAturn.RMSE*100,2));

curAx = mlrFig.CurrentAxes;
LineList = curAx.Children;
for i = [3 4 5 9 10 11]
    set(get(get(LineList(i),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
end
legend(['Steady state, r:' strCor ', RMSE: ' strRms ' cm'],['Other,             r:' turCor ', RMSE: ' turRms ' cm']);


% ML N MoS
mllFig = figure('name','ML MoS L');
% BlandAltman(mllFig,CumMLLMoS.Ref,CumMLLMoS.Est,varNames,'ML MoS Less Aff',{},'corrinfo',BAdispOpt);
[~, ~,MLNstrt] = BlandAltmanDual(mllFig,CumMLNMoS_SW.Ref,CumMLNMoS_SW.Est,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','b',...
    'baStatsMode',baStatsMode,'data1Mode',dat1Mode);
hold on;
[~, ~,MLNturn] = BlandAltmanDual(mllFig,CumMLNMoS_FL.Ref,CumMLNMoS_FL.Est,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','r','symbols','o',...
    'baStatsMode','Gaussian','data1Mode',dat1Mode);

SigDiff.MLMoSNAff_SW = ttest2(CumMLNMoS_SW.Ref,CumMLNMoS_SW.Est);
SigDiff.MLMoSNAff_FL = ttest2(CumMLNMoS_FL.Ref,CumMLNMoS_FL.Est);


OverAllMean.MLNAff_Ref = [mean([CumMLNMoS_SW.Ref; CumMLNMoS_FL.Ref]) std([CumMLNMoS_SW.Ref; CumMLNMoS_FL.Ref])];
OverAllMean.MLNAff_Est = [mean([CumMLNMoS_SW.Est; CumMLNMoS_FL.Est]) std([CumMLNMoS_SW.Est; CumMLNMoS_FL.Est])];

MLNs_MAD = mean(abs(CumMLNMoS_SW.Ref - CumMLNMoS_SW.Est));
MLNt_MAD = mean(abs(CumMLNMoS_FL.Ref - CumMLNMoS_FL.Est));

MLNstrLoA = [MLNstrt.differenceMean-MLNstrt.rpc MLNstrt.differenceMean+MLNstrt.rpc]*100; % cm
MLNturLoA = [MLNturn.differenceMean-MLNturn.rpc MLNturn.differenceMean+MLNturn.rpc]*100; % cm

strCorP = MLNstrt.corrP <= 0.05;
if strCorP
strCor = [num2str(round(MLNstrt.r,2)) char(strCorP* '*')];
else
    strCor = num2str(round(MLNstrt.r,2));
end

turCorP = MLNturn.corrP <= 0.05;
if turCorP
turCor = [num2str(round(MLNturn.r,2)) char(turCorP* '*')];
else
    turCor = num2str(round(MLNturn.r,2));
end

strRms = num2str(round(MLNstrt.RMSE*100,2));
turRms = num2str(round(MLNturn.RMSE*100,2));

curAx = mllFig.CurrentAxes;
LineList = curAx.Children;
for i = [3 4 5 9 10 11]
    set(get(get(LineList(i),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
end
legend(['Steady state, r:' strCor ', RMSE: ' strRms ' cm'],['Other,             r:' turCor ', RMSE: ' turRms ' cm']);

%% box plot Ml mos
% figure('name','BP ML MoS');
figure('name','BP ML MoS all');
clear VarArray
curpl =subplot(1,2,1);
val1 = [CumMLAMoS_SW.Ref; CumMLAMoS_FL.Ref]; val2 = [CumMLNMoS_SW.Ref; CumMLNMoS_FL.Ref]; ylabel('ML MoS(m)'); title('ML MoS ForceShoe')
% val1 = CumMLAMoS_SW.Ref; val2 = CumMLNMoS_SW.Ref;
VarArray = [val1; val2];
g = [zeros(length(val1), 1); ones(length(val2), 1); ];
bpAx = boxplot(VarArray,g);
set(bpAx,{'linew'},{2})
ylabel('ML MoS (m)'); title('Reference')
%     ylabel('ML MoS (m)'); title('ML MoS ForceShoe')
curpl.XTickLabel = {'Affected', 'Less Affected'};

diffTest = ttest2(val1,val2);

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
    
end
curpl.FontWeight = 'bold'; curpl.FontSize = 16;
curylim = [0.02 0.4];
ylim(curylim)

clear VarArray
curpl =subplot(1,2,2);
val1 = [CumMLAMoS_SW.Est; CumMLAMoS_FL.Est]; val2 = [CumMLNMoS_SW.Est; CumMLNMoS_FL.Est]; ylabel('ML MoS(m)'); title('ML MoS IMU')
% val1 = CumMLAMoS_SW.Est; val2 = CumMLNMoS_SW.Est;
VarArray = [val1; val2];
g = [zeros(length(val1), 1); ones(length(val2), 1); ];
bpAx = boxplot(VarArray,g);
set(bpAx,{'linew'},{2})
ylabel('ML MoS (m)'); title('PGL')
%     ylabel('ML MoS (m)'); title('ML MoS IMU')
curpl.XTickLabel = {'Affected', 'Less Affected'};

diffTest = ttest2(val1,val2);

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
    
end
curpl.FontWeight = 'bold'; curpl.FontSize = 16;
ylim(curylim)

%% AP MoS
% baStatsMode = 'Non-parametric'; % Gaussian or Non-parametric

imuVal = CumAPAMoS_SW.Est; refVal = CumAPAMoS_SW.Ref;
figure; plot(imuVal); hold on; plot(refVal);

% if apOutlierRemoval
%     allOutliers = fun_findIQROutliers(imuVal);
%     imuVal(allOutliers,:) = []; refVal(allOutliers,:) = []; warning('outliers removed for AP mos alone')
%     allOutliers = fun_findIQROutliers(refVal);
%     imuVal(allOutliers,:) = []; refVal(allOutliers,:) = [];
% end

apFig = figure('name','AP Aff MoS');
% BlandAltman(apFig,refVal,imuVal,varNames,'AP MoS',{},'corrinfo',BAdispOpt);

[~, ~,APAstrt] = BlandAltmanDual(apFig,refVal,imuVal,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','b',...
    'baStatsMode',baStatsMode,'data1Mode',dat1Mode);
hold on;
[~, ~,APAturn] = BlandAltmanDual(apFig,CumAPAMoS_FL.Ref,CumAPAMoS_FL.Est,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','r','symbols','o',...
    'baStatsMode','Gaussian','data1Mode',dat1Mode);

SigDiff.APMoSAff_SW = ttest2(refVal,imuVal);
SigDiff.APMoSAff_FL = ttest2(CumAPAMoS_FL.Ref,CumAPAMoS_FL.Est);

SigDiff.SLAff = ttest2([CumSLA_SW.Ref; CumSLA_FL.Ref],[CumSLA_SW.Est; CumSLA_FL.Est]);
SigDiff.SLNAff = ttest2([CumSLN_SW.Ref;CumSLN_FL.Ref],[CumSLN_SW.Est;CumSLN_FL.Est]);
SigDiff.MLMoSAff = ttest2([CumMLAMoS_SW.Ref; CumMLAMoS_FL.Ref],[CumMLAMoS_SW.Est; CumMLAMoS_FL.Est]);
SigDiff.MLMoSNAff = ttest2([CumMLNMoS_SW.Ref;CumMLNMoS_FL.Ref],[CumMLNMoS_SW.Est;CumMLNMoS_FL.Est]);
SigDiff.APMoSAff = ttest2([refVal;CumAPAMoS_FL.Ref],[imuVal;CumAPAMoS_FL.Est]);

OverAllMean.APAff_Ref = [mean([refVal;CumAPAMoS_FL.Ref]) std([refVal;CumAPAMoS_FL.Ref])];
OverAllMean.APAff_Est = [mean([imuVal;CumAPAMoS_FL.Est]) std([imuVal;CumAPAMoS_FL.Est])];



APAs_MAD = mean(abs(CumAPAMoS_SW.Ref - CumAPAMoS_SW.Est));
APAt_MAD = mean(abs(CumAPAMoS_FL.Ref - CumAPAMoS_FL.Est));

APAstrLoA = [APAstrt.differenceMean-APAstrt.rpc APAstrt.differenceMean+APAstrt.rpc]*100; % cm
APAturLoA = [APAturn.differenceMean-APAturn.rpc APAturn.differenceMean+APAturn.rpc]*100; % cm

strCorP = APAstrt.corrP <= 0.05;
strCor = [num2str(round(APAstrt.r,2)) char(strCorP* '*')];

turCorP = APAturn.corrP <= 0.05;
turCor = [num2str(round(APAturn.r,2)) char(turCorP* '*')];

strRms = num2str(round(APAstrt.RMSE*100,2));
turRms = num2str(round(APAturn.RMSE*100,2));

curAx = apFig.CurrentAxes;
LineList = curAx.Children;
for i = [3 4 5 9 10 11]
    set(get(get(LineList(i),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
end
legend(['Steady state, r:' strCor ', RMSE: ' strRms ' cm'],['Other,             r:' turCor ', RMSE: ' turRms ' cm']);



imuVal = CumAPNMoS_SW.Est; refVal = CumAPNMoS_SW.Ref;
figure; plot(imuVal); hold on; plot(refVal);

% if apOutlierRemoval
%     allOutliers = fun_findIQROutliers(imuVal);
%     imuVal(allOutliers,:) = []; refVal(allOutliers,:) = []; warning('outliers removed for AP mos alone')
%     allOutliers = fun_findIQROutliers(refVal);
%     imuVal(allOutliers,:) = []; refVal(allOutliers,:) = [];
% end

apFig = figure('name','AP Less Aff MoS');
% BlandAltman(apFig,refVal,imuVal,varNames,'AP MoS',{},'corrinfo',BAdispOpt);

[~, ~,APNstrt] = BlandAltmanDual(apFig,refVal,imuVal,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','b',...
    'baStatsMode',baStatsMode,'data1Mode',dat1Mode);
hold on;
[~, ~,APNturn] = BlandAltmanDual(apFig,CumAPNMoS_FL.Ref,CumAPNMoS_FL.Est,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','r','symbols','o',...
    'baStatsMode','Gaussian','data1Mode',dat1Mode);

SigDiff.APMoSNAff = ttest2([refVal;CumAPNMoS_FL.Ref],[imuVal;CumAPNMoS_FL.Est]);
SigDiff.APMoSNAff_SW = ttest2(refVal,imuVal);
SigDiff.APMoSNAff_FL = ttest2(CumAPNMoS_FL.Ref,CumAPNMoS_FL.Est);

OverAllMean.APNAff_Ref = [mean([refVal;CumAPNMoS_FL.Ref]) std([refVal;CumAPNMoS_FL.Ref])];
OverAllMean.APNAff_Est = [mean([imuVal;CumAPNMoS_FL.Est]) std([imuVal;CumAPNMoS_FL.Est])];

APNs_MAD = mean(abs(CumAPNMoS_SW.Ref - CumAPNMoS_SW.Est));
APNt_MAD = mean(abs(CumAPNMoS_FL.Ref - CumAPNMoS_FL.Est));

APNstrLoA = [APNstrt.differenceMean-APNstrt.rpc APNstrt.differenceMean+APNstrt.rpc]*100; % cm
APNturLoA = [APNturn.differenceMean-APNturn.rpc APNturn.differenceMean+APNturn.rpc]*100; % cm

strCorP = APNstrt.corrP <= 0.05;
strCor = [num2str(round(APNstrt.r,2)) char(strCorP* '*')];

turCorP = APNturn.corrP <= 0.05;
turCor = [num2str(round(APNturn.r,2)) char(turCorP* '*')];

strRms = num2str(round(APNstrt.RMSE*100,2));
turRms = num2str(round(APNturn.RMSE*100,2));

curAx = apFig.CurrentAxes;
LineList = curAx.Children;
for i = [3 4 5 9 10 11]
    set(get(get(LineList(i),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
end
legend(['Steady state, r:' strCor ', RMSE: ' strRms ' cm'],['Other,             r:' turCor ', RMSE: ' turRms ' cm']);

%% box plot AP MoS
figure('name','BP AP MoS');
clear VarArray
curpl =subplot(1,2,1);
val1 = [CumAPAMoS_SW.Ref; CumAPAMoS_FL.Ref]; val2 = [CumAPNMoS_SW.Ref; CumAPNMoS_FL.Ref];

%     val1 = CumAPAMoS_SW.Ref; val2 = CumAPNMoS_SW.Ref;

% if apOutlierRemoval
%     allOutliers = fun_findIQROutliers(val1);
%     val1(allOutliers,:) = []; val2(allOutliers,:) = [];
%     allOutliers = fun_findIQROutliers(val2);
%     val1(allOutliers,:) = []; val2(allOutliers,:) = []; warning('outliers removed for AP mos alone')
% end

VarArray = [val1; val2];
g = [zeros(length(val1), 1); ones(length(val2), 1); ];
bpAx = boxplot(VarArray,g); 
set(bpAx,{'linew'},{2})
ylabel('AP MoS (m)'); title('Reference')
curpl.XTickLabel = {'Affected', 'Less Affected'};

diffTest = ttest2(val1,val2);

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
    
end
curylim = [-0.2 0.4];
curpl.FontWeight = 'bold'; curpl.FontSize = 16;
ylim(curylim)

clear VarArray
curpl =subplot(1,2,2);
val1 = [CumAPAMoS_SW.Est; CumAPAMoS_FL.Est]; val2 = [CumAPNMoS_SW.Est; CumAPNMoS_FL.Est];
%     val1 = CumAPAMoS_SW.Est; val2 = CumAPNMoS_SW.Est;

% if apOutlierRemoval
%     allOutliers = fun_findIQROutliers(val1);
%     val1(allOutliers,:) = []; val2(allOutliers,:) = [];
%     allOutliers = fun_findIQROutliers(val2);
%     val1(allOutliers,:) = []; val2(allOutliers,:) = []; warning('outliers removed for AP mos alone')
% end

VarArray = [val1; val2];
g = [zeros(length(val1), 1); ones(length(val2), 1); ];
bpAx = boxplot(VarArray,g); 
set(bpAx,{'linew'},{2})
ylabel('AP MoS (m)'); title('PGL')
curpl.XTickLabel = {'Affected', 'Less Affected'};

diffTest = ttest2(val1,val2);

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
    
end
curpl.FontWeight = 'bold'; curpl.FontSize = 16;
ylim(curylim)


%% Step widths
swAFig = figure('name','SW Aff');
% [~, ~, swAoverall] = BlandAltman(swAFig,[CumSWA_SW.Ref; CumSWA_FL.Ref],[CumSWA_SW.Est; CumSWA_FL.Est],varNames,'Step widths Aff',{},'corrinfo',BAdispOpt);
[~, ~,swAStr] = BlandAltmanDual(swAFig,CumSWA_SW.Ref,CumSWA_SW.Est,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','b',...
    'baStatsMode',baStatsMode,'data1Mode',dat1Mode);
hold on;
[~, ~,swAturn] = BlandAltmanDual(swAFig,CumSWA_FL.Ref,CumSWA_FL.Est,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','r','symbols','o',...
    'baStatsMode','Gaussian','data1Mode',dat1Mode);

SigDiff.SWA_SW = ttest2(CumSWA_SW.Ref,CumSWA_SW.Est);
SigDiff.SWA_FL = ttest2(CumSWA_FL.Ref,CumSWA_FL.Est);

SWAstrLoA = [swAStr.differenceMean-swAStr.rpc swAStr.differenceMean+swAStr.rpc]*100; % cm
SWAturLoA = [swAturn.differenceMean-swAturn.rpc swAturn.differenceMean+swAturn.rpc]*100; % cm

strCorP = swAStr.corrP <= 0.05;
strCor = [num2str(round(swAStr.r,2)) char(strCorP* '*')];

turCorP = swAturn.corrP <= 0.05;
turCor = [num2str(round(swAturn.r,2)) char(turCorP* '*')];

strRms = num2str(round(swAStr.RMSE*100,2));
turRms = num2str(round(swAturn.RMSE*100,2));

curAx = swAFig.CurrentAxes;
LineList = curAx.Children;
for i = [3 4 5 9 10 11]
    set(get(get(LineList(i),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
end
legend(['Steady state, r:' strCor ', RMSE: ' strRms ' cm'],['Other,             r:' turCor ', RMSE: ' turRms ' cm']);



% xxxxxxxxxxxxxxxxxxxxxxxxxxxxx
swNFig = figure('name','SW Less Aff');
% [~, ~, swNoverall] = BlandAltman(swNFig,[CumSWN_SW.Ref; CumSWN_FL.Ref],[CumSWN_SW.Est; CumSWN_FL.Est],varNames,'Step widths Less Aff',{},'corrinfo',BAdispOpt);


[~, ~,swNStr] = BlandAltmanDual(swNFig,CumSWN_SW.Ref,CumSWN_SW.Est,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','b',...
    'baStatsMode',baStatsMode,'data1Mode',dat1Mode);
hold on;
[~, ~,swNturn] = BlandAltmanDual(swNFig,CumSWN_FL.Ref,CumSWN_FL.Est,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','r','symbols','o',...
    'baStatsMode','Gaussian','data1Mode',dat1Mode);

SigDiff.SWN_SW = ttest2(CumSWN_SW.Ref,CumSWN_SW.Est);
SigDiff.SWN_FL = ttest2(CumSWN_FL.Ref,CumSWN_FL.Est);



SWNstrLoA = [swNStr.differenceMean-swNStr.rpc swNStr.differenceMean+swNStr.rpc]*100; % cm
SWNturLoA = [swNturn.differenceMean-swNturn.rpc swNturn.differenceMean+swNturn.rpc]*100; % cm

strCorP = swNStr.corrP <= 0.05;
strCor = [num2str(round(swNStr.r,2)) char(strCorP* '*')];

turCorP = swNturn.corrP <= 0.05;
turCor = [num2str(round(swNturn.r,2)) char(turCorP* '*')];

strRms = num2str(round(swNStr.RMSE*100,2));
turRms = num2str(round(swNturn.RMSE*100,2));

curAx = swNFig.CurrentAxes;
LineList = curAx.Children;
for i = [3 4 5 9 10 11]
    set(get(get(LineList(i),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
end
legend(['Steady state, r:' strCor ', RMSE: ' strRms ' cm'],['Other,             r:' turCor ', RMSE: ' turRms ' cm']);

SWAs_MAD = mean(abs(CumSWA_SW.Ref - CumSWA_SW.Est));
SWAt_MAD = mean(abs(CumSWA_FL.Ref - CumSWA_FL.Est));
SWNs_MAD = mean(abs(CumSWN_SW.Ref - CumSWN_SW.Est));
SWNt_MAD = mean(abs(CumSWN_FL.Ref - CumSWN_FL.Est));

%% boxplot step width
figure('name','BP SW AffvLess Aff');
clear VarArray
curpl =subplot(1,2,1);
% val1 = [CumSWA_SW.Ref; CumSWA_FL.Ref]; val2 = [CumSWN_SW.Ref; CumSWN_FL.Ref]; ylabel('Step Widths (m)'); title('SW ForceShoe')
val1 = CumSWA_SW.Ref; val2 = CumSWN_SW.Ref;
VarArray = [val1; val2];
g = [zeros(length(val1), 1); ones(length(val2), 1); ];
boxplot(VarArray,g); ylabel('Step Widths Straight(m)'); title('SW Straight ForceShoe')
curpl.XTickLabel = {'Aff', 'Less Aff'};

diffTest = ttest2(val1,val2);

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
    
end

clear VarArray
curpl =subplot(1,2,2);
% val1 = [CumSWA_SW.Est; CumSWA_FL.Est]; val2 = [CumSWN_SW.Est; CumSWN_FL.Est]; ylabel('Step Widths (m)'); title('SW IMU')
val1 = CumSWA_SW.Est; val2 = CumSWN_SW.Est; ylabel('Step Widths straight (m)');
VarArray = [val1; val2];
g = [zeros(length(val1), 1); ones(length(val2), 1); ];
boxplot(VarArray,g); title('SW Straight IMU')
curpl.XTickLabel = {'Aff', 'Less Aff'};

diffTest = ttest2(val1,val2);

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
    
end

%% Overall Mean
connString = ' � ';
clear str1 OverAllMeanTable
headerNames = {'STA','STN','SLA','SLN',...   
	'APAMoS','APNMoS','MLAMoS','MLNMoS'};
OverAllMeanTable = array2table(zeros(length(headerNames),3));
OverAllMeanTable.Properties.RowNames = headerNames;
OverAllMeanTable.Properties.VariableNames = {'Reference','PGL','Sig'};
OverAllMeanTable.Reference = {
    [num2str(round(OverAllMean.STAff_Ref(1),2))  connString num2str(round(OverAllMean.STAff_Ref(2),2))];
    [num2str(round(OverAllMean.STNAff_Ref(1),2)) connString num2str(round(OverAllMean.STNAff_Ref(2),2))];
    [num2str(round(OverAllMean.SLAff_Ref(1),2))  connString num2str(round(OverAllMean.SLAff_Ref(2),2))];
    [num2str(round(OverAllMean.SLNAff_Ref(1),2)) connString num2str(round(OverAllMean.SLNAff_Ref(2),2))];
    [num2str(round(OverAllMean.APAff_Ref(1),2))  connString num2str(round(OverAllMean.APAff_Ref(2),2))];
    [num2str(round(OverAllMean.APNAff_Ref(1),2)) connString num2str(round(OverAllMean.APNAff_Ref(2),2))];
    [num2str(round(OverAllMean.MLAff_Ref(1),2))  connString num2str(round(OverAllMean.MLAff_Ref(2),2))];
    [num2str(round(OverAllMean.MLNAff_Ref(1),2)) connString num2str(round(OverAllMean.MLNAff_Ref(2),2))];};

OverAllMeanTable.PGL = {
    [num2str(round(OverAllMean.STAff_Est(1),2))  connString num2str(round(OverAllMean.STAff_Est(2),2))];
    [num2str(round(OverAllMean.STNAff_Est(1),2)) connString num2str(round(OverAllMean.STNAff_Est(2),2))];
    [num2str(round(OverAllMean.SLAff_Est(1),2))  connString num2str(round(OverAllMean.SLAff_Est(2),2))];
    [num2str(round(OverAllMean.SLNAff_Est(1),2)) connString num2str(round(OverAllMean.SLNAff_Est(2),2))];
    [num2str(round(OverAllMean.APAff_Est(1),2))  connString num2str(round(OverAllMean.APAff_Est(2),2))];
    [num2str(round(OverAllMean.APNAff_Est(1),2)) connString num2str(round(OverAllMean.APNAff_Est(2),2))];
    [num2str(round(OverAllMean.MLAff_Est(1),2))  connString num2str(round(OverAllMean.MLAff_Est(2),2))];
    [num2str(round(OverAllMean.MLNAff_Est(1),2)) connString num2str(round(OverAllMean.MLNAff_Est(2),2))];};

OverAllMeanTable.Sig = [SigDiff.StanceTimeAff; SigDiff.StanceTimeNAff;...
SigDiff.SLAff; SigDiff.SLNAff;...    
SigDiff.APMoSAff; SigDiff.APMoSNAff;...    
SigDiff.MLMoSAff; SigDiff.MLMoSNAff];