%% plotPub_APMoSSubwiseMarker
%%
dat1Mode = 'Compare'; % Compare or Truth
baStatsMode = 'Non-parametric'; % Gaussian or Non-parametric
baStatsMode = 'Gaussian'; % Gaussian or Non-parametric
varNames = {'ForceShoe','PGL', 'm'};
% BAdispOpt = {'eq';'r';'RMSE';'n'};
BAdispOptCor = {}; BAdispOptBA = {};

%%
% baStatsMode = 'Non-parametric'; % Gaussian or Non-parametric

imuVal = CumAPAMoS_SW.Est; refVal = CumAPAMoS_SW.Ref;
figure; plot(imuVal); hold on; plot(refVal);

% if apOutlierRemoval
%     allOutliers = fun_findIQROutliers(imuVal);
%     imuVal(allOutliers,:) = []; refVal(allOutliers,:) = []; warning('outliers removed for AP mos alone')
%     allOutliers = fun_findIQROutliers(refVal);
%     imuVal(allOutliers,:) = []; refVal(allOutliers,:) = [];
% end

apFig = figure('name','AP Aff MoS');
% BlandAltman(apFig,refVal,imuVal,varNames,'AP MoS',{},'corrinfo',BAdispOpt);

[~, ~,APAstrt,baPlotsA] = BlandAltmanDual(apFig,refVal,imuVal,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','b','MarkerSize',1,...
    'baStatsMode',baStatsMode,'data1Mode',dat1Mode);
hold on;
[~, ~,APAturn] = BlandAltmanDual(apFig,CumAPAMoS_FL.Ref,CumAPAMoS_FL.Est,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','r','MarkerSize',1,...
    'baStatsMode',baStatsMode,'data1Mode',dat1Mode);

SigDiff.APMoSAff_SW = ttest2(refVal,imuVal);
SigDiff.APMoSAff_FL = ttest2(CumAPAMoS_FL.Ref,CumAPAMoS_FL.Est);

SigDiff.SLAff = ttest2([CumSLA_SW.Ref; CumSLA_FL.Ref],[CumSLA_SW.Est; CumSLA_FL.Est]);
SigDiff.SLNAff = ttest2([CumSLN_SW.Ref;CumSLN_FL.Ref],[CumSLN_SW.Est;CumSLN_FL.Est]);
SigDiff.MLMoSAff = ttest2([CumMLAMoS_SW.Ref; CumMLAMoS_FL.Ref],[CumMLAMoS_SW.Est; CumMLAMoS_FL.Est]);
SigDiff.MLMoSNAff = ttest2([CumMLNMoS_SW.Ref;CumMLNMoS_FL.Ref],[CumMLNMoS_SW.Est;CumMLNMoS_FL.Est]);
SigDiff.APMoSAff = ttest2([refVal;CumAPAMoS_FL.Ref],[imuVal;CumAPAMoS_FL.Est]);

OverAllMean.APAff_Ref = [mean([refVal;CumAPAMoS_FL.Ref]) std([refVal;CumAPAMoS_FL.Ref])];
OverAllMean.APAff_Est = [mean([imuVal;CumAPAMoS_FL.Est]) std([imuVal;CumAPAMoS_FL.Est])];

APAs_MAD = mean(abs(CumAPAMoS_SW.Ref - CumAPAMoS_SW.Est));
APAt_MAD = mean(abs(CumAPAMoS_FL.Ref - CumAPAMoS_FL.Est));

APAstrLoA = [APAstrt.differenceMean-APAstrt.rpc APAstrt.differenceMean+APAstrt.rpc]*100; % cm
APAturLoA = [APAturn.differenceMean-APAturn.rpc APAturn.differenceMean+APAturn.rpc]*100; % cm

strCorP = APAstrt.corrP <= 0.05;
strCor = [num2str(round(APAstrt.r,2)) char(strCorP* '*')];

turCorP = APAturn.corrP <= 0.05;
turCor = [num2str(round(APAturn.r,2)) char(turCorP* '*')];

strRms = num2str(round(APAstrt.RMSE*100,2));
turRms = num2str(round(APAturn.RMSE*100,2));

for usCt = 1:length(usList)
    set(apFig,'CurrentAxes',baPlotsA{1})
    plot(CumSubAPAMoS_SW.Ref{usCt},CumSubAPAMoS_SW.Est{usCt},markerCell{usCt},'Color','b','LineWidth',2,'LineWidth',2,'MarkerSize',10);
    plot(CumSubAPAMoS_FL.Ref{usCt},CumSubAPAMoS_FL.Est{usCt},markerCell{usCt},'Color','r','LineWidth',2,'LineWidth',2,'MarkerSize',10)
    xlim([-0.2 0.4]); ylim([-0.2 0.4])
    
    set(apFig,'CurrentAxes',baPlotsA{2})
    plot((CumSubAPAMoS_SW.Ref{usCt}+CumSubAPAMoS_SW.Est{usCt})/2,CumSubAPAMoS_SW.Est{usCt}-CumSubAPAMoS_SW.Ref{usCt},markerCell{usCt},'Color','b','LineWidth',2,'LineWidth',2,'MarkerSize',10);
    plot((CumSubAPAMoS_FL.Ref{usCt}+CumSubAPAMoS_FL.Est{usCt})/2,CumSubAPAMoS_FL.Est{usCt}-CumSubAPAMoS_FL.Ref{usCt},markerCell{usCt},'Color','r','LineWidth',2,'LineWidth',2,'MarkerSize',10);
    
    
    xlim2 = 0.4;
    
    xlim([-0.1 xlim2]); ylim([-0.3 0.3])
    fontsize = 20;
    if APAstrt.differenceMeanP < 0.05
        text(xlim2,APAstrt.differenceMean+0.02,[mynum2str(APAstrt.differenceMean,2) '*'],'HorizontalAlignment','left','VerticalAlignment','middle','fontsize',fontsize,'FontWeight','bold');
    else
        text(xlim2,APAstrt.differenceMean+0.02,[mynum2str(APAstrt.differenceMean,2) ],'HorizontalAlignment','left','VerticalAlignment','middle','fontsize',fontsize,'FontWeight','bold');
    end
    if APAturn.differenceMeanP < 0.05
        text(xlim2,APAturn.differenceMean-0.02,[mynum2str(APAturn.differenceMean,2) '*'],'HorizontalAlignment','left','VerticalAlignment','middle','fontsize',fontsize,'FontWeight','bold');
    else
        text(xlim2,APAturn.differenceMean-0.02,[mynum2str(APAturn.differenceMean,2) ],'HorizontalAlignment','left','VerticalAlignment','middle','fontsize',fontsize,'FontWeight','bold');
    end
    
    
end

dim = [0.14 0.58 0.3 0.3];
str = ['Steady state, r:' strCor ', RMSE: ' strRms ' cm'  newline 'Other, r:' turCor ', RMSE: ' turRms ' cm'];
anH = annotation('textbox',dim,'String',str,'FitBoxToText','on');
anH.FontSize = 20; anH.FontWeight = 'bold'; anH.BackgroundColor = 'white';

f=get(gca,'Children');
legend([f(27),f(21)],'Steady state','Other')
%%

imuVal = CumAPNMoS_SW.Est; refVal = CumAPNMoS_SW.Ref;
figure; plot(imuVal); hold on; plot(refVal);

% if apOutlierRemoval
%     allOutliers = fun_findIQROutliers(imuVal);
%     imuVal(allOutliers,:) = []; refVal(allOutliers,:) = []; warning('outliers removed for AP mos alone')
%     allOutliers = fun_findIQROutliers(refVal);
%     imuVal(allOutliers,:) = []; refVal(allOutliers,:) = [];
% end

apFig = figure('name','AP Less Aff MoS');
% BlandAltman(apFig,refVal,imuVal,varNames,'AP MoS',{},'corrinfo',BAdispOpt);


[~, ~,APNstrt,baPlotsN] = BlandAltmanDual(apFig,refVal,imuVal,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','b','MarkerSize',1,...
    'baStatsMode',baStatsMode,'data1Mode',dat1Mode);

hold on;
[~, ~,APNturn] = BlandAltmanDual(apFig,CumAPNMoS_FL.Ref,CumAPNMoS_FL.Est,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','r','MarkerSize',1,...
    'baStatsMode',baStatsMode,'data1Mode',dat1Mode);

SigDiff.APMoSNAff = ttest2([refVal;CumAPNMoS_FL.Ref],[imuVal;CumAPNMoS_FL.Est]);
SigDiff.APMoSNAff_SW = ttest2(refVal,imuVal);
SigDiff.APMoSNAff_FL = ttest2(CumAPNMoS_FL.Ref,CumAPNMoS_FL.Est);

OverAllMean.APNAff_Ref = [mean([refVal;CumAPNMoS_FL.Ref]) std([refVal;CumAPNMoS_FL.Ref])];
OverAllMean.APNAff_Est = [mean([imuVal;CumAPNMoS_FL.Est]) std([imuVal;CumAPNMoS_FL.Est])];

APNs_MAD = mean(abs(CumAPNMoS_SW.Ref - CumAPNMoS_SW.Est));
APNt_MAD = mean(abs(CumAPNMoS_FL.Ref - CumAPNMoS_FL.Est));

APNstrLoA = [APNstrt.differenceMean-APNstrt.rpc APNstrt.differenceMean+APNstrt.rpc]*100; % cm
APNturLoA = [APNturn.differenceMean-APNturn.rpc APNturn.differenceMean+APNturn.rpc]*100; % cm

strCorP = APNstrt.corrP <= 0.05;
strCor = [num2str(round(APNstrt.r,2)) char(strCorP* '*')];

turCorP = APNturn.corrP <= 0.05;
turCor = [num2str(round(APNturn.r,2)) char(turCorP* '*')];

strRms = num2str(round(APNstrt.RMSE*100,2));
turRms = num2str(round(APNturn.RMSE*100,2));

for usCt = 1:length(usList)
    set(apFig,'CurrentAxes',baPlotsN{1})
    plot(CumSubAPNMoS_SW.Ref{usCt},CumSubAPNMoS_SW.Est{usCt},markerCell{usCt},'Color','b','LineWidth',2,'LineWidth',2,'MarkerSize',10);
    plot(CumSubAPNMoS_FL.Ref{usCt},CumSubAPNMoS_FL.Est{usCt},markerCell{usCt},'Color','r','LineWidth',2,'LineWidth',2,'MarkerSize',10)
    xlim([-0.2 0.4]); ylim([-0.2 0.4])
    
    set(apFig,'CurrentAxes',baPlotsN{2})
    plot((CumSubAPNMoS_SW.Ref{usCt}+CumSubAPNMoS_SW.Est{usCt})/2,CumSubAPNMoS_SW.Est{usCt}-CumSubAPNMoS_SW.Ref{usCt},markerCell{usCt},'Color','b','LineWidth',2,'LineWidth',2,'MarkerSize',10);
    plot((CumSubAPNMoS_FL.Ref{usCt}+CumSubAPNMoS_FL.Est{usCt})/2,CumSubAPNMoS_FL.Est{usCt}-CumSubAPNMoS_FL.Ref{usCt},markerCell{usCt},'Color','r','LineWidth',2,'LineWidth',2,'MarkerSize',10);
    xlim2 = 0.4;
    
    xlim([-0.1 xlim2]); ylim([-0.3 0.3])
    fontsize = 20;
    if APNstrt.differenceMeanP < 0.05
        text(xlim2,APNstrt.differenceMean+0.03,[mynum2str(APNstrt.differenceMean,2) '*'],'HorizontalAlignment','left','VerticalAlignment','middle','fontsize',fontsize,'FontWeight','bold');
    else
        text(xlim2,APNstrt.differenceMean+0.03,[mynum2str(APNstrt.differenceMean,2) ],'HorizontalAlignment','left','VerticalAlignment','middle','fontsize',fontsize,'FontWeight','bold');
    end
    if APNturn.differenceMeanP < 0.05
        text(xlim2,APNturn.differenceMean-0.03,[mynum2str(APNturn.differenceMean,2) '*'],'HorizontalAlignment','left','VerticalAlignment','middle','fontsize',fontsize,'FontWeight','bold');
    else
        text(xlim2,APNturn.differenceMean-0.03,[mynum2str(APNturn.differenceMean,2) ],'HorizontalAlignment','left','VerticalAlignment','middle','fontsize',fontsize,'FontWeight','bold');
    end
    
end

dim = [0.14 0.58 0.3 0.3];
str = ['Steady state, r:' strCor ', RMSE: ' strRms ' cm'  newline 'Other, r:' turCor ', RMSE: ' turRms ' cm'];
anH = annotation('textbox',dim,'String',str,'FitBoxToText','on');
anH.FontSize = 20; anH.FontWeight = 'bold'; anH.BackgroundColor = 'white';

f=get(gca,'Children');
legend([f(27),f(21)],'Steady state','Other')