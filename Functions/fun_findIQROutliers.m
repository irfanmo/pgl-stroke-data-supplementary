function allOutliers = fun_findIQROutliers(dataSetRef)

diffValues = dataSetRef;
% diffValues = dataSetRef - dataSetEst;
% diffValues = dataSetEst;
iqrtest = quantile(diffValues,[.025 .25 .50 .75 .975]);
iqrrange = iqr(diffValues);
whisker1 = iqrtest(2) - (1.5*iqrrange);
whisker2 = iqrtest(4) + (1.5*iqrrange);
lowerOutliers = find(diffValues<whisker1);
highrOutliers = find(diffValues>whisker2);
allOutliers = [lowerOutliers; highrOutliers];


end