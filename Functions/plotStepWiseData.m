%%
maxLength = length(estrfSeg);

%% first 15 steps

avgTestRMS = [];
mkrType = 'o';
startStep = 1 ;


figure('name','Step Wise Comparison - Snippet');
for i = startStep:maxLength
    sCt = i - startStep + 1;
    subplot(6,5,sCt);
    clear tVarR tVarL tVarP  tVarRefR tVarRefL tVarRefP
    for axC = 1:3
        tVarR(:,axC) = estrfSeg{i}(:,axC);
        tVarL(:,axC) = estlfSeg{i}(:,axC);
        tVarP(:,axC) = estpsSeg{i}(:,axC);
        
        tVarRefR(:,axC) = refrfSeg{i}(:,axC);
        tVarRefL(:,axC) = reflfSeg{i}(:,axC) ;
        tVarRefP(:,axC) = refpsSeg{i}(:,axC);
    end
    
    
    
    plot(tVarRefR(:,1),tVarRefR(:,2),'LineWidth',4,'Color',[1 0.5 0.5]); hold on;%, [],[1 0.5 0.5],mkrType,'filled'); hold on
    scatter(tVarR(:,1),tVarR(:,2), [],[134 19 19]/255); hold on
    
    plot(tVarRefL(:,1),tVarRefL(:,2),'LineWidth',4,'Color',[0.5 0.5 1]); hold on;%, [],[1 0.5 0.5],mkrType,'filled'); hold on
    scatter(tVarL(:,1),tVarL(:,2), [],[0 0 1]); hold on
    
    plot(tVarRefP(:,1),tVarRefP(:,2),'LineWidth',4,'Color',[0.5 1 0.5]); hold on;%, [],[1 0.5 0.5],mkrType,'filled'); hold on
    scatter(tVarP(:,1),tVarP(:,2), [],[11 108 46]/255); hold on
    
    curAx = gca;
    curAx.FontSize = 12;
    curAx.FontWeight = 'bold';
    ylimV = curAx.YLim;
    xlimV = curAx.XLim;
    [maxValBarSign, mvbInd] = max(abs(ylimV));
    
    yht = maxValBarSign*0.7*(abs(ylimV(mvbInd))/ylimV(mvbInd));
    xht = 1.15;

    xlim([-0.1 1.4]);
    if i == startStep
        %            lgH =   legend('Right PGL','Right Ref.','Left PGL','Left Ref','CoM PGL','CoM Ref');
        legend({'Right Ref','Right PGL','Left Ref','Left PGL','CoM Ref','CoM PGL'},'Orientation','horizontal');
    end
    text(xht,yht,['Step ' num2str(i)],'FontWeight','bold');
    
    
    
end

