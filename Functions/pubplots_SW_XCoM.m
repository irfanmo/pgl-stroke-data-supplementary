% pubplots_SW_XCoM
%%
% Estimate w0 as in Hof et al. 2005
% w0 = sqrt( g / leg length)

% Calculate leg length
legLengthBodyLengthRatioMale   = 0.260 + 0.2484; % Bron: drillis et al - body segment parameters
legLengthBodyLengthRatioFemale = 0.252 + 0.227;  % Bron: drillis et al - body segment parameters

switch subjectSpecs.Gender
    case 'F'
        legLengthBodyLengthRatio = legLengthBodyLengthRatioFemale;
    case 'M'
        legLengthBodyLengthRatio = legLengthBodyLengthRatioMale;
    otherwise
        error('Subject gender unknown')
end

subjectLegLength = subjectSpecs.Height * legLengthBodyLengthRatio;
legLengthFactor  = 1; % see Hof et al.
l0=subjectLegLength;
% Calculate w0
w0 = sqrt(9.81/(legLengthFactor * subjectLegLength));
% w1 = sqrt(9.81*(legLengthFactor * subjectLegLength));

%%
selSamp = 0; endSamp = 1;
selSamp = 3; endSamp = 0;

sumContact = sum(imuContact,2);
dsInstance = sumContact == 2;

%% first 15 steps

noOfPlots = 14;
if subjectSpecs.userSelect == 11
    noOfPlots = 5;
end



mkrType = 'o';
startStep = 1;
% if strcmpi(usName,'us3') && strcmpi(trName,'tr34') && iCt == 1
figure('name','Step Wise Comparison - Snippet');
for i = startStep:(startStep+noOfPlots)
    sCt = i - startStep + 1;
    subplot(5,3,sCt);
    clear tVarR tVarL tVarP tVarV tVarX ...
        tVarRefR tVarRefL tVarRefP tVarRefV tVarRefX
    for axC = 1:3
        tVarR(:,axC) = segmentedIMU2.RightFoot.pos.sets{i}(:,axC);
        tVarL(:,axC) = segmentedIMU2.LeftFoot.pos.sets{i}(:,axC);
        tVarP(:,axC) = segmentedIMU2.CoM.pos.sets{i}(:,axC);
        tVarV(:,axC) = segmentedIMU2.CoM.vel.sets{i}(:,axC);
        tVarX(:,axC) = tVarP(:,axC) + (tVarV(:,axC)/w0);
        
        tVarRefR(:,axC) = segmentedRef2.RightFoot.pos.sets{i}(:,axC);
        tVarRefL(:,axC) = segmentedRef2.LeftFoot.pos.sets{i}(:,axC) ;
        tVarRefP(:,axC) = segmentedRef2.CoM.pos.sets{i}(:,axC);
        tVarRefV(:,axC) = segmentedRef2.CoM.vel.sets{i}(:,axC);
        tVarRefX(:,axC) = tVarRefP(:,axC) + (tVarRefV(:,axC)/w0);
    end
    
    plot(tVarRefR(:,1),tVarRefR(:,2),'LineWidth',4,'Color',[1 0.5 0.5]); hold on;%, [],[1 0.5 0.5],mkrType,'filled'); hold on
    scatter(tVarR(:,1),tVarR(:,2), [],[134 19 19]/255); hold on
    
    plot(tVarRefL(:,1),tVarRefL(:,2),'LineWidth',4,'Color',[0.5 0.5 1]); hold on;%, [],[1 0.5 0.5],mkrType,'filled'); hold on
    scatter(tVarL(:,1),tVarL(:,2), [],[0 0 1]); hold on
    
    plot(tVarRefP(:,1),tVarRefP(:,2),'LineWidth',4,'Color',[0.5 1 0.5]); hold on;%, [],[1 0.5 0.5],mkrType,'filled'); hold on
    scatter(tVarP(:,1),tVarP(:,2), [],[11 108 46]/255); hold on
    
    if endSamp
        selSamp = length(tVarP);
    end
    
    crstepUsed = segmentedIMU2.stepsUsed(i,:);
    
    p1 = tVarP(selSamp,:); p2 = tVarX(selSamp,:);
    dp = p2-p1;                         % Difference
    arrows(p1(1),p1(2),dp(1),dp(2),'Cartesian','LineWidth',2,'EdgeColor','red');    %    quiver(p1(1),p1(2),dp(1),dp(2),0,'LineWidth',3,'LineStyle',':');
    
    
    p1 = tVarRefP(selSamp,:); p2 = tVarRefX(selSamp,:);
    dp = p2-p1;                         % Difference
    arrows(p1(1),p1(2),dp(1),dp(2),'Cartesian','LineWidth',2,'EdgeColor','blue');
    
    
    
    
    curAx = gca;
    curAx.FontSize = 12;
    curAx.FontWeight = 'bold';
    ylimV = curAx.YLim;
    xlimV = curAx.XLim;
    [maxValBarSign, mvbInd] = max(abs(ylimV));
    
    yht = maxValBarSign*0.7*(abs(ylimV(mvbInd))/ylimV(mvbInd));
    xht = 1.15;
    
    
    
    xlim([-0.1 1.4]);
    if i == startStep
        %            lgH =   legend('Right PGL','Right Ref.','Left PGL','Left Ref','CoM PGL','CoM Ref');
        legend({'Right Ref','Right PGL','Left Ref','Left PGL','CoM Ref','CoM PGL'},'Orientation','horizontal');
    end
    text(xht,yht,['Step ' num2str(i)],'FontWeight','bold');
    
    
    
end

%% comparing the mag and direction of CoM-XCoM lines
mkrType = 'o';
% startStep = 1;
clear magMoS_IMU magMoS_REF

for i = 1:length(segmentedIMU2.stepsUsed)
    clear tVarR tVarL tVarP tVarV tVarX ...
        tVarRefR tVarRefL tVarRefP tVarRefV tVarRefX ...
        
    
    for axC = 1:3
        tVarR(:,axC) = segmentedIMU2.RightFoot.pos.sets{i}(:,axC);
        tVarL(:,axC) = segmentedIMU2.LeftFoot.pos.sets{i}(:,axC);
        tVarP(:,axC) = segmentedIMU2.CoM.pos.sets{i}(:,axC);
        tVarV(:,axC) = segmentedIMU2.CoM.vel.sets{i}(:,axC);
        tVarX(:,axC) = tVarP(:,axC) + (tVarV(:,axC)/w0);
        
        tVarRefR(:,axC) = segmentedRef2.RightFoot.pos.sets{i}(:,axC);
        tVarRefL(:,axC) = segmentedRef2.LeftFoot.pos.sets{i}(:,axC) ;
        tVarRefP(:,axC) = segmentedRef2.CoM.pos.sets{i}(:,axC);
        tVarRefV(:,axC) = segmentedRef2.CoM.vel.sets{i}(:,axC);
        tVarRefX(:,axC) = tVarRefP(:,axC) + (tVarRefV(:,axC)/w0);
    end
    if endSamp
        selSamp = length(tVarP);
    end
    
    
    segmentedIMU2.XCoM.pos{i} = tVarX;
    segmentedRef2.XCoM.pos{i} = tVarRefX;
    
    segmentedIMU2.MoS.mag{i} = normh(tVarX(:,1:2) - tVarP(:,1:2));
    segmentedRef2.MoS.mag{i} = normh(tVarRefX(:,1:2) - tVarRefP(:,1:2));
    
    magMoS_IMU(i,:) = normh(tVarX(selSamp,1:2) - tVarP(selSamp,1:2));
    magMoS_REF(i,:) = normh(tVarRefX(selSamp,1:2) - tVarRefP(selSamp,1:2));
    
    headingChange = tVarX(selSamp,1:2) - tVarP(selSamp,1:2); headingChange(3)=0; theading=headingChange/norm(headingChange); % normalize
    normV = [1 0 0]; currV = theading; CosTheta = dot(normV,currV)/(norm(normV)*norm(currV)); ThetaInDegreesIMU = acosd(CosTheta);
    
    headingChange = tVarRefX(selSamp,1:2) - tVarRefP(selSamp,1:2); headingChange(3)=0; theading=headingChange/norm(headingChange); % normalize
    normV = [1 0 0]; currV = theading; CosTheta = dot(normV,currV)/(norm(normV)*norm(currV)); ThetaInDegreesRef = acosd(CosTheta);
    
    
    segmentedIMU2.MoS.dir_fStep(i,:) = ThetaInDegreesIMU;
    segmentedRef2.MoS.dir_fStep(i,:) = ThetaInDegreesRef;
    
    degMoS_IMU(i,:) = ThetaInDegreesIMU;
    degMoS_REF(i,:) = ThetaInDegreesRef;
end
%
% varNames = {'ForceShoe','IMU', 'm'};
% BAdispOpt = {'eq';'r';'RMSE';'n'};
%
% magCoMXCoMFig = figure;
% BlandAltman(magCoMXCoMFig,magMoS_REF(1:end,:),magMoS_IMU(1:end,:),varNames,'Magnitude MoS',{},'corrinfo',BAdispOpt);
%
% varNames = {'ForceShoe','IMU', 'theta'};
% degCoMXCoMFig = figure;
% BlandAltman(degCoMXCoMFig,degMoS_REF(1:end,:), degMoS_IMU(1:end,:),varNames,'Angle MoS',{},'corrinfo',BAdispOpt);

%%
switch userSelect
    case 1
        outliers = [3 13];
    case 3
        outliers = [3 5:9 11];
    case 4
        outliers = [3 5 6 10];
    case 11
        outliers = [5 6 10];
    case 12
        outliers = [4 8];
        case 15
        outliers = [2 4 7 11];
        
    case 2
        outliers = [2 12 14 18 20 24];
    case 6
        outliers = [2 8 10 14 18];
end

for i = 1:length(segmentedIMU2.stepsUsed)
    
    
    clear tVarR tVarL tVarP tVarV tVarX ...
        tVarRefR tVarRefL tVarRefP tVarRefV tVarRefX ...
        
    clear p_toe_r p_heel_r p_amtoe_r p_gtoe_r p_altoe_r p_pltoe_r p_pmtoe_r p_amheel_r p_alheel_r p_plheel_r p_pmheel_r   ...
        p_toe_l p_heel_l p_amtoe_l p_gtoe_l p_altoe_l p_pltoe_l p_pmtoe_l p_amheel_l p_alheel_l p_plheel_l p_pmheel_l   ...
        p_frf  p_frh    p_flf p_flh apMOS mlMOS_rt mlMOS_rh mlMOS_lt mlMOS_lh intersectPoint mosval frontBoS_Line CoMXCoM_Line
    
    
    tVarR = segmentedIMU2.RightFoot.pos.sets{i};
    tVarL = segmentedIMU2.LeftFoot.pos.sets{i};
    tVarP = segmentedIMU2.CoM.pos.sets{i};
    tVarV = segmentedIMU2.CoM.vel.sets{i};
    tVarX = segmentedIMU2.XCoM.pos{i};
    tVarRM_R = segmentedIMU2.RightFoot.RotM.sets{i};
    tVarRM_L = segmentedIMU2.LeftFoot.RotM.sets{i};
    
    if endSamp
        selSamp = length(tVarR);
    end
    
    % Get all shoe positions
    [p_toe_r, p_heel_r, p_amtoe_r, p_gtoe_r, p_altoe_r, p_pltoe_r, p_pmtoe_r, p_amheel_r, p_alheel_r, p_plheel_r, p_pmheel_r,   ...
        p_toe_l, p_heel_l, p_amtoe_l, p_gtoe_l, p_altoe_l, p_pltoe_l, p_pmtoe_l, p_amheel_l, p_alheel_l, p_plheel_l, p_pmheel_l,   ...
        p_frf  , p_frh,    p_flf,     p_flh] = ...
        p_feet( tVarR', tVarL', tVarRM_R, tVarRM_L, shoe_size);
    
    BoSposX = [p_heel_l(selSamp,1)   p_amheel_l(selSamp,1);   % #13 #20
        p_pmtoe_l(selSamp,1)  p_gtoe_l(selSamp,1);     % #17 #15
        p_gtoe_r(selSamp,1)   p_pmtoe_r(selSamp,1);    % #4  #6
        p_amheel_r(selSamp,1) p_heel_r(selSamp,1)  ];  % #9  #2
    
    BoSposY = [p_heel_l(selSamp,2)   p_amheel_l(selSamp,2);   % #13 #20
        p_pmtoe_l(selSamp,2)  p_gtoe_l(selSamp,2);     % #17 #15
        p_gtoe_r(selSamp,2)   p_pmtoe_r(selSamp,2);    % #4  #6
        p_amheel_r(selSamp,2) p_heel_r(selSamp,2)  ];  % #9  #2
    
    backLineX  = [BoSposX(1,1) BoSposX(4,2)]; %LH %RH
    backLineY  = [BoSposY(1,1) BoSposY(4,2)];
    frontLineX = [BoSposX(2,2) BoSposX(3,1)]; %LF %RF
    frontLineY = [BoSposY(2,2) BoSposY(3,1)];
    
    mosval = distancePointToLineSegment(tVarX(selSamp,1:2),[frontLineX(1) frontLineY(1)], [frontLineX(2) frontLineY(2)]);
    
    frontBoS_Line = [frontLineX; frontLineY];
    CoMXCoM_Line  = [tVarP(selSamp,1) tVarX(selSamp,1);
        tVarP(selSamp,2) tVarX(selSamp,2) ];
    
    
    intersectPoint = findIntersectionPoint(frontBoS_Line, CoMXCoM_Line );
    
    
    apMOS = normh(intersectPoint - tVarX(selSamp,1:2));
    mlMOS_rt = distancePointToLineSegment(p_altoe_r(selSamp,1:2),tVarP(selSamp,1:2), tVarX(selSamp,1:2));
    mlMOS_rh = distancePointToLineSegment(p_plheel_r(selSamp,1:2),tVarP(selSamp,1:2), tVarX(selSamp,1:2));
    
    mlMOS_lt = distancePointToLineSegment(p_altoe_l(selSamp,1:2),tVarP(selSamp,1:2), tVarX(selSamp,1:2));
    mlMOS_lh = distancePointToLineSegment(p_plheel_l(selSamp,1:2),tVarP(selSamp,1:2), tVarX(selSamp,1:2));
    
    segmentedIMU2.MoS.apMos(i,:) =  apMOS;
    segmentedIMU2.MoS.mlMOS_r(i,:) =  max([mlMOS_rt mlMOS_rh]);
    segmentedIMU2.MoS.mlMOS_l(i,:) =  max([mlMOS_lt mlMOS_lh]);
    segmentedIMU2.MoS.mosEucD(i,:) =  mosval;
    
    clear p_toe_r p_heel_r p_amtoe_r p_gtoe_r p_altoe_r p_pltoe_r p_pmtoe_r p_amheel_r p_alheel_r p_plheel_r p_pmheel_r   ...
        p_toe_l p_heel_l p_amtoe_l p_gtoe_l p_altoe_l p_pltoe_l p_pmtoe_l p_amheel_l p_alheel_l p_plheel_l p_pmheel_l   ...
        p_frf  p_frh    p_flf p_flh apMOS mlMOS_rt mlMOS_rh mlMOS_lt mlMOS_lh intersectPoint mosval frontBoS_Line CoMXCoM_Line
    
    % ref values
    
    tVarRefR = segmentedRef2.RightFoot.pos.sets{i};
    tVarRefL = segmentedRef2.LeftFoot.pos.sets{i};
    tVarRefP = segmentedRef2.CoM.pos.sets{i};
    tVarRefV = segmentedRef2.CoM.vel.sets{i};
    tVarRefX = segmentedRef2.XCoM.pos{i};
    tVarRefRM_R = segmentedRef2.RightFoot.RotM.sets{i};
    tVarRefRM_L = segmentedRef2.LeftFoot.RotM.sets{i};
    
    
    [p_toe_r, p_heel_r, p_amtoe_r, p_gtoe_r, p_altoe_r, p_pltoe_r, p_pmtoe_r, p_amheel_r, p_alheel_r, p_plheel_r, p_pmheel_r,   ...
        p_toe_l, p_heel_l, p_amtoe_l, p_gtoe_l, p_altoe_l, p_pltoe_l, p_pmtoe_l, p_amheel_l, p_alheel_l, p_plheel_l, p_pmheel_l,   ...
        p_frf  , p_frh,    p_flf,     p_flh] = ...
        p_feet( tVarRefR', tVarRefL', tVarRefRM_R, tVarRefRM_L, shoe_size);
    
    BoSposX = [p_heel_l(selSamp,1)   p_amheel_l(selSamp,1);   % #13 #20
        p_pmtoe_l(selSamp,1)  p_gtoe_l(selSamp,1);     % #17 #15
        p_gtoe_r(selSamp,1)   p_pmtoe_r(selSamp,1);    % #4  #6
        p_amheel_r(selSamp,1) p_heel_r(selSamp,1)  ];  % #9  #2
    
    BoSposY = [p_heel_l(selSamp,2)   p_amheel_l(selSamp,2);   % #13 #20
        p_pmtoe_l(selSamp,2)  p_gtoe_l(selSamp,2);     % #17 #15
        p_gtoe_r(selSamp,2)   p_pmtoe_r(selSamp,2);    % #4  #6
        p_amheel_r(selSamp,2) p_heel_r(selSamp,2)  ];  % #9  #2
    
    backLineX  = [BoSposX(1,1) BoSposX(4,2)]; %LH %RH
    backLineY  = [BoSposY(1,1) BoSposY(4,2)];
    frontLineX = [BoSposX(2,2) BoSposX(3,1)]; %LF %RF
    frontLineY = [BoSposY(2,2) BoSposY(3,1)];
    
    mosval = distancePointToLineSegment(tVarX(selSamp,1:2),[frontLineX(1) frontLineY(1)], [frontLineX(2) frontLineY(2)]);
    
    frontBoS_Line = [frontLineX; frontLineY];
    CoMXCoM_Line  = [tVarP(selSamp,1) tVarX(selSamp,1);
        tVarP(selSamp,2) tVarX(selSamp,2) ];
    
    
    intersectPoint = findIntersectionPoint(frontBoS_Line, CoMXCoM_Line );
    
    
    apMOS = normh(intersectPoint - tVarX(selSamp,1:2));
    mlMOS_rt = distancePointToLineSegment(p_altoe_r(selSamp,1:2),tVarP(selSamp,1:2), tVarX(selSamp,1:2));
    mlMOS_rh = distancePointToLineSegment(p_plheel_r(selSamp,1:2),tVarP(selSamp,1:2), tVarX(selSamp,1:2));
    
    mlMOS_lt = distancePointToLineSegment(p_altoe_l(selSamp,1:2),tVarP(selSamp,1:2), tVarX(selSamp,1:2));
    mlMOS_lh = distancePointToLineSegment(p_plheel_l(selSamp,1:2),tVarP(selSamp,1:2), tVarX(selSamp,1:2));
    
    segmentedRef2.MoS.apMos(i,:) =  apMOS;
    segmentedRef2.MoS.mlMOS_r(i,:) =  max([mlMOS_rt mlMOS_rh]);
    segmentedRef2.MoS.mlMOS_l(i,:) =  max([mlMOS_lt mlMOS_lh]);
    segmentedRef2.MoS.mosEucD(i,:) =  mosval;
    
    
end


varNames = {'ForceShoe','IMU', 'm/s'};
BAdispOpt = {'eq';'r';'RMSE';'n'};

figure('name','Boxplots ML MoS');
clear AllstepLengths
curpl =subplot(1,2,1);
AllstepLengths = [segmentedRef2.MoS.mlMOS_r(:,1); segmentedRef2.MoS.mlMOS_l(:,1)];
g = [zeros(length(segmentedRef2.MoS.mlMOS_r(:,1)), 1); ones(length(segmentedRef2.MoS.mlMOS_l(:,1)), 1); ];
boxplot(AllstepLengths,g);
curpl.XTickLabel = {'Right', 'Left'};
ylabel('ML MoS (m)')
title('ML MoS  ForceShoe')


diffTest = ttest2(segmentedRef2.MoS.mlMOS_r,segmentedRef2.MoS.mlMOS_l);

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
    
end

clear AllstepLengths
curpl =subplot(1,2,2);
AllstepLengths = [segmentedIMU2.MoS.mlMOS_r(:,1); segmentedIMU2.MoS.mlMOS_l(:,1)];
g = [zeros(length(segmentedIMU2.MoS.mlMOS_l(:,1)), 1); ones(length(segmentedIMU2.MoS.mlMOS_l(:,1)), 1); ];
boxplot(AllstepLengths,g);
curpl.XTickLabel = {'Right', 'Left'};
ylabel('ML MoS (m)')
title('ML MoS  IMU')


diffTest = ttest2(segmentedIMU2.MoS.mlMOS_r,segmentedIMU2.MoS.mlMOS_l);

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
    
end


varNames = {'ForceShoe','IMU', 'm'};
BAdispOpt = {'eq';'r';'RMSE';'n'};

MoSRFig = figure;
BlandAltman(MoSRFig,segmentedRef2.MoS.mlMOS_r(1:end,:), segmentedIMU2.MoS.mlMOS_r(1:end,:),varNames,'MoS R',{},'corrinfo',BAdispOpt);

MoSLFig = figure;
BlandAltman(MoSLFig,segmentedRef2.MoS.mlMOS_l(1:end,:), segmentedIMU2.MoS.mlMOS_l(1:end,:),varNames,'MoS L',{},'corrinfo',BAdispOpt);


refVal = segmentedRef2.MoS.apMos; imuVal = segmentedIMU2.MoS.apMos;
figure; plot(refVal); hold on; plot(imuVal);
% refVal(outliers,:) = []; imuVal(outliers,:) = [];
APMoSFig = figure;
BlandAltman(APMoSFig,refVal,imuVal,varNames,'AP MoS',{},'corrinfo',BAdispOpt);



%% Temporal parameters calculated from foot movement
imuStepsCell = {imustepsR; imustepsL};

for sideCt = 1:length(imuStepsCell)
    stepsSide = imuStepsCell{sideCt};
    step1 = stepsSide(1,2);
    stepL = stepsSide(end,1);
    noOfSteps = length(stepsSide) - 1;
    timeTaken = (stepL - step1)/sampFq;
    strideTimeIMU(:,sideCt) = timeTaken/noOfSteps;
end

refContactOG = refVICONsync.cgframe.Forces.Contact(1:endMeas,:);
[refstepsROG, refstepsLOG] = fun_findSteps(refContactOG);

refStepsCell = {refstepsROG; refstepsLOG};

for sideCt = 1:length(refStepsCell)
    stepsSide = refStepsCell{sideCt};
    step1 = stepsSide(1,2);
    stepL = stepsSide(end,1);
    noOfSteps = length(stepsSide) - 1;
    timeTaken = (stepL - step1)/sampFq;
    strideTimeREF(:,sideCt) = timeTaken/noOfSteps;
end


%% single support times Buurke 2019: between contralateral toe off and heel strike
refVel_r = butterfilterlpf(normh(refData.RightFoot.vel),15,sampFq,2);
refVel_l = butterfilterlpf(normh(refData.LeftFoot.vel),15,sampFq,2);

refVcont =  [refVel_r < 0.1 refVel_l < 0.1];

[refstepsR2, refstepsL2] = fun_findSteps(refVcont);
ssTime = (refstepsR2(:,2) - refstepsR2(:,1))./sampFq;
stanceTime_Ref_R = ssTime(2:end-1); clear ssTime

ssTime = (refstepsL2(:,2) - refstepsL2(:,1))./sampFq;
stanceTime_Ref_L = ssTime(2:end-1); clear ssTime

% estimData
estVel_r = butterfilterlpf(normh(estimData.RightFoot.vel),5,sampFq,2);
estVel_l = butterfilterlpf(normh(estimData.LeftFoot.vel),5,sampFq,2);

estVcont =  [estVel_r < 0.1 estVel_l < 0.1];

[eststepsR2, eststepsL2] = fun_findSteps(estVcont);
ssTime = (eststepsR2(:,2) - eststepsR2(:,1))./sampFq;
stanceTime_Est_R = ssTime(2:end-1); clear ssTime

ssTime = (eststepsL2(:,2) - eststepsL2(:,1))./sampFq;
stanceTime_Est_L = ssTime(2:end-1); clear ssTime


switch subjectSpecs.userSelect
    case 3
        stanceTime_Ref_R = stanceTime_Ref_R(2:end);
%          case 12
%         stanceTime_Ref_R = stanceTime_Ref_R(2:end);
        
end

varNames = {'ForceShoe','IMU', 'm/s'};
BAdispOpt = {'eq';'r';'RMSE';'n'};

figure('name','Boxplots Stance Time L v R');
clear AllstepLengths
curpl =subplot(1,2,1);
AllstepLengths = [stanceTime_Ref_R(:,1); stanceTime_Ref_L(:,1)];
g = [zeros(length(stanceTime_Ref_R(:,1)), 1); ones(length(stanceTime_Ref_L(:,1)), 1); ];
boxplot(AllstepLengths,g);
curpl.XTickLabel = {'Right', 'Left'};
ylabel('Single Stance Time (s)')
title('ST ForceShoe')


diffTest = ttest2(stanceTime_Ref_R(:,1),stanceTime_Ref_L(:,1));

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
    
end

clear AllstepLengths
curpl =subplot(1,2,2);
AllstepLengths = [stanceTime_Est_R(:,1); stanceTime_Est_L(:,1)];
g = [zeros(length(stanceTime_Est_R(:,1)), 1); ones(length(stanceTime_Est_L(:,1)), 1); ];
boxplot(AllstepLengths,g);
curpl.XTickLabel = {'Right', 'Left'};
ylabel('Single Stance Time (s)')
title('ST IMU')


diffTest = ttest2(stanceTime_Est_R(:,1),stanceTime_Est_L(:,1));

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
    
end


varNames = {'ForceShoe','IMU', 's'};
BAdispOpt = {'eq';'r';'RMSE';'n'};

stRFig = figure;
BlandAltman(stRFig,stanceTime_Ref_R,stanceTime_Est_R,varNames,'Stance Time Right',{},'corrinfo',BAdispOpt);

stLFig = figure;
BlandAltman(stLFig,stanceTime_Ref_L,stanceTime_Est_L,varNames,'Stance Time Left',{},'corrinfo',BAdispOpt);

%%
%%
%%

%% step lengths bar plots
% figure('name','L & R step lengths - Compare');
%
% curpl =subplot(1,3,1);
% bar(stepLengthsRef(:,1));
% curYlim = curpl.YLim;
% ylabel('Step length (m)')
% xlabel('Step number')
% title('Step lengths - Ref')
% box off
%
%
% subplot(1,3,2)
% bar(stepLengthsIMU(:,1))
% % legend('Vicon','Filter'
% ylabel('Step length (m)')
% % xlabel('Step number')
% title('Step lengths - Est')
% box off
% ylim(curYlim)
%
% subplot(1,3,3)
% bar(stepLengthswoZMP(:,1))
% % legend('Vicon','Filter'
% ylabel('Step length (m)')
% % xlabel('Step number')
% title('Step lengths - Est No ZMP')
% box off
% ylim(curYlim)
%
% %
%
% figure('name','L & R stride widths - Compare');
% % stride widths
% curpl =subplot(1,3,1);
% bar(stepWidthsRef(:,1))
% curYlim = curpl.YLim;
%
% ylabel('Step width (m)')
% xlabel('Step number')
% title('Step width - Ref')
% box off
%
%
% subplot(1,3,2)
% bar(stepWidthsIMU(:,1))
% % legend('Vicon','Filter'
% ylabel('Step width (m)')
% % xlabel('Step number')
% title('Step width - Est')
% box off
% ylim(curYlim)
%
% subplot(1,3,3)
% bar(stepWidthswoZMP(:,1))
% % legend('Vicon','Filter'
% ylabel('Step width (m)')
% % xlabel('Step number')
% title('Step width - Est No ZMP')
% box off
% ylim(curYlim)
%
%
%
% figure('name','L & R CoM widths - Compare');
% % stride widths
% curpl =subplot(1,3,1);
% bar(CoMWidRef(:,1))
% curYlim = curpl.YLim;
%
% ylabel('CoM width (m)')
% xlabel('Step number')
% title('CoM width - Ref')
% box off
%
%
% subplot(1,3,2)
% bar(CoMWidIMU(:,1))
% % legend('Vicon','Filter'
% ylabel('CoM width (m)')
% % xlabel('Step number')
% title('CoM width - Est')
% box off
% ylim(curYlim)
%
% subplot(1,3,3)
% bar(CoMWidwoZMP(:,1))
% % legend('Vicon','Filter'
% ylabel('CoM width (m)')
% % xlabel('Step number')
% title('CoM width - Est No ZMP')
% box off
% ylim(curYlim)

%%
figure('name','Boxplots');
clear AllstepLengths
curpl =subplot(1,3,1);
AllstepLengths = [stepLengthsRef(:,1); stepLengthsIMU(:,1);stepLengthswoZMP(:,1)];
g = [zeros(length(stepLengthsRef(:,1)), 1); ones(length(stepLengthsIMU(:,1)), 1); 2*ones(length(stepLengthswoZMP(:,1)), 1)];
boxplot(AllstepLengths,g);
curpl.XTickLabel = {'VICON', 'IMU', 'No ZMP'};
ylabel('Step length (m)')
title('Step lengths')


diffTest = ttest2(stepLengthsRef(:,1),stepLengthsIMU(:,1));

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
    
end

diffTest = ttest2(stepLengthswoZMP(:,1),stepLengthsIMU(:,1));

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([2 3]), [1 1]*max(yt)*1.1, '-k',  mean(xt([2 3])), max(yt)*1.15, '*k')
    
end

diffTest = ttest2(stepLengthswoZMP(:,1),stepLengthsRef(:,1));

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 3]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 3])), max(yt)*1.15, '*k')
    hold off
    box off
end


curpl =subplot(1,3,2);
AllstepWidths = [stepWidthsRef(:,1); stepWidthsIMU(:,1);stepWidthswoZMP(:,1)];
g = [zeros(length(stepWidthsRef(:,1)), 1); ones(length(stepWidthsIMU(:,1)), 1); 2*ones(length(stepWidthswoZMP(:,1)), 1)];
boxplot(AllstepWidths,g);
curpl.XTickLabel = {'VICON', 'IMU', 'No ZMP'};
ylabel('Step Width (m)')
title(['Step Widths - US ' usName 'tr: ' trName 'combo ' comboUsed])



diffTest = ttest2(stepWidthsRef(:,1),stepWidthsIMU(:,1));

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
end

diffTest = ttest2(stepWidthswoZMP(:,1),stepWidthsIMU(:,1));

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([2 3]), [1 1]*max(yt)*1.1, '-k',  mean(xt([2 3])), max(yt)*1.15, '*k')
end

diffTest = ttest2(stepWidthswoZMP(:,1),stepWidthsRef(:,1));

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 3]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 3])), max(yt)*1.15, '*k')
    hold off
    box off
end



curpl =subplot(1,3,3);
AllstepWidths = [CoMWidRef(:,1); CoMWidIMU(:,1); CoMWidwoZMP(:,1)];
g = [zeros(length(CoMWidRef(:,1)), 1); ones(length(CoMWidIMU(:,1)), 1); 2*ones(length(CoMWidwoZMP(:,1)), 1)];
boxplot(AllstepWidths,g);
curpl.XTickLabel = {'VICON', 'IMU', 'No ZMP'};
ylabel('CoM Widths (m)')
title('CoM Widths - US ')



diffTest = ttest2(CoMWidRef(:,1),CoMWidIMU(:,1));

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
end

diffTest = ttest2(CoMWidwoZMP(:,1),CoMWidIMU(:,1));

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([2 3]), [1 1]*max(yt)*1.1, '-k',  mean(xt([2 3])), max(yt)*1.15, '*k')
end

diffTest = ttest2(CoMWidwoZMP(:,1),CoMWidRef(:,1));

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 3]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 3])), max(yt)*1.15, '*k')
    hold off
    box off
end

%%
[rmsSLr]  = rms(stepLengthsRefR-stepLengthsIMUR);
[rmsSLl]  = rms(stepLengthsRefL-stepLengthsIMUL);
[corSLr]  = corr(stepLengthsRefR,stepLengthsIMUR);
[corSLl]  = corr(stepLengthsRefL,stepLengthsIMUL);
figure('name','Boxplots L v R');
clear AllstepLengths
curpl =subplot(1,2,1);
AllstepLengths = [stepLengthsRefR(:,1); stepLengthsRefL(:,1)];
g = [zeros(length(stepLengthsRefR(:,1)), 1); ones(length(stepLengthsRefL(:,1)), 1); ];
boxplot(AllstepLengths,g);
curpl.XTickLabel = {'Right', 'Left'};
ylabel('Step length (m)')
title('SL VICON')


diffTest = ttest2(stepLengthsRefR(:,1),stepLengthsRefL(:,1));

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
    
end

clear AllstepLengths
curpl =subplot(1,2,2);
AllstepLengths = [stepLengthsIMUR(:,1); stepLengthsIMUL(:,1)];
g = [zeros(length(stepLengthsIMUR(:,1)), 1); ones(length(stepLengthsIMUL(:,1)), 1); ];
boxplot(AllstepLengths,g);
curpl.XTickLabel = {'Right', 'Left'};
ylabel('Step length (m)')
title('SL IMU')


diffTest = ttest2(stepLengthsIMUR(:,1),stepLengthsIMUL(:,1));

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
    
end


%%
varNames = {'ForceShoe','IMU', 'm'};
BAdispOpt = {'eq';'r';'RMSE';'n'};

slFig = figure;
[~, ~, stepLStat] = BlandAltman(slFig,stepLengthsRef,stepLengthsIMU,varNames,'Step Lengths',{},'corrinfo',BAdispOpt);
if ~processStraightSteps && length(deviatingSteps) > 1
    axHandles =slFig.Children;
    ax1 = axHandles(3);
    scatter(ax1,stepLengthsRefturn,stepLengthsIMUturn,'ro','filled')
    
    ax2 = axHandles(2);
    delV = stepLengthsIMUturn - stepLengthsRefturn;
    avgV = mean([stepLengthsRefturn stepLengthsIMUturn],2);
    scatter(ax2,avgV,delV,'ro','filled')
end

swFig = figure;
[~, ~, stepWStat] = BlandAltman(swFig,stepWidthsRef,stepWidthsIMU,varNames,'Step Widths',{},'corrinfo',BAdispOpt);
if ~processStraightSteps && length(deviatingSteps) > 1
    axHandles =swFig.Children;
    ax1 = axHandles(3);
    scatter(ax1,stepWidthsRefturn,stepWidthsIMUturn,'ro','filled')
    
    ax2 = axHandles(2);
    delV = stepWidthsIMUturn - stepWidthsRefturn;
    avgV = mean([stepWidthsIMUturn stepWidthsRefturn],2);
    scatter(ax2,avgV,delV,'ro','filled')
end

cwFig = figure;
[~, ~, stepCStat] = BlandAltman(cwFig,CoMWidRef,CoMWidIMU,varNames,'Mean CoM Widths',{},'corrinfo',BAdispOpt);
if ~processStraightSteps && length(deviatingSteps) > 1
    axHandles =cwFig.Children;
    ax1 = axHandles(3);
    scatter(ax1,CoMWidRefturn,CoMWidIMUturn,'ro','filled')
    
    ax2 = axHandles(2);
    delV = CoMWidIMUturn - CoMWidRefturn;
    avgV = mean([CoMWidIMUturn CoMWidRefturn],2);
    scatter(ax2,avgV,delV,'ro','filled')
end

%% Temporal parameters calculated from foot movement
imuStepsCell = {imustepsR; imustepsL};

for sideCt = 1:length(imuStepsCell)
    stepsSide = imuStepsCell{sideCt};
    step1 = stepsSide(1,2);
    stepL = stepsSide(end,1);
    noOfSteps = length(stepsSide) - 1;
    timeTaken = (stepL - step1)/sampFq;
    strideTimeIMU(:,sideCt) = timeTaken/noOfSteps;
end

refContactOG = refVICONsync.cgframe.Forces.Contact(1:endMeas,:);
[refstepsROG, refstepsLOG] = fun_findSteps(refContactOG);

refStepsCell = {refstepsROG; refstepsLOG};

for sideCt = 1:length(refStepsCell)
    stepsSide = refStepsCell{sideCt};
    step1 = stepsSide(1,2);
    stepL = stepsSide(end,1);
    noOfSteps = length(stepsSide) - 1;
    timeTaken = (stepL - step1)/sampFq;
    strideTimeREF(:,sideCt) = timeTaken/noOfSteps;
end

%%

sLenStats(iCt,:) = [stepLStat.r stepLStat.RMSE stepLStat.differenceMean];
sWidStats(iCt,:) = [stepWStat.r stepWStat.RMSE stepWStat.differenceMean];
sCoMStats(iCt,:) = [stepCStat.r stepCStat.RMSE stepCStat.differenceMean];
sCumStats = [sLenStats sWidStats sCoMStats];

sCumStatRL(iCt,:) = [corSLr rmsSLr corSLl rmsSLl ...
    corSWr rmsSWr corSWl rmsSWl  ...
    corCWr rmsCWr corCWl rmsCWl    ];