function [stepsRL] = fun_findChronologicalSteps(contactMatrix, subSpecs)

% takes in matrix with boolean for foot contact
% returns matrix with foot

% contactMatrix = contact;
diffContact = diff(contactMatrix);

initContactR = [1;find(diffContact(:,1)==1)];
lastContactR = [find(diffContact(:,1)==-1); length(contactMatrix)];

if length(initContactR) ~= length(lastContactR)
    if length(initContactR) > length(lastContactR)
        initContactR = initContactR(1:end-1);
    else
        lastContactR = lastContactR(1:end-1);
    end
end

if all((lastContactR - initContactR)>0)
    stepsR = [initContactR, lastContactR];
else
    error('not handled')
end

initContactL = [1;find(diffContact(:,2)==1)];
lastContactL = [find(diffContact(:,2)==-1); length(contactMatrix)];

if length(initContactL) ~= length(lastContactL)
    if length(initContactL) > length(lastContactL)
        initContactL = initContactL(1:end-1);
    else
        lastContactL = lastContactL(1:end-1);
    end
end

if all((lastContactL - initContactL)>0)
    stepsL = [initContactL, lastContactL];
else
    error('not handled')
end

overallSteps = [stepsR ones(length(stepsR),1);
    stepsL 2*ones(length(stepsL),1)];

[~,idx] = sort(overallSteps(:,1)); % sort just the first column
tstepsRL = overallSteps(idx,:);   % sort the whole matrix using the sort indices

if strcmpi(subSpecs.startFoot,'Right')
    stepsRL = [[stepsR(1,:) 1; stepsL(1,:) 2;]; tstepsRL(3:end,:)];
elseif strcmpi(subSpecs.startFoot,'Left')
    stepsRL = [[stepsL(1,:) 2; stepsR(1,:) 1;]; tstepsRL(3:end,:)];
end

end