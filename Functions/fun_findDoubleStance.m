function [stepsD] = fun_findDoubleStance(contactMatrix)

% takes in matrix with boolean for foot contact
% returns matrix with foot at double stance

% contactMatrix = contact;
stContact = sum(contactMatrix(:,1:2),2);

doubleStance = (stContact==2);
diffdoubleStance  = diff(doubleStance);
initContact = [1;find(diffdoubleStance(:,1)==1)];
lastContact = [find(diffdoubleStance(:,1)==-1); length(contactMatrix)];


if length(initContact) ~= length(lastContact)
    if length(initContact) > length(lastContact)
        initContact = initContact(1:end-1);
    else
        lastContact = lastContact(1:end-1);
    end
end

if all((lastContact - initContact)>0)
    stepsD = [initContact, lastContact];
else
    error('not handled')
end

end