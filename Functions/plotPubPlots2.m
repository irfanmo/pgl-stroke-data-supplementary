%% plotPubPlots2;
% Subjectwise plot 
markerCell = {'+','o','v','s'};
%% Single Stance Times
plotPub_STSubwiseMarker

%% Step lengths
plotPub_steplengthSubwiseMarker;

%% AP MoS
plotPub_APMoSSubwiseMarker

%% ML MoS BA plots
plotPub_MLMoSSubwiseMarker;


