function [segmentedOp, spatTempParam] = fun_splitandTransformStepWise(dataSet,stepsAr)
%%
%%
dataSetStr = {'RightFoot','LeftFoot','CoM'};

for iSeg = 1:length(dataSetStr)
    clear R_temp
    curSeg = dataSetStr{iSeg};
    
    %     for iVar =1:length(filledVar)
    %     curVar = 'pos';
    
    for iStep = 1:length(stepsAr)
        setStrt  = stepsAr(iStep,1);
        setEnd   = stepsAr(iStep,2);
        
        if iStep == 1
            prevStepS = setStrt;
            prevStepE = setStrt;
        else
            prevStepS  = stepsAr(iStep-1,1)+5;
            prevStepE   = stepsAr(iStep-1,2)-5;
        end
        
        
        if stepsAr(iStep,3) == 1 %rightleg stance
            %changes in left leg
            pos_init=dataSet.LeftFoot.pos(setStrt,:);
            pos_fini=dataSet.LeftFoot.pos(setEnd,:);
            
            pos_initp=dataSet.LeftFoot.pos(prevStepS,:);
            pos_finip=dataSet.LeftFoot.pos(prevStepE,:);
            
            
            %             pos_stanceAvg = (dataSet.RightFoot.pos(setStrt:setEnd,:));
            %             pos_stanceAvg = (dataSet.RightFoot.pos(setStrt,:));
            otherFoot = 'RightFoot';
        elseif stepsAr(iStep,3) == 2 %leftleg stance
            %changes in right leg
            pos_init=dataSet.RightFoot.pos(setStrt,:);
            pos_fini=dataSet.RightFoot.pos(setEnd,:);
            
            pos_initp=dataSet.RightFoot.pos(prevStepS,:);
            pos_finip=dataSet.RightFoot.pos(prevStepE,:);
            
            
            
            %             pos_stanceAvg = mean(dataSet.LeftFoot.pos(setStrt:setEnd,:));
            %             pos_stanceAvg = mean(dataSet.LeftFoot.pos(setStrt,:));
            otherFoot = 'LeftFoot';
        end
        
        resetPos = pos_init;
        
        
        headingChange = (pos_fini - pos_init);
        headingChange(3)=0; % make z component zero
        heading_stepwise_estimate=headingChange/norm(headingChange); % normalize
        if all(isnan(heading_stepwise_estimate))
            theading = [1 0 0];
        else
            theading = heading_stepwise_estimate;
        end
        
        theadingps = theading;
        
        fakeZaxis = [zeros(5,1) zeros(5,1) ones(5,1)];
        R_temp=initorient_wHeading(fakeZaxis, theading');
        
        
        curDs = dataSet.(curSeg).pos(setStrt:setEnd,:);
        transDs = (R_temp*(curDs- resetPos)')';
        segmentedOp.(curSeg).pos.sets{iStep,:} = transDs;
        segmentedOp.(curSeg).HeadingArrayPS(iStep,:) = theadingps;
        
        if strcmpi(curSeg,otherFoot)
        else
            spatTempParam.strideDim(iStep,:) = transDs(end,:) - transDs(1,:);
        end
        
        if isfield(dataSet.(curSeg),'vel')
            curVs = dataSet.(curSeg).vel(setStrt:setEnd,:);
            segmentedOp.(curSeg).vel.sets{iStep,:} = (R_temp*(curVs)')';
            
        end
        
        if isfield(dataSet.(curSeg),'RotM')
            fInd = 1; clear curRsop
            for snipStep = setStrt:setEnd
                curRs = squeeze(dataSet.(curSeg).RotM(:,:,snipStep));                
                curRsop(:,:,fInd) = R_temp*curRs; fInd = fInd + 1 ;
            end
            
            segmentedOp.(curSeg).RotM.sets{iStep,:} = curRsop;
            
        end
    end
end
segmentedOp.stepsUsed = stepsAr;


end