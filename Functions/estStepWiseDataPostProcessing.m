%% estStepWiseDataPostProcessing

%% splitting into steps

[segmentedRef, strideParamRef] = fun_splitandTransformStepWise(rDatasync,stepsWithFeetInfoRefsync);
[segmentedIMU, strideParamIMU] = fun_splitandTransformStepWise2(eDatasync,stepsWithFeetInfoIMUsync);


refrfSeg = segmentedRef.RightFoot.pos.sets; estrfSeg = segmentedIMU.RightFoot.pos.sets;
reflfSeg = segmentedRef.LeftFoot.pos.sets;  estlfSeg = segmentedIMU.LeftFoot.pos.sets;
refpsSeg = segmentedRef.CoM.pos.sets;       estpsSeg = segmentedIMU.CoM.pos.sets;

%% estimating SL and SW
[tstepLengthsRef, tstepWidthsRef, tcomLenRef, tcomWidRef]       = fun_findStepWiseSpatioTempParam(rDatasync,stepsWithFeetInfoRefsync);
[tstepLengthsIMU, tstepWidthsIMU, tcomLenIMU, tcomWidIMU]       = fun_findStepWiseSpatioTempParamIMU(eDatasync,stepsWithFeetInfoIMUsync);

startSTStep = 1; endSTSteps = 0;

stepLengthsRef   = tstepLengthsRef(startSTStep:end-endSTSteps,1);
stepWidthsRef    = tstepWidthsRef(startSTStep:end-endSTSteps,1);
stepLengthsIMU   = tstepLengthsIMU(startSTStep:end-endSTSteps,1) ;
stepWidthsIMU    = tstepWidthsIMU(startSTStep:end-endSTSteps,1);

CoMLenRef  = tcomLenRef(startSTStep:end-endSTSteps,1);
CoMWidRef  = tcomWidRef(startSTStep:end-endSTSteps,1);
CoMLenIMU  = tcomLenIMU(startSTStep:end-endSTSteps,1);
CoMWidIMU  = tcomWidIMU(startSTStep:end-endSTSteps,1);

% opposite to the stance foot; if left is stance, it is right step
stepLengthsRefR = stepLengthsRef(tstepLengthsRef(startSTStep:end-endSTSteps,2)==2);
stepLengthsRefL = stepLengthsRef(tstepLengthsRef(startSTStep:end-endSTSteps,2)==1);
stepLengthsIMUR = stepLengthsIMU(tstepLengthsIMU(startSTStep:end-endSTSteps,2)==2);
stepLengthsIMUL = stepLengthsIMU(tstepLengthsIMU(startSTStep:end-endSTSteps,2)==1);

stepWidthsRefR = stepWidthsRef(tstepWidthsRef(startSTStep:end-endSTSteps,2)==2);
stepWidthsRefL = stepWidthsRef(tstepWidthsRef(startSTStep:end-endSTSteps,2)==1);
stepWidthsIMUR = stepWidthsIMU(tstepWidthsIMU(startSTStep:end-endSTSteps,2)==2);
stepWidthsIMUL = stepWidthsIMU(tstepWidthsIMU(startSTStep:end-endSTSteps,2)==1);

comWidthsRefR = CoMWidRef(tcomWidRef(startSTStep:end-endSTSteps,end)==2);
comWidthsRefL = CoMWidRef(tcomWidRef(startSTStep:end-endSTSteps,end)==1);
comWidthsIMUR = CoMWidIMU(tcomWidIMU(startSTStep:end-endSTSteps,end)==2);
comWidthsIMUL = CoMWidIMU(tcomWidIMU(startSTStep:end-endSTSteps,end)==1);