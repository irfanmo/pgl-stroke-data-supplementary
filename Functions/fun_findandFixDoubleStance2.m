function [contact_DSadj] = fun_findandFixDoubleStance2(contactMatrix,fs,WinArray)

% takes in matrix with boolean for foot contact
% returns matrix with foot at double stance

% contactMatrix = contact;
% Wl = fs*0.08; % OG val
Wl1 = WinArray(1); %fs*0.0;
Wl2 = WinArray(2); %fs*0.05;


contact_DSadj = zeros(length(contactMatrix),size(contactMatrix,2));
for iDim = 1:size(contactMatrix,2)
    tContact = contactMatrix(:,iDim);
    diffContact = diff(tContact);
    
    for i = 1:length(diffContact)
        if diffContact(i) == -1 % start of swing
            if tContact(i-1) == 1
                tContact(i-1:i+Wl1+2) = 1;
                
            end
            
        elseif diffContact(i) == 1 % end of swing
            if tContact(i+1) == 1
                tContact(i-Wl2+1:i+1) = 1;%             
            end
        end
        
    end
    contact_DSadj(:,iDim) = tContact;
end


end