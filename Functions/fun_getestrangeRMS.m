function [rangeRMSout, rangeRefout, minRefout, maxRefout] =...
    fun_getestrangeRMS(estimData,refData)



for iDim = 1:size(estimData,2)
    dimRange = range(estimData(:,iDim));
    rangeRef(iDim) = dimRange;
    minRefout(iDim) = min(refData(:,iDim));
    maxRefout(iDim) = max(refData(:,iDim));

    rmseV(iDim) = rms(estimData(:,iDim) - refData(:,iDim));    
    rangermseV(iDim) = ((rmseV(iDim))/dimRange)*100;
end



rangeRMSout = rangermseV;
rangeRefout = rangeRef;

end