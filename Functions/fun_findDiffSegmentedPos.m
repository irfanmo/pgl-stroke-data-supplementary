function [segRMSDiff] = fun_findDiffSegmentedPos(refData,imuData)

dataSetStr = {'RightFoot','LeftFoot','CoM'};


for sgCt = 1:length(dataSetStr)
    curSeg = dataSetStr{sgCt};
    refSets = refData.(curSeg).pos.sets;
    imuSets = imuData.(curSeg).pos.sets;
    
    if length(refSets) ~= length(imuSets)
        error('Sets are not same length?')        
    end
    
    
    for setCt = 1:length(refSets)
       curRef =  refSets{setCt};
       curIMU =  imuSets{setCt};
       opData.(curSeg).rmsDiff(setCt,:) =  rms(curRef - curIMU);       
    end
    
end


segRMSDiff = opData;
segRMSDiff.stepsUsed = imuData.stepsUsed;



end