%% assignProcessedVars

estDS = IMUProcessedData.(usName).(trName).EstData;
refDS = IMUProcessedData.(usName).(trName).RefData;
subSpecs = IMUProcessedData.(usName).(trName).subjectSpecs;

footVar = {'Right', 'Left'};
affFoot = subSpecs.affectedFoot;

switch affFoot
    case 'Right'
        othFoot = 'Left';
    case 'Left'
        othFoot = 'Right';
end
%%



if useFirst10m
    endMeas = subSpecs.midWindow(1);
else
    endMeas = length(refDS.gframe.RightFoot.pos);
end

tVar  = estDS.IMUdata{1}.t;
fsVar = estDS.IMUdata{1}.fs;
% startTask = min(refDS.Body.syncedEventTimes) + 0.5*fsVar;
% stopTask  = max(refDS.Body.syncedEventTimes) - 0.5*fsVar;


imuContactZV = estDS.IMUContacts.contactZV(1:endMeas,:);
imuContactDS = estDS.IMUContacts.contactDS(1:endMeas,:); warning('imu contact adjusted being used')
% refContact = imuContactDS;
refContactFS = refDS.gframe.Forces.Contact(1:endMeas,:);
% refContact = refContactFS;
% [refstepsRL_FS] = fun_findChronologicalSteps(refContactFS, subSpecs);

sampFq = refDS.sampFq;

switch dataOrigin
    case 'MVN'
        refData.RightFoot.pos = refDS.gframe.RightFoot.pos(1:endMeas,:);
        refData.LeftFoot.pos = refDS.gframe.LeftFoot.pos(1:endMeas,:);
        refData.CoM.pos = refDS.gframe.CoM.pos(1:endMeas,:);
        
        
        refData.RightFoot.vel = refDS.gframe.RightFoot.vel(1:endMeas,:);
        refData.LeftFoot.vel  = refDS.gframe.LeftFoot.vel(1:endMeas,:);
        refData.CoM.vel       = refDS.gframe.CoM.vel(1:endMeas,:);
        
        refData.RightFoot.acc = refDS.gframe.RightFoot.acc(1:endMeas,:);
        refData.LeftFoot.acc  = refDS.gframe.LeftFoot.acc(1:endMeas,:);
        refData.CoM.acc       = refDS.gframe.CoM.acc(1:endMeas,:);
        
        refDatacg.RightFoot.pos = refDS.cgframe.RightFoot.pos(1:endMeas,:);
        refDatacg.LeftFoot.pos  = refDS.cgframe.LeftFoot.pos(1:endMeas,:);
        refDatacg.CoM.pos       = refDS.cgframe.CoM.pos(1:endMeas,:);
        %
        refDatacg.RightFoot.vel = refDS.cgframe.RightFoot.vel(1:endMeas,:);
        refDatacg.LeftFoot.vel  = refDS.cgframe.LeftFoot.vel(1:endMeas,:);
        refDatacg.CoM.vel       = refDS.cgframe.CoM.vel(1:endMeas,:);
        
        refDatacg.RightFoot.acc = refDS.cgframe.RightFoot.acc(1:endMeas,:);
        refDatacg.LeftFoot.acc  = refDS.cgframe.LeftFoot.acc(1:endMeas,:);
        refDatacg.CoM.acc       = refDS.cgframe.CoM.acc(1:endMeas,:);
        
    case 'IFSUS'
        
        refData.RightFoot.pos = refDS.gframe.IFSUSVar.RFpos(1:endMeas,:);
        refData.LeftFoot.pos = refDS.gframe.IFSUSVar.LFpos(1:endMeas,:);
        refData.CoM.pos  = refDS.gframe.IFSUSVar.gCoMpos(1:endMeas,:);
        
        refData.RightFoot.vel = refDS.gframe.IFSUSVar.RFvel(1:endMeas,:);
        refData.LeftFoot.vel  = refDS.gframe.IFSUSVar.LFvel(1:endMeas,:);
        refData.CoM.vel       = refDS.gframe.IFSUSVar.gCoMvel(1:endMeas,:);
        
        refData.RightFoot.acc = refDS.gframe.IFSUSVar.RFacc(1:endMeas,:);
        refData.LeftFoot.acc  = refDS.gframe.IFSUSVar.LFacc(1:endMeas,:);
        refData.CoM.acc       = refDS.gframe.IFSUSVar.gCoMacc(1:endMeas,:);
        
        refData.RightFoot.RotM  = refDS.gframe.IFSUSVar.RotMatricesR(:,:,1:endMeas);
        refData.LeftFoot.RotM   = refDS.gframe.IFSUSVar.RotMatricesL(:,:,1:endMeas);
        
        refData.Body.Forces = refDS.gframe.Forces.Tot(1:endMeas,:);
        
        refDatacg.RightFoot.pos = refDS.cgframe.IFSUSVar.RFpos(1:endMeas,:);
        refDatacg.LeftFoot.pos  = refDS.cgframe.IFSUSVar.LFpos(1:endMeas,:);
        refDatacg.CoM.pos       = refDS.cgframe.IFSUSVar.gCoMpos(1:endMeas,:);
        %
        refDatacg.RightFoot.vel = refDS.cgframe.IFSUSVar.RFvel(1:endMeas,:);
        refDatacg.LeftFoot.vel  = refDS.cgframe.IFSUSVar.LFvel(1:endMeas,:);
        refDatacg.CoM.vel       = refDS.cgframe.IFSUSVar.gCoMvel(1:endMeas,:);
        
        refDatacg.RightFoot.acc = refDS.cgframe.IFSUSVar.RFacc(1:endMeas,:);
        refDatacg.LeftFoot.acc  = refDS.cgframe.IFSUSVar.LFacc(1:endMeas,:);
        refDatacg.CoM.acc       = refDS.cgframe.IFSUSVar.gCoMacc(1:endMeas,:);
        
        
end


%% Estimated Data
estimStatesR = estDS.IMUdata{1}.stateIndex.allIMU;
estimStatesL = estDS.IMUdata{2}.stateIndex.allIMU;
estimStatesP = estDS.IMUdata{3}.stateIndex.allIMU;

f_co = 7;

estimData.CoM.acc = refDS.EstimatedGRF(1:endMeas,:); % Estimated GRF present in Ref data file
estimData.CoM.vel = estDS.CoMVelVar.estCoMVel(1:endMeas,:);
estimData.CoM.pos = butterfilterlpf(estDS.Rcombo.(comboUsed).xMat(estimStatesP(1:3),1:endMeas)',f_co,sampFq,2);


estimData.RightFoot.acc = estDS.Rcombo.(comboUsed).xMat(estimStatesR(7:9),1:endMeas)' - estDS.IMUdata{1}.g';
estimData.RightFoot.vel = estDS.Rcombo.(comboUsed).xMat(estimStatesR(4:6),1:endMeas)';
estimData.RightFoot.pos = butterfilterlpf(estDS.Rcombo.(comboUsed).xMat(estimStatesR(1:3),1:endMeas)',f_co,sampFq,2);

estimData.LeftFoot.acc = estDS.Rcombo.(comboUsed).xMat(estimStatesL(7:9),1:endMeas)' - estDS.IMUdata{2}.g';
estimData.LeftFoot.vel = estDS.Rcombo.(comboUsed).xMat(estimStatesL(4:6),1:endMeas)';
estimData.LeftFoot.pos = butterfilterlpf(estDS.Rcombo.(comboUsed).xMat(estimStatesL(1:3),1:endMeas)',f_co,sampFq,2);

estimData.Body.HeadingArray = estDS.cgVar.headArry;

estimData.Fs = estDS.IMUdata{1}.fs;

estimData.RightFoot.RotM  = estDS.RcgKF.Right(:,:,1:endMeas);
estimData.LeftFoot.RotM   = estDS.RcgKF.Left(:,:,1:endMeas);
estimData.CoM.RotM        = estDS.RcgKF.CoM(:,:,1:endMeas);

%%
%% total distance travelled per segment

stInd = 1;
enInd = endMeas;

refCdist = normh(refData.CoM.pos(enInd,1:2) - refData.CoM.pos(stInd,1:2));
refendED = normh(refData.RightFoot.pos(enInd,1:2) - refData.LeftFoot.pos(enInd,1:2));
imuCdist = normh(estimData.CoM.pos(enInd,1:2) - estimData.CoM.pos(stInd,1:2));
imuendED = normh(estimData.RightFoot.pos(enInd,1:2) - estimData.LeftFoot.pos(enInd,1:2));

enInd2 = round(enInd/2);

refCdist2 = normh(refData.CoM.pos(enInd2,1:2) - refData.CoM.pos(stInd,1:2));
refendED2 = normh(refData.RightFoot.pos(enInd2,1:2) - refData.LeftFoot.pos(enInd2,1:2));
imuCdist2 = normh(estimData.CoM.pos(enInd2,1:2) - estimData.CoM.pos(stInd,1:2));
imuendED2 = normh(estimData.RightFoot.pos(enInd2,1:2) - estimData.LeftFoot.pos(enInd2,1:2));


totalDistError(1,1:4) = [refCdist2-imuCdist2  ...
    refendED2 - imuendED2 ...
    refCdist-imuCdist  ...
    refendED - imuendED];

totalDistError(1,5) = normh(totalDistError(1,1:4));


CumTypeWiseTotDistances(usCt,:) = totalDistError;