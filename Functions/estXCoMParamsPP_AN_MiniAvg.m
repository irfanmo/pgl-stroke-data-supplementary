%% estXCoMParamsPP_AN_MiniAvg

selSamp = 0; endSamp = 1;
% selSamp = 2; endSamp = 0;
%% Estimate w0 as in Hof et al. 2005
% w0 = sqrt( g / leg length)

% Calculate leg length
legLengthBodyLengthRatioMale   = 0.260 + 0.2484; % Bron: drillis et al - body segment parameters
legLengthBodyLengthRatioFemale = 0.252 + 0.227;  % Bron: drillis et al - body segment parameters

switch subSpecs.Gender
    case 'F'
        legLengthBodyLengthRatio = legLengthBodyLengthRatioFemale;
    case 'M'
        legLengthBodyLengthRatio = legLengthBodyLengthRatioMale;
    otherwise
        error('Subject gender unknown')
end

subjectLegLength = subSpecs.Height * legLengthBodyLengthRatio;
legLengthFactor  = 1; % see Hof et al.
l0=subjectLegLength;
% Calculate w0
w0 = sqrt(9.81/(legLengthFactor * subjectLegLength));

%%
changeOrder = 0;
refapMosrSeg = [];
refapMoslSeg = [];
estapMosrSeg = [];
estapMoslSeg = [];

refmlMOSrSeg = [];
estmlMOSrSeg = [];
refmlMOSlSeg = [];
estmlMOSlSeg = [];

strSet   = 1;
noofSets = length(segmentedIMU.stepsUsed);

for iStep = strSet:noofSets
    
    tVarR = estrfSeg{iStep};
    tVarL = estlfSeg{iStep};
    tVarP = estpsSeg{iStep};
    tVarV = segmentedIMU.CoM.vel.sets{iStep};
    tVarX = tVarP + (tVarV/w0);
    tVarRM_R = segmentedIMU.RightFoot.RotM.sets{iStep};
    tVarRM_L = segmentedIMU.LeftFoot.RotM.sets{iStep};
    
    tVarRefR = refrfSeg{iStep};
    tVarRefL = reflfSeg{iStep};
    tVarRefP = refpsSeg{iStep};
    tVarRefV = segmentedRef.CoM.vel.sets{iStep};
    tVarRefX = tVarRefP + (tVarRefV/w0);
    tVarRefRM_R = segmentedRef.RightFoot.RotM.sets{iStep};
    tVarRefRM_L = segmentedRef.LeftFoot.RotM.sets{iStep};
    
    stepIndArray = segmentedIMU.stepsUsed(iStep,1):segmentedIMU.stepsUsed(iStep,2);
    contactArray = imuContact;
    %     contactArray = refContactFS;
    curStance = segmentedIMU.stepsUsed(iStep,3);
    if endSamp
        %           selSamp = length(tVarP);
        selSamp = round(length(tVarP)/2);
        selSamp = 15;
        
        if iStep > 1 && iStep < noofSets
            stepIndInfo = segmentedIMU.stepsUsed(iStep,1:2);
      
            
            prevTO   = segmentedIMU.stepsUsed(iStep-1,2); nextTO   = segmentedIMU.stepsUsed(iStep+1,2);
            if prevTO > stepIndInfo(1) && prevTO < stepIndInfo(2)
                selSamp = prevTO - stepIndInfo(1);
            elseif nextTO > stepIndInfo(1) && nextTO < stepIndInfo(2)
                selSamp = nextTO - stepIndInfo(1);
            else
                error('what now?')
            end
     
            tselSamp = find(sum(contactArray(stepIndArray,:)') < 2, 2); selSamp = tselSamp(end); %curStance = curStance;
        end
    end
    
    estXcomSeg{iStep} = tVarX;
    refXcomSeg{iStep} = tVarRefX;

    
    clear p_toe_r p_heel_r p_amtoe_r p_gtoe_r p_altoe_r p_pltoe_r p_pmtoe_r p_amheel_r p_alheel_r p_plheel_r p_pmheel_r   ...
        p_toe_l p_heel_l p_amtoe_l p_gtoe_l p_altoe_l p_pltoe_l p_pmtoe_l p_amheel_l p_alheel_l p_plheel_l p_pmheel_l   ...
        p_frf  p_frh    p_flf p_flh apMOS mlMOS_rt mlMOS_rh mlMOS_lt mlMOS_lh intersectPoint mosval frontBoS_Line CoMXCoM_Line
    
    % Get all shoe positions
    [p_toe_r, p_heel_r, p_amtoe_r, p_gtoe_r, p_altoe_r, p_pltoe_r, p_pmtoe_r, p_amheel_r, p_alheel_r, p_plheel_r, p_pmheel_r,   ...
        p_toe_l, p_heel_l, p_amtoe_l, p_gtoe_l, p_altoe_l, p_pltoe_l, p_pmtoe_l, p_amheel_l, p_alheel_l, p_plheel_l, p_pmheel_l,   ...
        p_frf  , p_frh,    p_flf,     p_flh] = ...
        p_feet( tVarR', tVarL', tVarRM_R, tVarRM_L, subSpecs.shoe_size);
    
    if plotTestGraphs
    ax = 2;
    figure; subplot(211); plot(p_toe_r(:,ax)); hold on;plot(p_heel_r(:,ax))
    end
    
    selSampArray = [selSamp - 2:selSamp];
    
    if selSamp > 5
        resCt = 1;
        for selCt = selSampArray
            
            
            BoSposX = [p_heel_l(selCt,1)   p_amheel_l(selCt,1);   % #13 #20
                p_pmtoe_l(selCt,1)  p_gtoe_l(selCt,1);     % #17 #15
                p_gtoe_r(selCt,1)   p_pmtoe_r(selCt,1);    % #4  #6
                p_amheel_r(selCt,1) p_heel_r(selCt,1)  ];  % #9  #2
            
            BoSposY = [p_heel_l(selCt,2)   p_amheel_l(selCt,2);   % #13 #20
                p_pmtoe_l(selCt,2)  p_gtoe_l(selCt,2);     % #17 #15
                p_gtoe_r(selCt,2)   p_pmtoe_r(selCt,2);    % #4  #6
                p_amheel_r(selCt,2) p_heel_r(selCt,2)  ];  % #9  #2
            
            backLineX  = [BoSposX(1,1) BoSposX(4,2)]; %LH %RH
            backLineY  = [BoSposY(1,1) BoSposY(4,2)];
            frontLineX = [BoSposX(2,2) BoSposX(3,1)]; %LF %RF
            frontLineY = [BoSposY(2,2) BoSposY(3,1)];
            estmosval = distancePointToLineSegment(tVarX(selCt,1:2),[frontLineX(1) frontLineY(1)], [frontLineX(2) frontLineY(2)]);
            
            
            
            frontBoS_Line = [frontLineX; frontLineY];
            CoMXCoM_Line  = [tVarP(selCt,1) tVarX(selCt,1);
                tVarP(selCt,2) tVarX(selCt,2) ];
            
            intersectPoint = findIntersectionPoint(frontBoS_Line, CoMXCoM_Line);
            
            Rest(iStep,:) = isPointOnLine(tVarP(selCt,1:2), tVarX(selCt,1:2), intersectPoint);
            
            tapMOS(resCt,:) = normh(intersectPoint - tVarX(selCt,1:2));
            
            
            if curStance == 1 %rightleg stance
                tapMOS(resCt,:) = (p_toe_r(selCt,1) - tVarX(selCt,1));
            elseif curStance == 2
                tapMOS(resCt,:) = (p_toe_l(selCt,1) - tVarX(selCt,1));
            end
            
            tmlMOS_rt(resCt,:) = distancePointToLineSegment(p_altoe_r(selCt,1:2),tVarP(selCt,1:2), tVarX(selCt,1:2));
            tmlMOS_rh(resCt,:) = distancePointToLineSegment(p_plheel_r(selCt,1:2),tVarP(selCt,1:2), tVarX(selCt,1:2));
            
            tmlMOS_lt(resCt,:) = distancePointToLineSegment(p_altoe_l(selCt,1:2),tVarP(selCt,1:2), tVarX(selCt,1:2));
            tmlMOS_lh(resCt,:) = distancePointToLineSegment(p_plheel_l(selCt,1:2),tVarP(selCt,1:2), tVarX(selCt,1:2));
            
            resCt = resCt + 1;
        end
        
        apMOS = mean(tapMOS);
        mlMOS_rt = mean(tmlMOS_rt);
        mlMOS_rh = mean(tmlMOS_rh);
        
        mlMOS_lt = mean(tmlMOS_lt);
        mlMOS_lh = mean(tmlMOS_lh);
    else
        error('stop')
    end
    if curStance == 1
        estapMosrSeg =  [estapMosrSeg; apMOS];
        estmlMOSrSeg =  [estmlMOSrSeg; max([mlMOS_rt mlMOS_rh])];
    elseif curStance == 2
        estapMoslSeg =  [estapMoslSeg; apMOS];
        estmlMOSlSeg =  [estmlMOSlSeg; max([mlMOS_lt mlMOS_lh])];
    else
        error('what now')
    end
    
    estmosEucDSeg(iStep,:) =  estmosval;
    
    
    clear p_toe_r p_heel_r p_amtoe_r p_gtoe_r p_altoe_r p_pltoe_r p_pmtoe_r p_amheel_r p_alheel_r p_plheel_r p_pmheel_r   ...
        p_toe_l p_heel_l p_amtoe_l p_gtoe_l p_altoe_l p_pltoe_l p_pmtoe_l p_amheel_l p_alheel_l p_plheel_l p_pmheel_l   ...
        p_frf  p_frh    p_flf p_flh apMOS mlMOS_rt mlMOS_rh mlMOS_lt mlMOS_lh intersectPoint mosval frontBoS_Line CoMXCoM_Line
    
    [p_toe_r, p_heel_r, p_amtoe_r, p_gtoe_r, p_altoe_r, p_pltoe_r, p_pmtoe_r, p_amheel_r, p_alheel_r, p_plheel_r, p_pmheel_r,   ...
        p_toe_l, p_heel_l, p_amtoe_l, p_gtoe_l, p_altoe_l, p_pltoe_l, p_pmtoe_l, p_amheel_l, p_alheel_l, p_plheel_l, p_pmheel_l,   ...
        p_frf  , p_frh,    p_flf,     p_flh] = ...
        p_feet( tVarRefR', tVarRefL', tVarRefRM_R, tVarRefRM_L, subSpecs.shoe_size);
    
    if plotTestGraphs
    subplot(212); plot(p_toe_r(:,ax)); hold on;plot(p_heel_r(:,ax))
    end
    
    if selSamp > 5
        resCt = 1;
        for selCt = selSampArray
            
            
            BoSposX = [p_heel_l(selCt,1)   p_amheel_l(selCt,1);   % #13 #20
                p_pmtoe_l(selCt,1)  p_gtoe_l(selCt,1);     % #17 #15
                p_gtoe_r(selCt,1)   p_pmtoe_r(selCt,1);    % #4  #6
                p_amheel_r(selCt,1) p_heel_r(selCt,1)  ];  % #9  #2
            
            BoSposY = [p_heel_l(selCt,2)   p_amheel_l(selCt,2);   % #13 #20
                p_pmtoe_l(selCt,2)  p_gtoe_l(selCt,2);     % #17 #15
                p_gtoe_r(selCt,2)   p_pmtoe_r(selCt,2);    % #4  #6
                p_amheel_r(selCt,2) p_heel_r(selCt,2)  ];  % #9  #2
            
            backLineX  = [BoSposX(1,1) BoSposX(4,2)]; %LH %RH
            backLineY  = [BoSposY(1,1) BoSposY(4,2)];
            frontLineX = [BoSposX(2,2) BoSposX(3,1)]; %LF %RF
            frontLineY = [BoSposY(2,2) BoSposY(3,1)];
            
            
            mosval = distancePointToLineSegment(tVarRefX(selCt,1:2),[frontLineX(1) frontLineY(1)], [frontLineX(2) frontLineY(2)]);
            
            frontBoS_Line = [frontLineX; frontLineY];
            CoMXCoM_Line  = [tVarRefP(selCt,1) tVarRefX(selCt,1);
                tVarRefP(selCt,2) tVarRefX(selCt,2) ];
            
            intersectPoint = findIntersectionPoint(frontBoS_Line, CoMXCoM_Line );
            
            Rref(iStep,:) = isPointOnLine(tVarRefP(selCt,1:2), tVarRefX(selCt,1:2), intersectPoint);
            
            
            if curStance == 1 %rightleg stance
                tapMOS(resCt,:) = (p_toe_r(selCt,1) - tVarRefX(selCt,1));
            elseif curStance == 2
                tapMOS(resCt,:) = (p_toe_l(selCt,1) - tVarRefX(selCt,1));
            end
            
            tmlMOS_rt(resCt,:) = distancePointToLineSegment(p_altoe_r(selCt,1:2),tVarRefP(selCt,1:2), tVarRefX(selCt,1:2));
            tmlMOS_rh(resCt,:) = distancePointToLineSegment(p_plheel_r(selCt,1:2),tVarRefP(selCt,1:2), tVarRefX(selCt,1:2));
            
            tmlMOS_lt(resCt,:) = distancePointToLineSegment(p_altoe_l(selCt,1:2),tVarRefP(selCt,1:2), tVarRefX(selCt,1:2));
            tmlMOS_lh(resCt,:) = distancePointToLineSegment(p_plheel_l(selCt,1:2),tVarRefP(selCt,1:2), tVarRefX(selCt,1:2));
            resCt = resCt + 1;
        end
        
        apMOS = mean(tapMOS);
        mlMOS_rt = mean(tmlMOS_rt);
        mlMOS_rh = mean(tmlMOS_rh);
        
        mlMOS_lt = mean(tmlMOS_lt);
        mlMOS_lh = mean(tmlMOS_lh);
    else
        error('stop')
    end
    
    if curStance == 1 %rightleg stance
        refapMosrSeg =  [refapMosrSeg; apMOS];
        refmlMOSrSeg =  [refmlMOSrSeg; max([mlMOS_rt mlMOS_rh])];
        
    elseif curStance == 2 %leftleg stance
        refapMoslSeg =  [refapMoslSeg; apMOS];
        refmlMOSlSeg =  [refmlMOSlSeg; max([mlMOS_lt mlMOS_lh])];
    else
        error('what now')
    end
    
    refmosEucDSeg(iStep,:) =  mosval;
    
    
end

if plotTestGraphs
    figure; plot(Rest); hold on; plot(Rref)
    figure; plot(estapMosrSeg); hold on; plot(refapMosrSeg)
    figure; plot(estapMoslSeg); hold on; plot(refapMoslSeg)
    
end
%%
CumTypeWiseXCSeg.Ref.(usName).(typeStr) = [CumTypeWiseXCSeg.Ref.(usName).(typeStr); ...
    refXcomSeg];
CumTypeWiseXCSeg.Est.(usName).(typeStr) = [CumTypeWiseXCSeg.Est.(usName).(typeStr); ...
    estXcomSeg];



switch affFoot
    case 'Right'
        
        CumTypeWiseMoS.APA.Ref.(usName).(typeStr) = [CumTypeWiseMoS.APA.Ref.(usName).(typeStr); ...
            refapMosrSeg];
        CumTypeWiseMoS.APA.Est.(usName).(typeStr) = [CumTypeWiseMoS.APA.Est.(usName).(typeStr); ...
            estapMosrSeg];
        
        CumTypeWiseMoS.APN.Ref.(usName).(typeStr) = [CumTypeWiseMoS.APN.Ref.(usName).(typeStr); ...
            refapMoslSeg];
        CumTypeWiseMoS.APN.Est.(usName).(typeStr) = [CumTypeWiseMoS.APN.Est.(usName).(typeStr); ...
            estapMoslSeg];
        
        CumTypeWiseMoS.MLA.Ref.(usName).(typeStr) = [CumTypeWiseMoS.MLA.Ref.(usName).(typeStr); ...
            refmlMOSrSeg];
        CumTypeWiseMoS.MLA.Est.(usName).(typeStr) = [CumTypeWiseMoS.MLA.Est.(usName).(typeStr); ...
            estmlMOSrSeg];
        
        CumTypeWiseMoS.MLN.Ref.(usName).(typeStr) = [CumTypeWiseMoS.MLN.Ref.(usName).(typeStr); ...
            refmlMOSlSeg];
        CumTypeWiseMoS.MLN.Est.(usName).(typeStr) = [CumTypeWiseMoS.MLN.Est.(usName).(typeStr); ...
            estmlMOSlSeg];
        
    case 'Left'
        
        CumTypeWiseMoS.APA.Ref.(usName).(typeStr) = [CumTypeWiseMoS.APA.Ref.(usName).(typeStr); ...
            refapMoslSeg];
        CumTypeWiseMoS.APA.Est.(usName).(typeStr) = [CumTypeWiseMoS.APA.Est.(usName).(typeStr); ...
            estapMoslSeg];
        
        CumTypeWiseMoS.APN.Ref.(usName).(typeStr) = [CumTypeWiseMoS.APN.Ref.(usName).(typeStr); ...
            refapMosrSeg];
        CumTypeWiseMoS.APN.Est.(usName).(typeStr) = [CumTypeWiseMoS.APN.Est.(usName).(typeStr); ...
            estapMosrSeg];
        
        
        CumTypeWiseMoS.MLA.Ref.(usName).(typeStr) = [CumTypeWiseMoS.MLA.Ref.(usName).(typeStr); ...
            refmlMOSlSeg];
        CumTypeWiseMoS.MLA.Est.(usName).(typeStr) = [CumTypeWiseMoS.MLA.Est.(usName).(typeStr); ...
            estmlMOSlSeg];
        
        CumTypeWiseMoS.MLN.Ref.(usName).(typeStr) = [CumTypeWiseMoS.MLN.Ref.(usName).(typeStr); ...
            refmlMOSrSeg];
        CumTypeWiseMoS.MLN.Est.(usName).(typeStr) = [CumTypeWiseMoS.MLN.Est.(usName).(typeStr); ...
            estmlMOSrSeg];
        
        
end

