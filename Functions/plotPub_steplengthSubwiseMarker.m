%% Step lengths Bland Altman plots
% plotPub_steplengthSubwiseMarker;

%% Initial Parameters
dat1Mode = 'Compare'; % Compare or Truth
baStatsMode = 'Gaussian'; % Gaussian or Non-parametric
varNames = {'Reference','PGL', 'm'};
% BAdispOpt = {'eq';'r';'RMSE';'n'};
BAdispOptCor = {}; BAdispOptBA = {};

%%

slAFig = figure('name','SL Aff');
% BlandAltman(slRFig,CumSLR.Ref,CumSLR.Est,varNames,'Step lengths Aff',{},'corrinfo',BAdispOpt);
[~, ~,SLAstrt,baPlots] = BlandAltmanDual(slAFig,CumSLA_SW.Ref,CumSLA_SW.Est,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','b','MarkerSize',1,...
    'baStatsMode',baStatsMode,'data1Mode',dat1Mode);
hold on;
[~, ~,SLAturn] = BlandAltmanDual(slAFig,CumSLA_FL.Ref,CumSLA_FL.Est,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','r','MarkerSize',1,...
    'baStatsMode','Gaussian','data1Mode',dat1Mode);

SigDiff.SLAff_SW = ttest2(CumSLA_SW.Ref,CumSLA_SW.Est);
SigDiff.SLAff_FL = ttest2(CumSLA_FL.Ref,CumSLA_FL.Est);

OverAllMean.SLAff_Ref = [mean([CumSLA_SW.Ref; CumSLA_FL.Ref]) std([CumSLA_SW.Ref; CumSLA_FL.Ref])];
OverAllMean.SLAff_Est = [mean([CumSLA_SW.Est; CumSLA_FL.Est]) std([CumSLA_SW.Est; CumSLA_FL.Est])];

SLAs_MAD = mean(abs(CumSLA_SW.Ref - CumSLA_SW.Est));
SLAt_MAD = mean(abs(CumSLA_FL.Ref - CumSLA_FL.Est));

SLAstrLoA = [SLAstrt.differenceMean-SLAstrt.rpc SLAstrt.differenceMean+SLAstrt.rpc]*100; % cm
SLAturLoA = [SLAturn.differenceMean-SLAturn.rpc SLAturn.differenceMean+SLAturn.rpc]*100; % cm

strCorP = SLAstrt.corrP <= 0.05;
strCor = [num2str(round(SLAstrt.r,2)) char(strCorP* '*')];

turCorP = SLAturn.corrP <= 0.05;
turCor = [num2str(round(SLAturn.r,2)) char(turCorP* '*')];

strRms = num2str(round(SLAstrt.RMSE*100,2));
turRms = num2str(round(SLAturn.RMSE*100,2));



for usCt = 1:length(usList)
    set(slAFig,'CurrentAxes',baPlots{1})
    plot(CumSubSLA_SW.Ref{usCt},CumSubSLA_SW.Est{usCt},markerCell{usCt},'Color','b','LineWidth',2,'MarkerSize',10);
    plot(CumSubSLA_FL.Ref{usCt},CumSubSLA_FL.Est{usCt},markerCell{usCt},'Color','r','LineWidth',2,'MarkerSize',10)
    xlim([0.2 0.8]); ylim([0.2 0.8])
    
    set(slAFig,'CurrentAxes',baPlots{2})
    plot((CumSubSLA_SW.Ref{usCt}+CumSubSLA_SW.Est{usCt})/2,CumSubSLA_SW.Est{usCt}-CumSubSLA_SW.Ref{usCt},markerCell{usCt},'Color','b','LineWidth',2,'MarkerSize',10);
    plot((CumSubSLA_FL.Ref{usCt}+CumSubSLA_FL.Est{usCt})/2,CumSubSLA_FL.Est{usCt}-CumSubSLA_FL.Ref{usCt},markerCell{usCt},'Color','r','LineWidth',2,'MarkerSize',10);
    
    
    xlim2 = 0.65;
    
    xlim([0.2 xlim2]); ylim([-0.3 0.3])
    fontsize = 20;
    if SLAstrt.differenceMeanP < 0.05
        text(xlim2,SLAstrt.differenceMean+0.02,[mynum2str(SLAstrt.differenceMean,2) '*'],'HorizontalAlignment','left','VerticalAlignment','middle','fontsize',fontsize,'FontWeight','bold');
    else
        text(xlim2,SLAstrt.differenceMean+0.02,[mynum2str(SLAstrt.differenceMean,2) ],'HorizontalAlignment','left','VerticalAlignment','middle','fontsize',fontsize,'FontWeight','bold');
    end
    if SLAturn.differenceMeanP < 0.05
        text(xlim2,SLAturn.differenceMean-0.02,[mynum2str(SLAturn.differenceMean,2) '*'],'HorizontalAlignment','left','VerticalAlignment','middle','fontsize',fontsize,'FontWeight','bold');
    else
        text(xlim2,SLAturn.differenceMean-0.02,[mynum2str(SLAturn.differenceMean,2) ],'HorizontalAlignment','left','VerticalAlignment','middle','fontsize',fontsize,'FontWeight','bold');
    end
    
    
end

dim = [0.14 0.58 0.3 0.3];
str = ['Steady state, r:' strCor ', RMSE: ' strRms ' cm'  newline 'Other, r:' turCor ', RMSE: ' turRms ' cm'];
anH = annotation('textbox',dim,'String',str,'FitBoxToText','on');
anH.FontSize = 20; anH.FontWeight = 'bold'; anH.BackgroundColor = 'white';

f=get(gca,'Children');
legend([f(27),f(21)],'Steady state','Other')
% if strcmpi(f(27).DisplayName,'Steady state') && strcmpi(f(21).DisplayName,'Other')
%
% else
%     error('not handled')
% end
%%
slNFig = figure('name','SL Less Aff');
% BlandAltman(slLFig,CumSLL.Ref,CumSLL.Est,varNames,'Step lengths Less Aff',{},'corrinfo',BAdispOpt);
[~, ~,SLNstrt,baPlotsN] = BlandAltmanDual(slNFig,CumSLN_SW.Ref,CumSLN_SW.Est,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','b','MarkerSize',1,...
    'baStatsMode',baStatsMode,'data1Mode',dat1Mode);
hold on;
[~, ~,SLNturn] = BlandAltmanDual(slNFig,CumSLN_FL.Ref,CumSLN_FL.Est,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','r','MarkerSize',1,...
    'baStatsMode','Gaussian','data1Mode',dat1Mode);

SigDiff.SLNAff_SW = ttest2(CumSLN_SW.Ref,CumSLN_SW.Est);
SigDiff.SLNAff_FL = ttest2(CumSLN_FL.Ref,CumSLN_FL.Est);

OverAllMean.SLNAff_Ref = [mean([CumSLN_SW.Ref; CumSLN_FL.Ref]) std([CumSLN_SW.Ref; CumSLN_FL.Ref])];
OverAllMean.SLNAff_Est = [mean([CumSLN_SW.Est; CumSLN_FL.Est]) std([CumSLN_SW.Est; CumSLN_FL.Est])];

SLNs_MAD = mean(abs(CumSLN_SW.Ref - CumSLN_SW.Est));
SLNt_MAD = mean(abs(CumSLN_FL.Ref - CumSLN_FL.Est));

SLNstrLoA = [SLNstrt.differenceMean-SLNstrt.rpc SLNstrt.differenceMean+SLNstrt.rpc]*100; % cm
SLNturLoA = [SLNturn.differenceMean-SLNturn.rpc SLNturn.differenceMean+SLNturn.rpc]*100; % cm

strCorP = SLNstrt.corrP <= 0.05;
strCor = [num2str(round(SLNstrt.r,2)) char(strCorP* '*')];

turCorP = SLNturn.corrP <= 0.05;
turCor = [num2str(round(SLNturn.r,2)) char(turCorP* '*')];

strRms = num2str(round(SLNstrt.RMSE*100,2));
turRms = num2str(round(SLNturn.RMSE*100,2));


for usCt = 1:length(usList)
    set(slNFig,'CurrentAxes',baPlotsN{1})
    plot(CumSubSLN_SW.Ref{usCt},CumSubSLN_SW.Est{usCt},markerCell{usCt},'Color','b','LineWidth',2,'MarkerSize',10); hold on;
    plot(CumSubSLN_FL.Ref{usCt},CumSubSLN_FL.Est{usCt},markerCell{usCt},'Color','r','LineWidth',2,'MarkerSize',10)
    
%     plot(mean(CumSubSLN_SW.Ref{usCt}),mean(CumSubSLN_SW.Est{usCt}),markerCell{usCt},'Color','b','LineWidth',4);
%     plot(mean(CumSubSLN_FL.Ref{usCt}),mean(CumSubSLN_FL.Est{usCt}),markerCell{usCt},'Color','r','LineWidth',4)
    xlim([0.2 0.8]); ylim([0.2 0.8])
    
    set(slNFig,'CurrentAxes',baPlotsN{2})
    plot((CumSubSLN_SW.Ref{usCt}+CumSubSLN_SW.Est{usCt})/2,CumSubSLN_SW.Est{usCt}-CumSubSLN_SW.Ref{usCt},markerCell{usCt},'Color','b','LineWidth',2,'MarkerSize',10);
    plot((CumSubSLN_FL.Ref{usCt}+CumSubSLN_FL.Est{usCt})/2,CumSubSLN_FL.Est{usCt}-CumSubSLN_FL.Ref{usCt},markerCell{usCt},'Color','r','LineWidth',2,'MarkerSize',10);
    
    
    xlim2 = 0.65;
    
    xlim([0.2 xlim2]); ylim([-0.3 0.3])
    fontsize = 20;
    if SLNstrt.differenceMeanP < 0.05
        text(xlim2,SLNstrt.differenceMean+0.02,[mynum2str(SLNstrt.differenceMean,2) '*'],'HorizontalAlignment','left','VerticalAlignment','middle','fontsize',fontsize,'FontWeight','bold');
    else
        text(xlim2,SLNstrt.differenceMean+0.02,[mynum2str(SLNstrt.differenceMean,2) ],'HorizontalAlignment','left','VerticalAlignment','middle','fontsize',fontsize,'FontWeight','bold');
    end
    if SLNturn.differenceMeanP < 0.05
        text(xlim2,SLNturn.differenceMean-0.02,[mynum2str(SLNturn.differenceMean,2) '*'],'HorizontalAlignment','left','VerticalAlignment','middle','fontsize',fontsize,'FontWeight','bold');
    else
        text(xlim2,SLNturn.differenceMean-0.02,[mynum2str(SLNturn.differenceMean,2) ],'HorizontalAlignment','left','VerticalAlignment','middle','fontsize',fontsize,'FontWeight','bold');
    end
end

dim = [0.14 0.58 0.3 0.3];
str = ['Steady state, r:' strCor ', RMSE: ' strRms ' cm'  newline 'Other, r:' turCor ', RMSE: ' turRms ' cm'];
anH = annotation('textbox',dim,'String',str,'FitBoxToText','on');
anH.FontSize = 20; anH.FontWeight = 'bold'; anH.BackgroundColor = 'white';

f=get(gca,'Children');
legend([f(27),f(21)],'Steady state','Other')
% if strcmpi(f(27).DisplayName,'Steady state') && strcmpi(f(21).DisplayName,'Other')
%
%     legend([f(27),f(21)],'Steady state','Other')
% else
%     error('not handled')
% end