function n=normh(X)
% n=normh(X);
% Calculates norm of every row of matrix.
% Useful for calculating the absolute acceleration from
% a accerleration vector.
% Henk Luinge 24-8-98
% n=sqrt(sum((X.*X)')');
%
% Dirk Weenk 2011-09-09
n=sqrt(sum(X.*X,2)); % (sum(A,2) calculates the sum of each row)