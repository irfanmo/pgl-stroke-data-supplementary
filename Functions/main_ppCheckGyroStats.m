% Group Measure the CoM vel diff
% main_ppCheckGyroStats

% close all;
clearvars -except IMUProcessedData

CumGyroStat.RF = [];
CumGyroStat.LF = [];
CumGyroStat.PS = [];
CumGyroStat.LenList = [];

CumContactDS = [];
CumContactZV = [];
CumContactRF = [];

PosEstimatesRef.COM.Z = [];
PosEstimatesEst.COM.Z = [];

strCt = 1;
%%
usList = fieldnames(IMUProcessedData)   ;
usList = {'us3'};
for usCt = 1:length(usList)
    usName = usList{usCt};
    
    trialList = fieldnames(IMUProcessedData.(usName));
    for trCt = 1: length(trialList) %
        trName = trialList{trCt};
        
        
        estDS = IMUProcessedData.(usName).(trName).EstData;
        refDS = IMUProcessedData.(usName).(trName).RefData;
        
        gyroStat.RF = estDS.IMUdata{1}.gyroStat';
        gyroStat.LF = estDS.IMUdata{2}.gyroStat';
        gyroStat.PS = estDS.IMUdata{3}.gyroStat';
        
        contactDoS = estDS.IMUContacts.contactDS;
        contactZeV = estDS.IMUContacts.contactZV;
        contactReF = refDS.Body.contact;
        
        PosEstimatesRef.COM.Z = [PosEstimatesRef.COM.Z; refDS.cgFrame.CoM.pos(:,3)];
%         PosEstimatesEst.COM.Z = [PosEstimatesEst.COM.Z; refDS.cgFrame.CoM.pos(:,3)];
        
        CumGyroStat.RF = [CumGyroStat.RF; gyroStat.RF];
        CumGyroStat.LF = [CumGyroStat.LF; gyroStat.LF];
        CumGyroStat.PS = [CumGyroStat.PS; gyroStat.PS];
        
        CumContactDS = [CumContactDS; contactDoS];
        CumContactZV = [CumContactZV; contactZeV];
        CumContactRF = [CumContactRF; contactReF];
        
        CumGyroStat.LenList = [CumGyroStat.LenList; length(gyroStat.PS)];
        CumGyroStat.StrList{strCt,:} = [usName ' ' trName]; strCt = strCt+1;
        
    end
    
    
end

%% Summarizing trial type wise
tcumSize = cumsum(CumGyroStat.LenList);
cumSize = [1; tcumSize(1:end-1)];
figure;
plot(normh(CumGyroStat.PS));
for ilen = 1:length(cumSize)
    curInd = cumSize(ilen);
    hline = line([curInd curInd],[0 0.2]);
    hline.Color = 'black';
    text(curInd+2,0.1,num2str(ilen))
    
    
end
ylim([0 0.16])



%% contacts

%  tcumSize = cumsum(CumGyroStat.LenList);
%             cumSize = [1; tcumSize(1:end-1)];
figure;
plot(CumContactDS(:,1)); hold on;
plot(CumContactRF(:,1)); hold on;
plot(CumContactZV(:,1)); hold on;

for ilen = 1:length(cumSize)
    curInd = cumSize(ilen);
    hline = line([curInd curInd],[0 5]);
    hline.Color = 'black';
    text(curInd+2,0.1,num2str(ilen))
    
    
end
ylim([0 1.01])

%% CoM Height 

%  tcumSize = cumsum(CumGyroStat.LenList);
%             cumSize = [1; tcumSize(1:end-1)];
figure;
plot(PosEstimatesRef.COM.Z); hold on;
% plot(CumContactRF(:,1)); hold on;
% plot(CumContactZV(:,1)); hold on;

for ilen = 1:length(cumSize)
    curInd = cumSize(ilen);
    hline = line([curInd curInd],[0 5]);
    hline.Color = 'black';
    text(curInd+2,0.1,num2str(ilen))
    
    
end
ylim([0 1.01])





