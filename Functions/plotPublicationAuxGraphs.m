figure;

refCoMHt = refDatacg.CoM.pos(:,3);
estCoMHt = estimData.CoM.pos(:,3);

startInd = IMUdata{3}.indstartWalking;
stopInd  = IMUdata{3}.indstopWalking;

plot(resampleTime,estCoMHt,'LineWidth',2); hold on
plot(resampleTime,refCoMHt,'--','LineWidth',2)

xlim([startInd/100 stopInd/100])

curAx = gca;
curAx.Position = [curAx.Position(1)-0.06 curAx.Position(2)  0.91 0.85];

curAx.FontWeight = 'bold';
curAx.FontSize = 20;
legend('Estimated', 'VICON')
ylabel('CoM Height (m)')
xlabel('Time (s)')