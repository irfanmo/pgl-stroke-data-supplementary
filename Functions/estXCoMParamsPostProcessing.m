%% estXCoMParamsPostProcessing
% will break when using normal processing; needs fix!
selSamp = 0; endSamp = 1;
selSamp = 1; endSamp = 0;
%% Estimate w0 as in Hof et al. 2005
% w0 = sqrt( g / leg length)

% Calculate leg length
legLengthBodyLengthRatioMale   = 0.260 + 0.2484; % Bron: drillis et al - body segment parameters
legLengthBodyLengthRatioFemale = 0.252 + 0.227;  % Bron: drillis et al - body segment parameters

switch subSpecs.Gender
    case 'F'
        legLengthBodyLengthRatio = legLengthBodyLengthRatioFemale;
    case 'M'
        legLengthBodyLengthRatio = legLengthBodyLengthRatioMale;
    otherwise
        error('Subject gender unknown')
end

subjectLegLength = subSpecs.Height * legLengthBodyLengthRatio;
legLengthFactor  = 1; % see Hof et al.
l0=subjectLegLength;
% Calculate w0
w0 = sqrt(9.81/(legLengthFactor * subjectLegLength));

%%


strSet   = 1;
noofSets = length(segmentedIMU.stepsUsed);

for iStep = strSet:noofSets
    
    tVarR = estrfSeg{iStep};
    tVarL = estlfSeg{iStep};
    tVarP = estpsSeg{iStep};
    tVarV = segmentedIMU.CoM.vel.sets{iStep};
    tVarX = tVarP + (tVarV/w0);
    tVarRM_R = segmentedIMU.RightFoot.RotM.sets{iStep};
    tVarRM_L = segmentedIMU.LeftFoot.RotM.sets{iStep};
    
    tVarRefR = refrfSeg{iStep};
    tVarRefL = reflfSeg{iStep};
    tVarRefP = refpsSeg{iStep};
    tVarRefV = segmentedRef.CoM.vel.sets{iStep};
    tVarRefX = tVarRefP + (tVarRefV/w0);
    tVarRefRM_R = segmentedRef.RightFoot.RotM.sets{iStep};
    tVarRefRM_L = segmentedRef.LeftFoot.RotM.sets{iStep};
    
    
    
    if endSamp
        selSamp = length(tVarP);
    end
    
    estXcomSeg{iStep} = tVarX;
    refXcomSeg{iStep} = tVarRefX;
    
    %     segmentedIMU2.MoS.mag{iStep} = normh(tVarX(:,1:2) - tVarP(:,1:2));
    %     segmentedRef2.MoS.mag{iStep} = normh(tVarRefX(:,1:2) - tVarRefP(:,1:2));
    %
    %     magMoS_IMU(iStep,:) = normh(tVarX(selSamp,1:2) - tVarP(selSamp,1:2));
    %     magMoS_REF(iStep,:) = normh(tVarRefX(selSamp,1:2) - tVarRefP(selSamp,1:2));
    %
    %     headingChange = tVarX(selSamp,1:2) - tVarP(selSamp,1:2); headingChange(3)=0; theading=headingChange/norm(headingChange); % normalize
    %     normV = [1 0 0]; currV = theading; CosTheta = dot(normV,currV)/(norm(normV)*norm(currV)); ThetaInDegreesIMU = acosd(CosTheta);
    %
    %     headingChange = tVarRefX(selSamp,1:2) - tVarRefP(selSamp,1:2); headingChange(3)=0; theading=headingChange/norm(headingChange); % normalize
    %     normV = [1 0 0]; currV = theading; CosTheta = dot(normV,currV)/(norm(normV)*norm(currV)); ThetaInDegreesRef = acosd(CosTheta);
    %
    %
    %     segmentedIMU2.MoS.dir_fStep(i,:) = ThetaInDegreesIMU;
    %     segmentedRef2.MoS.dir_fStep(i,:) = ThetaInDegreesRef;
    %
    %     degMoS_IMU(i,:) = ThetaInDegreesIMU;
    %     degMoS_REF(i,:) = ThetaInDegreesRef;
    clear p_toe_r p_heel_r p_amtoe_r p_gtoe_r p_altoe_r p_pltoe_r p_pmtoe_r p_amheel_r p_alheel_r p_plheel_r p_pmheel_r   ...
        p_toe_l p_heel_l p_amtoe_l p_gtoe_l p_altoe_l p_pltoe_l p_pmtoe_l p_amheel_l p_alheel_l p_plheel_l p_pmheel_l   ...
        p_frf  p_frh    p_flf p_flh apMOS mlMOS_rt mlMOS_rh mlMOS_lt mlMOS_lh intersectPoint mosval frontBoS_Line CoMXCoM_Line
    
    % Get all shoe positions
    [p_toe_r, p_heel_r, p_amtoe_r, p_gtoe_r, p_altoe_r, p_pltoe_r, p_pmtoe_r, p_amheel_r, p_alheel_r, p_plheel_r, p_pmheel_r,   ...
        p_toe_l, p_heel_l, p_amtoe_l, p_gtoe_l, p_altoe_l, p_pltoe_l, p_pmtoe_l, p_amheel_l, p_alheel_l, p_plheel_l, p_pmheel_l,   ...
        p_frf  , p_frh,    p_flf,     p_flh] = ...
        p_feet( tVarR', tVarL', tVarRM_R, tVarRM_L, subSpecs.shoe_size);
    
    BoSposX = [p_heel_l(selSamp,1)   p_amheel_l(selSamp,1);   % #13 #20
        p_pmtoe_l(selSamp,1)  p_gtoe_l(selSamp,1);     % #17 #15
        p_gtoe_r(selSamp,1)   p_pmtoe_r(selSamp,1);    % #4  #6
        p_amheel_r(selSamp,1) p_heel_r(selSamp,1)  ];  % #9  #2
    
    BoSposY = [p_heel_l(selSamp,2)   p_amheel_l(selSamp,2);   % #13 #20
        p_pmtoe_l(selSamp,2)  p_gtoe_l(selSamp,2);     % #17 #15
        p_gtoe_r(selSamp,2)   p_pmtoe_r(selSamp,2);    % #4  #6
        p_amheel_r(selSamp,2) p_heel_r(selSamp,2)  ];  % #9  #2
    
    backLineX  = [BoSposX(1,1) BoSposX(4,2)]; %LH %RH
    backLineY  = [BoSposY(1,1) BoSposY(4,2)];
    frontLineX = [BoSposX(2,2) BoSposX(3,1)]; %LF %RF
    frontLineY = [BoSposY(2,2) BoSposY(3,1)];
    
    mosval = distancePointToLineSegment(tVarX(selSamp,1:2),[frontLineX(1) frontLineY(1)], [frontLineX(2) frontLineY(2)]);
    
    frontBoS_Line = [frontLineX; frontLineY];
    CoMXCoM_Line  = [tVarP(selSamp,1) tVarX(selSamp,1);
        tVarP(selSamp,2) tVarX(selSamp,2) ];
    
    
    intersectPoint = findIntersectionPoint(frontBoS_Line, CoMXCoM_Line );
    
    
    apMOS = normh(intersectPoint - tVarX(selSamp,1:2));
    mlMOS_rt = distancePointToLineSegment(p_altoe_r(selSamp,1:2),tVarP(selSamp,1:2), tVarX(selSamp,1:2));
    mlMOS_rh = distancePointToLineSegment(p_plheel_r(selSamp,1:2),tVarP(selSamp,1:2), tVarX(selSamp,1:2));
    
    mlMOS_lt = distancePointToLineSegment(p_altoe_l(selSamp,1:2),tVarP(selSamp,1:2), tVarX(selSamp,1:2));
    mlMOS_lh = distancePointToLineSegment(p_plheel_l(selSamp,1:2),tVarP(selSamp,1:2), tVarX(selSamp,1:2));
    
    estapMosSeg(iStep,:) =  apMOS;
    estmlMOSrSeg(iStep,:) =  max([mlMOS_rt mlMOS_rh]);
    estmlMOSlSeg(iStep,:) =  max([mlMOS_lt mlMOS_lh]);
    estmosEucDSeg(iStep,:) =  mosval;
    
    
    clear p_toe_r p_heel_r p_amtoe_r p_gtoe_r p_altoe_r p_pltoe_r p_pmtoe_r p_amheel_r p_alheel_r p_plheel_r p_pmheel_r   ...
        p_toe_l p_heel_l p_amtoe_l p_gtoe_l p_altoe_l p_pltoe_l p_pmtoe_l p_amheel_l p_alheel_l p_plheel_l p_pmheel_l   ...
        p_frf  p_frh    p_flf p_flh apMOS mlMOS_rt mlMOS_rh mlMOS_lt mlMOS_lh intersectPoint mosval frontBoS_Line CoMXCoM_Line
    
    [p_toe_r, p_heel_r, p_amtoe_r, p_gtoe_r, p_altoe_r, p_pltoe_r, p_pmtoe_r, p_amheel_r, p_alheel_r, p_plheel_r, p_pmheel_r,   ...
        p_toe_l, p_heel_l, p_amtoe_l, p_gtoe_l, p_altoe_l, p_pltoe_l, p_pmtoe_l, p_amheel_l, p_alheel_l, p_plheel_l, p_pmheel_l,   ...
        p_frf  , p_frh,    p_flf,     p_flh] = ...
        p_feet( tVarRefR', tVarRefL', tVarRefRM_R, tVarRefRM_L, subSpecs.shoe_size);
    
    BoSposX = [p_heel_l(selSamp,1)   p_amheel_l(selSamp,1);   % #13 #20
        p_pmtoe_l(selSamp,1)  p_gtoe_l(selSamp,1);     % #17 #15
        p_gtoe_r(selSamp,1)   p_pmtoe_r(selSamp,1);    % #4  #6
        p_amheel_r(selSamp,1) p_heel_r(selSamp,1)  ];  % #9  #2
    
    BoSposY = [p_heel_l(selSamp,2)   p_amheel_l(selSamp,2);   % #13 #20
        p_pmtoe_l(selSamp,2)  p_gtoe_l(selSamp,2);     % #17 #15
        p_gtoe_r(selSamp,2)   p_pmtoe_r(selSamp,2);    % #4  #6
        p_amheel_r(selSamp,2) p_heel_r(selSamp,2)  ];  % #9  #2
    
    backLineX  = [BoSposX(1,1) BoSposX(4,2)]; %LH %RH
    backLineY  = [BoSposY(1,1) BoSposY(4,2)];
    frontLineX = [BoSposX(2,2) BoSposX(3,1)]; %LF %RF
    frontLineY = [BoSposY(2,2) BoSposY(3,1)];
    
    mosval = distancePointToLineSegment(tVarX(selSamp,1:2),[frontLineX(1) frontLineY(1)], [frontLineX(2) frontLineY(2)]);
    
    frontBoS_Line = [frontLineX; frontLineY];
    CoMXCoM_Line  = [tVarP(selSamp,1) tVarX(selSamp,1);
        tVarP(selSamp,2) tVarX(selSamp,2) ];
    
    
    intersectPoint = findIntersectionPoint(frontBoS_Line, CoMXCoM_Line );
    
    
    apMOS = normh(intersectPoint - tVarX(selSamp,1:2));
    mlMOS_rt = distancePointToLineSegment(p_altoe_r(selSamp,1:2),tVarP(selSamp,1:2), tVarX(selSamp,1:2));
    mlMOS_rh = distancePointToLineSegment(p_plheel_r(selSamp,1:2),tVarP(selSamp,1:2), tVarX(selSamp,1:2));
    
    mlMOS_lt = distancePointToLineSegment(p_altoe_l(selSamp,1:2),tVarP(selSamp,1:2), tVarX(selSamp,1:2));
    mlMOS_lh = distancePointToLineSegment(p_plheel_l(selSamp,1:2),tVarP(selSamp,1:2), tVarX(selSamp,1:2));
    
    
    curStance = segmentedIMU.CoM.stepsUsed(iStep);
    if curStance == 1 && strcmpi(affFoot,'Right')
        refapMosSeg(iStep,:) =  apMOS;
    elseif curStance == 2
        
    end
        
    
    refapMosSeg(iStep,:) =  apMOS;
    refmlMOSrSeg(iStep,:) =  max([mlMOS_rt mlMOS_rh]);
    refmlMOSlSeg(iStep,:) =  max([mlMOS_lt mlMOS_lh]);
    refmosEucDSeg(iStep,:) =  mosval;
    
    
end
%%
CumTypeWiseXCSeg.Ref.(usName).(typeStr) = [CumTypeWiseXCSeg.Ref.(usName).(typeStr); ...
    refXcomSeg];
CumTypeWiseXCSeg.Est.(usName).(typeStr) = [CumTypeWiseXCSeg.Est.(usName).(typeStr); ...
    estXcomSeg];

CumTypeWiseMoS.AP.Ref.(usName).(typeStr) = [CumTypeWiseMoS.AP.Ref.(usName).(typeStr); ...
    refapMosSeg];
CumTypeWiseMoS.AP.Est.(usName).(typeStr) = [CumTypeWiseMoS.AP.Est.(usName).(typeStr); ...
    estapMosSeg];

switch affFoot
    case 'Right'
        
        CumTypeWiseMoS.MLA.Ref.(usName).(typeStr) = [CumTypeWiseMoS.MLA.Ref.(usName).(typeStr); ...
            refmlMOSrSeg];
        CumTypeWiseMoS.MLA.Est.(usName).(typeStr) = [CumTypeWiseMoS.MLA.Est.(usName).(typeStr); ...
            estmlMOSrSeg];
        
        CumTypeWiseMoS.MLN.Ref.(usName).(typeStr) = [CumTypeWiseMoS.MLN.Ref.(usName).(typeStr); ...
            refmlMOSlSeg];
        CumTypeWiseMoS.MLN.Est.(usName).(typeStr) = [CumTypeWiseMoS.MLN.Est.(usName).(typeStr); ...
            estmlMOSlSeg];
        
    case 'Left'
        
        CumTypeWiseMoS.MLA.Ref.(usName).(typeStr) = [CumTypeWiseMoS.MLA.Ref.(usName).(typeStr); ...
            refmlMOSlSeg];
        CumTypeWiseMoS.MLA.Est.(usName).(typeStr) = [CumTypeWiseMoS.MLA.Est.(usName).(typeStr); ...
            estmlMOSlSeg];
        
        CumTypeWiseMoS.MLN.Ref.(usName).(typeStr) = [CumTypeWiseMoS.MLN.Ref.(usName).(typeStr); ...
            refmlMOSrSeg];
        CumTypeWiseMoS.MLN.Est.(usName).(typeStr) = [CumTypeWiseMoS.MLN.Est.(usName).(typeStr); ...
            estmlMOSrSeg];
        
        
end

