segCurrentRef      = fun_splitandTransformStepWise(refData,stepsWithFeetInfoRef);

segCurrentOp = fun_normCompareStepWise(segCurrentIMU,segCurrentRef);

segFields = fieldnames(segmentedRef2);
selectBodySeg = {'RightFoot','LeftFoot','CoM'};



for iSeg = 1:length(segFields)
    if any(strcmpi(segFields{iSeg},selectBodySeg))
        
        segmentedRef2.(segFields{iSeg}).pos.sets = [segmentedRef2.(segFields{iSeg}).pos.sets; segCurrentRef.(segFields{iSeg}).pos.sets];
        segmentedRef2.(segFields{iSeg}).HeadingArrayPS = [segmentedRef2.(segFields{iSeg}).HeadingArrayPS; segCurrentRef.(segFields{iSeg}).HeadingArrayPS];
        segmentedIMU2.(segFields{iSeg}).pos.sets = [segmentedIMU2.(segFields{iSeg}).pos.sets; segCurrentIMU.(segFields{iSeg}).pos.sets];
%         segmentedIMU2.(segFields{iSeg}).HeadingArrayPS = [segmentedIMU2.(segFields{iSeg}).HeadingArrayPS; segCurrentIMU.(segFields{iSeg}).HeadingArrayPS];
        
    end
end

segmentedRef2.stepsUsed = [segmentedRef2.stepsUsed; segCurrentRef.stepsUsed];
segmentedIMU2.stepsUsed = [segmentedIMU2.stepsUsed; segCurrentIMU.stepsUsed];



segFields = fieldnames(segmentedOp);
for iSeg = 1:length(segFields)
    contFields = fieldnames(segmentedOp.(segFields{iSeg}));
    for iCont = 1:length(contFields)
    
     segmentedOp.(segFields{iSeg}).(contFields{iCont})= [segmentedOp.(segFields{iSeg}).(contFields{iCont}); segCurrentOp.(segFields{iSeg}).(contFields{iCont})];
    end
end