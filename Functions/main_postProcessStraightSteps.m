% Group Process tthe best outputs only straight steps, no turns


% close all;
clearvars -except IMUProcessedData

usList = fieldnames(IMUProcessedData)   ;


stepLengthRefSet = [];
stepWidthRefSet = [];
stepLengthIMUSet = [];
stepWidthIMUSet = [];
stepLengthwoZMPSet = [];
stepWidthwoZMPSet = [];

CoMLenRefSet  = [];
CoMWidRefSet  = [];
CoMLenIMUSet  = [];
CoMWidIMUSet  = [];
CoMLenwoZMPSet  = [];
CoMWidwoZMPSet  = [];


for usCt = 1:length(usList)
    usName = usList{usCt};
    
    trialList = fieldnames(IMUProcessedData.(usName));
    trialList = {'tr41','tr42','tr43', 'tr11','tr13','tr14',...
        'tr21','tr22','tr23','tr24','tr32','tr33','tr34'};
    %     trialList = {'tr51','tr52','tr53','tr54', 'tr62','tr63','tr64'};
    for trCt = 1: length(trialList) %
        trName = trialList{trCt};
        
        estDS = IMUProcessedData.(usName).(trName).EstData;
        refDS = IMUProcessedData.(usName).(trName).RefData;
        
        imuContact = estDS.IMUContacts.contactZV;
        refContact = refDS.Body.contact;
        
        refRFpos = refDS.RightFoot.IMUpos;
        refLFpos = refDS.LeftFoot.IMUpos;
        refPFpos = refDS.CoM.pos;
        
        refData.RightFoot.pos = refRFpos;
        refData.LeftFoot.pos  = refLFpos;
        refData.CoM.pos       = refPFpos;
        
        
        refDatacg.RightFoot.pos = refDS.cgFrame.RightFoot.IMUpos;
        refDatacg.LeftFoot.pos  = refDS.cgFrame.LeftFoot.IMUpos;
        refDatacg.CoM.pos       = refDS.cgFrame.CoM.pos;
        
        refDatacg.RightFoot.vel = refDS.cgFrame.RightFoot.IMUvel;
        refDatacg.LeftFoot.vel  = refDS.cgFrame.LeftFoot.IMUvel;
        refDatacg.CoM.vel       = refDS.cgFrame.CoM.vel;
        
        comboUsed = 'rval_4';
        
        estimStatesR = estDS.IMUdata{1}.stateIndex.allIMU;
        estimStatesL = estDS.IMUdata{2}.stateIndex.allIMU;
        estimStatesP = estDS.IMUdata{3}.stateIndex.allIMU;
        
        estimData.RightFoot.pos = estDS.Rcombo.(comboUsed).xMat(estimStatesR(1:3),:)';
        estimData.LeftFoot.pos  = estDS.Rcombo.(comboUsed).xMat(estimStatesL(1:3),:)';
        estimData.CoM.pos       = estDS.Rcombo.(comboUsed).xMat(estimStatesP(1:3),:)';
        
        estimDwoZMP.RightFoot.pos = estDS.wozmp.x(estimStatesR(1:3),:)';
        estimDwoZMP.LeftFoot.pos  = estDS.wozmp.x(estimStatesL(1:3),:)';
        estimDwoZMP.CoM.pos    = estDS.wozmp.x(estimStatesP(1:3),:)';
        
        
        [imustepsR, imustepsL] = fun_findSteps(imuContact);
        [refstepsR, refstepsL] = fun_findSteps(refContact);
        
        
        [imustepsRL] = fun_findChronologicalSteps(imuContact);
        [refstepsRL] = fun_findChronologicalSteps(refContact);
        
        startStep = 4; removeEndSteps = 2;
        imustepsRLsnip = imustepsRL(startStep:end-removeEndSteps,:);
        refstepsRLsnip = refstepsRL(startStep:end-removeEndSteps,:);
        
        
        stepsWithFeetInfoRefus = [refstepsR(3:end-2,:) ones(length(refstepsR(3:end-2,:)),1);
            refstepsL(2:end,:) 2*ones(length(refstepsL(2:end,:)),1)];
        
        stepsWithFeetInfoIMUus = [imustepsR(3:end-2,:) ones(length(imustepsR(3:end-2,:)),1);
            imustepsL(2:end,:) 2*ones(length(imustepsL(2:end,:)),1)];
        
        
        %         [~,idx] = sort(stepsWithFeetInfoRefus(:,1)); % sort just the first column
        %         stepsWithFeetInfoRef = stepsWithFeetInfoRefus(idx,:);   % sort the whole matrix using the sort indices
        
        %         [~,idx] = sort(stepsWithFeetInfoIMUus(:,1)); % sort just the first column
        %         stepsWithFeetInfoIMU = stepsWithFeetInfoIMUus(idx,:);   % sort the whole matrix using the sort indices
        
        stepsWithFeetInfoRef = refstepsRLsnip;   % sort the whole matrix using the sort indices
        stepsWithFeetInfoIMU = imustepsRLsnip;   % sort the whole matrix using the sort indices
        
        
        
        
        [stepLengthsRef, stepWidthsRef, comLenRef, comWidRef] = fun_findStepWiseSpatioTempParam(refData,stepsWithFeetInfoRef);
        [stepLengthsIMU, stepWidthsIMU, comLenIMU, comWidIMU] = fun_findStepWiseSpatioTempParam(estimData,stepsWithFeetInfoIMU);
        [stepLengthswoZMP, stepWidthswoZMP, comLenwoZMP, comWidwoZMP] = fun_findStepWiseSpatioTempParam(estimDwoZMP,stepsWithFeetInfoIMU);
        
        
        
        
        headingDir = estDS.cgVar.headInd;
        
        straightSteps = []; strStepsInd = [];
        if length(headingDir) == length(stepsWithFeetInfoIMU)
            strStepsInd = find(headingDir(:,4)<39);
            
        end
        startStep = 1; removeEndSteps = 1;
        
        stepLengthsRefstr   = stepLengthsRef(strStepsInd(startStep:end-removeEndSteps),1);
        stepWidthsRefstr    = stepWidthsRef(strStepsInd(startStep:end-removeEndSteps),1);
        stepLengthsIMUstr   = stepLengthsIMU(strStepsInd(startStep:end-removeEndSteps),1) ;
        stepWidthsIMUstr    = stepWidthsIMU(strStepsInd(startStep:end-removeEndSteps),1);
        stepLengthswoZMPstr = stepLengthswoZMP(strStepsInd(startStep:end-removeEndSteps),1);
        stepWidthswoZMPstr  = stepWidthswoZMP(strStepsInd(startStep:end-removeEndSteps),1);
        
        comLenRefstr   = comLenRef(strStepsInd(startStep:end-removeEndSteps),1);
        comWidRefstr    = comWidRef(strStepsInd(startStep:end-removeEndSteps),1);
        comLenIMUstr   = comLenIMU(strStepsInd(startStep:end-removeEndSteps),1) ;
        comWidIMUstr    = comWidIMU(strStepsInd(startStep:end-removeEndSteps),1);
        comLenwoZMPstr = comLenwoZMP(strStepsInd(startStep:end-removeEndSteps),1);
        comWidwoZMPstr  = comWidwoZMP(strStepsInd(startStep:end-removeEndSteps),1);
        
        stepLengthRefSet = [stepLengthRefSet; stepLengthsRefstr];
        stepWidthRefSet = [stepWidthRefSet; stepWidthsRefstr];
        stepLengthIMUSet = [stepLengthIMUSet; stepLengthsIMUstr];
        stepWidthIMUSet = [stepWidthIMUSet; stepWidthsIMUstr];
        stepLengthwoZMPSet = [stepLengthwoZMPSet; stepLengthswoZMPstr];
        stepWidthwoZMPSet = [stepWidthwoZMPSet; stepWidthswoZMPstr];
        
        
        
        CoMLenRefSet  = [CoMLenRefSet; comLenRefstr];
        CoMWidRefSet  = [CoMWidRefSet; comWidRefstr];
        CoMLenIMUSet  = [CoMLenIMUSet; comLenIMUstr];
        CoMWidIMUSet  = [CoMWidIMUSet; comWidIMUstr];
        CoMLenwoZMPSet  = [CoMLenwoZMPSet; comLenwoZMPstr];
        CoMWidwoZMPSet  = [CoMWidwoZMPSet; comWidwoZMPstr];
        
        
    end
    
    
end

%%
meanStepLengthRef   = [mean(stepLengthRefSet)   std(stepLengthRefSet)];
meanStepWidthsRef   = [mean(stepWidthRefSet)    std(stepWidthRefSet)];
meanStepLengthIMU   = [mean(stepLengthIMUSet)   std(stepLengthIMUSet)];
meanStepWidthsIMU   = [mean(stepWidthIMUSet)    std(stepWidthIMUSet)];
meanStepLengthwoZMP = [mean(stepLengthwoZMPSet) std(stepLengthwoZMPSet)];
meanStepWidthswoZMP = [mean(stepWidthwoZMPSet)  std(stepWidthwoZMPSet)];


%%
figure('name','L & R - Compare');

curpl =subplot(1,3,1);
AllstepLengths = [stepLengthRefSet(:,1); stepLengthIMUSet(:,1);stepLengthwoZMPSet(:,1)];
g = [zeros(length(stepLengthRefSet(:,1)), 1); ones(length(stepLengthIMUSet(:,1)), 1); 2*ones(length(stepLengthwoZMPSet(:,1)), 1)];
boxplot(AllstepLengths,g);
curpl.XTickLabel = {'VICON', 'IMU', 'No ZMP'};
ylabel('Step length (m)')
title('Step lengths')


diffTest = ttest2(stepLengthRefSet(:,1),stepLengthIMUSet(:,1));

if ~diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
    
end

diffTest = ttest2(stepLengthwoZMPSet(:,1),stepLengthIMUSet(:,1));

if ~diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([2 3]), [1 1]*max(yt)*1.1, '-k',  mean(xt([2 3])), max(yt)*1.15, '*k')
    
end

diffTest = ttest2(stepLengthRefSet(:,1),stepLengthwoZMPSet(:,1));

if ~diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 3]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 3])), max(yt)*1.15, '*k')
    hold off
    box off
end


curpl =subplot(1,3,2);
AllstepWidths = [stepWidthRefSet(:,1); stepWidthIMUSet(:,1);stepWidthwoZMPSet(:,1)];
g = [zeros(length(stepWidthRefSet(:,1)), 1); ones(length(stepWidthIMUSet(:,1)), 1); 2*ones(length(stepWidthwoZMPSet(:,1)), 1)];
boxplot(AllstepWidths,g);
curpl.XTickLabel = {'VICON', 'IMU', 'No ZMP'};
ylabel('Step Width (m)')
title(['Step Widths - US ' usName 'cmb: ' comboUsed])



diffTest = ttest2(stepWidthRefSet(:,1),stepWidthIMUSet(:,1));

if ~diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
end

diffTest = ttest2(stepWidthwoZMPSet(:,1),stepWidthIMUSet(:,1));

if ~diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([2 3]), [1 1]*max(yt)*1.1, '-k',  mean(xt([2 3])), max(yt)*1.15, '*k')
end

diffTest = ttest2(stepWidthwoZMPSet(:,1),stepWidthRefSet(:,1));

if ~diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 3]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 3])), max(yt)*1.15, '*k')
    hold off
    box off
end




curpl =subplot(1,3,3);
AllstepWidths = [CoMWidRefSet(:,1); CoMWidIMUSet(:,1); CoMWidwoZMPSet(:,1)];
g = [zeros(length(CoMWidRefSet(:,1)), 1); ones(length(CoMWidIMUSet(:,1)), 1); 2*ones(length(CoMWidwoZMPSet(:,1)), 1)];
boxplot(AllstepWidths,g);
curpl.XTickLabel = {'VICON', 'IMU', 'No ZMP'};
ylabel('CoM Widths (m)')
title('CoM Widths - US ')



diffTest = ttest2(CoMWidRefSet(:,1),CoMWidIMUSet(:,1));

if ~diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
end

diffTest = ttest2(CoMWidwoZMPSet(:,1),CoMWidIMUSet(:,1));

if ~diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([2 3]), [1 1]*max(yt)*1.1, '-k',  mean(xt([2 3])), max(yt)*1.15, '*k')
end

diffTest = ttest2(CoMWidwoZMPSet(:,1),CoMWidRefSet(:,1));

if ~diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 3]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 3])), max(yt)*1.15, '*k')
    hold off
    box off
end
%%
varNames = {'VICON','IMU', 'm'};
BAdispOpt = {'eq';'r';'RMSE';'n'};
[~, ~, stepLStat] = BlandAltman(stepLengthRefSet,stepLengthIMUSet,varNames,'Step Lengths Str',{},'corrinfo',BAdispOpt);
[~, ~, stepWStat] = BlandAltman(stepWidthRefSet,stepWidthIMUSet,varNames,'Step Widths Str',{},'corrinfo',BAdispOpt);
[~, ~, stepCStat] = BlandAltman(CoMWidRefSet,CoMWidIMUSet,varNames,'Mean CoM Widths Str',{},'corrinfo',BAdispOpt);

%%
%%
% figure;
% subplot(231)
% histogram(stepLengthRefSet(1:end))
% subplot(234)
% histogram(stepLengthIMUSet(1:end))
%
% subplot(232)
% histogram(stepWidthRefSet(1:end))
% subplot(235)
% histogram(stepWidthIMUSet(1:end))
%
% subplot(233)
% histogram(CoMWidRefSet(1:end))
% subplot(236)
% histogram(CoMWidIMUSet(1:end))