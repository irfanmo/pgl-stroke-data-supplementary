%% plotPub_STSubwiseMarker

%%
% BAdispOpt = {'eq';'r';'RMSE';'n'};
CorrdispOpt = {};
BAdispOpt = {};
baStatsMode = 'Gaussian'; % Gaussian or Non-parametric
varNames = {'Reference','PGL', 's'};

%%
[~, ~,STABA,baPlots] =  BlandAltman(CumSTA.Ref,CumSTA.Est,varNames,{},{},'corrinfo',CorrdispOpt,'MarkerSize',1,...,...
    'baStatsMode',baStatsMode,'baInfo',BAdispOpt);
SigDiff.StanceTimeAff = ttest2(CumSTA.Ref,CumSTA.Est);
OverAllMean.STAff_Ref = [mean(CumSTA.Ref) std(CumSTA.Ref)];
OverAllMean.STAff_Est = [mean(CumSTA.Est) std(CumSTA.Est)];

strCorP = STABA.corrP <= 0.05;
strCor = [num2str(round(STABA.r,2)) char(strCorP* '*')];
strRms = num2str(round(STABA.RMSE*1000,2));


curfig = gcf;
curfig.Name = 'ST Aff';
for usCt = 1:length(usList)
set(curfig,'CurrentAxes',baPlots{1})
plot(CumSubSTA.Ref{usCt},CumSubSTA.Est{usCt},markerCell{usCt},'Color','b');

set(curfig,'CurrentAxes',baPlots{2})
plot((CumSubSTA.Ref{usCt}+CumSubSTA.Est{usCt})/2,CumSubSTA.Est{usCt}-CumSubSTA.Ref{usCt},markerCell{usCt},'Color','b');

end

dim = [0.14 0.58 0.3 0.3];
str = ['r:' strCor ', RMSE: ' strRms ' ms'];
anH = annotation('textbox',dim,'String',str,'FitBoxToText','on');
anH.FontSize = 16; anH.FontWeight = 'bold'; anH.BackgroundColor = 'white';
%%

[~, ~,STNBA,baPlotsN] =  BlandAltman(CumSTN.Ref,CumSTN.Est,varNames,{},{},'corrinfo',CorrdispOpt,'MarkerSize',1,...,...
    'baStatsMode',baStatsMode,'baInfo',BAdispOpt);
SigDiff.StanceTimeNAff = ttest2(CumSTN.Ref,CumSTN.Est);

OverAllMean.STNAff_Ref = [mean(CumSTN.Ref) std(CumSTN.Ref)];
OverAllMean.STNAff_Est = [mean(CumSTN.Est) std(CumSTN.Est)];

strCorP = STNBA.corrP <= 0.05;
strCor = [num2str(round(STNBA.r,2)) char(strCorP* '*')];
strRms = num2str(round(STNBA.RMSE*1000,2));

curfig = gcf;
curfig.Name = 'ST Less Aff';

for usCt = 1:length(usList)
set(curfig,'CurrentAxes',baPlotsN{1})
plot(CumSubSTN.Ref{usCt},CumSubSTN.Est{usCt},markerCell{usCt},'Color','b');

set(curfig,'CurrentAxes',baPlotsN{2})
plot((CumSubSTN.Ref{usCt}+CumSubSTN.Est{usCt})/2,CumSubSTN.Est{usCt}-CumSubSTN.Ref{usCt},markerCell{usCt},'Color','b');

end

dim = [0.14 0.58 0.3 0.3];
str = ['r:' strCor ', RMSE: ' strRms ' ms'];
anH = annotation('textbox',dim,'String',str,'FitBoxToText','on');
anH.FontSize = 16; anH.FontWeight = 'bold'; anH.BackgroundColor = 'white';