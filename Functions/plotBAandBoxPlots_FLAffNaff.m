%% plotBAandBoxPlots_FLAffNaff;

showStanceTimes = 1;
showAPMoS = 1;
showMLMoS = 1;
showSL_SW = 1;

BAdis= {'eq';'r';'RMSE';'n'};
baStatsMode = 'Gaussian'; % Gaussian or Non-parametric
%% Single Stance Times
if showStanceTimes
    
    varNames = {'ForceShoe','IMU', 's'};    
    
    stRFig = figure('name','ST Aff');
   [~, ~,STABA] =  BlandAltman(stRFig,CumSTA.Ref,CumSTA.Est,varNames,'Stance Time Aff',{},'corrinfo',BAdispOpt,...
        'baStatsMode',baStatsMode);
    SigDiff.StanceTimeAff = ttest2(CumSTA.Ref,CumSTA.Est);
    
    stLFig = figure('name','ST Less Aff');
    [~, ~,STNBA] =  BlandAltman(stLFig,CumSTN.Ref,CumSTN.Est,varNames,'Stance Time Less Aff',{},'corrinfo',BAdispOpt,...
        'baStatsMode',baStatsMode);
    SigDiff.StanceTimeNAff = ttest2(CumSTN.Ref,CumSTN.Est);
    
    figure('name','Boxplots Stance Time L v R');
    clear AllstepLengths
    curpl =subplot(1,2,1);
    val1 = CumSTA.Ref; val2 = CumSTN.Ref;
    
    VarArray = [val1; val2];
    g = [zeros(length(val1), 1); ones(length(val2), 1); ];
    boxplot(VarArray,g);
    curpl.XTickLabel = {'Aff', 'Less Aff'};
    ylabel('Single Stance Time (s)')
    title('ST ForceShoe')
    
    
    diffTest = ttest2(val1,val2);
    
    if diffTest
        yt = curpl.YTick;
        axis([xlim    0  ceil(max(yt)*1.2)])
        xt = curpl.XTick;
        hold on
        plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
        
    end
    
    clear VarArray
    curpl =subplot(1,2,2);
    val1 = CumSTA.Est; val2 = CumSTN.Est;
    VarArray = [val1; val2];
    g = [zeros(length(val1), 1); ones(length(val2), 1); ];
    boxplot(VarArray,g);
    curpl.XTickLabel = {'Aff', 'Less Aff'};
    ylabel('Single Stance Time (s)')
    title('ST IMU')
    
    
    diffTest = ttest2(val1,val2);
    
    if diffTest
        yt = curpl.YTick;
        axis([xlim    0  ceil(max(yt)*1.2)])
        xt = curpl.XTick;
        hold on
        plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
        
    end
    
    
end
%% MoS
dat1Mode = 'Compare'; % Compare or Truth
baStatsMode = 'Non-parametric'; % Gaussian or Non-parametric
varNames = {'ForceShoe','IMU', 'm'};
% BAdispOpt = {'eq';'r';'RMSE';'n'};
BAdispOptCor = {}; BAdispOptBA = {};

if showMLMoS
    
    % ML R
    mlrFig = figure('name','ML MoS R');
    % BlandAltman(mlrFig,CumMLRMoS.Ref,CumMLRMoS.Est,varNames,'ML MoS Aff',{},'corrinfo',BAdispOpt);
    [~, ~,MLAstrt] = BlandAltmanDual(mlrFig,CumMLAMoS_SW.Ref,CumMLAMoS_SW.Est,varNames,...
        {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','b',...
        'baStatsMode',baStatsMode,'data1Mode',dat1Mode);
    hold on;
    [~, ~,MLAturn] = BlandAltmanDual(mlrFig,CumMLAMoS_FL.Ref,CumMLAMoS_FL.Est,varNames,...
        {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','r','symbols','o',...
        'baStatsMode','Gaussian','data1Mode',dat1Mode);
    
    SigDiff.MLMoSAff_SW = ttest2(CumMLAMoS_SW.Ref,CumMLAMoS_SW.Est);
    SigDiff.MLMoSAff_FL = ttest2(CumMLAMoS_FL.Ref,CumMLAMoS_FL.Est);
    
    MLAs_MAD = mean(abs(CumMLAMoS_SW.Ref - CumMLAMoS_SW.Est));
    MLAt_MAD = mean(abs(CumMLAMoS_FL.Ref - CumMLAMoS_FL.Est));
    
    MLAstrLoA = [MLAstrt.differenceMean-MLAstrt.rpc MLAstrt.differenceMean+MLAstrt.rpc]*100; % cm
    MLAturLoA = [MLAturn.differenceMean-MLAturn.rpc MLAturn.differenceMean+MLAturn.rpc]*100; % cm
    
    strCorP = MLAstrt.corrP <= 0.05;
    strCor = [num2str(round(MLAstrt.r,2)) char(strCorP* '*')];
    
    turCorP = MLAturn.corrP <= 0.05;
    turCor = [num2str(round(MLAturn.r,2)) char(turCorP* '*')];
    
    strRms = num2str(round(MLAstrt.RMSE*100,2));
    turRms = num2str(round(MLAturn.RMSE*100,2));
    
    curAx = mlrFig.CurrentAxes;
    LineList = curAx.Children;
    for i = [3 4 5 9 10 11]
        set(get(get(LineList(i),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
    end
    legend(['Straight,  r:' strCor ', RMSE: ' strRms ' cm'],['First and Last,        r:' turCor ', RMSE: ' turRms ' cm']);
    
    
    % ML N MoS
    mllFig = figure('name','ML MoS L');
    % BlandAltman(mllFig,CumMLLMoS.Ref,CumMLLMoS.Est,varNames,'ML MoS Less Aff',{},'corrinfo',BAdispOpt);
    [~, ~,MLNstrt] = BlandAltmanDual(mllFig,CumMLNMoS_SW.Ref,CumMLNMoS_SW.Est,varNames,...
        {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','b',...
        'baStatsMode',baStatsMode,'data1Mode',dat1Mode);
    hold on;
    [~, ~,MLNturn] = BlandAltmanDual(mllFig,CumMLNMoS_FL.Ref,CumMLNMoS_FL.Est,varNames,...
        {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','r','symbols','o',...
        'baStatsMode','Gaussian','data1Mode',dat1Mode);
    
    SigDiff.MLMoSNAff_SW = ttest2(CumMLNMoS_SW.Ref,CumMLNMoS_SW.Est);
    SigDiff.MLMoSNAff_FL = ttest2(CumMLNMoS_FL.Ref,CumMLNMoS_FL.Est);
    
    MLNs_MAD = mean(abs(CumMLNMoS_SW.Ref - CumMLNMoS_SW.Est));
    MLNt_MAD = mean(abs(CumMLNMoS_FL.Ref - CumMLNMoS_FL.Est));
    
    MLNstrLoA = [MLNstrt.differenceMean-MLNstrt.rpc MLNstrt.differenceMean+MLNstrt.rpc]*100; % cm
    MLNturLoA = [MLNturn.differenceMean-MLNturn.rpc MLNturn.differenceMean+MLNturn.rpc]*100; % cm
    
    strCorP = MLNstrt.corrP <= 0.05;
    strCor = [num2str(round(MLNstrt.r,2)) char(strCorP* '*')];
    
    turCorP = MLNturn.corrP <= 0.05;
    turCor = [num2str(round(MLNturn.r,2)) char(turCorP* '*')];
    
    strRms = num2str(round(MLNstrt.RMSE*100,2));
    turRms = num2str(round(MLNturn.RMSE*100,2));
    
    curAx = mllFig.CurrentAxes;
    LineList = curAx.Children;
    for i = [3 4 5 9 10 11]
        set(get(get(LineList(i),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
    end
    legend(['Straight,  r:' strCor ', RMSE: ' strRms ' cm'],['First and Last,        r:' turCor ', RMSE: ' turRms ' cm']);
    
    
    figure('name','BP ML MoS');
    clear VarArray
    curpl =subplot(1,2,1);
%         val1 = [CumMLAMoS_SW.Ref; CumMLAMoS_FL.Ref]; val2 = [CumMLNMoS_SW.Ref; CumMLNMoS_FL.Ref]; ylabel('ML MoS(m)'); title('ML MoS ForceShoe')
    val1 = CumMLAMoS_SW.Ref; val2 = CumMLNMoS_SW.Ref;
    VarArray = [val1; val2];
    g = [zeros(length(val1), 1); ones(length(val2), 1); ];
    boxplot(VarArray,g); 
    ylabel('ML MoS Straight (m)'); title('ML MoS Straight ForceShoe')
%     ylabel('ML MoS (m)'); title('ML MoS ForceShoe')
    curpl.XTickLabel = {'Aff', 'Less Aff'};
    
    diffTest = ttest2(val1,val2);
    
    if diffTest
        yt = curpl.YTick;
        axis([xlim    0  ceil(max(yt)*1.2)])
        xt = curpl.XTick;
        hold on
        plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
        
    end
    
    clear VarArray
    curpl =subplot(1,2,2);
%         val1 = [CumMLAMoS_SW.Est; CumMLAMoS_FL.Est]; val2 = [CumMLNMoS_SW.Est; CumMLNMoS_FL.Est]; ylabel('ML MoS(m)'); title('ML MoS IMU')
    val1 = CumMLAMoS_SW.Est; val2 = CumMLNMoS_SW.Est;
    VarArray = [val1; val2];
    g = [zeros(length(val1), 1); ones(length(val2), 1); ];
    boxplot(VarArray,g); 
    ylabel('ML MoS Straight(m)'); title('ML MoS Straight IMU')
%     ylabel('ML MoS (m)'); title('ML MoS IMU')
    curpl.XTickLabel = {'Aff', 'Less Aff'};
    
    diffTest = ttest2(val1,val2);
    
    if diffTest
        yt = curpl.YTick;
        axis([xlim    0  ceil(max(yt)*1.2)])
        xt = curpl.XTick;
        hold on
        plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
        
    end
end

%% AP MoS
% baStatsMode = 'Non-parametric'; % Gaussian or Non-parametric
if showAPMoS
    imuVal = CumAPAMoS_SW.Est; refVal = CumAPAMoS_SW.Ref;
    figure; plot(imuVal); hold on; plot(refVal);
    
    if apOutlierRemoval
    allOutliers = fun_findIQROutliers(imuVal);
    imuVal(allOutliers,:) = []; refVal(allOutliers,:) = []; warning('outliers removed for AP mos alone')
    allOutliers = fun_findIQROutliers(refVal);
    imuVal(allOutliers,:) = []; refVal(allOutliers,:) = [];
    end
    
    apFig = figure('name','AP Aff MoS');
    % BlandAltman(apFig,refVal,imuVal,varNames,'AP MoS',{},'corrinfo',BAdispOpt);
    
    [~, ~,APAstrt] = BlandAltmanDual(apFig,refVal,imuVal,varNames,...
        {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','b',...
        'baStatsMode',baStatsMode,'data1Mode',dat1Mode);
    hold on;
    [~, ~,APAturn] = BlandAltmanDual(apFig,CumAPAMoS_FL.Ref,CumAPAMoS_FL.Est,varNames,...
        {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','r','symbols','o',...
        'baStatsMode','Gaussian','data1Mode',dat1Mode);
    
    SigDiff.APMoSAff_SW = ttest2(refVal,imuVal);
    SigDiff.APMoSAff_FL = ttest2(CumAPAMoS_FL.Ref,CumAPAMoS_FL.Est);
    
    APAs_MAD = mean(abs(CumAPAMoS_SW.Ref - CumAPAMoS_SW.Est));
    APAt_MAD = mean(abs(CumAPAMoS_FL.Ref - CumAPAMoS_FL.Est));
    
    APAstrLoA = [APAstrt.differenceMean-APAstrt.rpc APAstrt.differenceMean+APAstrt.rpc]*100; % cm
    APAturLoA = [APAturn.differenceMean-APAturn.rpc APAturn.differenceMean+APAturn.rpc]*100; % cm
    
    strCorP = APAstrt.corrP <= 0.05;
    strCor = [num2str(round(APAstrt.r,2)) char(strCorP* '*')];
    
    turCorP = APAturn.corrP <= 0.05;
    turCor = [num2str(round(APAturn.r,2)) char(turCorP* '*')];
    
    strRms = num2str(round(APAstrt.RMSE*100,2));
    turRms = num2str(round(APAturn.RMSE*100,2));
    
    curAx = apFig.CurrentAxes;
    LineList = curAx.Children;
    for i = [3 4 5 9 10 11]
        set(get(get(LineList(i),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
    end
    legend(['Straight,  r:' strCor ', RMSE: ' strRms ' cm'],['First and Last,        r:' turCor ', RMSE: ' turRms ' cm']);
    
    
    
    imuVal = CumAPNMoS_SW.Est; refVal = CumAPNMoS_SW.Ref;
    figure; plot(imuVal); hold on; plot(refVal);
    
    if apOutlierRemoval
    allOutliers = fun_findIQROutliers(imuVal);
    imuVal(allOutliers,:) = []; refVal(allOutliers,:) = []; warning('outliers removed for AP mos alone')
    allOutliers = fun_findIQROutliers(refVal);
    imuVal(allOutliers,:) = []; refVal(allOutliers,:) = [];
    end
    
    apFig = figure('name','AP Less Aff MoS');
    % BlandAltman(apFig,refVal,imuVal,varNames,'AP MoS',{},'corrinfo',BAdispOpt);
    
    [~, ~,APNstrt] = BlandAltmanDual(apFig,refVal,imuVal,varNames,...
        {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','b',...
        'baStatsMode',baStatsMode,'data1Mode',dat1Mode);
    hold on;
    [~, ~,APNturn] = BlandAltmanDual(apFig,CumAPNMoS_FL.Ref,CumAPNMoS_FL.Est,varNames,...
        {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','r','symbols','o',...
        'baStatsMode','Gaussian','data1Mode',dat1Mode);
    
        SigDiff.APMoSNAff_SW = ttest2(refVal,imuVal);
    SigDiff.APMoSNAff_FL = ttest2(CumAPNMoS_FL.Ref,CumAPNMoS_FL.Est);
    
    APNs_MAD = mean(abs(CumAPNMoS_SW.Ref - CumAPNMoS_SW.Est));
    APNt_MAD = mean(abs(CumAPNMoS_FL.Ref - CumAPNMoS_FL.Est));
    
    APNstrLoA = [APNstrt.differenceMean-APNstrt.rpc APNstrt.differenceMean+APNstrt.rpc]*100; % cm
    APNturLoA = [APNturn.differenceMean-APNturn.rpc APNturn.differenceMean+APNturn.rpc]*100; % cm
    
    strCorP = APNstrt.corrP <= 0.05;
    strCor = [num2str(round(APNstrt.r,2)) char(strCorP* '*')];
    
    turCorP = APNturn.corrP <= 0.05;
    turCor = [num2str(round(APNturn.r,2)) char(turCorP* '*')];
    
    strRms = num2str(round(APNstrt.RMSE*100,2));
    turRms = num2str(round(APNturn.RMSE*100,2));
    
    curAx = apFig.CurrentAxes;
    LineList = curAx.Children;
    for i = [3 4 5 9 10 11]
        set(get(get(LineList(i),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
    end
    legend(['Straight,  r:' strCor ', RMSE: ' strRms ' cm'],['First and Last,        r:' turCor ', RMSE: ' turRms ' cm']);
    
    
    
    %
    figure('name','BP AP MoS');
    clear VarArray
    curpl =subplot(1,2,1);
    %     val1 = [CumMLAMoS_SW.Ref; CumMLAMoS_FL.Ref]; val2 = [CumMLNMoS_SW.Ref; CumMLNMoS_FL.Ref]; ylabel('ML MoS(m)'); title('ML MoS ForceShoe')
    val1 = CumAPAMoS_SW.Ref; val2 = CumAPNMoS_SW.Ref;
    
    if apOutlierRemoval
    allOutliers = fun_findIQROutliers(val1);
    val1(allOutliers,:) = []; val2(allOutliers,:) = [];
    allOutliers = fun_findIQROutliers(val2);
    val1(allOutliers,:) = []; val2(allOutliers,:) = []; warning('outliers removed for AP mos alone')
    end
    
    VarArray = [val1; val2];
    g = [zeros(length(val1), 1); ones(length(val2), 1); ];
    boxplot(VarArray,g); ylabel('AP MoS Straight (m)'); title('AP MoS Straight ForceShoe')
    curpl.XTickLabel = {'Aff', 'Less Aff'};
    
    diffTest = ttest2(val1,val2);
    
    if diffTest
        yt = curpl.YTick;
        axis([xlim    0  ceil(max(yt)*1.2)])
        xt = curpl.XTick;
        hold on
        plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
        
    end
%     ylim([-0.01 2])
    
    clear VarArray
    curpl =subplot(1,2,2);
    val1 = CumAPAMoS_SW.Est; val2 = CumAPNMoS_SW.Est;
    
    if apOutlierRemoval
     allOutliers = fun_findIQROutliers(val1);
    val1(allOutliers,:) = []; val2(allOutliers,:) = [];
    allOutliers = fun_findIQROutliers(val2);
    val1(allOutliers,:) = []; val2(allOutliers,:) = []; warning('outliers removed for AP mos alone')
    end
    
    VarArray = [val1; val2];
    g = [zeros(length(val1), 1); ones(length(val2), 1); ];
    boxplot(VarArray,g); ylabel('AP MoS Straight(m)'); title('AP MoS Straight IMU')
    curpl.XTickLabel = {'Aff', 'Less Aff'};
    
    diffTest = ttest2(val1,val2);
    
    if diffTest
        yt = curpl.YTick;
        axis([xlim    0  ceil(max(yt)*1.2)])
        xt = curpl.XTick;
        hold on
        plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
        
    end
%     ylim([-0.01 2])
    %
end 
%% Step lengths
dat1Mode = 'Compare'; % Compare or Truth
baStatsMode = 'Gaussian'; % Gaussian or Non-parametric
varNames = {'ForceShoe','IMU', 'm'};
% BAdispOpt = {'eq';'r';'RMSE';'n'};
BAdispOptCor = {}; BAdispOptBA = {};

slAFig = figure('name','SL Aff');
% BlandAltman(slRFig,CumSLR.Ref,CumSLR.Est,varNames,'Step lengths Aff',{},'corrinfo',BAdispOpt);
[~, ~,SLAstrt] = BlandAltmanDual(slAFig,CumSLA_SW.Ref,CumSLA_SW.Est,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','b',...
    'baStatsMode',baStatsMode,'data1Mode',dat1Mode);
hold on;
[~, ~,SLAturn] = BlandAltmanDual(slAFig,CumSLA_FL.Ref,CumSLA_FL.Est,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','r','symbols','o',...
    'baStatsMode','Gaussian','data1Mode',dat1Mode);

       SigDiff.SLAff_SW = ttest2(CumSLA_SW.Ref,CumSLA_SW.Est);
    SigDiff.SLAff_FL = ttest2(CumSLA_FL.Ref,CumSLA_FL.Est);

SLAs_MAD = mean(abs(CumSLA_SW.Ref - CumSLA_SW.Est));
SLAt_MAD = mean(abs(CumSLA_FL.Ref - CumSLA_FL.Est));

SLAstrLoA = [SLAstrt.differenceMean-SLAstrt.rpc SLAstrt.differenceMean+SLAstrt.rpc]*100; % cm
SLAturLoA = [SLAturn.differenceMean-SLAturn.rpc SLAturn.differenceMean+SLAturn.rpc]*100; % cm

strCorP = SLAstrt.corrP <= 0.05;
strCor = [num2str(round(SLAstrt.r,2)) char(strCorP* '*')];

turCorP = SLAturn.corrP <= 0.05;
turCor = [num2str(round(SLAturn.r,2)) char(turCorP* '*')];

strRms = num2str(round(SLAstrt.RMSE*100,2));
turRms = num2str(round(SLAturn.RMSE*100,2));

curAx = slAFig.CurrentAxes;
LineList = curAx.Children;
for i = [3 4 5 9 10 11]
    set(get(get(LineList(i),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
end
legend(['Straight,  r:' strCor ', RMSE: ' strRms ' cm'],['First and Last,        r:' turCor ', RMSE: ' turRms ' cm']);

slNFig = figure('name','SL Less Aff');
% BlandAltman(slLFig,CumSLL.Ref,CumSLL.Est,varNames,'Step lengths Less Aff',{},'corrinfo',BAdispOpt);
[~, ~,SLNstrt] = BlandAltmanDual(slNFig,CumSLN_SW.Ref,CumSLN_SW.Est,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','b',...
    'baStatsMode',baStatsMode,'data1Mode',dat1Mode);
hold on;
[~, ~,SLNturn] = BlandAltmanDual(slNFig,CumSLN_FL.Ref,CumSLN_FL.Est,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','r','symbols','o',...
    'baStatsMode','Gaussian','data1Mode',dat1Mode);

   SigDiff.SLNAff_SW = ttest2(CumSLN_SW.Ref,CumSLN_SW.Est);
    SigDiff.SLNAff_FL = ttest2(CumSLN_FL.Ref,CumSLN_FL.Est);

SLNs_MAD = mean(abs(CumSLN_SW.Ref - CumSLN_SW.Est));
SLNt_MAD = mean(abs(CumSLN_FL.Ref - CumSLN_FL.Est));

SLNstrLoA = [SLNstrt.differenceMean-SLNstrt.rpc SLNstrt.differenceMean+SLNstrt.rpc]*100; % cm
SLNturLoA = [SLNturn.differenceMean-SLNturn.rpc SLNturn.differenceMean+SLNturn.rpc]*100; % cm

strCorP = SLNstrt.corrP <= 0.05;
strCor = [num2str(round(SLNstrt.r,2)) char(strCorP* '*')];

turCorP = SLNturn.corrP <= 0.05;
turCor = [num2str(round(SLNturn.r,2)) char(turCorP* '*')];

strRms = num2str(round(SLNstrt.RMSE*100,2));
turRms = num2str(round(SLNturn.RMSE*100,2));

curAx = slNFig.CurrentAxes;
LineList = curAx.Children;
for i = [3 4 5 9 10 11]
    set(get(get(LineList(i),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
end
legend(['Straight,  r:' strCor ', RMSE: ' strRms ' cm'],['First and Last,        r:' turCor ', RMSE: ' turRms ' cm']);



figure('name','BP SL AvLA');
clear VarArray
curpl =subplot(1,2,1);
% val1 = [CumSLA_SW.Ref; CumSLA_FL.Ref]; val2 = [CumSLN_SW.Ref; CumSLN_FL.Ref]; ylabel('Step Lengths(m)'); title('SL ForceShoe')
val1 = CumSLA_SW.Ref; val2 = CumSLN_SW.Ref;
VarArray = [val1; val2];
g = [zeros(length(val1), 1); ones(length(val2), 1); ];
boxplot(VarArray,g); ylabel('Step Lengths - Straight(m)'); title('SL Straight ForceShoe')
curpl.XTickLabel = {'Aff', 'Less Aff'};

diffTest = ttest2(val1,val2);

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
    
end

clear VarArray
curpl =subplot(1,2,2);
% val1 = [CumSLA_SW.Est; CumSLA_FL.Est]; val2 = [CumSLN_SW.Est; CumSLN_FL.Est]; ylabel('Step Lengths(m)'); title('SL IMU')
val1 = CumSLA_SW.Est; val2 = CumSLN_SW.Est;
VarArray = [val1; val2];
g = [zeros(length(val1), 1); ones(length(val2), 1); ];
boxplot(VarArray,g); ylabel('Step Lengths Straight(m)'); title('SL Straight IMU')
curpl.XTickLabel = {'Aff', 'Less Aff'};



diffTest = ttest2(val1,val2);

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
    
end


%% Step widths
swAFig = figure('name','SW Aff');
% [~, ~, swAoverall] = BlandAltman(swAFig,[CumSWA_SW.Ref; CumSWA_FL.Ref],[CumSWA_SW.Est; CumSWA_FL.Est],varNames,'Step widths Aff',{},'corrinfo',BAdispOpt);
[~, ~,swAStr] = BlandAltmanDual(swAFig,CumSWA_SW.Ref,CumSWA_SW.Est,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','b',...
    'baStatsMode',baStatsMode,'data1Mode',dat1Mode);
hold on;
[~, ~,swAturn] = BlandAltmanDual(swAFig,CumSWA_FL.Ref,CumSWA_FL.Est,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','r','symbols','o',...
    'baStatsMode','Gaussian','data1Mode',dat1Mode);

SigDiff.SWA_SW = ttest2(CumSWA_SW.Ref,CumSWA_SW.Est);
SigDiff.SWA_FL = ttest2(CumSWA_FL.Ref,CumSWA_FL.Est);

SWAstrLoA = [swAStr.differenceMean-swAStr.rpc swAStr.differenceMean+swAStr.rpc]*100; % cm
SWAturLoA = [swAturn.differenceMean-swAturn.rpc swAturn.differenceMean+swAturn.rpc]*100; % cm

strCorP = swAStr.corrP <= 0.05;
strCor = [num2str(round(swAStr.r,2)) char(strCorP* '*')];

turCorP = swAturn.corrP <= 0.05;
turCor = [num2str(round(swAturn.r,2)) char(turCorP* '*')];

strRms = num2str(round(swAStr.RMSE*100,2));
turRms = num2str(round(swAturn.RMSE*100,2));

curAx = swAFig.CurrentAxes;
LineList = curAx.Children;
for i = [3 4 5 9 10 11]
    set(get(get(LineList(i),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
end
legend(['Straight,  r:' strCor ', RMSE: ' strRms ' cm'],['First and Last,        r:' turCor ', RMSE: ' turRms ' cm']);



% xxxxxxxxxxxxxxxxxxxxxxxxxxxxx
swNFig = figure('name','SW Less Aff');
% [~, ~, swNoverall] = BlandAltman(swNFig,[CumSWN_SW.Ref; CumSWN_FL.Ref],[CumSWN_SW.Est; CumSWN_FL.Est],varNames,'Step widths Less Aff',{},'corrinfo',BAdispOpt);


[~, ~,swNStr] = BlandAltmanDual(swNFig,CumSWN_SW.Ref,CumSWN_SW.Est,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','b',...
    'baStatsMode',baStatsMode,'data1Mode',dat1Mode);
hold on;
[~, ~,swNturn] = BlandAltmanDual(swNFig,CumSWN_FL.Ref,CumSWN_FL.Est,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','r','symbols','o',...
    'baStatsMode','Gaussian','data1Mode',dat1Mode);

SigDiff.SWN_SW = ttest2(CumSWN_SW.Ref,CumSWN_SW.Est);
SigDiff.SWN_FL = ttest2(CumSWN_FL.Ref,CumSWN_FL.Est);



SWNstrLoA = [swNStr.differenceMean-swNStr.rpc swNStr.differenceMean+swNStr.rpc]*100; % cm
SWNturLoA = [swNturn.differenceMean-swNturn.rpc swNturn.differenceMean+swNturn.rpc]*100; % cm

strCorP = swNStr.corrP <= 0.05;
strCor = [num2str(round(swNStr.r,2)) char(strCorP* '*')];

turCorP = swNturn.corrP <= 0.05;
turCor = [num2str(round(swNturn.r,2)) char(turCorP* '*')];

strRms = num2str(round(swNStr.RMSE*100,2));
turRms = num2str(round(swNturn.RMSE*100,2));

curAx = swNFig.CurrentAxes;
LineList = curAx.Children;
for i = [3 4 5 9 10 11]
    set(get(get(LineList(i),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
end
legend(['Straight,  r:' strCor ', RMSE: ' strRms ' cm'],['First and Last,        r:' turCor ', RMSE: ' turRms ' cm']);

SWAs_MAD = mean(abs(CumSWA_SW.Ref - CumSWA_SW.Est));
SWAt_MAD = mean(abs(CumSWA_FL.Ref - CumSWA_FL.Est));
SWNs_MAD = mean(abs(CumSWN_SW.Ref - CumSWN_SW.Est));
SWNt_MAD = mean(abs(CumSWN_FL.Ref - CumSWN_FL.Est));


figure('name','BP SW AffvLess Aff');
clear VarArray
curpl =subplot(1,2,1);
% val1 = [CumSWA_SW.Ref; CumSWA_FL.Ref]; val2 = [CumSWN_SW.Ref; CumSWN_FL.Ref]; ylabel('Step Widths (m)'); title('SW ForceShoe')
val1 = CumSWA_SW.Ref; val2 = CumSWN_SW.Ref;
VarArray = [val1; val2];
g = [zeros(length(val1), 1); ones(length(val2), 1); ];
boxplot(VarArray,g); ylabel('Step Widths Straight(m)'); title('SW Straight ForceShoe')
curpl.XTickLabel = {'Aff', 'Less Aff'};

diffTest = ttest2(val1,val2);

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
    
end

clear VarArray
curpl =subplot(1,2,2);
% val1 = [CumSWA_SW.Est; CumSWA_FL.Est]; val2 = [CumSWN_SW.Est; CumSWN_FL.Est]; ylabel('Step Widths (m)'); title('SW IMU')
val1 = CumSWA_SW.Est; val2 = CumSWN_SW.Est; ylabel('Step Widths straight (m)');
VarArray = [val1; val2];
g = [zeros(length(val1), 1); ones(length(val2), 1); ];
boxplot(VarArray,g); title('SW Straight IMU')
curpl.XTickLabel = {'Aff', 'Less Aff'};

diffTest = ttest2(val1,val2);

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
    
end

%% Graph check variable
clear str1 compTableGaitQuality
headerNames = {'MLAstrtRMSE','MLAstrtMAD','MLAturnRMSE','MLAturnMAD',...
    'MLNstrtRMSE','MLNstrtMAD','MLNturnRMSE','MLNturnMAD',...
    'SLAstrtRMSE','SLAstrtMAD','SLAturnRMSE','SLAturnMAD',...
    'SLNstrtRMSE','SLNstrtMAD','SLNturnRMSE','SLNturnMAD',...
    'APAstrtRMSE','APAstrtMAD','APAturnRMSE','APAturnMAD',...
	'APNstrtRMSE','APNstrtMAD','APNturnRMSE','APNturnMAD',...
	'SWAstrtRMSE','SWAstrtMAD','SWAturnRMSE','SWAturnMAD',...
	'SWNstrtRMSE','SWNstrtMAD','SWNturnRMSE','SWNturnMAD'};
compTableGaitQuality = array2table(zeros(0,length(headerNames)));
compTableGaitQuality.Properties.VariableNames = headerNames;
trialTypeList = fieldnames(CumStatsCpos.Overall.RMSE);
eCol = 1;
bookEnders = '';% '' or '$'
connString = ' � '; % ' \pm ' or �

cTrialType = trialTypeList{1};

% str1{1,eCol} = cTrialType; eCol = eCol + 1;
% all values in cm
% ML Aff MoS
str1{1,eCol} = ([bookEnders num2str(round(MLAstrt.RMSE*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(MLAs_MAD*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(MLAturn.RMSE*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(MLAt_MAD*100,sigDig)) bookEnders]);eCol = eCol + 1;

% ML LAff MoS
str1{1,eCol} = ([bookEnders num2str(round(MLNstrt.RMSE*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(MLNs_MAD*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(MLNturn.RMSE*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(MLNt_MAD*100,sigDig)) bookEnders]);eCol = eCol + 1;

% SL Aff
str1{1,eCol} = ([bookEnders num2str(round(SLAstrt.RMSE*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(SLAs_MAD*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(SLAturn.RMSE*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(SLAt_MAD*100,sigDig)) bookEnders]);eCol = eCol + 1;

% SL LAff
str1{1,eCol} = ([bookEnders num2str(round(SLNstrt.RMSE*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(SLNs_MAD*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(SLNturn.RMSE*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(SLNt_MAD*100,sigDig)) bookEnders]);eCol = eCol + 1;

% AP Aff
str1{1,eCol} = ([bookEnders num2str(round(APAstrt.RMSE*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(APAs_MAD*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(APAturn.RMSE*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(APAt_MAD*100,sigDig)) bookEnders]);eCol = eCol + 1;

% AP LAff
str1{1,eCol} = ([bookEnders num2str(round(APNstrt.RMSE*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(APNs_MAD*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(APNturn.RMSE*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(APNt_MAD*100,sigDig)) bookEnders]);eCol = eCol + 1;

%SW Aff
str1{1,eCol} = ([bookEnders num2str(round(swAStr.RMSE*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(SWAs_MAD*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(swAturn.RMSE*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(SWAt_MAD*100,sigDig)) bookEnders]);eCol = eCol + 1;

%SW LAff
str1{1,eCol} = ([bookEnders num2str(round(swNStr.RMSE*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(SWNs_MAD*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(swNturn.RMSE*100,sigDig)) bookEnders]);eCol = eCol + 1;
str1{1,eCol} = ([bookEnders num2str(round(SWNt_MAD*100,sigDig)) bookEnders]);eCol = eCol + 1;

% % SW R
% str1{1,eCol} = ([bookEnders num2str(round(swAoverall.RMSE*100,sigDig)) bookEnders]);eCol = eCol + 1;
% % str1{1,eCol} = ([bookEnders num2str(round(APs_MAD*100,sigDig)) bookEnders]);eCol = eCol + 1;
% % SW L
% str1{1,eCol} = ([bookEnders num2str(round(swNoverall.RMSE*100,sigDig)) bookEnders]);eCol = eCol + 1;
% % str1{1,eCol} = ([bookEnders num2str(round(APt_MAD*100,sigDig)) bookEnders]);eCol = eCol + 1;


compTableGaitQuality = [compTableGaitQuality; str1];
