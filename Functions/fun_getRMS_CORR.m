function [RMSout, CORRout, PVALout] = fun_getRMS_CORR(estimData,refData)




for iDim = 1:size(estimData,2)
     
        rmseV(iDim) = rms(estimData(:,iDim) - refData(:,iDim));
        [corrV(iDim) pValV(iDim)] = corr(estimData(:,iDim),refData(:,iDim));

end



RMSout = rmseV;
CORRout = corrV;
PVALout = pValV;

end