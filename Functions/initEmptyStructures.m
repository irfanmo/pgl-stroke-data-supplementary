% initialize useful structures
% initEmptyStructures

if typeCur ~= prevType || ~strcmpi(prevUser,usCur)
    CumTypeWiseF.Ref.(usName).(typeStr) = [];
    CumTypeWiseF.Est.(usName).(typeStr) = [];
    CumTypeWise.N.(usName).(typeStr)    = [];
    
    CumTypeWiseLen.Ref.(usName).(typeStr) = [];
    CumTypeWiseLen.Est.(usName).(typeStr) = [];
    
    CumTypeWiseApos.Ref.(usName).(typeStr) = [];
    CumTypeWiseApos.Est.(usName).(typeStr) = [];
    CumTypeWiseNpos.Ref.(usName).(typeStr) = [];
    CumTypeWiseNpos.Est.(usName).(typeStr) = [];
    CumTypeWiseCpos.Ref.(usName).(typeStr) = [];
    CumTypeWiseCpos.Est.(usName).(typeStr) = [];
    
    CumTypeWiseAFSeg.Ref.(usName).(typeStr) = [];
    CumTypeWiseAFSeg.Est.(usName).(typeStr) = [];
    CumTypeWiseNFSeg.Ref.(usName).(typeStr) = [];
    CumTypeWiseNFSeg.Est.(usName).(typeStr) = [];
    CumTypeWisePSSeg.Ref.(usName).(typeStr) = [];
    CumTypeWisePSSeg.Est.(usName).(typeStr) = [];
    
    CumTypeWiseStabParam.SStanceTA.Ref.(usName).(typeStr) = [];
    CumTypeWiseStabParam.SStanceTA.Est.(usName).(typeStr) = [];
    CumTypeWiseStabParam.SStanceTN.Ref.(usName).(typeStr) = [];
    CumTypeWiseStabParam.SStanceTN.Est.(usName).(typeStr) = [];
    
    
    CumTypeWiseXCSeg.Ref.(usName).(typeStr) = [];
    CumTypeWiseXCSeg.Est.(usName).(typeStr) = [];
    
    CumTypeWiseMoS.APA.Ref.(usName).(typeStr) = [];
    CumTypeWiseMoS.APA.Est.(usName).(typeStr) = [];
    
    CumTypeWiseMoS.APN.Ref.(usName).(typeStr) = [];
    CumTypeWiseMoS.APN.Est.(usName).(typeStr) = [];
    
    CumTypeWiseMoS.MLA.Ref.(usName).(typeStr) = [];
    CumTypeWiseMoS.MLA.Est.(usName).(typeStr) = [];
    
    CumTypeWiseMoS.MLN.Ref.(usName).(typeStr) = [];
    CumTypeWiseMoS.MLN.Est.(usName).(typeStr) = [];
    
    CumTypeWiseSLA.Ref.(usName).(typeStr) = [];
    CumTypeWiseSLA.Est.(usName).(typeStr) = [];
    
    CumTypeWiseSLN.Ref.(usName).(typeStr) = [];
    CumTypeWiseSLN.Est.(usName).(typeStr) = [];
    
    CumTypeWiseSWA.Ref.(usName).(typeStr) = [];
    CumTypeWiseSWA.Est.(usName).(typeStr) = [];
    
    CumTypeWiseSWN.Ref.(usName).(typeStr) = [];
    CumTypeWiseSWN.Est.(usName).(typeStr) = [];    
    
    prevType = typeCur;
    prevUser = usCur;
end