%% main_ProcessAllRCombos

%%
close all;
clearvars -except IMUProcessedData
% comboList = 1:9;
comboList = 1:12;

for iCL = 1:length(comboList)
    comboUsed = ['rval_' num2str(comboList(iCL))];
    %         usList = {'us6'};
    usList = fieldnames(IMUProcessedData);
    %     usList = {'us3'};
    main_ProcessBestData;
    %         main_postProcessTrialWiseData; stLenDiff{iCL,:} = [hstLenRLRef pstLenRLRef; hstLenRLIMU pstLenRLIMU];
    
    sLenStats(iCL,:) = [stepLStat.r stepLStat.RMSE stepLStat.differenceMean stepLStat.differenceSTD];
    sWidStats(iCL,:) = [stepWStat.r stepWStat.RMSE stepWStat.differenceMean stepWStat.differenceSTD];
    sCoMStats(iCL,:) = [stepCStat.r stepCStat.RMSE stepCStat.differenceMean stepCStat.differenceSTD];
    
    sLenStrStats(iCL,:) = [stepLengthsStrCorr.r stepLengthsStrCorr.RMSE stepLengthsStrCorr.differenceMean stepLengthsStrCorr.differenceSTD];
    sWidStrStats(iCL,:) = [stepWidthsStrCorr.r  stepWidthsStrCorr.RMSE  stepWidthsStrCorr.differenceMean  stepWidthsStrCorr.differenceSTD];
    sCoMStrStats(iCL,:) = [CoMWidthsStrCorr.r   CoMWidthsStrCorr.RMSE   CoMWidthsStrCorr.differenceMean   CoMWidthsStrCorr.differenceSTD];
    
    sRefEstRMSVal{iCL,1} = [mean(segmentedOp.RMS.R(2:end-1,:)); std(segmentedOp.RMS.R(2:end-1,:))];
    sRefEstRMSVal{iCL,2} = [mean(segmentedOp.RMS.L(2:end-1,:)); std(segmentedOp.RMS.L(2:end-1,:))];
    sRefEstRMSVal{iCL,3} = [mean(segmentedOp.RMS.C(2:end-1,:)); std(segmentedOp.RMS.C(2:end-1,:))];
    sMeanRMSVal(iCL,:) = [mean(errSteps)*100];
    
    clearvars -except IMUProcessedData comboUsed comboList iCL sLenStats ...
        sWidStats sCoMStats stLenDiff sRefEstRMSVal sMeanRMSVal sLenStrStats ...
        sWidStrStats sCoMStrStats
end