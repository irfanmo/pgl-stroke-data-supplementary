%% plotBAandBoxPlots;

%% Single Stance Times
varNames = {'ForceShoe','IMU', 's'};
BAdispOpt = {'eq';'r';'RMSE';'n'};

stRFig = figure('name','ST R');
BlandAltman(stRFig,CumSTR.Ref,CumSTR.Est,varNames,'Stance Time Right',{},'corrinfo',BAdispOpt);

stLFig = figure('name','ST L');
BlandAltman(stLFig,CumSTL.Ref,CumSTL.Est,varNames,'Stance Time Left',{},'corrinfo',BAdispOpt);

figure('name','Boxplots Stance Time L v R');
clear AllstepLengths
curpl =subplot(1,2,1);
val1 = CumSTR.Ref; val2 = CumSTL.Ref;

VarArray = [val1; val2];
g = [zeros(length(val1), 1); ones(length(val2), 1); ];
boxplot(VarArray,g);
curpl.XTickLabel = {'Right', 'Left'};
ylabel('Single Stance Time (s)')
title('ST ForceShoe')


diffTest = ttest2(val1,val2);

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
    
end

clear VarArray
curpl =subplot(1,2,2);
val1 = CumSTR.Est; val2 = CumSTL.Est;
VarArray = [val1; val2];
g = [zeros(length(val1), 1); ones(length(val2), 1); ];
boxplot(VarArray,g);
curpl.XTickLabel = {'Right', 'Left'};
ylabel('Single Stance Time (s)')
title('ST IMU')


diffTest = ttest2(val1,val2);

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
    
end

%% ML R MoS
varNames = {'ForceShoe','IMU', 'm'};
BAdispOpt = {'eq';'r';'RMSE';'n'};

mlrFig = figure('name','ML MoS R');
BlandAltman(mlrFig,CumMLRMoS.Ref,CumMLRMoS.Est,varNames,'ML MoS Right',{},'corrinfo',BAdispOpt);

% ML L MoS
mllFig = figure('name','ML MoS L');
BlandAltman(mllFig,CumMLLMoS.Ref,CumMLLMoS.Est,varNames,'ML MoS Left',{},'corrinfo',BAdispOpt);

figure('name','BP ML MoS');
clear VarArray
curpl =subplot(1,2,1);
val1 = CumMLRMoS.Ref; val2 = CumMLLMoS.Ref;
VarArray = [val1; val2];
g = [zeros(length(val1), 1); ones(length(val2), 1); ];
boxplot(VarArray,g);
curpl.XTickLabel = {'Right', 'Left'};
ylabel('ML MoS(m)')
title('ML MoS ForceShoe')


diffTest = ttest2(val1,val2);

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
    
end

clear VarArray
curpl =subplot(1,2,2);
val1 = CumMLRMoS.Est; val2 = CumMLRMoS.Est;
VarArray = [val1; val2];
g = [zeros(length(val1), 1); ones(length(val2), 1); ];
boxplot(VarArray,g);
curpl.XTickLabel = {'Right', 'Left'};
ylabel('ML MoS(m)')
title('ML MoS IMU')


diffTest = ttest2(val1,val2);

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
    
end

%% AP MoS
imuVal = CumAPMoS.Est; refVal = CumAPMoS.Ref;
figure; plot(imuVal); hold on; plot(refVal);  

% outliers = [4 27 31 32 37 41 43 45 46 60 64 66 68 86];

% imuVal(outliers,:) = []; refVal(outliers,:) = []; warning('outliers removed for AP mos alone')
apFig = figure('name','AP MoS');
BlandAltman(apFig,refVal,imuVal,varNames,'AP MoS',{},'corrinfo',BAdispOpt);


%% Step lengths
slRFig = figure('name','SL R');
BlandAltman(slRFig,CumSLR.Ref,CumSLR.Est,varNames,'Step lengths Right',{},'corrinfo',BAdispOpt);

slLFig = figure('name','SL L');
BlandAltman(slLFig,CumSLL.Ref,CumSLL.Est,varNames,'Step lengths Left',{},'corrinfo',BAdispOpt);

figure('name','BP SL LvR');
clear VarArray
curpl =subplot(1,2,1);
val1 = CumSLR.Ref; val2 = CumSLL.Ref;
VarArray = [val1; val2];
g = [zeros(length(val1), 1); ones(length(val2), 1); ];
boxplot(VarArray,g);
curpl.XTickLabel = {'Right', 'Left'};
ylabel('Step Lengths(m)')
title('SL ForceShoe')


diffTest = ttest2(val1,val2);

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
    
end

clear VarArray
curpl =subplot(1,2,2);
val1 = CumSLR.Est; val2 = CumSLL.Est;
VarArray = [val1; val2];
g = [zeros(length(val1), 1); ones(length(val2), 1); ];
boxplot(VarArray,g);
curpl.XTickLabel = {'Right', 'Left'};
ylabel('Step Lengths(m)')
title('SL IMU')


diffTest = ttest2(val1,val2);

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
    
end


%% Step widths
swRFig = figure('name','SW R');
BlandAltman(swRFig,CumSWR.Ref,CumSWR.Est,varNames,'Step widths Right',{},'corrinfo',BAdispOpt);

swLFig = figure('name','SW L');
BlandAltman(swLFig,CumSWL.Ref,CumSWL.Est,varNames,'Step widths Left',{},'corrinfo',BAdispOpt);

figure('name','BP SW LvR');
clear VarArray
curpl =subplot(1,2,1);
val1 = CumSWR.Ref; val2 = CumSWL.Ref;
VarArray = [val1; val2];
g = [zeros(length(val1), 1); ones(length(val2), 1); ];
boxplot(VarArray,g);
curpl.XTickLabel = {'Right', 'Left'};
ylabel('Step Widths (m)')
title('SW ForceShoe')


diffTest = ttest2(val1,val2);

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
    
end

clear VarArray
curpl =subplot(1,2,2);
val1 = CumSWR.Est; val2 = CumSWL.Est;
VarArray = [val1; val2];
g = [zeros(length(val1), 1); ones(length(val2), 1); ];
boxplot(VarArray,g);
curpl.XTickLabel = {'Right', 'Left'};
ylabel('Step Widths (m)')
title('SW IMU')


diffTest = ttest2(val1,val2);

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
    
end
