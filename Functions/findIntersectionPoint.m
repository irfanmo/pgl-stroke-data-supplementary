function [intersectPoint] = findIntersectionPoint(Line1XY, Line2XY)
% wikipedia https://en.wikipedia.org/wiki/Line%E2%80%93line_intersection

x1 = Line1XY(1,1); x2 = Line1XY(1,2);
y1 = Line1XY(2,1); y2 = Line1XY(2,2);

x3 = Line2XY(1,1); x4 = Line2XY(1,2);
y3 = Line2XY(2,1); y4 = Line2XY(2,2);

denomVal = ((x1 - x2) * (y3 - y4)) - ((y1 - y2) * (x3 - x4));

if denomVal == 0
    denomVal = 1e-7; % arbitrary value to avoid NaN
end

numValx  = (((x1*y2) - (y1*x2)) * (x3 - x4)) - ((x1 - x2) * ((x3 * y4) - (y3 * x4)));

numValy  = (((x1*y2) - (y1*x2)) * (y3 - y4)) - ((y1 - y2) * ((x3 * y4) - (y3 * x4)));


px = numValx/denomVal;
py = numValy/denomVal;

intersectPoint = [px, py];

end