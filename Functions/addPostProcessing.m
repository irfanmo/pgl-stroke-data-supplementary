%% addPostProcessing;


        %% transform if needed
        if transformSignals
            refDatacg      = fun_transformStepWise(refData,stepsWithFeetInfoRef);
            refDatacg.Body = refData.Body;
            estimDatacg    = fun_transformStepWise(estimData,stepsWithFeetInfoIMU);
            
        else
            
            refDatacg      = refData;
            estimDatacg    = estimData;
        end
        
        %% remove some steps if looking at complete length
        if ~useFirst10m
            switch subjectSpecs.userSelect          
                    
                case 6
                    switch subjectSpecs.trialSelect
                        case 21
                            imustepsRLsnip = [imustepsL(1,2) imustepsL(2,1) 1; imustepsRLsnip];
                            refstepsRLsnip = [refstepsL(1,2) refstepsL(2,1) 1; refstepsRLsnip];
                            
                            turnInd = [25:32];
                            finInd = 55;
                            imustepsRLsnip([turnInd finInd:end],:) = [];
                            refstepsRLsnip([turnInd finInd:end],:) = [];
                            
                            headingDir([turnInd finInd:end],:) = [];
                    end
            end
        end
        
        %% second synchronization check
        clear stepsWithFeetInfoIMUsync stepsWithFeetInfoRefsync
        if synchSignals
            axus = 3;
            syncsigest = estimDatacg.RightFoot.pos(:,axus);
            syncsigref = refDatacg.RightFoot.pos(:,axus);            %
            syncsigestfilt = butterfilterlpf(syncsigest,2,fsVar,2);
            syncsigreffilt = butterfilterlpf(syncsigref,2,fsVar,2);            %
            [tdelay] = finddelay(syncsigestfilt,syncsigreffilt);
            
            if tdelay < 0
                sInd_est = abs(tdelay);
                eInd_est = length(estimDatacg.RightFoot.pos);
                
                sInd_ref = 1;
                eInd_ref = length(estimDatacg.RightFoot.pos(sInd_est:end,:));
                
                stepsWithFeetInfoIMUsync = [stepsWithFeetInfoIMU(:,1:2)-sInd_est+1 stepsWithFeetInfoIMU(:,3)];
                if stepsWithFeetInfoIMUsync(end,2) > eInd_ref
                    stepsWithFeetInfoIMUsync(end,2) = eInd_ref;
                end
                stepsWithFeetInfoRefsync = stepsWithFeetInfoRef;
                
                if stepsWithFeetInfoRefsync(end,2) > eInd_ref
                    stepsWithFeetInfoRefsync(end,2) = eInd_ref;
                end
                
                
            elseif tdelay > 0
                sInd_ref = (tdelay);
                eInd_ref = length(refDatacg.RightFoot.pos);
                
                sInd_est = 1;
                eInd_est = length(refDatacg.RightFoot.pos(sInd_ref:end,:));
                
                stepsWithFeetInfoIMUsync = stepsWithFeetInfoIMU;
                if stepsWithFeetInfoIMUsync(end,2) > eInd_est
                    stepsWithFeetInfoIMUsync(end,2) = eInd_est;
                end
                stepsWithFeetInfoRefsync = [stepsWithFeetInfoRef(:,1:2)-sInd_ref+1 stepsWithFeetInfoRef(:,3)];
                if stepsWithFeetInfoRefsync(end,2) > eInd_est
                    stepsWithFeetInfoRefsync(end,2) = eInd_est;
                end
                
                
            else
                sInd_est = 1;
                sInd_ref = 1;
                eInd_est = length(estimDatacg.RightFoot.pos);
                eInd_ref = length(refDatacg.RightFoot.pos);
                
                stepsWithFeetInfoIMUsync = stepsWithFeetInfoIMU;
                stepsWithFeetInfoRefsync = stepsWithFeetInfoRef;
                
            end
            StartEndIndices = [sInd_est, eInd_est];
            eDatasync = syncStructFields(estimDatacg, StartEndIndices);
            
            StartEndIndices = [sInd_ref, eInd_ref];
            rDatasync = syncStructFields(refDatacg, StartEndIndices);
            
        else
            eDatasync = estimDatacg;
            rDatasync = refDatacg;
            
            stepsWithFeetInfoIMUsync = stepsWithFeetInfoIMU;
            stepsWithFeetInfoRefsync = stepsWithFeetInfoRef;
        end
        
        eDatasync.Body = estimData.Body;