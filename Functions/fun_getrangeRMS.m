function [rangeRMSout, rangeRefout, minRefout, maxRefout] =...
    fun_getrangeRMS(estimData,refData)



for iDim = 1:size(estimData,2)
    dimRange = range(refData(:,iDim));
    rangeRef(iDim) = dimRange;
    minRefout(iDim) = min(refData(:,iDim));
    maxRefout(iDim) = max(refData(:,iDim));
    
    dimMin   = min(refData(:,iDim));
    rmseV(iDim) = rms(estimData(:,iDim) - refData(:,iDim));    
    rangermseV(iDim) = ((rmseV(iDim))/dimRange)*100;
end



rangeRMSout = rangermseV;
rangeRefout = rangeRef;

end