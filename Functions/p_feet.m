function [      ...
    p_toe_r,     ...
    p_heel_r,    ...
    p_amtoe_r,   ...
    p_gtoe_r,    ...
    p_altoe_r,   ...
    p_pltoe_r,  ...
    p_pmtoe_r,   ...
    p_amheel_r,  ...
    p_alheel_r,  ...
    p_plheel_r,  ...
    p_pmheel_r , ...
    p_toe_l    , ...
    p_heel_l   , ...
    p_amtoe_l  , ...
    p_gtoe_l   , ...
    p_altoe_l  , ...
    p_pltoe_l  , ...
    p_pmtoe_l  , ...
    p_amheel_l , ...
    p_alheel_l , ...
    p_plheel_l , ...
    p_pmheel_l , ...
    p_frf      , ...
    p_frh      , ...
    p_flf      , ...
    p_flh]=     ...
    p_feet(p_imu_r,p_imu_l,R_imu_r,R_imu_l,shoeSize,nSamplesFirst)
% returns various points on both shoes (3xnSamples), see below.
% Inputs are:
% imu positions (3xnSamples) and orientations (3x3xnSamples),
% assuming that forefoot and heel segment have the same orientation
% Outputs are:
% Right:                Left:
% #1  - p_toe_r         #12 - p_toe_l
% #2  - p_heel_r        #13 - p_heel_l
% #3  - p_amtoe_r       #14 - p_amtoe_l
% #4  - p_gtoe_r        #15 - p_gtoe_l
% #5  - p_altoe_r       #16 - p_altoe_l
% #6  - p_pltoe_r       #17 - p_pltoe_l
% #7  - p_pmtoe_r       #18 - p_pmtoe_l
% #8  - p_amheel_r      #19 - p_amheel_l
% #9  - p_alheel_r      #20 - p_alheel_l
% #10 - p_plheel_r      #21 - p_plheel_l
% #11 - p_pmheel_r      #22 - p_pmheel_l
%
% Frf - p_frf           Flf - p_flf
% Frh - p_frh           Flh - p_flh
%
%    FRONT (ANTERIOR)                        BACK (POSTERIOR)
%                             LATERAL
%
%       #5 _ _ _ _ _ _ _ _ 6#         #9_ _ _ _ _ _ _ _ 10#
%      /                     \       /                     \
%     /                       |     |                       |   FOOT
%    |             X     X    |     |    X     X            |   RIGHT
%    |      Rgs_KF            |     |                       |  
%    #1               oFrf    |     |       oFrh           2#
%    |                        |     |                       |
%    #4            X     X    /     \    X     X           /
%     \#3_ _ _ _ _ _ _ _ _ 7#/       \#8_ _ _ _ _ _ _ _ 11#
%
%                              MEDIAL
%
%      #14 _ _ _ _ _ _ _ _18#         #19 _ _ _ _ _ _ _ 22#
%     #15                    \       /                     \
%    /             X     X    \     /    X     X            |
%    |                        |     |                       |   FOOT
%    #12              oFlf    |     |       oFlh          13#   LEFT
%    |      Rgs_KF            |     |                       |   
%    |             X     X    |     |    X     X            |
%     \                       |     |                       |
%      \                     /       \                     /
%       #16_ _ _ _ _ _ _ _17#         #20 _ _ _ _ _ _ _ 21#
%
%                             LATERAL
%
% x-axis point from heel-->toe
% y-axis points left
% z-axis points up

%% input check
narginchk(5,6);
if nargin==5;
    nSamplesFirst=1;% default
end
if size(p_imu_r,1)~=3
    error('p_imu_r should have 3 rows')
end
if size(p_imu_l,1)~=3
    error('p_imu_l should have 3 rows')
end
numSamples=size(p_imu_r,2);
% check size of p_imu_l input
if size(p_imu_l)~=[3 numSamples]
    error('Size of p_imu_l not correct')
end
% check size of orientation inputs
if size(R_imu_r)~=[3 3 numSamples]
    error('Size of R_imu_r not correct')
end
if size(R_imu_l)~=[3 3 numSamples]
    error('Size of R_imu_l not correct')
end


%% position force sensor wrt origin acc in shoe frame
%         this is till the edge of black square (top)    to force sensor     to bottom of shoe
%         <-------------------------------------------> <---------------->    <------------->
p_imu2flf=[0;0;0]+[7.4;4.5;9.5]*1e-3+[7; 8; 2.5]*1e-3  +[-64;-29.5;0]*1e-3  + [0;0;-27]*1e-3;
p_imu2frf=[0;0;0]+[7.4;4.5;9.5]*1e-3+[7; 8; 2.5]*1e-3  +[-64;-29.5;0]*1e-3  + [0;0;-27]*1e-3;
% position force sensors heel wrt forefoot:
switch shoeSize
    case 44
        % shoe size 44
        p_flf2flh=[-159.8;0;0]*1e-3;
        p_frf2frh=[-159.8;0;0]*1e-3;
    case 40
        p_flf2flh=[-145;0;0]*1e-3;
        p_frf2frh=[-145;0;0]*1e-3;
    otherwise
        error('please specify a correct shoe size (40 or 44)');
end

%% positions of points wrt force sensors
% right
p_frf2toe_r    =[94;0;0]*1e-3;
p_frh2heel_r   =[-45;0;0]*1e-3;
p_frf2amtoe_r  =[73;45;0]*1e-3;
p_frf2gtoe_r   =p_frf2toe_r+[1;20;0]*1e-3;% 1 mm to the front, 20 mm to medial, wrt toe
p_frf2altoe_r  =[73;-29;0]*1e-3;
p_frf2pltoe_r  =[-18;-57;0]*1e-3;
p_frf2pmtoe_r  =[-18;48;0]*1e-3;
p_frh2amheel_r =[44;43;0]*1e-3;
p_frh2alheel_r =[44;-47;0]*1e-3;
p_frh2plheel_r =[-18;-38;0]*1e-3;
p_frh2pmheel_r =[-18;40;0]*1e-3;
% left
p_flf2toe_l    =[94;0;0]*1e-3;
p_flh2heel_l   =[-45;0;0]*1e-3;
p_flf2amtoe_l  =[73;-45;0]*1e-3;
p_flf2gtoe_l   =p_frf2toe_r+[1;-20;0]*1e-3;% 1 mm to the front, 20 mm to medial, wrt toe
p_flf2altoe_l  =[73;29;0]*1e-3;
p_flf2pltoe_l  =[-18;57;0]*1e-3;
p_flf2pmtoe_l  =[-18;-48;0]*1e-3;
p_flh2amheel_l =[44;-43;0]*1e-3;
p_flh2alheel_l =[44;47;0]*1e-3;
p_flh2plheel_l =[-18;38;0]*1e-3;
p_flh2pmheel_l =[-18;-40;0]*1e-3;
%% calculate positions
p_toe_r     =zeros(3,numSamples);
p_heel_r    =zeros(3,numSamples);
p_amtoe_r   =zeros(3,numSamples);
p_gtoe_r    =zeros(3,numSamples);
p_altoe_r   =zeros(3,numSamples);
p_pltoe_r   =zeros(3,numSamples);
p_pmtoe_r   =zeros(3,numSamples);
p_amheel_r  =zeros(3,numSamples);
p_alheel_r  =zeros(3,numSamples);
p_plheel_r  =zeros(3,numSamples);
p_pmheel_r  =zeros(3,numSamples);
p_toe_l     =zeros(3,numSamples);
p_heel_l    =zeros(3,numSamples);
p_amtoe_l   =zeros(3,numSamples);
p_gtoe_l    =zeros(3,numSamples);
p_altoe_l   =zeros(3,numSamples);
p_pltoe_l   =zeros(3,numSamples);
p_pmtoe_l   =zeros(3,numSamples);
p_amheel_l  =zeros(3,numSamples);
p_alheel_l  =zeros(3,numSamples);
p_plheel_l  =zeros(3,numSamples);
p_pmheel_l  =zeros(3,numSamples);
p_frf       =zeros(3,numSamples);
p_frh       =zeros(3,numSamples);
p_flf       =zeros(3,numSamples);
p_flh       =zeros(3,numSamples);
for i=1:numSamples
    % right
    p_toe_r(:,i)      =p_imu_r(:,i)+ R_imu_r(:,:,i)*(p_imu2frf+p_frf2toe_r);
    p_heel_r(:,i)     =p_imu_r(:,i)+ R_imu_r(:,:,i)*(p_imu2frf+p_frf2frh+p_frh2heel_r);
    p_amtoe_r(:,i)    =p_imu_r(:,i)+ R_imu_r(:,:,i)*(p_imu2frf+p_frf2amtoe_r);
    p_gtoe_r(:,i)     =p_imu_r(:,i)+ R_imu_r(:,:,i)*(p_imu2frf+p_frf2gtoe_r);
    p_altoe_r(:,i)    =p_imu_r(:,i)+ R_imu_r(:,:,i)*(p_imu2frf+p_frf2altoe_r);
    p_pltoe_r(:,i)    =p_imu_r(:,i)+ R_imu_r(:,:,i)*(p_imu2frf+p_frf2pltoe_r);
    p_pmtoe_r(:,i)    =p_imu_r(:,i)+ R_imu_r(:,:,i)*(p_imu2frf+p_frf2pmtoe_r);
    p_amheel_r(:,i)   =p_imu_r(:,i)+ R_imu_r(:,:,i)*(p_imu2frf+p_frf2frh+p_frh2amheel_r);
    p_alheel_r(:,i)   =p_imu_r(:,i)+ R_imu_r(:,:,i)*(p_imu2frf+p_frf2frh+p_frh2alheel_r);
    p_plheel_r(:,i)   =p_imu_r(:,i)+ R_imu_r(:,:,i)*(p_imu2frf+p_frf2frh+p_frh2plheel_r);
    p_pmheel_r(:,i)   =p_imu_r(:,i)+ R_imu_r(:,:,i)*(p_imu2frf+p_frf2frh+p_frh2pmheel_r);
    % left
    p_toe_l(:,i)      =p_imu_l(:,i)+ R_imu_l(:,:,i)*(p_imu2flf+p_flf2toe_l);
    p_heel_l(:,i)     =p_imu_l(:,i)+ R_imu_l(:,:,i)*(p_imu2flf+p_flf2flh+p_flh2heel_l);
    p_amtoe_l(:,i)    =p_imu_l(:,i)+ R_imu_l(:,:,i)*(p_imu2flf+p_flf2amtoe_l);
    p_gtoe_l(:,i)     =p_imu_l(:,i)+ R_imu_l(:,:,i)*(p_imu2flf+p_flf2gtoe_l);
    p_altoe_l(:,i)    =p_imu_l(:,i)+ R_imu_l(:,:,i)*(p_imu2flf+p_flf2altoe_l);
    p_pltoe_l(:,i)    =p_imu_l(:,i)+ R_imu_l(:,:,i)*(p_imu2flf+p_flf2pltoe_l);
    p_pmtoe_l(:,i)    =p_imu_l(:,i)+ R_imu_l(:,:,i)*(p_imu2flf+p_flf2pmtoe_l);
    p_amheel_l(:,i)   =p_imu_l(:,i)+ R_imu_l(:,:,i)*(p_imu2flf+p_flf2flh+p_flh2amheel_l);
    p_alheel_l(:,i)   =p_imu_l(:,i)+ R_imu_l(:,:,i)*(p_imu2flf+p_flf2flh+p_flh2alheel_l);
    p_plheel_l(:,i)   =p_imu_l(:,i)+ R_imu_l(:,:,i)*(p_imu2flf+p_flf2flh+p_flh2plheel_l);
    p_pmheel_l(:,i)   =p_imu_l(:,i)+ R_imu_l(:,:,i)*(p_imu2flf+p_flf2flh+p_flh2pmheel_l);
    %right
    p_frf(:,i)        =p_imu_r(:,i)+ R_imu_r(:,:,i)*(p_imu2frf);
    p_frh(:,i)        =p_imu_r(:,i)+ R_imu_r(:,:,i)*(p_imu2frf+p_frf2frh);
    % left
    p_flf(:,i)        =p_imu_l(:,i)+ R_imu_l(:,:,i)*(p_imu2flf);
    p_flh(:,i)        =p_imu_l(:,i)+ R_imu_l(:,:,i)*(p_imu2flf+p_flf2flh);
end
if nSamplesFirst==1
    %     transpose all
    p_toe_r     	=	p_toe_r';
    p_heel_r    	=	p_heel_r';
    p_amtoe_r   	=	p_amtoe_r';
    p_gtoe_r    	=	p_gtoe_r';
    p_altoe_r   	=	p_altoe_r';
    p_pltoe_r   	=	p_pltoe_r';
    p_pmtoe_r   	=	p_pmtoe_r';
    p_amheel_r  	=	p_amheel_r';
    p_alheel_r  	=	p_alheel_r';
    p_plheel_r  	=	p_plheel_r';
    p_pmheel_r  	=	p_pmheel_r';
    p_toe_l     	=	p_toe_l';
    p_heel_l    	=	p_heel_l';
    p_amtoe_l   	=	p_amtoe_l';
    p_gtoe_l    	=	p_gtoe_l';
    p_altoe_l   	=	p_altoe_l';
    p_pltoe_l   	=	p_pltoe_l';
    p_pmtoe_l   	=	p_pmtoe_l';
    p_amheel_l  	=	p_amheel_l';
    p_alheel_l  	=	p_alheel_l';
    p_plheel_l  	=	p_plheel_l';
    p_pmheel_l  	=	p_pmheel_l';
    p_frf       	=	p_frf';
    p_frh       	=	p_frh';
    p_flf       	=	p_flf';
    p_flh       	=	p_flh';
end
