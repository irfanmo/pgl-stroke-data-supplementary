function [stepLengths, stepWidths, comLengths, comWidths] = fun_findStepWiseSpatioTempParam(dataSet,stepsAr)



for iStep = 1:length(stepsAr)
    
    setStrt = stepsAr(iStep,1);
    setEnd  = stepsAr(iStep,2);
    
    tRF = dataSet.RightFoot.pos(setStrt:setEnd,:);
    tLF = dataSet.LeftFoot.pos(setStrt:setEnd,:);
    tPS = dataSet.CoM.pos(setStrt:setEnd,:);
    
    CoMinit =tPS(1,:);
    CoMfinl =tPS(end,:);
    
    if stepsAr(iStep,3) == 1 %rightleg stance
        %changes in left leg
        
        %changes in left leg
        pos_init=tLF(1,:);
        pos_fini=tLF(end,:);
        
        resetPos = pos_init;
        headingChange = (pos_fini - pos_init);
        headingChange(3)=0; % make z component zero
        heading_stepwise_estimate=headingChange/norm(headingChange); % normalize
        if all(isnan(heading_stepwise_estimate))
            theading = [1 0 0];
        else
            theading = heading_stepwise_estimate;
        end
        fakeZaxis = [zeros(5,1) zeros(5,1) ones(5,1)];
        R_temp=initorient_wHeading(fakeZaxis, theading');
        tRF = (R_temp*(tRF- resetPos)')';
        tLF = (R_temp*(tLF- resetPos)')';
        tPS = (R_temp*(tPS- resetPos)')';
        
        RLmean = mean(tRF);
        RLinit = tRF(1,:);
        stancefoot = RLmean;
        %         RLmean = tRF(end,1:2);
        
        a = normh(stancefoot(1,1:2) - tLF(1,1:2));
        b = normh(stancefoot(1,1:2) - tLF(end,1:2));
        c = normh(tLF(1,1:2) - tLF(end,1:2));
        
        swingfoot  = tLF;
        
        
    elseif stepsAr(iStep,3) == 2 %leftleg stance
        %changes in right leg
        pos_init=tRF(1,:);
        pos_fini=tRF(end,:);
        
        resetPos = pos_init;
        headingChange = (pos_fini - pos_init);
        headingChange(3)=0; % make z component zero
        heading_stepwise_estimate=headingChange/norm(headingChange); % normalize
        if all(isnan(heading_stepwise_estimate))
            theading = [1 0 0];
        else
            theading = heading_stepwise_estimate;
        end
        fakeZaxis = [zeros(5,1) zeros(5,1) ones(5,1)];
        R_temp=initorient_wHeading(fakeZaxis, theading');
        tRF = (R_temp*(tRF- resetPos)')';
        tLF = (R_temp*(tLF- resetPos)')';
        tPS = (R_temp*(tPS- resetPos)')';
        
        LLmean = mean(tLF);
        LLinit = tLF(1,:);
        %         LLmean = tLF(end,1:2);
        stancefoot = LLmean;
        
        a = normh(stancefoot(1,1:2) - tRF(1,1:2));
        b = normh(stancefoot(1,1:2) - tRF(end,1:2));
        c = normh(tRF(1,1:2) - tRF(end,1:2));
        
        
        swingfoot  = tRF;
    end
    
    stepLengths(iStep,1) = (b^2+c^2-a^2)/(2*c);
    stepWidths(iStep,1)  = sqrt(b^2-stepLengths(iStep,1)^2);
    
    stepLengths(iStep,2) = stepsAr(iStep,3);
    stepWidths(iStep,2)  = stepsAr(iStep,3);
    
    comLengths(iStep,1) = norm(CoMfinl(:,1:2) - CoMinit(:,1:2));
    comLengths(iStep,2) = stepsAr(iStep,3);
    
    selectedFoot = stancefoot; % stancefoot swingfoot
    comWidths(iStep,1)  = mean(normh(tPS(:,1:2) - selectedFoot(:,1:2)));
    comWidths(iStep,2)  = std(normh(tPS(:,1:2) - selectedFoot(:,1:2)));
    comWidths(iStep,3) = stepsAr(iStep,3);
    
end