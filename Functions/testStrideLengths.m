%%
clear stepLengths stepWidths
%%

selStep = 5; % ind = 2 left leg stance
% selStep = 12; % ind = 1 righ leg stance

tRF = segmentedIMU2.RightFoot.pos.sets{selStep};
tLF = segmentedIMU2.LeftFoot.pos.sets{selStep};
tPS = segmentedIMU2.CoM.pos.sets{selStep};


%%
selStep = 5; % ind = 2 left leg stance
% selStep = 12; % ind = 1 righ leg stance

tRF = segmentedRef2.RightFoot.pos.sets{selStep};
tLF = segmentedRef2.LeftFoot.pos.sets{selStep};
tPS = segmentedRef2.CoM.pos.sets{selStep};

%%
LLmean = mean(tLF);
stancefoot = [tLF(1,1) LLmean(:,2) tLF(1,3)];
stancefoot = mean(tLF);
a = normh(stancefoot(1,1:2) - tRF(1,1:2));
b = normh(stancefoot(1,1:2) - tRF(end,1:2));
c = normh(tRF(1,1:2) - tRF(end,1:2));

stepLengths(selStep,1) = (b^2+c^2-a^2)/(2*c);
stepWidths(selStep,1)  = sqrt(b^2-stepLengths(selStep,1)^2);
%%
RLmean = mean(tRF);
% stancefoot = mean(tRF);
stancefoot = [tRF(1,1) RLmean(:,2) tRF(1,3)];
a = normh(stancefoot(1,1:2) - tLF(1,1:2));
b = normh(stancefoot(1,1:2) - tLF(end,1:2));
c = normh(tLF(1,1:2) - tLF(end,1:2));

stepLengths(selStep,1) = (b^2+c^2-a^2)/(2*c);
stepWidths(selStep,1)  = sqrt(b^2-stepLengths(selStep,1)^2);