% Group Process tthe best outputs
% main_ProcessBestData
%%
% clearvars -except IMUProcessedData
% comboUsed = 'rval_4';
% close all;
% usList = fieldnames(IMUProcessedData);
% usList = {'us8'};
% usList = {'us3','us4','us5','us6'};
publicationplot = 0;
removeTrials = 1;
%%


set(0,'defaultfigurewindowstyle','docked');

% error('havent fixed steps correlation issue')
%%
stepLengthRefSet = [];
stepWidthRefSet = [];
stepLengthIMUSet = [];
stepWidthIMUSet = [];
stepLengthwoZMPSet = [];
stepWidthwoZMPSet = [];

CoMLenRefSet  = [];
CoMWidRefSet  = [];
CoMLenIMUSet  = [];
CoMWidIMUSet  = [];
CoMLenwoZMPSet  = [];
CoMWidwoZMPSet  = [];


stepLengthRefSetturn = [];
stepWidthRefSetturn = [];
stepLengthIMUSetturn = [] ;
stepWidthIMUSetturn = [];
stepLengthwoZMPSetturn = [];
stepWidthwoZMPSetturn = [];

CoMLenRefSetturn  = [];
CoMWidRefSetturn  = [];
CoMLenIMUSetturn  = [];
CoMWidIMUSetturn  = [];
CoMLenwoZMPSetturn  = [];
CoMWidwoZMPSetturn  = [];

stepLengthRefSetStr = [];
stepWidthRefSetStr = [];
stepLengthIMUSetStr = [] ;
stepWidthIMUSetStr = [];
CoMWidRefSetStr  = [];
CoMWidIMUSetStr  = [];



%%
for usCt = 1:length(usList)
    usName = usList{usCt};
    trialList = fieldnames(IMUProcessedData.(usName));
    %         trialList = {'tr41','tr42','tr43', 'tr11','tr13','tr14',...
    %             'tr21','tr22','tr23','tr24','tr32','tr33','tr34'}; %, ...
    %         'tr52','tr53','tr54'};
    %     trialList = {'tr51','tr52','tr53','tr54', 'tr62','tr63','tr64'};
    %      trialList = {'tr41','tr42','tr43','tr44'};
    clear unselectTrial;
    unselectTrial(:,1) = cellfun(@(x) startsWith(x,'tr1'), trialList);
    unselectTrial(:,2) = cellfun(@(x) startsWith(x,'tr2'), trialList);
    unselectTrial(:,3) = cellfun(@(x) startsWith(x,'tr3'), trialList);
    unselectTrial(:,4) = cellfun(@(x) startsWith(x,'tr4'), trialList);
    unselectTrial(:,5) = cellfun(@(x) startsWith(x,'tr5'), trialList);
    unselectTrial(:,6) = cellfun(@(x) startsWith(x,'tr6'), trialList);
    unselectedTrials = sum(unselectTrial(:,[6]),2);
    trialList = trialList(~unselectedTrials); warning('trialselection happening')
    
    for trCt = 1: length(trialList) %
        trName = trialList{trCt};
        
        
        estDS = IMUProcessedData.(usName).(trName).EstData;
        refDS = IMUProcessedData.(usName).(trName).RefData;
        
        imuContact = estDS.IMUContacts.contactZV;
        imuContact = estDS.IMUContacts.contactDS; warning('imu contact adjusted being used')
        refContact = refDS.Body.contact;
        
        refRFpos = refDS.RightFoot.IMUpos;
        refLFpos = refDS.LeftFoot.IMUpos;
        refPFpos = refDS.CoM.pos;
        
        refData.RightFoot.pos = refRFpos;
        refData.LeftFoot.pos  = refLFpos;
        refData.CoM.pos       = refPFpos;
        
        refDatacg.RightFoot.pos = refDS.cgFrame.RightFoot.IMUpos;
        refDatacg.LeftFoot.pos  = refDS.cgFrame.LeftFoot.IMUpos;
        refDatacg.CoM.pos       = refDS.cgFrame.CoM.pos;
        
        refDatacg.RightFoot.vel = refDS.cgFrame.RightFoot.IMUvel;
        refDatacg.LeftFoot.vel  = refDS.cgFrame.LeftFoot.IMUvel;
        refDatacg.CoM.vel       = refDS.cgFrame.CoM.vel;
        
        estimStatesR = estDS.IMUdata{1}.stateIndex.allIMU;
        estimStatesL = estDS.IMUdata{2}.stateIndex.allIMU;
        estimStatesP = estDS.IMUdata{3}.stateIndex.allIMU;
        
        estimData.RightFoot.pos = estDS.Rcombo.(comboUsed).xMat(estimStatesR(1:3),:)';
        estimData.LeftFoot.pos  = estDS.Rcombo.(comboUsed).xMat(estimStatesL(1:3),:)';
        estimData.CoM.pos       = estDS.Rcombo.(comboUsed).xMat(estimStatesP(1:3),:)';
        
        estimDwoZMP.RightFoot.pos = estDS.wozmp.x(estimStatesR(1:3),:)';
        estimDwoZMP.LeftFoot.pos  = estDS.wozmp.x(estimStatesL(1:3),:)';
        estimDwoZMP.CoM.pos       = estDS.wozmp.x(estimStatesP(1:3),:)';
        
        estimData.Body.HeadingArray = estDS.cgVar.headArry;
        
        [imustepsR, imustepsL] = fun_findSteps(imuContact);
        [refstepsR, refstepsL] = fun_findSteps(refContact);
        
        
        [imustepsRL] = fun_findChronologicalSteps(imuContact);
        [refstepsRL] = fun_findChronologicalSteps(refContact);
        
        startStep = 4; removeEndSteps = 2;
        imustepsRLsnip = imustepsRL(startStep:end-removeEndSteps,:);
        refstepsRLsnip = refstepsRL(startStep:end-removeEndSteps,:);
        
        stepsArray.imu.SR{usCt,trCt} = imustepsR;
        stepsArray.imu.SL{usCt,trCt} = imustepsL;
        stepsArray.ref.SR{usCt,trCt} = refstepsR;
        stepsArray.ref.SL{usCt,trCt} = refstepsL;
        
        stepsWithFeetInfoRefus = [refstepsR(3:end-2,:) ones(length(refstepsR(3:end-2,:)),1);
            refstepsL(2:end-1,:) 2*ones(length(refstepsL(2:end-1,:)),1)];
        stepsWithFeetInfoIMUus = [imustepsR(3:end-2,:) ones(length(imustepsR(3:end-2,:)),1);
            imustepsL(2:end-1,:) 2*ones(length(imustepsL(2:end-1,:)),1)];
        
        %         [~,idx] = sort(stepsWithFeetInfoRefus(:,1)); % sort just the first column
        %         stepsWithFeetInfoRef = stepsWithFeetInfoRefus(idx,:);   % sort the whole matrix using the sort indices
        
        %         [~,idx] = sort(stepsWithFeetInfoIMUus(:,1)); % sort just the first column
        %         stepsWithFeetInfoIMU = stepsWithFeetInfoIMUus(idx,:);   % sort the whole matrix using the sort indices
        
        stepsWithFeetInfoRef = refstepsRLsnip;   % sort the whole matrix using the sort indices
        stepsWithFeetInfoIMU = imustepsRLsnip;   % sort the whole matrix using the sort indices
        
        [stepLengthsRef, stepWidthsRef, comLenRef, comWidRef]        = fun_findStepWiseSpatioTempParam(refData,stepsWithFeetInfoRef);
        [stepLengthsIMU, stepWidthsIMU, comLenIMU, comWidIMU]        = fun_findStepWiseSpatioTempParam(estimData,stepsWithFeetInfoIMU);
        [stepLengthswoZMP, stepWidthswoZMP, comLenwoZMP, comWidwoZMP]= fun_findStepWiseSpatioTempParam(estimDwoZMP,stepsWithFeetInfoIMU);
        
        
        if trCt == 1
        
        segmentedRef2      = fun_splitandTransformStepWise(refData,stepsWithFeetInfoRef);
        segmentedIMU2      = fun_splitandTransformStepWise5(estimData,stepsWithFeetInfoIMU);
        segmentedOp = fun_normCompareStepWise(segmentedIMU2,segmentedRef2);
        else
            segCurrentIMU      = fun_splitandTransformStepWise5(estimData,stepsWithFeetInfoIMU);
            createTrialWiseSegmentedVariable;
            
  
        
        
        end
        
        
        headingDir = estDS.cgVar.headInd;
        headingDir(:,1)  = 3 - headingDir(:,1);
        
        turnStepsInd = [];
        if length(headingDir) == length(stepsWithFeetInfoIMU)
            turnStepsInd = find(headingDir(:,4)>39);
            StrStepsInd = find(headingDir(:,4)<=39);
        end
        
        startStep = 2; removeEndSteps = 1;
        
        stepLengthRefSet   = [stepLengthRefSet; stepLengthsRef(startStep:end-removeEndSteps,1)];
        stepWidthRefSet    = [stepWidthRefSet; stepWidthsRef(startStep:end-removeEndSteps,1)];
        stepLengthIMUSet   = [stepLengthIMUSet; stepLengthsIMU(startStep:end-removeEndSteps,1)];
        stepWidthIMUSet    = [stepWidthIMUSet; stepWidthsIMU(startStep:end-removeEndSteps,1)];
        stepLengthwoZMPSet = [stepLengthwoZMPSet; stepLengthswoZMP(startStep:end-removeEndSteps,1)];
        stepWidthwoZMPSet  = [stepWidthwoZMPSet; stepWidthswoZMP(startStep:end-removeEndSteps,1)];
        
        
        CoMLenRefSet   = [CoMLenRefSet; comLenRef(startStep:end-removeEndSteps,1)];
        CoMWidRefSet   = [CoMWidRefSet; comWidRef(startStep:end-removeEndSteps,1)];
        CoMLenIMUSet   = [CoMLenIMUSet; comLenIMU(startStep:end-removeEndSteps,1)];
        CoMWidIMUSet   = [CoMWidIMUSet; comWidIMU(startStep:end-removeEndSteps,1)];
        CoMLenwoZMPSet = [CoMLenwoZMPSet; comLenwoZMP(startStep:end-removeEndSteps,1)];
        CoMWidwoZMPSet = [CoMWidwoZMPSet; comWidwoZMP(startStep:end-removeEndSteps,1)];
        
        
        if length(turnStepsInd) > 1
            stepLengthRefSetturn = [stepLengthRefSetturn; stepLengthsRef(turnStepsInd,1)];
            stepWidthRefSetturn = [stepWidthRefSetturn;  stepWidthsRef(turnStepsInd,1)];
            stepLengthIMUSetturn = [stepLengthIMUSetturn; stepLengthsIMU(turnStepsInd,1)] ;
            stepWidthIMUSetturn = [stepWidthIMUSetturn; stepWidthsIMU(turnStepsInd,1)];
            stepLengthwoZMPSetturn = [stepLengthwoZMPSetturn;  stepLengthswoZMP(turnStepsInd,1)];
            stepWidthwoZMPSetturn = [stepWidthwoZMPSetturn;  stepWidthswoZMP(turnStepsInd,1)];
            
            CoMLenRefSetturn  = [CoMLenRefSetturn;comLenRef(turnStepsInd,1)];
            CoMWidRefSetturn  = [CoMWidRefSetturn; comWidRef(turnStepsInd,1)];
            CoMLenIMUSetturn  = [CoMLenIMUSetturn; comLenIMU(turnStepsInd,1)];
            CoMWidIMUSetturn  = [CoMWidIMUSetturn;comWidIMU(turnStepsInd,1)];
            CoMLenwoZMPSetturn  = [CoMLenwoZMPSetturn;comLenwoZMP(turnStepsInd,1)];
            CoMWidwoZMPSetturn  = [CoMWidwoZMPSetturn; comWidwoZMP(turnStepsInd,1)];
            
            
            stepLengthRefSetStr = [stepLengthRefSetStr; stepLengthsRef(StrStepsInd,1)];
            stepWidthRefSetStr = [stepWidthRefSetStr;  stepWidthsRef(StrStepsInd,1)];
            stepLengthIMUSetStr = [stepLengthIMUSetStr; stepLengthsIMU(StrStepsInd,1)] ;
            stepWidthIMUSetStr = [stepWidthIMUSetStr; stepWidthsIMU(StrStepsInd,1)];
            CoMWidRefSetStr  = [CoMWidRefSetStr; comWidRef(StrStepsInd,1)];
            CoMWidIMUSetStr  = [CoMWidIMUSetStr;comWidIMU(StrStepsInd,1)];
            
            
        end
        
    end
    
    
end

IndOutliers = [898,1322,1323,1325,1327];
IndOutliers = [1067];
IndOutliers = [898];
if removeTrials
    stepLengthRefSet(IndOutliers,:) = [];
    stepWidthRefSet(IndOutliers,:) = [];
    stepLengthIMUSet(IndOutliers,:) = [];
    stepWidthIMUSet(IndOutliers,:) = [];
    stepLengthwoZMPSet(IndOutliers,:) = [];
    stepWidthwoZMPSet(IndOutliers,:) = [];
    
    CoMWidRefSet(IndOutliers,:) = []; warning('Removing a few outlier datapoints')
    CoMWidIMUSet(IndOutliers,:) = []; warning('Removing a few outlier datapoints')
end
%%
meanStepLengthRef   = [mean(stepLengthRefSet)   std(stepLengthRefSet)];
meanStepWidthsRef   = [mean(stepWidthRefSet)    std(stepWidthRefSet)];
meanStepLengthIMU   = [mean(stepLengthIMUSet)   std(stepLengthIMUSet)];
meanStepWidthsIMU   = [mean(stepWidthIMUSet)    std(stepWidthIMUSet)];
meanStepLengthwoZMP = [mean(stepLengthwoZMPSet) std(stepLengthwoZMPSet)];
meanStepWidthswoZMP = [mean(stepWidthwoZMPSet)  std(stepWidthwoZMPSet)];

IndOutliers2 = [639 642 644];
IndOutliers2 = [];
if removeTrials
    stepLengthRefSetStr(IndOutliers2,:) = [];
    stepLengthIMUSetStr(IndOutliers2,:) = [];
    stepWidthRefSetStr(IndOutliers2,:) = [];
    stepWidthIMUSetStr(IndOutliers2,:) = [];
    CoMWidRefSetStr(IndOutliers2,:) = [];
    CoMWidIMUSetStr(IndOutliers2,:) = [];
    
    warning('Removing a few outlier datapoints')
    warning('Removing a few outlier datapoints')
end
%%
CumTypeWiseRFNorm.Ref = [];
CumTypeWiseRFNorm.Est = [];
CumTypeWiseLFNorm.Ref = [];
CumTypeWiseLFNorm.Est = [];
CumTypeWisePSNorm.Ref = [];
CumTypeWisePSNorm.Est = [];
comLength =  length(segmentedIMU2.RightFoot.pos.sets);
colCount = 3;
corrSteps = [];
errSteps = [];
% figure('name','IMU and Ref step wise');

for i = 1:comLength
    %     subplot(5,colCount,i);
    clear tVarR tVarL tVarP  tVarRefR tVarRefL tVarRefP
    for axC = 1:3
        tVarR(:,axC) = normalizetimebase(segmentedIMU2.RightFoot.pos.sets{i}(:,axC));
        tVarL(:,axC) = normalizetimebase(segmentedIMU2.LeftFoot.pos.sets{i}(:,axC));
        tVarP(:,axC) = normalizetimebase(segmentedIMU2.CoM.pos.sets{i}(:,axC));
        
        tVarRefR(:,axC) = normalizetimebase(segmentedRef2.RightFoot.pos.sets{i}(:,axC));
        tVarRefL(:,axC) = normalizetimebase(segmentedRef2.LeftFoot.pos.sets{i}(:,axC) );
        tVarRefP(:,axC) = normalizetimebase(segmentedRef2.CoM.pos.sets{i}(:,axC));
    end
    %     scatter(tVarR(:,1),tVarR(:,2), [],[134 19 19]/255); hold on
    %     scatter(tVarRefR(:,1),tVarRefR(:,2), [],[1 0.5 0.5]); hold on
    %     scatter(tVarL(:,1),tVarL(:,2), [],[0 0 1]); hold on
    %     scatter(tVarRefL(:,1),tVarRefL(:,2), [],[0.7 0.7 1]); hold on
    %     scatter(tVarP(:,1),tVarP(:,2), [],[11 108 46]/255); hold on
    %     scatter(tVarRefP(:,1),tVarRefP(:,2), [],[0.5 1 0.5]); hold on
    
    errSteps = [errSteps; rms(tVarR-tVarRefR) rms(tVarL-tVarRefL) rms(tVarP-tVarRefP)];
    corrSteps = [corrSteps; diag(corr(tVarR,tVarRefR))'  diag(corr(tVarL,tVarRefL))'  diag(corr(tVarP,tVarRefP))'];
    
    CumTypeWiseRFNorm.Ref = [CumTypeWiseRFNorm.Ref; tVarR];
    CumTypeWiseRFNorm.Est = [CumTypeWiseRFNorm.Est; tVarRefR];
    CumTypeWiseLFNorm.Ref = [CumTypeWiseLFNorm.Ref; tVarL];
    CumTypeWiseLFNorm.Est = [CumTypeWiseLFNorm.Est; tVarRefL];
    CumTypeWisePSNorm.Ref = [CumTypeWisePSNorm.Ref; tVarP];
    CumTypeWisePSNorm.Est = [CumTypeWisePSNorm.Est; tVarRefP];
    
    
    %     xlim([-0.05 1.3]);
    %     ylim([-0.3 0.45]);
    
    %     if i == 1
    %         lgH =   legend('RI','RR','LI','LR','PI','PR');
    %     end
end

%%
figure('name','L & R - Compare');

curpl =subplot(1,3,1);
AllstepLengths = [stepLengthRefSet(:,1); stepLengthIMUSet(:,1);stepLengthwoZMPSet(:,1)];
g = [zeros(length(stepLengthRefSet(:,1)), 1); ones(length(stepLengthIMUSet(:,1)), 1); 2*ones(length(stepLengthwoZMPSet(:,1)), 1)];
boxplot(AllstepLengths,g);
curpl.XTickLabel = {'VICON', 'IMU', 'No ZMP'};
ylabel('Step length (m)')
title('Step lengths')


diffTest = ttest2(stepLengthRefSet(:,1),stepLengthIMUSet(:,1));

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
    
end

diffTest = ttest2(stepLengthwoZMPSet(:,1),stepLengthIMUSet(:,1));

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([2 3]), [1 1]*max(yt)*1.1, '-k',  mean(xt([2 3])), max(yt)*1.15, '*k')
    
end

diffTest = ttest2(stepLengthRefSet(:,1),stepLengthwoZMPSet(:,1));

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 3]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 3])), max(yt)*1.15, '*k')
    hold off
    box off
end


curpl =subplot(1,3,2);
AllstepWidths = [stepWidthRefSet(:,1); stepWidthIMUSet(:,1);stepWidthwoZMPSet(:,1)];
g = [zeros(length(stepWidthRefSet(:,1)), 1); ones(length(stepWidthIMUSet(:,1)), 1); 2*ones(length(stepWidthwoZMPSet(:,1)), 1)];
boxplot(AllstepWidths,g);
curpl.XTickLabel = {'VICON', 'IMU', 'No ZMP'};
ylabel('Step Width (m)')
title(['Step Widths cmb: ' comboUsed])



diffTest = ttest2(stepWidthRefSet(:,1),stepWidthIMUSet(:,1));

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
end

diffTest = ttest2(stepWidthwoZMPSet(:,1),stepWidthIMUSet(:,1));

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([2 3]), [1 1]*max(yt)*1.1, '-k',  mean(xt([2 3])), max(yt)*1.15, '*k')
end

diffTest = ttest2(stepWidthwoZMPSet(:,1),stepWidthRefSet(:,1));

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 3]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 3])), max(yt)*1.15, '*k')
    hold off
    box off
end



curpl =subplot(1,3,3);
AllstepWidths = [CoMWidRefSet(:,1); CoMWidIMUSet(:,1); CoMWidwoZMPSet(:,1)];
g = [zeros(length(CoMWidRefSet(:,1)), 1); ones(length(CoMWidIMUSet(:,1)), 1); 2*ones(length(CoMWidwoZMPSet(:,1)), 1)];
boxplot(AllstepWidths,g);
curpl.XTickLabel = {'VICON', 'IMU', 'No ZMP'};
ylabel('CoM Widths (m)')
title('CoM Widths - US ')



diffTest = ttest2(CoMWidRefSet(:,1),CoMWidIMUSet(:,1));

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 2]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 2])), max(yt)*1.15, '*k')
end

diffTest = ttest2(CoMWidwoZMPSet(:,1),CoMWidIMUSet(:,1));

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([2 3]), [1 1]*max(yt)*1.1, '-k',  mean(xt([2 3])), max(yt)*1.15, '*k')
end

diffTest = ttest2(CoMWidwoZMPSet(:,1),CoMWidRefSet(:,1));

if diffTest
    yt = curpl.YTick;
    axis([xlim    0  ceil(max(yt)*1.2)])
    xt = curpl.XTick;
    hold on
    plot(xt([1 3]), [1 1]*max(yt)*1.1, '-k',  mean(xt([1 3])), max(yt)*1.15, '*k')
    hold off
    box off
end

%%

%%
varNames = {'VICON','IMU', 'm'};
BAdispOpt = {'eq';'r';'RMSE';'n'};
% figure;
slFig = figure('name','Step Lengths BA');
% slFig = subplot(311);
[~, ~, stepLStat] = BlandAltman(slFig,stepLengthRefSet(1:end),stepLengthIMUSet(1:end),varNames,...
    {},{},'corrinfo',BAdispOpt);
swFig = figure('name','Step Widths BA');
% swFig = subplot(312);
[~, ~, stepWStat] = BlandAltman(swFig,stepWidthRefSet(1:end),stepWidthIMUSet(1:end),varNames,{},{},'corrinfo',BAdispOpt);
cwFig = figure('name','CoM Widths BA');
% cwFig = subplot(313);
[~, ~, stepCStat] = BlandAltman(cwFig,CoMWidRefSet(1:end),CoMWidIMUSet,varNames,{},{},'corrinfo',BAdispOpt);


[~, ~,stepLengthsStrCorr] = BlandAltman(stepLengthRefSetStr,stepLengthIMUSetStr,varNames,{},{},'corrinfo',BAdispOpt);
[~, ~,stepWidthsStrCorr]  = BlandAltman(stepWidthRefSetStr,stepWidthIMUSetStr,varNames,{},{},'corrinfo',BAdispOpt);
[~, ~,CoMWidthsStrCorr] = BlandAltman(CoMWidRefSetStr,CoMWidIMUSetStr,varNames,{},{},'corrinfo',BAdispOpt);

%%
fntSize = 20;


axHandles =slFig.Children;
ax1 = axHandles(2);
scatter(ax1,stepLengthRefSetturn,stepLengthIMUSetturn,'ro','filled')

ax2 = axHandles(1);
delV = stepLengthIMUSetturn - stepLengthRefSetturn;
avgV = mean([stepLengthRefSetturn stepLengthIMUSetturn],2);
scatter(ax2,avgV,delV,'ro','filled')
if publicationplot
    ax1.FontSize = fntSize;
    ax1.FontWeight = 'bold';
    ax2.FontSize = fntSize;
    ax2.FontWeight = 'bold';
    ax1.Position = ax1.Position + [-0.05 -0.01 0.05 0.05];
    ax2.Position = ax2.Position + [-0.04 -0.01 0.05 0.05];
end




axHandles =swFig.Children;
ax1 = axHandles(2);
scatter(ax1,stepWidthRefSetturn,stepWidthIMUSetturn,'ro','filled')

ax2 = axHandles(1);
delV = stepWidthIMUSetturn - stepWidthRefSetturn;
avgV = mean([stepWidthIMUSetturn stepWidthRefSetturn],2);
scatter(ax2,avgV,delV,'ro','filled')

if publicationplot
    ax1.FontSize = fntSize;
    ax1.FontWeight = 'bold';
    ax2.FontSize = fntSize;
    ax2.FontWeight = 'bold';
    ax1.Position = ax1.Position + [-0.05 -0.01 0.05 0.05];
    ax2.Position = ax2.Position + [-0.04 -0.01 0.05 0.05];
end


axHandles =cwFig.Children;
ax1 = axHandles(2);
scatter(ax1,CoMWidRefSetturn,CoMWidIMUSetturn,'ro','filled')

ax2 = axHandles(1);
delV = CoMWidIMUSetturn - CoMWidRefSetturn;
avgV = mean([CoMWidIMUSetturn CoMWidRefSetturn],2);
scatter(ax2,avgV,delV,'ro','filled')

if publicationplot
    ax1.FontSize = fntSize;
    ax1.FontWeight = 'bold';
    ax2.FontSize = fntSize;
    ax2.FontWeight = 'bold';
    ax1.Position = ax1.Position + [-0.05 -0.01 0.05 0.05];
    ax2.Position = ax2.Position + [-0.03 -0.01 0.05 0.05];
end
%%
% figure;
% subplot(231)
% histogram(stepLengthRefSet(1:end))
% subplot(234)
% histogram(stepLengthIMUSet(1:end))
%
% subplot(232)
% histogram(stepWidthRefSet(1:end))
% subplot(235)
% histogram(stepWidthIMUSet(1:end))
%
% subplot(233)
% histogram(CoMWidRefSet(1:end))
% subplot(236)
% histogram(CoMWidIMUSet(1:end))
mean(errSteps)*100
