%% plotPub_MLMoSSubwiseMarker

%%
dat1Mode = 'Compare'; % Compare or Truth
baStatsMode = 'Gaussian'; % Gaussian or Non-parametric
varNames = {'ForceShoe','PGL', 'm'};
% BAdispOpt = {'eq';'r';'RMSE';'n'};
BAdispOptCor = {}; BAdispOptBA = {};

%%
% ML R
mlrFig = figure('name','ML MoS Aff');
% BlandAltman(CumMLAMoS_SW.Ref,CumMLAMoS_SW.Est,varNames,'ML MoS Aff',{},'corrinfo',BAdispOpt);
[~, ~,MLAstrt,baPlotsA] = BlandAltmanDual(mlrFig,CumMLAMoS_SW.Ref,CumMLAMoS_SW.Est,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','b','MarkerSize',1,...
    'baStatsMode',baStatsMode,'data1Mode',dat1Mode);
hold on;
[~, ~,MLAturn] = BlandAltmanDual(mlrFig,CumMLAMoS_FL.Ref,CumMLAMoS_FL.Est,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','r','MarkerSize',1,...
    'baStatsMode',baStatsMode,'data1Mode',dat1Mode);

SigDiff.MLMoSAff_SW = ttest2(CumMLAMoS_SW.Ref,CumMLAMoS_SW.Est);
SigDiff.MLMoSAff_FL = ttest2(CumMLAMoS_FL.Ref,CumMLAMoS_FL.Est);

OverAllMean.MLAff_Ref = [mean([CumMLAMoS_SW.Ref; CumMLAMoS_FL.Ref]) std([CumMLAMoS_SW.Ref; CumMLAMoS_FL.Ref])];
OverAllMean.MLAff_Est = [mean([CumMLAMoS_SW.Est; CumMLAMoS_FL.Est]) std([CumMLAMoS_SW.Est; CumMLAMoS_FL.Est])];

MLAs_MAD = mean(abs(CumMLAMoS_SW.Ref - CumMLAMoS_SW.Est));
MLAt_MAD = mean(abs(CumMLAMoS_FL.Ref - CumMLAMoS_FL.Est));

MLAstrLoA = [MLAstrt.differenceMean-MLAstrt.rpc MLAstrt.differenceMean+MLAstrt.rpc]*100; % cm
MLAturLoA = [MLAturn.differenceMean-MLAturn.rpc MLAturn.differenceMean+MLAturn.rpc]*100; % cm

strCorP = MLAstrt.corrP <= 0.05;
strCor = [num2str(round(MLAstrt.r,2)) char(strCorP* '*')];

turCorP = MLAturn.corrP <= 0.05;
turCor = [num2str(round(MLAturn.r,2)) char(turCorP* '*')];

strRms = num2str(round(MLAstrt.RMSE*100,2));
turRms = num2str(round(MLAturn.RMSE*100,2));

for usCt = 1:length(usList)
    set(mlrFig,'CurrentAxes',baPlotsA{1})
    plot(CumSubMLAMoS_SW.Ref{usCt},CumSubMLAMoS_SW.Est{usCt},markerCell{usCt},'Color','b','LineWidth',2,'LineWidth',2,'MarkerSize',10);
    plot(CumSubMLAMoS_FL.Ref{usCt},CumSubMLAMoS_FL.Est{usCt},markerCell{usCt},'Color','r','LineWidth',2,'LineWidth',2,'MarkerSize',10)
    xlim([0 0.4]); ylim([0 0.4])
    
    set(mlrFig,'CurrentAxes',baPlotsA{2})
    plot((CumSubMLAMoS_SW.Ref{usCt}+CumSubMLAMoS_SW.Est{usCt})/2,CumSubMLAMoS_SW.Est{usCt}-CumSubMLAMoS_SW.Ref{usCt},markerCell{usCt},'Color','b','LineWidth',2,'LineWidth',2,'MarkerSize',10);
    plot((CumSubMLAMoS_FL.Ref{usCt}+CumSubMLAMoS_FL.Est{usCt})/2,CumSubMLAMoS_FL.Est{usCt}-CumSubMLAMoS_FL.Ref{usCt},markerCell{usCt},'Color','r','LineWidth',2,'LineWidth',2,'MarkerSize',10);
    xlim2 = 0.35;
    
    xlim([0 xlim2]); ylim([-0.3 0.3])
    fontsize = 20;
    if MLAstrt.differenceMeanP < 0.05
        text(xlim2,MLAstrt.differenceMean+0.02,[mynum2str(MLAstrt.differenceMean,2) '*'],'HorizontalAlignment','left','VerticalAlignment','middle','fontsize',fontsize,'FontWeight','bold');
    else
        text(xlim2,MLAstrt.differenceMean+0.02,[mynum2str(MLAstrt.differenceMean,2) ],'HorizontalAlignment','left','VerticalAlignment','middle','fontsize',fontsize,'FontWeight','bold');
    end
    if MLAturn.differenceMeanP < 0.05
        text(xlim2,MLAturn.differenceMean-0.02,[mynum2str(MLAturn.differenceMean,2) '*'],'HorizontalAlignment','left','VerticalAlignment','middle','fontsize',fontsize,'FontWeight','bold');
    else
        text(xlim2,MLAturn.differenceMean-0.02,[mynum2str(MLAturn.differenceMean,2) ],'HorizontalAlignment','left','VerticalAlignment','middle','fontsize',fontsize,'FontWeight','bold');
    end
    
end

dim = [0.14 0.58 0.3 0.3];
str = ['Steady state, r:' strCor ', RMSE: ' strRms ' cm'  newline 'Other, r:' turCor ', RMSE: ' turRms ' cm'];
anH = annotation('textbox',dim,'String',str,'FitBoxToText','on');
anH.FontSize = 20; anH.FontWeight = 'bold'; anH.BackgroundColor = 'white';

f=get(gca,'Children');
legend([f(27),f(21)],'Steady state','Other')
%%
% ML N MoS
mllFig = figure('name','ML MoS Less Aff');
% BlandAltman(mllFig,CumMLLMoS.Ref,CumMLLMoS.Est,varNames,'ML MoS Less Aff',{},'corrinfo',BAdispOpt);
[~, ~,MLNstrt,baPlotsN] = BlandAltmanDual(mllFig,CumMLNMoS_SW.Ref,CumMLNMoS_SW.Est,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','b','MarkerSize',1,...
    'baStatsMode',baStatsMode,'data1Mode',dat1Mode);
hold on;
[~, ~,MLNturn] = BlandAltmanDual(mllFig,CumMLNMoS_FL.Ref,CumMLNMoS_FL.Est,varNames,...
    {},{},'corrinfo',BAdispOptCor,'bainfo',BAdispOptBA,'colors','r','MarkerSize',1,...
    'baStatsMode',baStatsMode,'data1Mode',dat1Mode);

SigDiff.MLMoSNAff_SW = ttest2(CumMLNMoS_SW.Ref,CumMLNMoS_SW.Est);
SigDiff.MLMoSNAff_FL = ttest2(CumMLNMoS_FL.Ref,CumMLNMoS_FL.Est);

OverAllMean.MLNAff_Ref = [mean([CumMLNMoS_SW.Ref; CumMLNMoS_FL.Ref]) std([CumMLNMoS_SW.Ref; CumMLNMoS_FL.Ref])];
OverAllMean.MLNAff_Est = [mean([CumMLNMoS_SW.Est; CumMLNMoS_FL.Est]) std([CumMLNMoS_SW.Est; CumMLNMoS_FL.Est])];

MLNs_MAD = mean(abs(CumMLNMoS_SW.Ref - CumMLNMoS_SW.Est));
MLNt_MAD = mean(abs(CumMLNMoS_FL.Ref - CumMLNMoS_FL.Est));

MLNstrLoA = [MLNstrt.differenceMean-MLNstrt.rpc MLNstrt.differenceMean+MLNstrt.rpc]*100; % cm
MLNturLoA = [MLNturn.differenceMean-MLNturn.rpc MLNturn.differenceMean+MLNturn.rpc]*100; % cm

strCorP = MLNstrt.corrP <= 0.05;
if strCorP
    strCor = [num2str(round(MLNstrt.r,2)) char(strCorP* '*')];
else
    strCor = num2str(round(MLNstrt.r,2));
end

turCorP = MLNturn.corrP <= 0.05;
if turCorP
    turCor = [num2str(round(MLNturn.r,2)) char(turCorP* '*')];
else
    turCor = num2str(round(MLNturn.r,2));
end

strRms = num2str(round(MLNstrt.RMSE*100,2));
turRms = num2str(round(MLNturn.RMSE*100,2));

for usCt = 1:length(usList)
    set(mllFig,'CurrentAxes',baPlotsN{1})
    plot(CumSubMLNMoS_SW.Ref{usCt},CumSubMLNMoS_SW.Est{usCt},markerCell{usCt},'Color','b','LineWidth',2,'LineWidth',2,'MarkerSize',10);
    plot(CumSubMLNMoS_FL.Ref{usCt},CumSubMLNMoS_FL.Est{usCt},markerCell{usCt},'Color','r','LineWidth',2,'LineWidth',2,'MarkerSize',10)
    xlim([0 0.4]); ylim([0 0.4])
    
    set(mllFig,'CurrentAxes',baPlotsN{2})
    plot((CumSubMLNMoS_SW.Ref{usCt}+CumSubMLNMoS_SW.Est{usCt})/2,CumSubMLNMoS_SW.Est{usCt}-CumSubMLNMoS_SW.Ref{usCt},markerCell{usCt},'Color','b','LineWidth',2,'LineWidth',2,'MarkerSize',10);
    plot((CumSubMLNMoS_FL.Ref{usCt}+CumSubMLNMoS_FL.Est{usCt})/2,CumSubMLNMoS_FL.Est{usCt}-CumSubMLNMoS_FL.Ref{usCt},markerCell{usCt},'Color','r','LineWidth',2,'LineWidth',2,'MarkerSize',10);
    xlim2 = 0.35;
    
    xlim([0 xlim2]); ylim([-0.3 0.3])
    fontsize = 20;
    if MLNstrt.differenceMeanP < 0.05
        text(xlim2,MLNstrt.differenceMean+0.02,[mynum2str(MLNstrt.differenceMean,2) '*'],'HorizontalAlignment','left','VerticalAlignment','middle','fontsize',fontsize,'FontWeight','bold');
    else
        text(xlim2,MLNstrt.differenceMean+0.02,[mynum2str(MLNstrt.differenceMean,2) ],'HorizontalAlignment','left','VerticalAlignment','middle','fontsize',fontsize,'FontWeight','bold');
    end
    if MLNturn.differenceMeanP < 0.05
        text(xlim2,MLNturn.differenceMean-0.02,[mynum2str(MLNturn.differenceMean,2) '*'],'HorizontalAlignment','left','VerticalAlignment','middle','fontsize',fontsize,'FontWeight','bold');
    else
        text(xlim2,MLNturn.differenceMean-0.02,[mynum2str(MLNturn.differenceMean,2) ],'HorizontalAlignment','left','VerticalAlignment','middle','fontsize',fontsize,'FontWeight','bold');
    end
end

dim = [0.14 0.58 0.3 0.3];
str = ['Steady state, r:' strCor ', RMSE: ' strRms ' cm'  newline 'Other, r:' turCor ', RMSE: ' turRms ' cm'];
anH = annotation('textbox',dim,'String',str,'FitBoxToText','on');
anH.FontSize = 20; anH.FontWeight = 'bold'; anH.BackgroundColor = 'white';

f=get(gca,'Children');
legend([f(27),f(21)],'Steady state','Other')