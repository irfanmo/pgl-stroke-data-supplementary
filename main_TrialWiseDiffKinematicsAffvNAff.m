%% main_TrialWiseDiffKinematics
% Generate results for Paper
% M I Mohamed Refai 2021
% edited for ICORR 2022
%% Overview
% Script loads the processed data from the Portable Gait Lab algorithm
% ProcessedData_1218a contains data from four subjects
% Data includes reference and PGL output for 3D CoM and Foot positions

%% Results:
% compTableSubwiseErrors summarizes all errors
% CumStatsAllSegments.RMS CumStatsFtSegments.RMS
% CumStatsCsegpos.Everything.RMS
% CumStatsSLDiff.ty_2 CumStatsAPMoSDiff.ty_2 CumStatsMLMoSDiff.ty_2 
% CumStatsAPMoSDiffP.ty_2 CumStatsMLMoSDiffP.ty_2 
%
% plotPubPlots2.m plots the Figures 2 - 4 provided in the article
%
%%
clear all; close all;
load('ProcessedData_1218a.mat')
addpath(genpath('Functions'))
set(0,'defaultfigurewindowstyle','docked');
%%
preselCombo = 0;

prevType = 0; usCur = 0;
useFirst10m = 1;
dataOrigin = 'IFSUS'; % IFSUS

plotTestGraphs = 0;
balanceAnalysis = 1;

synchSignals = 0;
transformSignals = 0;
apOutlierRemoval = 0;
refcontactUsingFS = 0;
stanceTimeReverseVel = 0;
%%

%%
usList = fieldnames(IMUProcessedData);

for usCt = 1:length(usList)
    usName = usList{usCt};
    usCur  = usName;
    
    
    if ~preselCombo
        switch usName
            case 'us1'
                comboUsed = 'rval_3';
            case 'us6'
                comboUsed = 'rval_6';
            case 'us7'
                comboUsed = 'rval_4';
            case 'us12'
                comboUsed = 'rval_4';
        end
    end
    
    trialList = fieldnames(IMUProcessedData.(usName));
    
    for trCt = 1: length(trialList) %
        %% trial name
        trName = trialList{trCt};
        trialCur = str2num(trName(end-1:end));
        trialNo  = mod(trialCur,10);
        typeCur  = (trialCur - trialNo)/10;
        typeStr  = (['ty_' num2str(typeCur)]);
        
        %% initializing output variables
        initEmptyStructures;
        
        %% restructuring and assigning variables
        assignProcessedVars;
        %         assignProcessedVarsAffNAff
        
        %% estimating steps
        if strcmpi(affFoot,'Right')
            WinAr1 = [sampFq*0.0 sampFq*0.03]; WinAr2 = [sampFq*0.0 sampFq*0.1];
        elseif strcmpi(affFoot,'Left')
            WinAr1 = [sampFq*0.0 sampFq*0.1]; WinAr2 = [sampFq*0.0 sampFq*0.03];
        end
        
        clear contact_DSadj
        [contact_DSadj(:,1)] = fun_findandFixDoubleStance2(imuContactZV(:,1),sampFq,WinAr1);
        [contact_DSadj(:,2)] = fun_findandFixDoubleStance2(imuContactZV(:,2),sampFq,WinAr2);
        
        imuContact = imuContactDS;
        %         imuContact = contact_DSadj;
        %                 imuContact = imuContactZV;
        refContact = imuContact;
        
        if plotTestGraphs
            figure;
            subplot(211); plot(contact_DSadj(:,1)); hold on; plot(refContactFS(:,1)*0.9)
            subplot(212); plot(contact_DSadj(:,2)); hold on; plot(refContactFS(:,2)*0.9)
        end
        
        [imustepsRL] = fun_findChronologicalSteps(imuContact, subSpecs);
        [refstepsRL] = fun_findChronologicalSteps(refContact, subSpecs);
        startStep = 2; removeEndSteps = 0;
        
        stepsWithFeetInfoIMU = imustepsRL(startStep:end-removeEndSteps,:);
        stepsWithFeetInfoRef = refstepsRL(startStep:end-removeEndSteps,:);
        
        stepsWithFeetInfoIMU(2:end,1) = stepsWithFeetInfoIMU(2:end,1); % reducing heel strike estimation as this was overestimated in data
        stepsWithFeetInfoRef(2:end,1) = stepsWithFeetInfoRef(2:end,1);
        
        stepsWithFeetInfoIMU(2:end-1,2) = stepsWithFeetInfoIMU(2:end-1,2); % reducing heel strike estimation as this was overestimated in data
        stepsWithFeetInfoRef(2:end-1,2) = stepsWithFeetInfoRef(2:end-1,2);
        
        
        
        snipLen = min([length(stepsWithFeetInfoIMU), length(estDS.cgVar.headInd)]);
        
        headingDir = estDS.cgVar.headInd(1:snipLen,:);
        strStepsInd = find(headingDir(:,4)<50);
        
        
        %% Additional Processing Steps
        addPostProcessing; % transform and synch if needed
        
        %% stepwise data
        
        estStepWiseDataPostProcessing;
        
        
        %% separating affected footWise
        
        switch affFoot
            case 'Right'
                CumTypeWiseAFSeg.Ref.(usName).(typeStr) = [CumTypeWiseAFSeg.Ref.(usName).(typeStr); refrfSeg];
                CumTypeWiseAFSeg.Est.(usName).(typeStr) = [CumTypeWiseAFSeg.Est.(usName).(typeStr); estrfSeg];
                CumTypeWiseNFSeg.Ref.(usName).(typeStr) = [CumTypeWiseNFSeg.Ref.(usName).(typeStr); reflfSeg];
                CumTypeWiseNFSeg.Est.(usName).(typeStr) = [CumTypeWiseNFSeg.Est.(usName).(typeStr); estlfSeg];
                
                CumTypeWiseSLA.Ref.(usName).(typeStr) = [CumTypeWiseSLA.Ref.(usName).(typeStr); stepLengthsRefR];
                CumTypeWiseSLA.Est.(usName).(typeStr) = [CumTypeWiseSLA.Est.(usName).(typeStr); stepLengthsIMUR];
                CumTypeWiseSWA.Ref.(usName).(typeStr) = [CumTypeWiseSWA.Ref.(usName).(typeStr); stepWidthsRefR];
                CumTypeWiseSWA.Est.(usName).(typeStr) = [CumTypeWiseSWA.Est.(usName).(typeStr); stepWidthsIMUR];
                CumTypeWiseSLN.Ref.(usName).(typeStr) = [CumTypeWiseSLN.Ref.(usName).(typeStr); stepLengthsRefL];
                CumTypeWiseSLN.Est.(usName).(typeStr) = [CumTypeWiseSLN.Est.(usName).(typeStr); stepLengthsIMUL];
                CumTypeWiseSWN.Ref.(usName).(typeStr) = [CumTypeWiseSWN.Ref.(usName).(typeStr); stepWidthsRefL];
                CumTypeWiseSWN.Est.(usName).(typeStr) = [CumTypeWiseSWN.Est.(usName).(typeStr); stepWidthsIMUL];
                
            case 'Left'
                CumTypeWiseAFSeg.Ref.(usName).(typeStr) = [CumTypeWiseAFSeg.Ref.(usName).(typeStr); reflfSeg];
                CumTypeWiseAFSeg.Est.(usName).(typeStr) = [CumTypeWiseAFSeg.Est.(usName).(typeStr); estlfSeg];
                CumTypeWiseNFSeg.Ref.(usName).(typeStr) = [CumTypeWiseNFSeg.Ref.(usName).(typeStr); refrfSeg];
                CumTypeWiseNFSeg.Est.(usName).(typeStr) = [CumTypeWiseNFSeg.Est.(usName).(typeStr); estrfSeg];
                
                
                CumTypeWiseSLA.Ref.(usName).(typeStr) = [CumTypeWiseSLA.Ref.(usName).(typeStr); stepLengthsRefL];
                CumTypeWiseSLA.Est.(usName).(typeStr) = [CumTypeWiseSLA.Est.(usName).(typeStr); stepLengthsIMUL];
                CumTypeWiseSWA.Ref.(usName).(typeStr) = [CumTypeWiseSWA.Ref.(usName).(typeStr); stepWidthsRefL];
                CumTypeWiseSWA.Est.(usName).(typeStr) = [CumTypeWiseSWA.Est.(usName).(typeStr); stepWidthsIMUL];
                
                CumTypeWiseSLN.Ref.(usName).(typeStr) = [CumTypeWiseSLN.Ref.(usName).(typeStr); stepLengthsRefR];
                CumTypeWiseSLN.Est.(usName).(typeStr) = [CumTypeWiseSLN.Est.(usName).(typeStr); stepLengthsIMUR];
                CumTypeWiseSWN.Ref.(usName).(typeStr) = [CumTypeWiseSWN.Ref.(usName).(typeStr); stepWidthsRefR];
                CumTypeWiseSWN.Est.(usName).(typeStr) = [CumTypeWiseSWN.Est.(usName).(typeStr); stepWidthsIMUR];
                
        end
        
        if plotTestGraphs
            plotStepWiseData;
        end
        
        CumTypeWisePSSeg.Ref.(usName).(typeStr) = [CumTypeWisePSSeg.Ref.(usName).(typeStr); refpsSeg];
        CumTypeWisePSSeg.Est.(usName).(typeStr) = [CumTypeWisePSSeg.Est.(usName).(typeStr); estpsSeg];
        
        CumTypeWiseLen.Ref.(usName).(typeStr) = [ CumTypeWiseLen.Ref.(usName).(typeStr); length(stepsWithFeetInfoRefsync)];
        CumTypeWiseLen.Est.(usName).(typeStr) = [ CumTypeWiseLen.Est.(usName).(typeStr); length(stepsWithFeetInfoIMUsync)];
        
        
        
        %% useful kinematics
        %% single stance estimations
        %% single support times Buurke 2019: between contralateral toe off and heel strike
        velCoff = 0.10;
        
        refVel_r = butterfilterlpf(normh(rDatasync.RightFoot.vel),10,sampFq,2);
        refVel_l = butterfilterlpf(normh(rDatasync.LeftFoot.vel),10,sampFq,2);
        refVel_r = refVel_r - mean(refVel_r(1:50));  refVel_l = refVel_l - mean(refVel_l(1:50));
        
        refVcont =  [refVel_r < velCoff refVel_l < velCoff];
        if stanceTimeReverseVel
            refVcont =  1 - [refVel_r > velCoff refVel_l > velCoff];
        elseif refcontactUsingFS
            refVcont = refDS.gframe.Forces.Contact(1:endMeas,:);
        end
        
        
        [refstepsR2, refstepsL2] = fun_findSteps(refVcont);
        ssTime = (refstepsR2(:,2) - refstepsR2(:,1))./sampFq;
        stanceTime_Ref_R = ssTime(2:end-1); clear ssTime
        
        ssTime = (refstepsL2(:,2) - refstepsL2(:,1))./sampFq;
        stanceTime_Ref_L = ssTime(2:end-1); clear ssTime
        
        % estimData
        estVel_r = butterfilterlpf(normh(eDatasync.RightFoot.vel),10,sampFq,2);
        estVel_l = butterfilterlpf(normh(eDatasync.LeftFoot.vel),10,sampFq,2);
        estVel_r = estVel_r - mean(estVel_r(1:50));  estVel_l = estVel_l - mean(estVel_l(1:50));
        
        if plotTestGraphs
            figure; subplot(411); plot(refVel_r); hold on; plot([1 length(refVel_r)], [velCoff velCoff])
            subplot(412); plot(refVel_l); hold on; plot([1 length(refVel_l)], [velCoff velCoff])
            subplot(413); plot(estVel_r); hold on; plot([1 length(estVel_r)], [velCoff velCoff])
            subplot(414); plot(estVel_l); hold on; plot([1 length(estVel_l)], [velCoff velCoff])
        end
        
        
        estVcont =  [estVel_r < velCoff estVel_l < velCoff];
        if stanceTimeReverseVel
            estVcont =  1 -[estVel_r > velCoff estVel_l > velCoff];
        elseif refcontactUsingFS
            estVcont = imuContact;
        end
        
        [eststepsR2, eststepsL2] = fun_findSteps(estVcont);
        ssTime = (eststepsR2(:,2) - eststepsR2(:,1))./sampFq;
        stanceTime_Est_R = ssTime(2:end-1); clear ssTime
        
        ssTime = (eststepsL2(:,2) - eststepsL2(:,1))./sampFq;
        stanceTime_Est_L = ssTime(2:end-1); clear ssTime
        
        testArray1 = [stanceTime_Ref_R-stanceTime_Est_R]; testArray2 = [stanceTime_Ref_L-stanceTime_Est_L];
        
        [imustepsRL_vel] = fun_findChronologicalSteps(estVcont, subSpecs);
        [refstepsRL_vel] = fun_findChronologicalSteps(refVcont, subSpecs);
        
        switch affFoot
            case 'Right'
                CumTypeWiseStabParam.SStanceTA.Ref.(usName).(typeStr) = [CumTypeWiseStabParam.SStanceTA.Ref.(usName).(typeStr); ...
                    stanceTime_Ref_R];
                CumTypeWiseStabParam.SStanceTA.Est.(usName).(typeStr) = [CumTypeWiseStabParam.SStanceTA.Est.(usName).(typeStr); ...
                    stanceTime_Est_R ];
                CumTypeWiseStabParam.SStanceTN.Ref.(usName).(typeStr) = [CumTypeWiseStabParam.SStanceTN.Ref.(usName).(typeStr); ...
                    stanceTime_Ref_L];
                CumTypeWiseStabParam.SStanceTN.Est.(usName).(typeStr) = [CumTypeWiseStabParam.SStanceTN.Est.(usName).(typeStr); ...
                    stanceTime_Est_L];
            case 'Left'
                CumTypeWiseStabParam.SStanceTA.Ref.(usName).(typeStr) = [CumTypeWiseStabParam.SStanceTA.Ref.(usName).(typeStr); ...
                    stanceTime_Ref_L];
                CumTypeWiseStabParam.SStanceTA.Est.(usName).(typeStr) = [CumTypeWiseStabParam.SStanceTA.Est.(usName).(typeStr); ...
                    stanceTime_Est_L ];
                CumTypeWiseStabParam.SStanceTN.Ref.(usName).(typeStr) = [CumTypeWiseStabParam.SStanceTN.Ref.(usName).(typeStr); ...
                    stanceTime_Ref_R];
                CumTypeWiseStabParam.SStanceTN.Est.(usName).(typeStr) = [CumTypeWiseStabParam.SStanceTN.Est.(usName).(typeStr); ...
                    stanceTime_Est_R];
        end
        
        CumStatsSTDiff.(typeStr)(usCt,:).Ref   = ttest2(stanceTime_Ref_R, stanceTime_Ref_L);
        CumStatsSTDiff.(typeStr)(usCt,:).Est   = ttest2(stanceTime_Est_R, stanceTime_Est_L);
        CumStatsSTDiff.(typeStr)(usCt,:).Right = ttest2(stanceTime_Est_R, stanceTime_Ref_R);
        CumStatsSTDiff.(typeStr)(usCt,:).Left  = ttest2(stanceTime_Est_L, stanceTime_Ref_L);
        %% XCoM params
        estXCoMParamsPP_AN_MiniAvg;
        %% force ratios
        refOPpos = rDatasync.Body.Forces; estOPpos = eDatasync.CoM.acc;
        CumTypeWiseF.Ref.(usName).(typeStr) = [CumTypeWiseF.Ref.(usName).(typeStr); refOPpos];
        CumTypeWiseF.Est.(usName).(typeStr) = [CumTypeWiseF.Est.(usName).(typeStr); estOPpos];
        
        CumTypeWise.N.(usName).(typeStr) = [CumTypeWise.N.(usName).(typeStr); length(estOPpos)];
        
        %% CoM height
        % rms of difference
        % end point difference
        
        switch affFoot
            case 'Right'
                
                refOPpos = rDatasync.RightFoot.pos; estOPpos = eDatasync.RightFoot.pos;
                CumTypeWiseApos.Ref.(usName).(typeStr) = [CumTypeWiseApos.Ref.(usName).(typeStr); refOPpos];
                CumTypeWiseApos.Est.(usName).(typeStr) = [CumTypeWiseApos.Est.(usName).(typeStr); estOPpos];
                
                refOPpos = rDatasync.LeftFoot.pos; estOPpos = eDatasync.LeftFoot.pos;
                CumTypeWiseNpos.Ref.(usName).(typeStr) = [CumTypeWiseNpos.Ref.(usName).(typeStr); refOPpos];
                CumTypeWiseNpos.Est.(usName).(typeStr) = [CumTypeWiseNpos.Est.(usName).(typeStr); estOPpos];
            case 'Left'
                
                refOPpos = rDatasync.LeftFoot.pos; estOPpos = eDatasync.LeftFoot.pos;
                CumTypeWiseApos.Ref.(usName).(typeStr) = [CumTypeWiseApos.Ref.(usName).(typeStr); refOPpos];
                CumTypeWiseApos.Est.(usName).(typeStr) = [CumTypeWiseApos.Est.(usName).(typeStr); estOPpos];
                
                refOPpos = rDatasync.RightFoot.pos; estOPpos = eDatasync.RightFoot.pos;
                CumTypeWiseNpos.Ref.(usName).(typeStr) = [CumTypeWiseNpos.Ref.(usName).(typeStr); refOPpos];
                CumTypeWiseNpos.Est.(usName).(typeStr) = [CumTypeWiseNpos.Est.(usName).(typeStr); estOPpos];
                
        end
        refOPpos = rDatasync.CoM.pos; estOPpos = eDatasync.CoM.pos;
        CumTypeWiseCpos.Ref.(usName).(typeStr) = [CumTypeWiseCpos.Ref.(usName).(typeStr); refOPpos];
        CumTypeWiseCpos.Est.(usName).(typeStr) = [CumTypeWiseCpos.Est.(usName).(typeStr); estOPpos];
        
        
        
    end
end


%% estimating average of each subject
usList = fieldnames(CumTypeWiseCpos.Ref);
for usCt = 1:length(usList)
    usName = usList{usCt};
    
    tyList = fieldnames(CumTypeWise.N.(usName));
    
    for tyCt = 1:length(tyList)
        tyName = tyList{tyCt};
        
        refValp = CumTypeWiseCpos.Ref.(usName).(tyName);
        estValp = CumTypeWiseCpos.Est.(usName).(tyName);
        [RMSout, CORRout, PVALout] = fun_getRMS_CORR(estValp,refValp);
        
        CumStatsCpos.RMSe.(tyName)(usCt,:)  = RMSout;
        CumStatsCpos.CORRe.(tyName)(usCt,:) = CORRout;
        CumStatsCpos.PVALe.(tyName)(usCt,:) = PVALout;
        
        refValp = CumTypeWiseF.Ref.(usName).(tyName);
        refRatX = refValp(:,1)./refValp(:,3); refRatY = refValp(:,2)./refValp(:,3);
        estValp = CumTypeWiseF.Est.(usName).(tyName);
        estRatX = estValp(:,1)./estValp(:,3); estRatY = estValp(:,2)./estValp(:,3);
        
        [RMSout, CORRout, PVALout] = fun_getRMS_CORR(estRatX,refRatX);
        [rangeRMSout] = fun_getrangeRMS(estRatX,refRatX);
        
        CumStatsFx.RMSe.(tyName)(usCt,:)  = RMSout;
        CumStatsFx.CORRe.(tyName)(usCt,:) = CORRout;
        CumStatsFx.rRMSe.(tyName)(usCt,:) = rangeRMSout;
        CumStatsFx.PVALe.(tyName)(usCt,:) = PVALout;
        
        [RMSout, CORRout, PVALout] = fun_getRMS_CORR(estRatY,refRatY);
        [rangeRMSout] = fun_getrangeRMS(estRatY,refRatY);
        
        CumStatsFy.RMSe.(tyName)(usCt,:)  = RMSout;
        CumStatsFy.CORRe.(tyName)(usCt,:) = CORRout;
        CumStatsFy.rRMSe.(tyName)(usCt,:) = rangeRMSout;
        CumStatsFy.PVALe.(tyName)(usCt,:) = PVALout;
        
        refValp = CumTypeWiseApos.Ref.(usName).(tyName);
        estValp = CumTypeWiseApos.Est.(usName).(tyName);
        [RMSout, CORRout, PVALout] = fun_getRMS_CORR(estValp,refValp);
        [rangeRMSout] = fun_getrangeRMS(estValp,refValp);
        
        CumStatsApos.RMSe.(tyName)(usCt,:)  = RMSout;
        CumStatsApos.CORRe.(tyName)(usCt,:) = CORRout;
        CumStatsApos.rRMSe.(tyName)(usCt,:) = rangeRMSout;
        CumStatsApos.PVALe.(tyName)(usCt,:) = PVALout;
        
        refValp = CumTypeWiseNpos.Ref.(usName).(tyName);
        estValp = CumTypeWiseNpos.Est.(usName).(tyName);
        [RMSout, CORRout, PVALout] = fun_getRMS_CORR(estValp,refValp);
        [rangeRMSout] = fun_getrangeRMS(estValp,refValp);
        
        CumStatsNpos.RMSe.(tyName)(usCt,:)  = RMSout;
        CumStatsNpos.CORRe.(tyName)(usCt,:) = CORRout;
        CumStatsNpos.rRMSe.(tyName)(usCt,:) = rangeRMSout;
        CumStatsNpos.PVALe.(tyName)(usCt,:) = PVALout;
        
        refValp = CumTypeWiseApos.Ref.(usName).(tyName)(end,1:2) - CumTypeWiseNpos.Ref.(usName).(tyName)(end,1:2);
        estValp = CumTypeWiseApos.Est.(usName).(tyName)(end,1:2) - CumTypeWiseNpos.Est.(usName).(tyName)(end,1:2);
        RMSout = rms(normh(refValp) - normh(estValp));
        
        CumStatsEDpos.RMSe.(tyName)(usCt,:)  = RMSout;
        
        refValp = CumTypeWiseSLA.Ref.(usName).(tyName); estValp = CumTypeWiseSLA.Est.(usName).(tyName) ;
        CumStatsSLA.RMSe.(tyName)(usCt,:)  = rms(estValp - refValp);
        CumStatsSLA.MAD.(tyName)(usCt,:)  = mean(abs(estValp - refValp));
        
        refValp = CumTypeWiseSLN.Ref.(usName).(tyName); estValp = CumTypeWiseSLN.Est.(usName).(tyName) ;
        CumStatsSLN.RMSe.(tyName)(usCt,:)  = rms(estValp - refValp);
        CumStatsSLN.MAD.(tyName)(usCt,:)  = mean(abs(estValp - refValp));
        
        refValp = CumTypeWiseSWA.Ref.(usName).(tyName);estValp =    CumTypeWiseSWA.Est.(usName).(tyName) ;
        CumStatsSWA.RMSe.(tyName)(usCt,:)  = rms(estValp - refValp);
        CumStatsSWA.MAD.(tyName)(usCt,:)  = mean(abs(estValp - refValp));
        
        refValp = CumTypeWiseSWN.Ref.(usName).(tyName); estValp =  CumTypeWiseSWN.Est.(usName).(tyName);
        CumStatsSWN.RMSe.(tyName)(usCt,:)  = rms(estValp - refValp);
        CumStatsSWN.MAD.(tyName)(usCt,:)  = mean(abs(estValp - refValp));
        
        % difference between users!
        aValp = CumTypeWiseSLA.Ref.(usName).(tyName); nValp = CumTypeWiseSLN.Ref.(usName).(tyName) ;
        CumStatsSLDiff.(tyName)(usCt,:).Ref  = ttest2(aValp, nValp);
        [~, ~, ~, tStat]  = ttest2(aValp, nValp);
        CumStatsSLDiff_stat.(tyName)(usCt,:).Ref = tStat;
        
        
        aValp = CumTypeWiseSLA.Est.(usName).(tyName); nValp = CumTypeWiseSLN.Est.(usName).(tyName) ;
        CumStatsSLDiff.(tyName)(usCt,:).Est  = ttest2(aValp, nValp);
        [~, ~, ~, tStat]  = ttest2(aValp, nValp);
        CumStatsSLDiff_stat.(tyName)(usCt,:).Est = tStat;
        
        aValp = CumTypeWiseSLA.Ref.(usName).(tyName); nValp = CumTypeWiseSLA.Est.(usName).(tyName) ;
        CumStatsSLDiff.(tyName)(usCt,:).Aff  = ttest2(aValp, nValp);
        
        aValp = CumTypeWiseSLN.Ref.(usName).(tyName); nValp = CumTypeWiseSLN.Est.(usName).(tyName) ;
        CumStatsSLDiff.(tyName)(usCt,:).Naff  = ttest2(aValp, nValp);
        
        
        aValp = CumTypeWiseSWA.Ref.(usName).(tyName); nValp = CumTypeWiseSWN.Ref.(usName).(tyName) ;
        CumStatsSWDiff.(tyName)(usCt,:).Ref  = ttest2(aValp, nValp);
        
        aValp = CumTypeWiseSWA.Est.(usName).(tyName); nValp = CumTypeWiseSWN.Est.(usName).(tyName) ;
        CumStatsSWDiff.(tyName)(usCt,:).Est  = ttest2(aValp, nValp);
        
        aValp = CumTypeWiseSWA.Ref.(usName).(tyName); nValp = CumTypeWiseSWA.Est.(usName).(tyName) ;
        CumStatsSWDiff.(tyName)(usCt,:).Aff  = ttest2(aValp, nValp);
        
        aValp = CumTypeWiseSWN.Ref.(usName).(tyName); nValp = CumTypeWiseSWN.Est.(usName).(tyName) ;
        CumStatsSWDiff.(tyName)(usCt,:).Naff  = ttest2(aValp, nValp);
        
        
        
    end
end

%% Step wise analysis of rms and corr
usList = fieldnames(CumTypeWiseCpos.Ref);
for usCt = 1:length(usList)
    
    
    usName = usList{usCt};
    tyList = fieldnames(CumTypeWise.N.(usName));
    
    for tyCt = 1:length(tyList)
        tyName = tyList{tyCt};
        
        nSets = length(CumTypeWisePSSeg.Ref.(usName).(tyName));
        CumTypeWiseAFNorm.Ref.(usName).(tyName) = [];
        CumTypeWiseAFNorm.Est.(usName).(tyName) = [];
        CumTypeWiseNFNorm.Ref.(usName).(tyName) = [];
        CumTypeWiseNFNorm.Est.(usName).(tyName) = [];
        CumTypeWisePSNorm.Ref.(usName).(tyName) = [];
        CumTypeWisePSNorm.Est.(usName).(tyName) = [];
        
        padZ = zeros(2,1);
        padZ = [];
        
        for iSet = 1:nSets
            clear tVarR  tVarR tVarL tVarP tVarX ...
                tVarRefR tVarRefL tVarRefP tVarRefX
            
            for axC = 1:3
                tVarR(:,axC) = CumTypeWiseAFSeg.Est.(usName).(tyName){iSet}(:,axC);
                tVarL(:,axC) = CumTypeWiseNFSeg.Est.(usName).(tyName){iSet}(:,axC);
                tVarP(:,axC) = CumTypeWisePSSeg.Est.(usName).(tyName){iSet}(:,axC);
                tVarX(:,axC) = CumTypeWiseXCSeg.Est.(usName).(tyName){iSet}(:,axC);
                
                tVarRefR(:,axC) = CumTypeWiseAFSeg.Ref.(usName).(tyName){iSet}(:,axC);
                tVarRefL(:,axC) = CumTypeWiseNFSeg.Ref.(usName).(tyName){iSet}(:,axC);
                tVarRefP(:,axC) = CumTypeWisePSSeg.Ref.(usName).(tyName){iSet}(:,axC);
                tVarRefX(:,axC) = CumTypeWiseXCSeg.Ref.(usName).(tyName){iSet}(:,axC);
            end
            
            [RRMSout(iSet,:), RCORRout(iSet,:), RPVALout(iSet,:)] = fun_getRMS_CORR(tVarR,tVarRefR);
            [LRMSout(iSet,:), LCORRout(iSet,:), LPVALout(iSet,:)] = fun_getRMS_CORR(tVarL,tVarRefL);
            [CRMSout(iSet,:), CCORRout(iSet,:), CPVALout(iSet,:)] = fun_getRMS_CORR(tVarP,tVarRefP);
            [XRMSout(iSet,:), XCORRout(iSet,:), XPVALout(iSet,:)] = fun_getRMS_CORR(tVarX,tVarRefX);
            
            
            CumTypeWiseAFNorm.Ref.(usName).(tyName) = [CumTypeWiseAFNorm.Ref.(usName).(tyName); tVarR];
            CumTypeWiseAFNorm.Est.(usName).(tyName) = [CumTypeWiseAFNorm.Est.(usName).(tyName); tVarRefR];
            CumTypeWiseNFNorm.Ref.(usName).(tyName) = [CumTypeWiseNFNorm.Ref.(usName).(tyName); tVarL];
            CumTypeWiseNFNorm.Est.(usName).(tyName) = [CumTypeWiseNFNorm.Est.(usName).(tyName); tVarRefL];
            CumTypeWisePSNorm.Ref.(usName).(tyName) = [CumTypeWisePSNorm.Ref.(usName).(tyName); tVarP];
            CumTypeWisePSNorm.Est.(usName).(tyName) = [CumTypeWisePSNorm.Est.(usName).(tyName); tVarRefP];
            
        end
        
        CumStatsAsegpos.RMSe.(tyName)(usCt,:)  = mean(RRMSout);
        CumStatsAsegpos.CORRe.(tyName)(usCt,:) = mean(RCORRout);
        CumStatsNsegpos.RMSe.(tyName)(usCt,:)  = mean(LRMSout);
        CumStatsNsegpos.CORRe.(tyName)(usCt,:) = mean(LCORRout);
        CumStatsCsegpos.RMSe.(tyName)(usCt,:)  = mean(CRMSout);
        CumStatsCsegpos.CORRe.(tyName)(usCt,:) = mean(CCORRout);
        
        CumStatsXsegpos.RMSe.(tyName)(usCt,:)  = mean(XRMSout);
        CumStatsXsegpos.CORRe.(tyName)(usCt,:) = mean(XCORRout);
        
        % MoS
        apaMoSref = CumTypeWiseMoS.APA.Ref.(usName).(tyName) ;
        apaMoSest = CumTypeWiseMoS.APA.Est.(usName).(tyName);
        
        apnMoSref = CumTypeWiseMoS.APN.Ref.(usName).(tyName) ;
        apnMoSest = CumTypeWiseMoS.APN.Est.(usName).(tyName);
        
        mlaMoSref = CumTypeWiseMoS.MLA.Ref.(usName).(tyName);
        mlaMoSest = CumTypeWiseMoS.MLA.Est.(usName).(tyName);
        
        mlnMoSref = CumTypeWiseMoS.MLN.Ref.(usName).(tyName);
        mlnMoSest = CumTypeWiseMoS.MLN.Est.(usName).(tyName);
        
        [apaRMSout, apaCORRout, apaPVALout] = fun_getRMS_CORR(apaMoSest,apaMoSref);
        [apnRMSout, apnCORRout, apnPVALout] = fun_getRMS_CORR(apnMoSest,apnMoSref);
        [mlaRMSout, mlaCORRout, mlaPVALout] = fun_getRMS_CORR(mlaMoSest,mlaMoSref);
        [mlnRMSout, mlnCORRout, mlnPVALout] = fun_getRMS_CORR(mlnMoSest,mlnMoSref);
        
        
        CumStatsAPAMoS.RMSe.(tyName)(usCt,:)   = apaRMSout;
        CumStatsAPAMoS.CORRe.(tyName)(usCt,:)  = apaCORRout;
        CumStatsAPAMoS.MAD.(tyName)(usCt,:) = mean(abs(apaMoSest - apaMoSref));
        
        CumStatsAPNMoS.RMSe.(tyName)(usCt,:)   = apnRMSout;
        CumStatsAPNMoS.CORRe.(tyName)(usCt,:)  = apnCORRout;
        CumStatsAPNMoS.MAD.(tyName)(usCt,:) = mean(abs(apnMoSest - apnMoSref));
        
        CumStatsMLAMoS.RMSe.(tyName)(usCt,:)  = mlaRMSout;
        CumStatsMLAMoS.CORRe.(tyName)(usCt,:) = mlaCORRout;
        CumStatsMLAMoS.MAD.(tyName)(usCt,:) = mean(abs(mlaMoSest - mlaMoSref));
        
        CumStatsMLNMoS.RMSe.(tyName)(usCt,:)  = mlnRMSout;
        CumStatsMLNMoS.CORRe.(tyName)(usCt,:) = mlnCORRout;
        CumStatsMLNMoS.MAD.(tyName)(usCt,:) = mean(abs(mlnMoSest - mlnMoSref));
        
        [htstatsAPMoSDiff_statr, ptstatsAPMoSDiff_statr, ~, tstatsAPMoSDiff_statr] = ttest2(apaMoSref,apnMoSref);
        [htstatsAPMoSDiff_state, ptstatsAPMoSDiff_state, ~, tstatsAPMoSDiff_state] = ttest2(apaMoSest,apnMoSest);
        [htstatsMLMoSDiff_statr, ptstatsMLMoSDiff_statr, ~, tstatsMLMoSDiff_statr] = ttest2(mlaMoSref,mlnMoSref);
        [htstatsMLMoSDiff_state, ptstatsMLMoSDiff_state, ~, tstatsMLMoSDiff_state] = ttest2(mlaMoSest,mlnMoSest);
                
        CumStatsAPMoSDiff.(tyName)(usCt,:).Ref = htstatsAPMoSDiff_statr;
        CumStatsAPMoSDiff.(tyName)(usCt,:).Est = htstatsAPMoSDiff_state;
        CumStatsMLMoSDiff.(tyName)(usCt,:).Ref = htstatsMLMoSDiff_statr;
        CumStatsMLMoSDiff.(tyName)(usCt,:).Est = htstatsMLMoSDiff_state;
               
        CumStatsAPMoSDiffP.(tyName)(usCt,:).Ref = ptstatsAPMoSDiff_statr;
        CumStatsAPMoSDiffP.(tyName)(usCt,:).Est = ptstatsAPMoSDiff_state;
        CumStatsMLMoSDiffP.(tyName)(usCt,:).Ref = ptstatsMLMoSDiff_statr;
        CumStatsMLMoSDiffP.(tyName)(usCt,:).Est = ptstatsMLMoSDiff_state;        
        
        CumStatsAPMoSDiff_stat.(tyName)(usCt,:).Ref = tstatsAPMoSDiff_statr;
        CumStatsAPMoSDiff_stat.(tyName)(usCt,:).Est = tstatsAPMoSDiff_state;
        CumStatsMLMoSDiff_stat.(tyName)(usCt,:).Ref = tstatsMLMoSDiff_statr;
        CumStatsMLMoSDiff_stat.(tyName)(usCt,:).Est = tstatsMLMoSDiff_state;
        
        CumStatsAPMoSDiff.(tyName)(usCt,:).Aff = ttest2(apaMoSest,apaMoSref);
        CumStatsAPMoSDiff.(tyName)(usCt,:).Naff = ttest2(apnMoSest,apnMoSref);
        CumStatsMLMoSDiff.(tyName)(usCt,:).Aff = ttest2(mlaMoSest,mlaMoSref);
        CumStatsMLMoSDiff.(tyName)(usCt,:).Naff = ttest2(mlnMoSest,mlnMoSref);
        
        
    end
end

%% Cumulating Ref and Est separately for BA and box plots
CumSTA.Ref = [];     CumSTA.Est = [];
CumSTN.Ref = [];     CumSTN.Est = [];

% straight walking params
CumMLAMoS_SW.Ref = [];  CumMLAMoS_SW.Est = [];
CumMLNMoS_SW.Ref = [];  CumMLNMoS_SW.Est = [];
CumAPAMoS_SW.Ref = [];   CumAPAMoS_SW.Est = [];
CumAPNMoS_SW.Ref = [];   CumAPNMoS_SW.Est = [];

CumSLA_SW.Ref = [];     CumSLA_SW.Est = [];
CumSLN_SW.Ref = [];     CumSLN_SW.Est = [];

CumSWA_SW.Ref = [];     CumSWA_SW.Est = [];
CumSWN_SW.Ref = [];     CumSWN_SW.Est = [];

% first and last steps
CumMLAMoS_FL.Ref = [];  CumMLAMoS_FL.Est = [];
CumMLNMoS_FL.Ref = [];  CumMLNMoS_FL.Est = [];
CumAPAMoS_FL.Ref = [];   CumAPAMoS_FL.Est = [];
CumAPNMoS_FL.Ref = [];   CumAPNMoS_FL.Est = [];

CumSLA_FL.Ref = [];     CumSLA_FL.Est = [];
CumSLN_FL.Ref = [];     CumSLN_FL.Est = [];

CumSWA_FL.Ref = [];     CumSWA_FL.Est = [];
CumSWN_FL.Ref = [];     CumSWN_FL.Est = [];

usList = fieldnames(CumTypeWiseCpos.Ref);
arCounter = 0;
for usCt = 1:length(usList)
    usName = usList{usCt};
    
    arCounter = arCounter + 1;
    
    tyList = fieldnames(CumTypeWise.N.(usName));
    
    for tyCt = 1:length(tyList)
        tyName = tyList{tyCt};
        
        totLen     =  length(CumTypeWiseMoS.MLA.Ref.(usName).(tyName));
        totLenStepAff  =  length(CumTypeWiseSWA.Ref.(usName).(tyName));
        totLenStepNAff =  length(CumTypeWiseSWN.Ref.(usName).(tyName));
        totLenMoSAff  =  length(CumTypeWiseMoS.MLA.Ref.(usName).(tyName));
        totLenMoSNAff =  length(CumTypeWiseMoS.MLN.Ref.(usName).(tyName));
        
        
        fStrStepIndAff = 2; fStrStepIndNAff = 2; fStrStepInd = 2;
        
        switch usName % this is for ProcData_1214
            case 'us1'
                lStrStepInd = totLen-2; % number of steps considered for last step;
                turnStepInd = totLen-1;
                % left aff
                lStrStepIndAff = totLenStepAff-1;  lStrStepIndNAff = totLenStepNAff-2;
                lStrMoSIndAff = totLenMoSAff - 1; lStrMoSIndNAff = totLenMoSNAff - 2;
                
                lStrStepIndAff = [totLenStepAff-2 totLenStepAff- 1]; lStrStepIndNAff = [totLenStepNAff-3 totLenStepNAff-2];
                lStrMoSIndAff  = [totLenMoSAff-2 totLenMoSAff- 1];   lStrMoSIndNAff  = [totLenMoSNAff-3 totLenMoSNAff-2];
                
                if changeOrder; lStrMoSIndAff = totLenMoSAff - 2; lStrMoSIndNAff = totLenMoSNAff - 1; end
                
                
            case 'us6'
                lStrStepInd = totLen-1; % number of steps considered for last step;
                turnStepInd = totLen;
                % left aff
                lStrStepIndAff = totLenStepAff; lStrStepIndNAff = totLenStepNAff-1;
                lStrMoSIndAff = totLenMoSAff; lStrMoSIndNAff = totLenMoSNAff - 1;
                
                lStrStepIndAff  = [totLenStepAff-1 totLenStepAff]; lStrStepIndNAff = [totLenStepNAff-2 totLenStepNAff-1];
                lStrMoSIndAff   = [totLenMoSAff-1 totLenMoSAff]; lStrMoSIndNAff = [totLenMoSNAff-2 totLenMoSNAff-1];
                
                if changeOrder; lStrMoSIndAff = totLenMoSAff - 1; lStrMoSIndNAff = totLenMoSNAff; end
            case 'us7'
                lStrStepInd = [totLen-2 totLen-1]; % number of steps considered for last step;
                turnStepInd = totLen;
                % left aff
                lStrStepIndAff  = totLenStepAff; lStrStepIndNAff = totLenStepNAff-1;
                lStrMoSIndAff  = totLenMoSAff; lStrMoSIndNAff = totLenMoSNAff - 1;
                
                lStrStepIndAff  = [totLenStepAff-1 totLenStepAff]; lStrStepIndNAff = [totLenStepNAff-2 totLenStepNAff-1];
                lStrMoSIndAff  = [totLenMoSAff-1 totLenMoSAff]; lStrMoSIndNAff = [totLenMoSNAff-2 totLenMoSNAff-1];
                
                if changeOrder; lStrMoSIndAff  = totLenMoSAff - 1; lStrMoSIndNAff = totLenMoSNAff; end
                
            case 'us12'
                lStrStepInd = totLen-1; % number of steps considered for last step;
                turnStepInd = totLen;
                % right aff
                lStrStepIndAff = totLenStepAff-1; lStrStepIndNAff = totLenStepNAff;
                lStrMoSIndAff  = totLenMoSAff - 1; lStrMoSIndNAff = totLenMoSNAff;
                
                lStrStepIndAff  = [totLenStepAff-2 totLenStepAff-1]; lStrStepIndNAff = [totLenStepNAff-1 totLenStepNAff];
                lStrMoSIndAff  = [totLenMoSAff-2 totLenMoSAff-1]; lStrMoSIndNAff = [totLenMoSNAff-1 totLenMoSNAff];
                
                if changeOrder;  lStrMoSIndAff  = totLenMoSAff; lStrMoSIndNAff = totLenMoSNAff - 1; end
                
        end
        
        
        % ST
        CumSTA.Ref = [CumSTA.Ref; CumTypeWiseStabParam.SStanceTA.Ref.(usName).(tyName)];
        CumSTA.Est = [CumSTA.Est; CumTypeWiseStabParam.SStanceTA.Est.(usName).(tyName)];
        CumSTN.Ref = [CumSTN.Ref; CumTypeWiseStabParam.SStanceTN.Ref.(usName).(tyName)];
        CumSTN.Est = [CumSTN.Est; CumTypeWiseStabParam.SStanceTN.Est.(usName).(tyName)];
        
        CumSubSTA.Ref{arCounter,:} = CumTypeWiseStabParam.SStanceTA.Ref.(usName).(tyName);
        CumSubSTA.Est{arCounter,:} = CumTypeWiseStabParam.SStanceTA.Est.(usName).(tyName);
        CumSubSTN.Ref{arCounter,:} = CumTypeWiseStabParam.SStanceTN.Ref.(usName).(tyName);
        CumSubSTN.Est{arCounter,:} = CumTypeWiseStabParam.SStanceTN.Est.(usName).(tyName);
        
        %
        % MoS separated between stance and Aff/Naff
        CumMLAMoS_SW.Ref = [CumMLAMoS_SW.Ref; CumTypeWiseMoS.MLA.Ref.(usName).(tyName)(fStrStepIndAff:lStrMoSIndAff(1)-1,:)];
        CumMLAMoS_SW.Est = [CumMLAMoS_SW.Est; CumTypeWiseMoS.MLA.Est.(usName).(tyName)(fStrStepIndAff:lStrMoSIndAff(1)-1,:)];
        CumMLNMoS_SW.Ref = [CumMLNMoS_SW.Ref; CumTypeWiseMoS.MLN.Ref.(usName).(tyName)(fStrStepIndNAff:lStrMoSIndNAff(1)-1,:)];
        CumMLNMoS_SW.Est = [CumMLNMoS_SW.Est; CumTypeWiseMoS.MLN.Est.(usName).(tyName)(fStrStepIndNAff:lStrMoSIndNAff(1)-1,:)];
        
        CumSubMLAMoS_SW.Ref{arCounter,:} = CumTypeWiseMoS.MLA.Ref.(usName).(tyName)(fStrStepIndAff:lStrMoSIndAff(1)-1,:);
        CumSubMLAMoS_SW.Est{arCounter,:} = CumTypeWiseMoS.MLA.Est.(usName).(tyName)(fStrStepIndAff:lStrMoSIndAff(1)-1,:);
        CumSubMLNMoS_SW.Ref{arCounter,:} = CumTypeWiseMoS.MLN.Ref.(usName).(tyName)(fStrStepIndNAff:lStrMoSIndNAff(1)-1,:);
        CumSubMLNMoS_SW.Est{arCounter,:} = CumTypeWiseMoS.MLN.Est.(usName).(tyName)(fStrStepIndNAff:lStrMoSIndNAff(1)-1,:);
        
        CumMLAMoS_FL.Ref = [CumMLAMoS_FL.Ref; CumTypeWiseMoS.MLA.Ref.(usName).(tyName)([1:fStrStepIndAff-1 lStrMoSIndAff],:)];
        CumMLAMoS_FL.Est = [CumMLAMoS_FL.Est; CumTypeWiseMoS.MLA.Est.(usName).(tyName)([1:fStrStepIndAff-1 lStrMoSIndAff],:)];
        CumMLNMoS_FL.Ref = [CumMLNMoS_FL.Ref; CumTypeWiseMoS.MLN.Ref.(usName).(tyName)([1:fStrStepIndNAff-1 lStrMoSIndNAff],:)];
        CumMLNMoS_FL.Est = [CumMLNMoS_FL.Est; CumTypeWiseMoS.MLN.Est.(usName).(tyName)([1:fStrStepIndNAff-1 lStrMoSIndNAff],:)];
        
        CumSubMLAMoS_FL.Ref{arCounter,:} = CumTypeWiseMoS.MLA.Ref.(usName).(tyName)([1:fStrStepIndAff-1 lStrMoSIndAff],:);
        CumSubMLAMoS_FL.Est{arCounter,:} = CumTypeWiseMoS.MLA.Est.(usName).(tyName)([1:fStrStepIndAff-1 lStrMoSIndAff],:);
        CumSubMLNMoS_FL.Ref{arCounter,:} = CumTypeWiseMoS.MLN.Ref.(usName).(tyName)([1:fStrStepIndNAff-1 lStrMoSIndNAff],:);
        CumSubMLNMoS_FL.Est{arCounter,:} = CumTypeWiseMoS.MLN.Est.(usName).(tyName)([1:fStrStepIndNAff-1 lStrMoSIndNAff],:);
        
        CumStatsMLSWDiff.(tyName)(usCt,:).Ref = ttest2(CumTypeWiseMoS.MLA.Ref.(usName).(tyName)(fStrStepIndAff:lStrMoSIndAff(1)-1,:),CumTypeWiseMoS.MLN.Ref.(usName).(tyName)(fStrStepIndNAff:lStrMoSIndNAff(1)-1,:));
        CumStatsMLSWDiff.(tyName)(usCt,:).Est = ttest2(CumTypeWiseMoS.MLA.Est.(usName).(tyName)(fStrStepIndAff:lStrMoSIndAff(1)-1,:),CumTypeWiseMoS.MLN.Est.(usName).(tyName)(fStrStepIndNAff:lStrMoSIndNAff(1)-1,:));
        CumStatsMLSWDiff.(tyName)(usCt,:).Aff = ttest2(CumTypeWiseMoS.MLA.Est.(usName).(tyName)(fStrStepIndAff:lStrMoSIndAff(1)-1,:),CumTypeWiseMoS.MLA.Ref.(usName).(tyName)(fStrStepIndAff:lStrMoSIndAff(1)-1,:));
        CumStatsMLSWDiff.(tyName)(usCt,:).Naff = ttest2(CumTypeWiseMoS.MLN.Est.(usName).(tyName)(fStrStepIndAff:lStrMoSIndNAff(1)-1,:),CumTypeWiseMoS.MLN.Ref.(usName).(tyName)(fStrStepIndAff:lStrMoSIndNAff(1)-1,:));
        
        
        %AP MoS
        CumAPAMoS_SW.Ref  = [CumAPAMoS_SW.Ref; CumTypeWiseMoS.APA.Ref.(usName).(tyName)(fStrStepIndAff:lStrMoSIndAff(1)-1,:)];
        CumAPAMoS_SW.Est  = [CumAPAMoS_SW.Est; CumTypeWiseMoS.APA.Est.(usName).(tyName)(fStrStepIndAff:lStrMoSIndAff(1)-1,:)];
        CumAPNMoS_SW.Ref  = [CumAPNMoS_SW.Ref; CumTypeWiseMoS.APN.Ref.(usName).(tyName)(fStrStepIndNAff:lStrMoSIndNAff(1)-1,:)];
        CumAPNMoS_SW.Est  = [CumAPNMoS_SW.Est; CumTypeWiseMoS.APN.Est.(usName).(tyName)(fStrStepIndNAff:lStrMoSIndNAff(1)-1,:)];
        
        CumSubAPAMoS_SW.Ref{arCounter,:}  = CumTypeWiseMoS.APA.Ref.(usName).(tyName)(fStrStepIndAff:lStrMoSIndAff(1)-1,:);
        CumSubAPAMoS_SW.Est{arCounter,:}  = CumTypeWiseMoS.APA.Est.(usName).(tyName)(fStrStepIndAff:lStrMoSIndAff(1)-1,:);
        CumSubAPNMoS_SW.Ref{arCounter,:}  = CumTypeWiseMoS.APN.Ref.(usName).(tyName)(fStrStepIndNAff:lStrMoSIndNAff(1)-1,:);
        CumSubAPNMoS_SW.Est{arCounter,:}  = CumTypeWiseMoS.APN.Est.(usName).(tyName)(fStrStepIndNAff:lStrMoSIndNAff(1)-1,:);
        
        CumAPAMoS_FL.Ref  = [CumAPAMoS_FL.Ref; CumTypeWiseMoS.APA.Ref.(usName).(tyName)([1:fStrStepIndAff-1 lStrMoSIndAff],:)];
        CumAPAMoS_FL.Est  = [CumAPAMoS_FL.Est; CumTypeWiseMoS.APA.Est.(usName).(tyName)([1:fStrStepIndAff-1 lStrMoSIndAff],:)];
        CumAPNMoS_FL.Ref  = [CumAPNMoS_FL.Ref; CumTypeWiseMoS.APN.Ref.(usName).(tyName)([1:fStrStepIndNAff-1 lStrMoSIndNAff],:)];
        CumAPNMoS_FL.Est  = [CumAPNMoS_FL.Est; CumTypeWiseMoS.APN.Est.(usName).(tyName)([1:fStrStepIndNAff-1 lStrMoSIndNAff],:)];
        
        CumSubAPAMoS_FL.Ref{arCounter,:}  = CumTypeWiseMoS.APA.Ref.(usName).(tyName)([1:fStrStepIndAff-1 lStrMoSIndAff],:);
        CumSubAPAMoS_FL.Est{arCounter,:}  = CumTypeWiseMoS.APA.Est.(usName).(tyName)([1:fStrStepIndAff-1 lStrMoSIndAff],:);
        CumSubAPNMoS_FL.Ref{arCounter,:}  = CumTypeWiseMoS.APN.Ref.(usName).(tyName)([1:fStrStepIndNAff-1 lStrMoSIndNAff],:);
        CumSubAPNMoS_FL.Est{arCounter,:}  = CumTypeWiseMoS.APN.Est.(usName).(tyName)([1:fStrStepIndNAff-1 lStrMoSIndNAff],:);
        
        CumStatsAPSWDiff.(tyName)(usCt,:).Ref = ttest2(CumTypeWiseMoS.APA.Ref.(usName).(tyName)(fStrStepIndAff:lStrMoSIndAff(1)-1,:),CumTypeWiseMoS.APN.Ref.(usName).(tyName)(fStrStepIndNAff:lStrMoSIndNAff(1)-1,:));
        CumStatsAPSWDiff.(tyName)(usCt,:).Est = ttest2(CumTypeWiseMoS.APA.Est.(usName).(tyName)(fStrStepIndAff:lStrMoSIndAff(1)-1,:),CumTypeWiseMoS.APN.Est.(usName).(tyName)(fStrStepIndNAff:lStrMoSIndNAff(1)-1,:));
        CumStatsAPSWDiff.(tyName)(usCt,:).Aff = ttest2(CumTypeWiseMoS.APA.Est.(usName).(tyName)(fStrStepIndAff:lStrMoSIndAff(1)-1,:),CumTypeWiseMoS.APA.Ref.(usName).(tyName)(fStrStepIndAff:lStrMoSIndAff(1)-1,:));
        CumStatsAPSWDiff.(tyName)(usCt,:).Naff = ttest2(CumTypeWiseMoS.APN.Est.(usName).(tyName)(fStrStepIndAff:lStrMoSIndNAff(1)-1,:),CumTypeWiseMoS.APN.Ref.(usName).(tyName)(fStrStepIndAff:lStrMoSIndNAff(1)-1,:));
        
        
        
        % Step length/width
        CumSLA_SW.Ref = [CumSLA_SW.Ref; CumTypeWiseSLA.Ref.(usName).(tyName)(fStrStepIndAff:lStrStepIndAff(1)-1,:)];
        CumSLA_SW.Est = [CumSLA_SW.Est; CumTypeWiseSLA.Est.(usName).(tyName)(fStrStepIndAff:lStrStepIndAff(1)-1,:)];
        CumSLN_SW.Ref = [CumSLN_SW.Ref; CumTypeWiseSLN.Ref.(usName).(tyName)(fStrStepIndNAff:lStrStepIndNAff(1)-1,:)];
        CumSLN_SW.Est = [CumSLN_SW.Est; CumTypeWiseSLN.Est.(usName).(tyName)(fStrStepIndNAff:lStrStepIndNAff(1)-1,:)];
        
        CumSubSLA_SW.Ref{arCounter,:} = CumTypeWiseSLA.Ref.(usName).(tyName)(fStrStepIndAff:lStrStepIndAff(1)-1,:);
        CumSubSLA_SW.Est{arCounter,:} = CumTypeWiseSLA.Est.(usName).(tyName)(fStrStepIndAff:lStrStepIndAff(1)-1,:);
        CumSubSLN_SW.Ref{arCounter,:} = CumTypeWiseSLN.Ref.(usName).(tyName)(fStrStepIndNAff:lStrStepIndNAff(1)-1,:);
        CumSubSLN_SW.Est{arCounter,:} = CumTypeWiseSLN.Est.(usName).(tyName)(fStrStepIndNAff:lStrStepIndNAff(1)-1,:);
        
        CumStatsSLSWDiff.(tyName)(usCt,:).Ref  = ttest2(CumTypeWiseSLA.Ref.(usName).(tyName)(fStrStepIndAff:lStrStepIndAff(1)-1,:),CumTypeWiseSLN.Ref.(usName).(tyName)(fStrStepIndNAff:lStrStepIndNAff(1)-1,:));
        CumStatsSLSWDiff.(tyName)(usCt,:).Est  = ttest2(CumTypeWiseSLA.Est.(usName).(tyName)(fStrStepIndAff:lStrStepIndAff(1)-1,:),CumTypeWiseSLN.Est.(usName).(tyName)(fStrStepIndNAff:lStrStepIndNAff(1)-1,:));
        CumStatsSLSWDiff.(tyName)(usCt,:).Aff  = ttest2(CumTypeWiseSLA.Est.(usName).(tyName)(fStrStepIndAff:lStrStepIndAff(1)-1,:),CumTypeWiseSLA.Ref.(usName).(tyName)(fStrStepIndAff:lStrStepIndAff(1)-1,:));
        CumStatsSLSWDiff.(tyName)(usCt,:).Naff = ttest2(CumTypeWiseSLN.Est.(usName).(tyName)(fStrStepIndAff:lStrStepIndNAff(1)-1,:),CumTypeWiseSLN.Ref.(usName).(tyName)(fStrStepIndAff:lStrStepIndNAff(1)-1,:));
        
        CumSWA_SW.Ref = [CumSWA_SW.Ref; CumTypeWiseSWA.Ref.(usName).(tyName)(fStrStepIndAff:lStrStepIndAff(1)-1,:)];
        CumSWA_SW.Est = [CumSWA_SW.Est; CumTypeWiseSWA.Est.(usName).(tyName)(fStrStepIndAff:lStrStepIndAff(1)-1,:)];
        CumSWN_SW.Ref = [CumSWN_SW.Ref; CumTypeWiseSWN.Ref.(usName).(tyName)(fStrStepIndNAff:lStrStepIndNAff(1)-1,:)];
        CumSWN_SW.Est = [CumSWN_SW.Est; CumTypeWiseSWN.Est.(usName).(tyName)(fStrStepIndNAff:lStrStepIndNAff(1)-1,:)];
        
        CumSubSWA_SW.Ref{arCounter,:} = CumTypeWiseSWA.Ref.(usName).(tyName)(fStrStepIndAff:lStrStepIndAff(1)-1,:);
        CumSubSWA_SW.Est{arCounter,:} = CumTypeWiseSWA.Est.(usName).(tyName)(fStrStepIndAff:lStrStepIndAff(1)-1,:);
        CumSubSWN_SW.Ref{arCounter,:} = CumTypeWiseSWN.Ref.(usName).(tyName)(fStrStepIndNAff:lStrStepIndNAff(1)-1,:);
        CumSubSWN_SW.Est{arCounter,:} = CumTypeWiseSWN.Est.(usName).(tyName)(fStrStepIndNAff:lStrStepIndNAff(1)-1,:);
        
        CumStatsSWSWDiff.(tyName)(usCt,:).Ref  = ttest2(CumTypeWiseSWA.Ref.(usName).(tyName)(fStrStepIndAff:lStrStepIndAff(1)-1,:),CumTypeWiseSWN.Ref.(usName).(tyName)(fStrStepIndNAff:lStrStepIndNAff(1)-1,:));
        CumStatsSWSWDiff.(tyName)(usCt,:).Est  = ttest2(CumTypeWiseSWA.Est.(usName).(tyName)(fStrStepIndAff:lStrStepIndAff(1)-1,:),CumTypeWiseSWN.Est.(usName).(tyName)(fStrStepIndNAff:lStrStepIndNAff(1)-1,:));
        CumStatsSWSWDiff.(tyName)(usCt,:).Aff  = ttest2(CumTypeWiseSWA.Est.(usName).(tyName)(fStrStepIndAff:lStrStepIndAff(1)-1,:),CumTypeWiseSWA.Ref.(usName).(tyName)(fStrStepIndAff:lStrStepIndAff(1)-1,:));
        CumStatsSWSWDiff.(tyName)(usCt,:).Naff = ttest2(CumTypeWiseSWN.Est.(usName).(tyName)(fStrStepIndAff:lStrStepIndNAff(1)-1,:),CumTypeWiseSWN.Ref.(usName).(tyName)(fStrStepIndAff:lStrStepIndNAff(1)-1,:));
        
        CumSLA_FL.Ref = [CumSLA_FL.Ref; CumTypeWiseSLA.Ref.(usName).(tyName)([1:fStrStepIndAff-1 lStrStepIndAff],:)];
        CumSLA_FL.Est = [CumSLA_FL.Est; CumTypeWiseSLA.Est.(usName).(tyName)([1:fStrStepIndAff-1 lStrStepIndAff],:)];
        CumSLN_FL.Ref = [CumSLN_FL.Ref; CumTypeWiseSLN.Ref.(usName).(tyName)([1:fStrStepIndNAff-1 lStrStepIndNAff],:)];
        CumSLN_FL.Est = [CumSLN_FL.Est; CumTypeWiseSLN.Est.(usName).(tyName)([1:fStrStepIndNAff-1 lStrStepIndNAff],:)];
        
        CumSubSLA_FL.Ref{arCounter,:} = CumTypeWiseSLA.Ref.(usName).(tyName)([1:fStrStepIndAff-1 lStrStepIndAff],:);
        CumSubSLA_FL.Est{arCounter,:} = CumTypeWiseSLA.Est.(usName).(tyName)([1:fStrStepIndAff-1 lStrStepIndAff],:);
        CumSubSLN_FL.Ref{arCounter,:} = CumTypeWiseSLN.Ref.(usName).(tyName)([1:fStrStepIndNAff-1 lStrStepIndNAff],:);
        CumSubSLN_FL.Est{arCounter,:} = CumTypeWiseSLN.Est.(usName).(tyName)([1:fStrStepIndNAff-1 lStrStepIndNAff],:);
        
        CumSWA_FL.Ref = [CumSWA_FL.Ref; CumTypeWiseSWA.Ref.(usName).(tyName)([1:fStrStepIndAff-1 lStrStepIndAff],:)];
        CumSWA_FL.Est = [CumSWA_FL.Est; CumTypeWiseSWA.Est.(usName).(tyName)([1:fStrStepIndAff-1 lStrStepIndAff],:)];
        CumSWN_FL.Ref = [CumSWN_FL.Ref; CumTypeWiseSWN.Ref.(usName).(tyName)([1:fStrStepIndNAff-1 lStrStepIndNAff],:)];
        CumSWN_FL.Est = [CumSWN_FL.Est; CumTypeWiseSWN.Est.(usName).(tyName)([1:fStrStepIndNAff-1 lStrStepIndNAff],:)];
        
        CumSubSWA_FL.Ref{arCounter,:} = CumTypeWiseSWA.Ref.(usName).(tyName)([1:fStrStepIndAff-1 lStrStepIndAff],:);
        CumSubSWA_FL.Est{arCounter,:} = CumTypeWiseSWA.Est.(usName).(tyName)([1:fStrStepIndAff-1 lStrStepIndAff],:);
        CumSubSWN_FL.Ref{arCounter,:} = CumTypeWiseSWN.Ref.(usName).(tyName)([1:fStrStepIndNAff-1 lStrStepIndNAff],:);
        CumSubSWN_FL.Est{arCounter,:} = CumTypeWiseSWN.Est.(usName).(tyName)([1:fStrStepIndNAff-1 lStrStepIndNAff],:);
        
        
    end
end

%%
tyList = fieldnames(CumTypeWise.N.(usName));
for tyCt = 1:length(tyList)
    tyName = tyList{tyCt};
    CumStatsEDpos.Overall.RMSE.(tyName)(1,:) = mean(CumStatsEDpos.RMSe.(tyName));
    CumStatsEDpos.Overall.RMSE.(tyName)(2,:) = std(CumStatsEDpos.RMSe.(tyName));
    
    CumStatsAsegpos.Overall.RMSE.(tyName)(1,:) = mean(CumStatsAsegpos.RMSe.(tyName));
    CumStatsAsegpos.Overall.RMSE.(tyName)(2,:) = std(CumStatsAsegpos.RMSe.(tyName));
    
    CumStatsAsegpos.Overall.CORR.(tyName)(1,:) = mean(CumStatsAsegpos.CORRe.(tyName));
    CumStatsAsegpos.Overall.CORR.(tyName)(2,:) = std(CumStatsAsegpos.CORRe.(tyName));
    
    CumStatsNsegpos.Overall.RMSE.(tyName)(1,:) = mean(CumStatsNsegpos.RMSe.(tyName));
    CumStatsNsegpos.Overall.RMSE.(tyName)(2,:) = std(CumStatsNsegpos.RMSe.(tyName));
    
    CumStatsNsegpos.Overall.CORR.(tyName)(1,:) = mean(CumStatsNsegpos.CORRe.(tyName));
    CumStatsNsegpos.Overall.CORR.(tyName)(2,:) = std(CumStatsNsegpos.CORRe.(tyName));
    
    CumStatsCsegpos.Overall.RMSE.(tyName)(1,:) = mean(CumStatsCsegpos.RMSe.(tyName));
    CumStatsCsegpos.Overall.RMSE.(tyName)(2,:) = std(CumStatsCsegpos.RMSe.(tyName));
    
    CumStatsCsegpos.Overall.CORR.(tyName)(1,:) = mean(CumStatsCsegpos.CORRe.(tyName));
    CumStatsCsegpos.Overall.CORR.(tyName)(2,:) = std(CumStatsCsegpos.CORRe.(tyName));
    
    CumStatsCpos.Overall.RMSE.(tyName)(1,:) = mean(CumStatsCpos.RMSe.(tyName));
    CumStatsCpos.Overall.RMSE.(tyName)(2,:) = std(CumStatsCpos.RMSe.(tyName));
    
    CumStatsCpos.Overall.CORR.(tyName)(1,:) = mean(CumStatsCpos.CORRe.(tyName));
    CumStatsCpos.Overall.CORR.(tyName)(2,:) = std(CumStatsCpos.CORRe.(tyName));
    
    CumStatsFx.Overall.rRMSE.(tyName)(1,:) = mean(CumStatsFx.rRMSe.(tyName));
    CumStatsFx.Overall.rRMSE.(tyName)(2,:) = std(CumStatsFx.rRMSe.(tyName));
    
    CumStatsFx.Overall.RMSE.(tyName)(1,:) = mean(CumStatsFx.RMSe.(tyName));
    CumStatsFx.Overall.RMSE.(tyName)(2,:) = std(CumStatsFx.RMSe.(tyName));
    
    CumStatsFx.Overall.CORR.(tyName)(1,:) = mean(CumStatsFx.CORRe.(tyName));
    CumStatsFx.Overall.CORR.(tyName)(2,:) = std(CumStatsFx.CORRe.(tyName));
    
    CumStatsFy.Overall.rRMSE.(tyName)(1,:) = mean(CumStatsFy.rRMSe.(tyName));
    CumStatsFy.Overall.rRMSE.(tyName)(2,:) = std(CumStatsFy.rRMSe.(tyName));
    
    CumStatsFy.Overall.RMSE.(tyName)(1,:) = mean(CumStatsFy.RMSe.(tyName));
    CumStatsFy.Overall.RMSE.(tyName)(2,:) = std(CumStatsFy.RMSe.(tyName));
    
    CumStatsFy.Overall.CORR.(tyName)(1,:) = mean(CumStatsFy.CORRe.(tyName));
    CumStatsFy.Overall.CORR.(tyName)(2,:) = std(CumStatsFy.CORRe.(tyName));
    
    CumStatsSLA.Overall.RMSE.(tyName)(1,:) = mean(CumStatsSLA.RMSe.(tyName));
    CumStatsSLA.Overall.RMSE.(tyName)(2,:) = std(CumStatsSLA.RMSe.(tyName));
    
    CumStatsSLN.Overall.RMSE.(tyName)(1,:) = mean(CumStatsSLN.RMSe.(tyName));
    CumStatsSLN.Overall.RMSE.(tyName)(2,:) = std(CumStatsSLN.RMSe.(tyName));
    
    CumStatsSWA.Overall.RMSE.(tyName)(1,:) = mean(CumStatsSWA.RMSe.(tyName));
    CumStatsSWA.Overall.RMSE.(tyName)(2,:) = std(CumStatsSWA.RMSe.(tyName));
    
    CumStatsSWN.Overall.RMSE.(tyName)(1,:) = mean(CumStatsSWN.RMSe.(tyName));
    CumStatsSWN.Overall.RMSE.(tyName)(2,:) = std(CumStatsSWN.RMSe.(tyName));
    
    CumStatsSLA.Overall.MAD.(tyName)(1,:) = mean(CumStatsSLA.MAD.(tyName));
    CumStatsSLA.Overall.MAD.(tyName)(2,:) = std(CumStatsSLA.MAD.(tyName));
    
    CumStatsSLN.Overall.MAD.(tyName)(1,:) = mean(CumStatsSLN.MAD.(tyName));
    CumStatsSLN.Overall.MAD.(tyName)(2,:) = std(CumStatsSLN.MAD.(tyName));
    
    CumStatsSWA.Overall.MAD.(tyName)(1,:) = mean(CumStatsSWA.MAD.(tyName));
    CumStatsSWA.Overall.MAD.(tyName)(2,:) = std(CumStatsSWA.MAD.(tyName));
    
    CumStatsSWN.Overall.MAD.(tyName)(1,:) = mean(CumStatsSWN.MAD.(tyName));
    CumStatsSWN.Overall.MAD.(tyName)(2,:) = std(CumStatsSWN.MAD.(tyName));
    
    
    % AP MoS
    CumStatsAPAMoS.Overall.RMSE.(tyName)(1,:) = mean(CumStatsAPAMoS.RMSe.(tyName));
    CumStatsAPAMoS.Overall.RMSE.(tyName)(2,:) = std(CumStatsAPAMoS.RMSe.(tyName));
    
    CumStatsAPAMoS.Overall.MAD.(tyName)(1,:) = mean(CumStatsAPAMoS.MAD.(tyName));
    CumStatsAPAMoS.Overall.MAD.(tyName)(2,:) = std(CumStatsAPAMoS.MAD.(tyName));
    
    CumStatsAPNMoS.Overall.RMSE.(tyName)(1,:) = mean(CumStatsAPNMoS.RMSe.(tyName));
    CumStatsAPNMoS.Overall.RMSE.(tyName)(2,:) = std(CumStatsAPNMoS.RMSe.(tyName));
    
    CumStatsAPNMoS.Overall.MAD.(tyName)(1,:) = mean(CumStatsAPNMoS.MAD.(tyName));
    CumStatsAPNMoS.Overall.MAD.(tyName)(2,:) = std(CumStatsAPNMoS.MAD.(tyName));
    
    % ML MoS
    CumStatsMLAMoS.Overall.RMSE.(tyName)(1,:) = mean(CumStatsMLAMoS.RMSe.(tyName));
    CumStatsMLAMoS.Overall.RMSE.(tyName)(2,:) = std(CumStatsMLAMoS.RMSe.(tyName));
    
    CumStatsMLAMoS.Overall.MAD.(tyName)(1,:) = mean(CumStatsMLAMoS.MAD.(tyName));
    CumStatsMLAMoS.Overall.MAD.(tyName)(2,:) = std(CumStatsMLAMoS.MAD.(tyName));
    
    CumStatsMLNMoS.Overall.RMSE.(tyName)(1,:) = mean(CumStatsMLNMoS.RMSe.(tyName));
    CumStatsMLNMoS.Overall.RMSE.(tyName)(2,:) = std(CumStatsMLNMoS.RMSe.(tyName));
    
    CumStatsMLNMoS.Overall.MAD.(tyName)(1,:) = mean(CumStatsMLNMoS.MAD.(tyName));
    CumStatsMLNMoS.Overall.MAD.(tyName)(2,:) = std(CumStatsMLNMoS.MAD.(tyName));
    
    
end


%%
for tyCt = 1:length(tyList)
    tyName = tyList{tyCt};
    tRMSE(tyCt,:) = CumStatsCpos.Overall.RMSE.(tyName)(1,:);
end
CumStatsCpos.Everything.RMSE = mean(mean(tRMSE));
CumStatsCpos.Everything.STD  = mean(std(tRMSE));


for tyCt = 1:length(tyList)
    tyName = tyList{tyCt};
    tRMSE(tyCt,:) = CumStatsAsegpos.Overall.RMSE.(tyName)(1,:);
end
CumStatsAsegpos.Everything.RMSE = mean(tRMSE);
CumStatsAsegpos.Everything.STD  = std(tRMSE);

for tyCt = 1:length(tyList)
    tyName = tyList{tyCt};
    tRMSE(tyCt,:) = CumStatsNsegpos.Overall.RMSE.(tyName)(1,:);
end
CumStatsNsegpos.Everything.RMSE = mean(tRMSE);
CumStatsNsegpos.Everything.STD  = std(tRMSE);


for tyCt = 1:length(tyList)
    tyName = tyList{tyCt};
    tRMSE(tyCt,:) = CumStatsCsegpos.Overall.RMSE.(tyName)(1,:);
end
CumStatsCsegpos.Everything.RMSE = mean(tRMSE);
CumStatsCsegpos.Everything.STD  = std(tRMSE);

CumStatsSL_Both.MAD = mean([CumStatsSLA.Overall.MAD.(tyName)(1,:) CumStatsSLN.Overall.MAD.(tyName)(1,:)]);
CumStatsSL_Both.STD  = std([CumStatsSLA.Overall.MAD.(tyName)(1,:) CumStatsSLN.Overall.MAD.(tyName)(1,:)]);

CumStatsAllSegments.RMS = mean([CumStatsCsegpos.Everything.RMSE CumStatsNsegpos.Everything.RMSE CumStatsAsegpos.Everything.RMSE]);
CumStatsAllSegments.STD = std([CumStatsCsegpos.Everything.RMSE CumStatsNsegpos.Everything.RMSE CumStatsAsegpos.Everything.RMSE]);

CumStatsFtSegments.RMS = mean([CumStatsNsegpos.Everything.RMSE CumStatsAsegpos.Everything.RMSE]);
CumStatsFtSegments.STD = std([CumStatsNsegpos.Everything.RMSE CumStatsAsegpos.Everything.RMSE]);
%% Table for CoM Vel comparison
headerNames = {'RCOMZ','CCOMZ','rRatFx','rRatFy',...
    'RMS_Ax','RMS_Ay','RMS_Az','RMS_Nx','RMS_Ny','RMS_Nz',...
    'RMS_Cx','RMS_Cy','RMS_ED',...
    'RMS_SLA','MAD_SLA','RMS_SLN','MAD_SLN','RMS_SWA','MAD_SWA','RMS_SWN','MAD_SWN',...
    'RMS_MLA','MAD_MLA','RMS_MLN','MAD_MLN', ...
    'RMS_APA','MAD_APA','RMS_APN','MAD_APN'};
compTableSubwiseErrors = array2table(zeros(0,length(headerNames)));
compTableSubwiseErrors.Properties.VariableNames = headerNames;
trialTypeList = fieldnames(CumStatsCpos.Overall.RMSE);

sigDig = 2;
sigDig2 = 2;

bookEnders = '';% '' or '$'
connString = ' � '; % ' \pm ' or �

for trialTypeCt = 1:length(trialTypeList)
    cTrialType = trialTypeList{trialTypeCt};
    
    tEDposRMSm = CumStatsEDpos.Overall.RMSE.(cTrialType)(1,:)*100;
    tEDposRMSs = CumStatsEDpos.Overall.RMSE.(cTrialType)(2,:)*100;
    
    tSLaRMSm = CumStatsSLA.Overall.RMSE.(cTrialType)(1,:)*100;
    tSLaRMSs = CumStatsSLA.Overall.RMSE.(cTrialType)(2,:)*100;
    
    tSLnRMSm = CumStatsSLN.Overall.RMSE.(cTrialType)(1,:)*100;
    tSLnRMSs = CumStatsSLN.Overall.RMSE.(cTrialType)(2,:)*100;
    
    tSWaRMSm = CumStatsSWA.Overall.RMSE.(cTrialType)(1,:)*100;
    tSWaRMSs = CumStatsSWA.Overall.RMSE.(cTrialType)(2,:)*100;
    
    tSWnRMSm = CumStatsSWN.Overall.RMSE.(cTrialType)(1,:)*100;
    tSWnRMSs = CumStatsSWN.Overall.RMSE.(cTrialType)(2,:)*100;
    
    tAPaRMSm = CumStatsAPAMoS.Overall.RMSE.(cTrialType)(1,:)*100;
    tAPaRMSs = CumStatsAPAMoS.Overall.RMSE.(cTrialType)(2,:)*100;
    
    tAPaMADm = CumStatsAPAMoS.Overall.MAD.(cTrialType)(1,:)*100;
    tAPaMADs = CumStatsAPAMoS.Overall.MAD.(cTrialType)(2,:)*100;
    
    tAPnRMSm = CumStatsAPNMoS.Overall.RMSE.(cTrialType)(1,:)*100;
    tAPnRMSs = CumStatsAPNMoS.Overall.RMSE.(cTrialType)(2,:)*100;
    
    tAPnMADm = CumStatsAPNMoS.Overall.MAD.(cTrialType)(1,:)*100;
    tAPnMADs = CumStatsAPNMoS.Overall.MAD.(cTrialType)(2,:)*100;
    
    tMLaRMSm = CumStatsMLAMoS.Overall.RMSE.(cTrialType)(1,:)*100;
    tMLaRMSs = CumStatsMLAMoS.Overall.RMSE.(cTrialType)(2,:)*100;
    
    tMLaMADm = CumStatsMLAMoS.Overall.MAD.(cTrialType)(1,:)*100;
    tMLaMADs = CumStatsMLAMoS.Overall.MAD.(cTrialType)(2,:)*100;
    
    tMLnRMSm = CumStatsMLNMoS.Overall.RMSE.(cTrialType)(1,:)*100;
    tMLnRMSs = CumStatsMLNMoS.Overall.RMSE.(cTrialType)(2,:)*100;
    
    tMLnMADm = CumStatsMLNMoS.Overall.MAD.(cTrialType)(1,:)*100;
    tMLnMADs = CumStatsMLNMoS.Overall.MAD.(cTrialType)(2,:)*100;
    
    
    tSLaMADm = CumStatsSLA.Overall.MAD.(cTrialType)(1,:)*100;
    tSLaMADs = CumStatsSLA.Overall.MAD.(cTrialType)(2,:)*100;
    
    tSLnMADm = CumStatsSLN.Overall.MAD.(cTrialType)(1,:)*100;
    tSLnMADs = CumStatsSLN.Overall.MAD.(cTrialType)(2,:)*100;
    
    tSWaMADm = CumStatsSWA.Overall.MAD.(cTrialType)(1,:)*100;
    tSWaMADs = CumStatsSWA.Overall.MAD.(cTrialType)(2,:)*100;
    
    tSWnMADm = CumStatsSWN.Overall.MAD.(cTrialType)(1,:)*100;
    tSWnMADs = CumStatsSWN.Overall.MAD.(cTrialType)(2,:)*100;
    
    
    tAposRMSm = CumStatsAsegpos.Overall.RMSE.(cTrialType)(1,:)*100;
    tAposRMSs = CumStatsAsegpos.Overall.RMSE.(cTrialType)(2,:)*100;
    
    tAposCORm = CumStatsAsegpos.Overall.CORR.(cTrialType)(1,:);
    tAposCORs = CumStatsAsegpos.Overall.CORR.(cTrialType)(2,:);
    
    tNposRMSm = CumStatsNsegpos.Overall.RMSE.(cTrialType)(1,:)*100;
    tLposRMSs = CumStatsNsegpos.Overall.RMSE.(cTrialType)(2,:)*100;
    
    tNposCORm = CumStatsNsegpos.Overall.CORR.(cTrialType)(1,:);
    tNposCORs = CumStatsNsegpos.Overall.CORR.(cTrialType)(2,:);
    
    tCposRMSm = CumStatsCsegpos.Overall.RMSE.(cTrialType)(1,:)*100;
    tCposRMSs = CumStatsCsegpos.Overall.RMSE.(cTrialType)(2,:)*100;
    
    tCposCORm = CumStatsCsegpos.Overall.CORR.(cTrialType)(1,:);
    tCposCORs = CumStatsCsegpos.Overall.CORR.(cTrialType)(2,:);
    
    tRMSm = CumStatsCpos.Overall.RMSE.(cTrialType)(1,:)*100;
    tRMSs = CumStatsCpos.Overall.RMSE.(cTrialType)(2,:)*100;
    
    tCORm =  CumStatsCpos.Overall.CORR.(cTrialType)(1,:);
    tCORs = CumStatsCpos.Overall.CORR.(cTrialType)(2,:);
    
    tfratxRMSm =  CumStatsFx.Overall.RMSE.(cTrialType)(1,:);
    tfratxRMSs = CumStatsFx.Overall.RMSE.(cTrialType)(2,:);
    
    tfratxCORm =  CumStatsFx.Overall.CORR.(cTrialType)(1,:);
    tfratxCORs = CumStatsFx.Overall.CORR.(cTrialType)(2,:);
    
    tfratxrRMSm =  CumStatsFx.Overall.rRMSE.(cTrialType)(1,:);
    tfratxrRMSs = CumStatsFx.Overall.rRMSE.(cTrialType)(2,:);
    
    
    tfratyRMSm =  CumStatsFy.Overall.RMSE.(cTrialType)(1,:);
    tfratyRMSs = CumStatsFy.Overall.RMSE.(cTrialType)(2,:);
    
    tfratyCORm =  CumStatsFy.Overall.CORR.(cTrialType)(1,:);
    tfratyCORs = CumStatsFy.Overall.CORR.(cTrialType)(2,:);
    
    tfratyrRMSm =  CumStatsFy.Overall.rRMSE.(cTrialType)(1,:);
    tfratyrRMSs = CumStatsFy.Overall.rRMSE.(cTrialType)(2,:);
    
    
    %%
    eCol = 1;
    % CoM ht rms
    str1{1,eCol} = ([bookEnders num2str(round(tRMSm(3),sigDig)) connString num2str(round(tRMSs(3),sigDig2)) bookEnders]);eCol = eCol + 1;
    % CoM ht corr
    str1{1,eCol} = ([bookEnders num2str(round(tCORm(3),sigDig)) connString num2str(round(tCORs(3),sigDig2)) bookEnders]);eCol = eCol + 1;
    % Force Ratio X range RMS
    str1{1,eCol} = ([bookEnders num2str(round(tfratxrRMSm(1),sigDig)) connString num2str(round(tfratxrRMSs(1),sigDig2)) bookEnders]);eCol = eCol + 1;
    % Force Ratio Y range RMS
    str1{1,eCol} = ([bookEnders num2str(round(tfratyrRMSm(1),sigDig)) connString num2str(round(tfratyrRMSs(1),sigDig2)) bookEnders]);eCol = eCol + 1;
    % RF RMS x
    str1{1,eCol} = ([bookEnders num2str(round(tAposRMSm(1),sigDig)) connString num2str(round(tAposRMSs(1),sigDig2)) bookEnders]);eCol = eCol + 1;
    % RF RMS y
    str1{1,eCol} = ([bookEnders num2str(round(tAposRMSm(2),sigDig)) connString num2str(round(tAposRMSs(2),sigDig2)) bookEnders]);eCol = eCol + 1;
    % RF RMS z
    str1{1,eCol} = ([bookEnders num2str(round(tAposRMSm(3),sigDig)) connString num2str(round(tAposRMSs(3),sigDig2)) bookEnders]);eCol = eCol + 1;
    % LF RMS x
    str1{1,eCol} = ([bookEnders num2str(round(tNposRMSm(1),sigDig)) connString num2str(round(tLposRMSs(1),sigDig2)) bookEnders]);eCol = eCol + 1;
    % LF RMS y
    str1{1,eCol} = ([bookEnders num2str(round(tNposRMSm(2),sigDig)) connString num2str(round(tLposRMSs(2),sigDig2)) bookEnders]);eCol = eCol + 1;
    % LF RMS z
    str1{1,eCol} = ([bookEnders num2str(round(tNposRMSm(3),sigDig)) connString num2str(round(tLposRMSs(3),sigDig2)) bookEnders]);eCol = eCol + 1;
    % CoM RMS x
    str1{1,eCol} = ([bookEnders num2str(round(tCposRMSm(1),sigDig)) connString num2str(round(tCposRMSs(1),sigDig2)) bookEnders]);eCol = eCol + 1;
    % CoM RMS y
    str1{1,eCol} = ([bookEnders num2str(round(tCposRMSm(2),sigDig)) connString num2str(round(tCposRMSs(2),sigDig2)) bookEnders]);eCol = eCol + 1;
    % 2D End distance
    str1{1,eCol} = ([bookEnders num2str(round(tEDposRMSm(1),sigDig)) connString num2str(round(tEDposRMSs(1),sigDig2)) bookEnders]);eCol = eCol + 1;
    
    % SL R TrialWise
    str1{1,eCol} = ([bookEnders num2str(round(tSLaRMSm(1),sigDig)) connString num2str(round(tSLaRMSs(1),sigDig2)) bookEnders]);eCol = eCol + 1;
    str1{1,eCol} = ([bookEnders num2str(round(tSLaMADm(1),sigDig)) connString num2str(round(tSLaMADs(1),sigDig2)) bookEnders]);eCol = eCol + 1;
    
    % SL L TrialWise
    str1{1,eCol} = ([bookEnders num2str(round(tSLnRMSm(1),sigDig)) connString num2str(round(tSLnRMSs(1),sigDig2)) bookEnders]);eCol = eCol + 1;
    str1{1,eCol} = ([bookEnders num2str(round(tSLnMADm(1),sigDig)) connString num2str(round(tSLnMADs(1),sigDig2)) bookEnders]);eCol = eCol + 1;
    
    % SW R Trial Wise
    str1{1,eCol} = ([bookEnders num2str(round(tSWaRMSm(1),sigDig)) connString num2str(round(tSWaRMSs(1),sigDig2)) bookEnders]);eCol = eCol + 1;
    str1{1,eCol} = ([bookEnders num2str(round(tSWaMADm(1),sigDig)) connString num2str(round(tSWaMADs(1),sigDig2)) bookEnders]);eCol = eCol + 1;
    
    % SW L Trial Wise
    str1{1,eCol} = ([bookEnders num2str(round(tSWnRMSm(1),sigDig)) connString num2str(round(tSWnRMSs(1),sigDig2)) bookEnders]);eCol = eCol + 1;
    str1{1,eCol} = ([bookEnders num2str(round(tSWnMADm(1),sigDig)) connString num2str(round(tSWnMADs(1),sigDig2)) bookEnders]);eCol = eCol + 1;
    
    % ML Aff
    str1{1,eCol} = ([bookEnders num2str(round(tMLaRMSm(1),sigDig)) connString num2str(round(tMLaRMSs(1),sigDig2)) bookEnders]);eCol = eCol + 1;
    str1{1,eCol} = ([bookEnders num2str(round(tMLaMADm(1),sigDig)) connString num2str(round(tMLaMADs(1),sigDig2)) bookEnders]);eCol = eCol + 1;
    
    % ML LAff
    str1{1,eCol} = ([bookEnders num2str(round(tMLnRMSm(1),sigDig)) connString num2str(round(tMLnRMSs(1),sigDig2)) bookEnders]);eCol = eCol + 1;
    str1{1,eCol} = ([bookEnders num2str(round(tMLnMADm(1),sigDig)) connString num2str(round(tMLnMADs(1),sigDig2)) bookEnders]);eCol = eCol + 1;
    
    % AP Aff
    str1{1,eCol} = ([bookEnders num2str(round(tAPaRMSm(1),sigDig)) connString num2str(round(tAPaRMSs(1),sigDig2)) bookEnders]);eCol = eCol + 1;
    str1{1,eCol} = ([bookEnders num2str(round(tAPaMADm(1),sigDig)) connString num2str(round(tAPaMADs(1),sigDig2)) bookEnders]);eCol = eCol + 1;
    
    % AP L Aff
    str1{1,eCol} = ([bookEnders num2str(round(tAPnRMSm(1),sigDig)) connString num2str(round(tAPnRMSs(1),sigDig2)) bookEnders]);eCol = eCol + 1;
    str1{1,eCol} = ([bookEnders num2str(round(tAPnMADm(1),sigDig)) connString num2str(round(tAPnMADs(1),sigDig2)) bookEnders]);eCol = eCol + 1;
    
    compStructSubwise.(cTrialType) = str1;
    compTableSubwiseErrors =[compTableSubwiseErrors; str1];
end

%% Bland Altman Plots and Boxplot distributions
if balanceAnalysis
    plotPubPlots2; % each subject gets their symbol
end